<?php

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use User\Form\UsereditForm;
use User\Model\User;
use User\Model\UserTable;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;
use User\Form\RegisterForm;

class UsermanagerController extends AbstractActionController {

    public function IndexAction() {
        $this->layout('layout/common-layout');
        $userTable = $this->getServiceLocator()->get('UserTable');
        $paginator = $userTable->fetchAll(true);
        $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 8));
     // set the number of items per page to 10
        $paginator->setItemCountPerPage(8);
        $viewModel = new Viewmodel(array('paginator' => $paginator));

        //$this->getServiceLocator()->get('log')->debug("A Debug Log Message");
        return $viewModel;
    }

    public function EditAction() {
          $this->layout('layout/common-layout');
        $userTable = $this->getServiceLocator()->get('UserTable');
        $user = $userTable->getUser($this->params()->fromRoute('id'));
        $form = $this->getServiceLocator()->get('UsereditForm');
        $form->bind($user);
        $viewModel = new ViewModel(array('form' => $form, 'user_id' => $this->params()->fromRoute('id')));
        return $viewModel;
    }

    public function DeleteAction() {
         $this->layout('layout/common-layout');
        $this->getServiceLocator()->get('UserTable')->deleteUser($this->params()->fromRoute('id'));
        return $this->redirect()->toRoute('user/usermanager');
    }

    public function AddAction() {
         $this->layout('layout/common-layout');
        $form = $this->getServiceLocator()->get('UsereditForm');
        $form->get('submit')->setValue('Add');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $user = new User();
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $user->exchangeArray($form->getData());
               $this->getServiceLocator()->get('UserTable')->saveUser($user);
                return $this->redirect()->toRoute('user/usermanager', array('action' => 'index'));
            }
        }
      return array('form' => $form);
    }

    public function ProcessAction() {
        if (!$this->request->isPost()) {
            return $this->redirect()->toRoute('user/usermanager', array('action' => 'edit'));
        }

        $post = $this->request->getPost();
        $userTable = $this->getServiceLocator()->get('UserTable');
        $user = $userTable->getUser($post->id);

        $form = $this->getServiceLocator()->get('UserEditForm');
        $form->bind($user);
        $form->setData($post);

        if (!$form->isValid()) {
            $model = new ViewModel(array(
                'error' => true,
                'form' => $form,
            ));
            $model->setTemplate('user/usermanager/edit');
            return $model;
        }

        $this->getServiceLocator()->get('UserTable')->saveUser($user);

        return $this->redirect()->toRoute('user/usermanager');
    }

}
