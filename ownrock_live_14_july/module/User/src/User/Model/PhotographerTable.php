<?php

namespace User\Model;

use Zend\Text\Table\Row;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class PhotographerTable 
{

    protected $tableGateway;
	private $offset;
	private $limit;
	private $keyword;

    public function __construct(TableGateway $tableGateway) 
    {
        $this->tableGateway = $tableGateway;
    }


    public function savePhotographer(photographer $photographer) 
    {
        $data = array(
            'pg_id' => $photographer->id,
            'user_id' => $photographer->user_id,
            'occupation' => $photographer->occupation,
            'location' => $photographer->location,
            'description' => $photographer->description,
            'profilepic' => $photographer->profilepic,
            'status' => 'Pending',
        );
       
        $user_id = (int) $photographer->user_id;
        $pg_id = (int) $photographer->pg_id;
        echo '<pre>';print_r($data);exit;
        if ($pg_id == 0) 
        {
            $this->tableGateway->insert($data);
        } 
        else 
        {
            $this->tableGateway->update($data, array('user_id' => $user_id));
        } 
    }
    
}

 