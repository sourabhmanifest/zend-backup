<?php

namespace User\Model;

class Projectskill {

    public $id;
    public $project_shortcut;
    public $project_id;
    public $skill;
    
    public function exchangeArray($data) {

        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->project_shortcut = (!empty($data['project_shortcut'])) ? $data['project_shortcut'] : null;
        $this->project_id = (!empty($data['project_id'])) ? $data['project_id'] : null;
        $this->skill = (!empty($data['skill'])) ? $data['skill'] : null;
       
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}