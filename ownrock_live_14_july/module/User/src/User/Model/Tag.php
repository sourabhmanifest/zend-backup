<?php

namespace User\Model;

class Tag {

    public $tag_id;
    public $tag;

    public function exchangeArray($data) {

        $this->tag_id = (!empty($data['tag_id'])) ? $data['tag_id'] : null;
        $this->tags = (!empty($data['tag'])) ? $data['tag'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}
