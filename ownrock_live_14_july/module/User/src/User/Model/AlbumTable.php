<?php

namespace User\Model;

use Zend\Text\Table\Row;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class AlbumTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }
    
    public function saveAlbum(album $album) 
    {
        $data = array(
            'album_id' => $album->album_id,
            'user_id' => $album->user_id,
            'album_name' => $album->album_name,
            'description' => $album->description,
        );
        $id = (int) $album->album_id;
        if ($id == 0) 
        {
            $this->tableGateway->insert($data);        
            return $this->tableGateway->getLastInsertValue();
        } 
        else 
        {
            if($this->getProject($album->album_id)) 
            {
                $this->tableGateway->update($data, array('album_id' => $id));
            }
            else 
            {
                throw new \Exception('Project id does not exist');
            }
        }
    }
    public function getTotalAlbumsByUserid($user_id)
    { 
        $resultSet = $this->tableGateway->select(function(Select $select) {
                    $select->where->equalTo('user_id',$user_id);
                });
        //echo $resultSet->count();exit;
        return $resultSet->count(); 

    }
    
}