<?php
// filename : module/Users/src/Users/Form/RegisterForm.php
namespace User\Form;

use Zend\Form\Form;

class UsereditForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('Edit User');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype','multipart/form-data');
        
        $this->add(array(
        	'name' => 'id',
        	'attributes' => array(
                'type'  => 'hidden',
        	),
        ));
        
        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type'  => 'text',
                'required' => 'required',
                'class' => 'form-control',
                                'placeholder' => 'Enter Name',

            ),
            'options' => array(
            ),
        ));

        
        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type'  => 'email',
                'required' => 'required',
                'class' => 'form-control',
                                'placeholder' => 'Enter email',
            ),
            'options' => array(
               
            ),
        )); 
        
        $this->add(array(
            'name' => 'password',
            'attributes' => array(
                'type'  => 'password', 
                'class' => 'form-control',
                                'placeholder' => 'Enter password',
            ),
            'options' => array(
            ),
        )); 
        
        $this->add(array(
            'name' => 'user_type',
            'attributes' => array(
                'type'  => 'text',  
                'class' => 'form-control',
                                'placeholder' => 'Enter user type',
            ),
            'options' => array(
            ),
        )); 
        
        $this->add(array(
            'name' => 'registration_date',
            'attributes' => array(
                'type'  => 'date',      
                'class' => 'form-control',
                'placeholder' => 'Enter registration date',
            ),
            'options' => array(
            ),
        )); 
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Save'
            ),
        )); 
        
         $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Cancel'
            ),
        )); 
    }
}
