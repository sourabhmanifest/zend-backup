<?php

namespace User\Form;

use Zend\Form\Form;

class CampaignForm extends Form {

    Public Function __construct($name = NULL) {
        parent:: __construct('campaign');

        $this->add(array('name' => 'campaign_id',
            'attributes' => array(
                'type' => 'text',
              
            ),
            'options' => array(
            )
        ));
        $this->add(array('name' => 'preview',
            'attributes' => array(
                'type' => 'text',
                'class'=>'col-md-10'
            ),
            'options' => array(
             
            )
        ));

        $this->add(array(
            'name' => 'payout',
            'attributes' => array(
                'type' => 'text',
                   'class'=>'col-md-10',
            ),
            'options' => array(
                
            )
        ));

        $this->add(array(
            'name' => 'categories',
            'attributes' => array(
                'type' => 'text',
                'class'=>'col-md-10',
                 
            ),
            'options' => array(
                
            )
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Submit',
                'id' => 'submitbutton',
            ),
        ));
        $this->add(array(
            'name' => 'MerchantName',
            'type' => 'text',
            'class' => 'form-control validate[required] text-input',
            'attributes' => array(
                'label' => 'Merchant Name',
                'placeholder' => 'Merchant Name',
            ),
        ));
        $this->add(array(
            'name' => 'description',
            'type' => 'text',

             'class'=>'animatedTextArea form-control validate[required]',
            'attributes' => array(
            'label' => 'Description',
             'placeholder' => 'Description',
                'class'=>'col-md-10',

               
            ),
        ));
        $this->add(array(
            'name' => 'websiteUrl',
            'type' => 'text',
            'attributes' => array(
                'label' => 'Website Url',
                'placeholder' => 'Please enter website url',
            ),
        ));

        $this->add(array(
            'name' => 'email',
            'type' => 'text',
            'attributes' => array(
                'label' => 'Email',
                'placeholder' => 'Please enter email',
            ),
        ));
        $this->add(array(
            'name' => 'phonenumber',
            'type' => 'text',
            'attributes' => array(
                'label' => 'Phone',
                'placeholder' => 'Please enter Phone Number',
            ),
        ));
        
        $this->add(array(
            'name' => 'address',
            'type' => 'text',
            'class' => "form-control",
            'id'=>"findLocationArea",
            'value' => "",
            'attributes' => array(
                'label' => 'address',
                'placeholder' => 'Please enter address',
            ),
        ));
    }

}
