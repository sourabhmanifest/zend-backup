<?php
namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Zend\Http\Headers;

use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;

use User\Form\RegisterForm;
//use User\Form\RegisterFilter;

use User\Model\User;
use User\Model\UserTable;
use User\Model\Uploads;

use User\Model\ImageUpload;
use User\Model\ImageUploadTable;

use ZendSearch\Lucene;
use ZendSearch\Lucene\Document;
use ZendSearch\Lucene\Index;

use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;

class SearchController extends AbstractActionController
{
	protected $storage;
	protected $authservice;
	
	public function getAuthService()
	{
		if (! $this->authservice) {
			$this->authservice = $this->getServiceLocator()->get('AuthService');
		}
		return $this->authservice;
	}
	
	public function getIndexLocation()
	{
		// Fetch Configuration from Module Config
		$config  = $this->getServiceLocator()->get('config');
		if ($config instanceof Traversable) {
                    echo 'nikita';die;
			$config = ArrayUtils::iteratorToArray($config);
		}
		if (!empty($config['module_config']['search_index'])) {

			return $config['module_config']['search_index'];
		} else {
			return FALSE;
		}
	}
	
	public function getFileUploadLocation()
	{
		// Fetch Configuration from Module Config
		$config  = $this->getServiceLocator()->get('config');
		if ($config instanceof Traversable) {
			$config = ArrayUtils::iteratorToArray($config);
		}
		if (!empty($config['module_config']['upload_location'])) {
			return $config['module_config']['upload_location'];
		} else {
			return FALSE;
		}
	}	
	
    public function indexAction()
    {
		
		//skills autosuggest code
		$skillsTable = $this->getServiceLocator()->get('skillstable');
        $allSkills = $skillsTable->getAllSkills();
        
		foreach($allSkills as $val){

			$newArr[] = "'".$val['skill']."'";
		}
		$skills = implode(', ',$newArr);
		$this->layout()->skills = $skills;		
		//skills autosuggest code end
		$this->layout('layout/search-layout');
		return new ViewModel();
    }

   
    public function generateindexAction()
    {
        $searchIndexLocation = $this->getIndexLocation();
    
    	$index = Lucene\Lucene::create($searchIndexLocation);
    	print_r($searchIndexLocation);die();
    	$userTable = $this->getServiceLocator()->get('UserTable');
    	$uploadTable = $this->getServiceLocator()->get('UploadTable');
    	$allUploads = $uploadTable->fetchAll();  
    	foreach($allUploads as $fileUpload) {
    		//
    		$uploadOwner = $userTable->getUser($fileUpload->user_id);
    		
	   		// id field
    		$fileUploadId= Document\Field::unIndexed('upload_id', $fileUpload->id);
    		// label field
    		$label = Document\Field::Text('label', $fileUpload->label);
    		// owner field    		
    		$owner = Document\Field::Text('owner', $uploadOwner->name);
    		

    		if (substr_compare($fileUpload->filename, ".xlsx", strlen($fileUpload->filename)-strlen(".xlsx"), strlen(".xlsx")) === 0) {
    			// index excel sheet
    			$uploadPath    = $this->getFileUploadLocation();
    			$indexDoc = Lucene\Document\Xlsx::loadXlsxFile($uploadPath ."/" . $fileUpload->filename);
    		} else if (substr_compare($fileUpload->filename, ".docx", strlen($fileUpload->filename)-strlen(".docx"), strlen(".docx")) === 0) {
    			// index word doc
    			$uploadPath    = $this->getFileUploadLocation();
    			$indexDoc = Lucene\Document\Docx::loadDocxFile($uploadPath ."/" . $fileUpload->filename);
    		} else {
	    		$indexDoc = new Lucene\Document();
    		}
    		$indexDoc->addField($label);
    		$indexDoc->addField($owner);
    		$indexDoc->addField($fileUploadId);
    		$index->addDocument($indexDoc);
    	}
    	$index->commit();
    }
    
}
