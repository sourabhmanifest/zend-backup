<?php

namespace User\Model;

use Zend\Text\Table\Row;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class ProfileskillTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function saveProfileskill(Profileskill $profileskill) {
        $data = array(
            'id' => $profileskill->id,
            'user_shortcut' => $profileskill->user_shortcut,
            'skill' => \Zend\Json\Json::encode($profileskill->skill),
            
        );
      

        
        $id = (int) $profileskill->id;
        if ($id == 0) {

            $this->tableGateway->insert($data);

        } else {
            if ($this->getProject($profileskill->user_shortcut)) {
                $this->tableGateway->update($data, array('user_id' => $id));
            } else {
                throw new \Exception('User id does not exist');
            }
        }
    }

    public function getProject($project_shortcut) {
        //$id = (int) $id;
        $rowset = $this->tableGateway->select(array('project_shortcut' => $project_shortcut));
        $row = $rowset->current();
        
        if (!$row) {
            throw new \Exception("Could not find row $project_shortcut");
        }
        return $row;
    }

    public function fetchAll($paginated=false) {
   
        if ($paginated) {
             // create a new Select object for the table album
             $select = new Select('profileskill');
                
             // create a new result set based on the Album entity
             $resultSetPrototype = new ResultSet();
             $resultSetPrototype->setArrayObjectPrototype(new Project());
             // create a new pagination adapter object
             $paginatorAdapter = new DbSelect(
                 // our configured select object
                 $select,
                 // the adapter to run it against
                 $this->tableGateway->getAdapter(),
                 // the result set to hydrate
                 $resultSetPrototype
             );
        
             $paginator = new Paginator($paginatorAdapter);
             return $paginator;
         }
        $resultSet = $this->tableGateway->select();
        $resultSet->buffer();
        return $resultSet;
    }

    public function deleteProfileskill($skill) {
        $this->tableGateway->delete(array('skill' =>  $skill));
    }

    public function selectProfileskilluserid($user_id) {      
        $rowset = $this->tableGateway->select(array('user_id' => c));
        return $rowset;    
    }
    
     public function selectProfileskill($skill) {     
        $rowset = $this->tableGateway->select(array('skill' => $skill));
        return $rowset;    
    }

}
