<?php

namespace User\Model;

use Zend\Text\Table\Row;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class MerchantTable {

    protected $tableGateway;
	private $offset;
	private $limit;
	private $keyword;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

     public function insertMerchant(merchant $merchant) {
        $data = array(
            'name' => $merchant->name,
            'email' => $merchant->email,
        );
        $this->tableGateway->insert($data);        
    }

    public function saveMerchantdashboard(merchant $merchant) {
        $data = array(
            'name' => $merchant->name,
            'profession' => $merchant->profession,
            'country' => "India",
            'city' => $merchant->city,
            'rate' => $merchant->rate,
            'availability' => $merchant->availability,
            'smalldescription' => $merchant->smalldescription,
            'longdescription' => $merchant->longdescription,
            'profilepic' => $merchant->profilepic,
            'email' => $merchant->email,
            'phone' => $merchant->phone,
            'id' => $merchant->id,
            'status' => 'Pending',
            'profile_progress' => $merchant->profile_progress,

        );
        
        
        $id = (int) $merchant->id;
        
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getMerchant($merchant->name)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Campaign id does not exist');
            }
        }
    }
    
        public function saveMerchant(merchant $merchant) {
        $data = array(
            'name' => $merchant->name,
            'profession' => $merchant->profession,
            'country' => $merchant->country,
            'city' => $merchant->city,
            'rate' => $merchant->rate,
            'availability' => $merchant->availability,
            'smalldescription' => $merchant->smalldescription,
            'longdescription' => $merchant->longdescription,
            'profilepic' => $merchant->profilepic,
            'email' => $merchant->email,
            'phone' => $merchant->phone,
            'id' => $merchant->id,
            'status' => $merchant->status,
            'website_url' => $merchant->website_url,
            'profile_progress' => $merchant->profile_progress,
        );
        
        //echo '<pre>'; print_r($data);exit;
        $id = (int) $merchant->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getMerchant($merchant->name)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Campaign id does not exist');
            }
        }
    }

    public function getMerchant($id) {
        //$id = (int) $id;
        $rowset = $this->tableGateway->select(array('name' => $id));
        $row = $rowset->current();
        
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function getMerchantById($id,$name="") {
       
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        
        /*if($name != ''){
            $resultSet = $this->tableGateway->select(function(Select $select) {
                $select->join('project', 'merchant.name = project.user_name')
                       ->where->equalTo('project.user_name', 'Tahir Khan');
            });
            echo '<pre>';
            $r = $resultSet->current();
               
            echo $r->about;
            echo '::';
            echo $r->website_url;
            
            print_r($resultSet);
            return $resultSet;
        }else{
            return $row;
        }*/
        
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }
    
    public function getActiveMerchantById($id,$name="") {
       
        $rowset = $this->tableGateway->select(array('id' => $id,'status'=>'verified'));
        $row = $rowset->current();

        if (!$row) {
            //throw new \Exception("Could not find row $id");
        }
        return $row;
    }
    
    public function fetchAll($paginated=false,$searchtext="") {
        if ($paginated) {
             // create a new Select object for the table album
             $select = new Select('merchant');
             
             if($searchtext != ""){
                 $select->where->like('name', "%" . "$searchtext" . "%");
             }
             
             // create a new result set based on the Album entity
             $resultSetPrototype = new ResultSet();
             $resultSetPrototype->setArrayObjectPrototype(new Merchant());
             // create a new pagination adapter object
             $paginatorAdapter = new DbSelect(
                 // our configured select object
                 $select,
                 // the adapter to run it against
                 $this->tableGateway->getAdapter(),
                 // the result set to hydrate
                 $resultSetPrototype
             );
             $paginator = new Paginator($paginatorAdapter);
              
             return $paginator;
         }
        $resultSet = $this->tableGateway->select();
        $resultSet->buffer();
        return $resultSet;
    }

    public function deleteMerchant($id) {
        $this->tableGateway->delete(array('name' => (int) $id));
    }

    public function selectMerchant($header) {      
        $rowset = $this->tableGateway->select(array('header' => $header));
        return $rowset;    
    }
    
     public function selectMerchantname($name) {     
        $rowset = $this->tableGateway->select(array('name' => $name));
        return $rowset;    
    }
    
    public function updateMerchantstatus($merchant){
        $data = array(
            'status' => $merchant['status'],
        );
        
        $id = (int) $merchant['id'];
        $this->tableGateway->update($data, array('id' => $id));
        
    }

    public function getMerchantByEmail($email) {
    
        $rowset = $this->tableGateway->select(array('email' => $email));
        $row = $rowset->current();
        if (!$row) {
        
            $row = 0;
            return $row;
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function updateProfileImage($merchant_email,$profilepic) {
        
        $data = array('profilepic'=>$profilepic);
        $this->tableGateway->update($data, array('email'=>$merchant_email));
            
    }

	public function getMerchantByKeyword($keyword=array(), $offset=0, $limit=3, $location='', $sort_by='', $rate, $availability) 
    { 
        $this->keyword = array();   
        $response = array();
        $this->keyword = $keyword;


        
        //$select = $this->tableGateway->getSql()->select()
        //->join('project', 'project.user_name=merchant.name',array('p'=>'user_name'));


       /* $select = $db->select()
             ->from(array('p' => 'products'),
                    array('product_id'))
             ->join(array('l' => 'line_items'),
                    'p.product_id = l.product_id',
                    array('line_items_per_product' => 'COUNT(*)'))
             ->group('p.product_id')
             ->order(array('line_items_per_product DESC',
                           'product_id'));
*/



        $rowset = $this->tableGateway->select(function(Select $select) use ($limit, $offset, $location, $sort_by, $rate, $availability )
        {   
            $select->join('project', 'project.user_name = name',array('user_name' => 'user_name','cnt' => new \Zend\Db\Sql\Expression('COUNT(project.user_name)')));
            
        
            

           
            $select->where->EqualTo('status', 'verified');

            if(!empty($this->keyword)){
                    $select->where(array('profession' => $this->keyword));
            }
            if(trim($location) != ''){

                    $select->where->like('country', "$location");               
            }
            
            if(trim($rate) != '0')
            {
                if(trim($rate)=="1")
                {
                    $select->where->lessThanOrEqualTo('rate', 5);
                }
                if(trim($rate)=="2")
                {
                    $select->where->lessThanOrEqualTo('rate', 10);
                }
                if(trim($rate)=="3")
                {
                    $select->where->lessThanOrEqualTo('rate', 15);
                }
                if(trim($rate)=="4")
                {
                    $select->where->lessThanOrEqualTo('rate', 20);
                }            
            }

            if(trim($availability) != '0')
            {
                if(trim($availability)=="1")
                {
                    $select->where->lessThanOrEqualTo('availability', 10);
                }
                if(trim($availability)=="2")
                {
                    $select->where->lessThanOrEqualTo('availability', 20);
                }
                if(trim($availability)=="3")
                {
                    $select->where->lessThanOrEqualTo('availability', 30);
                }
                if(trim($availability)=="4")
                {
                    $select->where->lessThanOrEqualTo('availability', 40);
                }
                                   
            }

            $select->group('project.user_name');
            $select->having('cnt>1');

            if(trim($sort_by) != '')
            {
                if(trim($sort_by)=="0")
                {
                    $select->order('id ASC');
                }
                if(trim($sort_by)=="1")
                {
                    $select->order('rate DESC');
                }
                if(trim($sort_by)=="2")
                {
                    $select->order('rate ASC');
                }
                if(trim($sort_by)=="3")
                {
                    $select->order('id DESC');
                }
            }

        });

        //echo  $select;
        
        //$response['result'] = $rowset->toArray();      
        //$row = $rowset->current();
        //print_r($response['result']); die;
        $response['totalcount'] = $rowset->count();     
       
        $rowset1 = $this->tableGateway->select(function(Select $select) use ($limit, $offset, $location, $sort_by, $rate, $availability)
        {
            $select->join('project', 'project.user_name = name',array('user_name' => 'user_name','cnt' => new \Zend\Db\Sql\Expression('COUNT(project.user_name)')));
            
            $select->where->EqualTo('status', 'verified');
            if(!empty($this->keyword)){
                $select->where(array('profession' => $this->keyword));
            }
            
            if(trim($location) != ''){
                $select->where->like('country', "$location");               
            }

            if(trim($rate) != '0')
            {
                if(trim($rate)=="1")
                {
                    $select->where->lessThanOrEqualTo('rate', 5);
                }
                if(trim($rate)=="2")
                {
                    $select->where->lessThanOrEqualTo('rate', 10);
                }
                if(trim($rate)=="3")
                {
                    $select->where->lessThanOrEqualTo('rate', 15);
                }
                if(trim($rate)=="4")
                {
                    $select->where->lessThanOrEqualTo('rate', 20);
                }            
            }

            if(trim($availability) != '0')
            {
                if(trim($availability)=="1")
                {
                    $select->where->lessThanOrEqualTo('availability', 10);
                }
                if(trim($availability)=="2")
                {
                    $select->where->lessThanOrEqualTo('availability', 20);
                }
                if(trim($availability)=="3")
                {
                    $select->where->lessThanOrEqualTo('availability', 30);
                }
                if(trim($availability)=="4")
                {
                    $select->where->lessThanOrEqualTo('availability', 40);
                }
            }

            $select->group('project.user_name');
            $select->having('cnt>1');

            if(trim($sort_by) != '')
            {
                if(trim($sort_by)=="0")
                {
                    $select->order('id ASC');
                }
                if(trim($sort_by)=="1")
                {
                    $select->order('rate DESC');
                }
                if(trim($sort_by)=="2")
                {
                    $select->order('rate ASC');
                }
                if(trim($sort_by)=="3")
                {
                    $select->order('id DESC');
                }
            }

            
            $select->limit($limit)->offset($offset);
                
        });
        $response['result'] = $rowset1->toArray();      
        return $response;
    }       


    public function searchMerchant($keyword=array(), $location='', $sort_by, $fromrate, $torate, $offset, $limit=6) 
    { 
        //echo 'sort'.$sortby;exit;
        $this->keyword = array();   
        // $this->$type = array();
        $response = array();
        $this->keyword = $keyword;

        $rowset = $this->tableGateway->select(function(Select $select) use ($limit, $offset, $location, $sort_by, $fromrate, $torate )
        {   
            $select->join('project', 'project.user_name = name',array('user_name' => 'user_name','cnt' => new \Zend\Db\Sql\Expression('COUNT(project.user_name)')));
            
            $select->where->EqualTo('status', 'verified');

            if(!empty($this->keyword)){
                    $select->where(array('profession' => $this->keyword));
            }
            if(trim($location) != ''){

                $select->where->like('country', $location);               
            }
            /*if(!empty($this->$type)){
                    $select->where(array('type' => $this->type));
            }*/
            
            $select->where->between('rate', $fromrate, $torate);
            
            $select->group('project.user_name');
            $select->having('cnt>1');

            if(trim($sort_by) != '')
            {
                if(trim($sort_by)=="0")
                {
                    $select->order('id ASC');
                }
                if(trim($sort_by)=="1")
                {
                    $select->order('rate DESC');
                }
                if(trim($sort_by)=="2")
                {
                    $select->order('rate ASC');
                }
                if(trim($sort_by)=="3")
                {
                    $select->order('id DESC');
                }
            }

        });

        $response['totalcount'] = $rowset->count(); 
       
        $rowset1 = $this->tableGateway->select(function(Select $select) use ($limit, $offset, $location, $sort_by, $fromrate, $torate )
        {   
            $offset =(int)$offset;
            
            $select->join('project', 'project.user_name = name',array('user_name' => 'user_name','cnt' => new \Zend\Db\Sql\Expression('COUNT(project.user_name)')));
            
            $select->where->EqualTo('status', 'verified');

            if(!empty($this->keyword)){
                    $select->where(array('profession' => $this->keyword));
            }
            if(trim($location) != ''){

                $select->where->like('country', $location);               
            }
            /*if(!empty($this->$type)){
                    $select->where(array('type' => $this->type));
            }*/
            
            $select->where->between('rate', $fromrate, $torate);
            
            $select->group('project.user_name');
            $select->having('cnt>1');

            if(trim($sort_by) != '')
            {
                if(trim($sort_by)=="0")
                {
                    $select->order('id ASC');
                }
                if(trim($sort_by)=="1")
                {
                    $select->order('rate DESC');
                }
                if(trim($sort_by)=="2")
                {
                    $select->order('rate ASC');
                }
                if(trim($sort_by)=="3")
                {
                    $select->order('id DESC');
                }
            }
            $select->limit($limit)->offset($offset);
        });
  
        $response['result'] = $rowset1->toArray();  
        //echo '<pre>';print_r($response['result'])  ;exit;  
        return $response;
    }       
}

 