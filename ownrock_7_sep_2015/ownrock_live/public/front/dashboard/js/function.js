function valid_profile()
{
  //alert("hii");
  var name            = $("#name").val();
  var location        = $("#location").val();
  var phone           = $("#phone").val();
  var full_country    = $("#full_country").val();
  var price           = $("#price").val();
  var description     = $("#description").val();
  var valid = true;

  $("#nameerr").html("");
  $("#phoneerr").html("");
  $("#locationerr").html("");
  $("#priceerr").html("");
  $("#descriptionerr").html("");
  var string = $('#description').val();
    string = string.replace(/(^\s*)|(\s*$)/gi,"");
    string = string.replace(/[ ]{2,}/gi," ");
    string = string.replace(/\n /,"\n");
  var length = string.split(' ').length;
  //alert(phone.length);
  if(full_country=="")
  {
     $("#location").val("");
  }

  if(name=="")
  {
    //alert("Please fill all the required fields.");
    $("#nameerr").html("Name should not be blank.");
    $("#name").css("border", "2px solid red");
    $("#nameerr").css("color", "red");
    valid = false;
  }
  if(location=="")
  {
    $("#locationerr").html("Location should not be blank.");
    $("#full_country").css("border", "2px solid red");
    $("#locationerr").css("color", "red");
    valid = false;
  }
  if(price=="")
  {
    $("#priceerr").html("Price should not be blank.");
    $("#price").css("border", "2px solid red");
    $("#priceerr").css("color", "red");
    valid = false;
  }

  if($.trim($("#description").val())=='' || parseInt(length) < 50) { 
  //alert(parseInt($('#about').val().length)); 
  $('#descriptionerr').html(""); 
  $("#description").css('border-color', '#FF0000');
  $("#descriptionerr").css('color', '#FF0000');
  $('#descriptionerr').html("About me should be min 50 words long.");
    valid = false;
  }
  if(parseInt(length) > 500) { 
     //alert(parseInt($('#about').val().length)); 
    $('#textarea_error').html(""); 
    $("#description").css('border-color', '#FF0000');
    $("#textarea_error").css('color', '#FF0000');
        $('#textarea_error').html("About me  can't be more than 500 words long.");
    valid = false;
  }

    
  if(phone.length!=10)
  {
    $("#phoneerr").html("Telephone no. should not be less than 10 digits.");
    $("#phone").css("border", "2px solid red");
    $("#phoneerr").css("color", "red");
    valid = false;
  }

  else if(isNaN(phone))
  {
    $("#phoneerr").html("Telephone no. should be numeric.");
    $("#phone").css("border", "2px solid red");
    $("#phoneerr").css("color", "red");
    valid = false;
  }

  if(isNaN(price))
  {
    $("#priceerr").html("Price should be Numeric .");
    $("#price").css("border", "2px solid red");
    $("#priceerr").css("color", "red");
    valid = false;
  }
  if(valid == true)
  {
    $("#PhotographerForm").submit();
  }
}


$(document).ready(function () {
  var trigger = $('.hamburger'),
      overlay = $('.overlay'),
     isClosed = false;

    trigger.click(function () {
      hamburger_cross();      
    });

    function hamburger_cross() {

      if (isClosed == true) {          
        overlay.hide();
        trigger.removeClass('is-open');
        trigger.addClass('is-closed');
        isClosed = false;
      } else {   
        overlay.show();
        trigger.removeClass('is-closed');
        trigger.addClass('is-open');
        isClosed = true;
      }
  }
  
  $('[data-toggle="offcanvas"]').click(function () {
        $('#wrapper').toggleClass('toggled');
  });  
});




 

 

<!--  City Auto Suggesstion -->

<!--  login and signup code -->
        
function validate_register()
{
  /*var emailRegex = /^[A-Za-z0-9._]*\@[A-Za-z]*\.[A-Za-z]{2,5}$/; */
  var emailRegex= /[-0-9a-zA-Z.+]+@[-0-9a-zA-Z.+]+\.[a-zA-Z]{2,4}/;
  var name = $("#name").val();
  var email = $("#email").val();
  var password = $("#password").val();
  var confirm_password = $("#confirm_password").val();

   if(name == "" ){
     //$("#email").attr( "border-color", "red");
     $("#name").attr("placeholder", "Name cant be empty");
     $("#name").css("border", "2px solid red");
     return false;
   }else if(email == "" ){
     //$("#email").attr( "border-color", "red");
     $("#email").attr("placeholder", "Email cant be empty");
     $("#email").css("border", "2px solid red");
     return false;
   }else if(!emailRegex.test(email)){
     $("#email").val("");
     $("#email").attr("placeholder", "Email is not valid");
     $("#email").css( "border", "2px solid red");
     return false;
   }else if(password == ""){
     //$("#password").focus();
     $("#password").attr("placeholder", "Password cant be empty");
     $("#password").css( "border", "2px solid red");
     return false;
   }else if(confirm_password == ""){
     $("#confirm_password").attr("placeholder", "Confirm password cant be empty");
     $("#confirm_password").css( "border", "2px solid red");
     return false;
   }else if(password !== confirm_password){
     $("#confirm_password").val("");
     $("#confirm_password").attr("placeholder", "Password does not match");
     $("#confirm_password").css( "border", "2px solid red");
     return false;
   }else if($('input[name=tnc]:checked').length<=0){
     alert('Please accept terms and condition');
     return false;
   //}else if($(email != '' && password != '' && confirm_password != '')){
   }else if(email != '' && password != '' && confirm_password != '' && name != ''){    
        //$("#reg-form").submit();
    var reg_url= site_url + '/user/home/register';
    var success_url= site_url + '/user/home/dashboard';
    $.ajax({
       url: reg_url,
       error: function() {
          $('#info').html('<p>An error has occurred</p>');
       },
       data: {email: email, password: password, name: name},
       beforeSend: function() {
            $('#loader_img_reg').show();
            $('#regBtn').attr('disabled',true);
       },
       success: function(data) {
          $('#loader_img_reg').hide();
          $('#regBtn').removeAttr('disabled');
              $("#name").val("");
              $("#email").val("");
              $("#password").val("");
              $("#confirm_password").val("");
          $('#tnc').attr('checked', false);
          if(data.response === 'failed'){
              alert('Email already registered!');
              $( "#email" ).focus();
          }else{
              $( "#signup_close" ).click();
              alert('Registration Success ! Kindly check your email for further instructions!');
              //window.location = success_url;
          }
       },
       type: 'POST',
       dataType: 'json',
    });
  }
}

function validate_login()
{
  //var emailRegex = /^[A-Za-z0-9._]*\@[A-Za-z]*\.[A-Za-z]{2,5}$/;
  var emailRegex= /[-0-9a-zA-Z.+]+@[-0-9a-zA-Z.+]+\.[a-zA-Z]{2,4}/;
  var loginemail = $("#loginemail").val();
  var loginpassword = $("#loginpassword").val();

   if(loginemail == "" ){
      $("#loginemail").css("border", "2px solid red");
     return false;
   }else if(!emailRegex.test(loginemail)){
      $("#loginemail").val("");
      $("#loginemail").attr("placeholder", "Email is not valid");
      $("#loginemail").css( "border", "2px solid red");
     return false;
   }else if(loginpassword == ""){
      $("#loginpassword").attr("placeholder", "Password is required");
     $("#loginpassword").css( "border", "2px solid red");
     return false;
   }else if(loginemail != '' && loginpassword != ''){
      //$("#logn-form").submit();
    var login_url= site_url + '/user/home/login';
    $.ajax({
       url: login_url,
       error: function() {
          $('#info').html('<p>An error has occurred</p>');
       },
       data: {loginemail: loginemail, loginpassword: loginpassword},
       beforeSend: function() {
            $('#loader_img').show();
            $('#loginBtn').attr('disabled',true);
       },
       success: function(data) {
          $('#loader_img').hide();
          $('#loginBtn').removeAttr('disabled');
          if(data.response === 'failed'){
              alert('Invalid credentials ! Please try again.');
              $("#loginemail").val("");
              $("#loginpassword").val("");
              $( "#loginemail" ).focus();
          }else{
              $("#signup_close" ).click();
              if(data.user_type == 1) {
                window.location = site_url + '/user/home/dashboard';


              }else {
                 window.location = site_url + '/user/photographer/dashboard';
              }
          }
       },
       type: 'POST',
       dataType: 'json',
    });
  }
}

  
function validate_email()
{ 
    //var emailRegex = /^[A-Za-z0-9._]*\@[A-Za-z]*\.[A-Za-z]{2,5}$/;
    var emailRegex= /[-0-9a-zA-Z.+]+@[-0-9a-zA-Z.+]+\.[a-zA-Z]{2,4}/;
    var forgetemail = $("#forgetemail").val();
    if(forgetemail == "" )
    {
		$("#forgetemail").css("border", "2px solid red");
		return false;
    }
    else if(!emailRegex.test(forgetemail))
    {
		$("#forgetemail").val("");
		$("#forgetemail").attr("placeholder", "Email is not valid");
		$("#forgetemail").css( "border", "2px solid red");
		return false;
    }
    else if(forgetemail != '')
    {
      var forget_url= site_url + '/user/photographer/forgot';

      $.ajax({
         url: forget_url,
         error: function() {
            $('#info').html('<p>An error has occurred</p>');
         },
         data: {forgetemail: forgetemail},
         beforeSend: function() 
         {
            $('#loader_img').show();
            $('#forgetBtn').attr('disabled',true);
         },
        success: function(data) 
        { 
          $('#loader_img').hide();
          $('#forgetBtn').removeAttr('disabled');
          if(data.response === 'failed')
          {
              alert('This email does not exist on our site!');
              $( "#forgetemail" ).focus();
          }
          else
          {
            $( "#sent-msg" ).html("Please check your email! We have sent you reset password link!").show();
            setTimeout(function(){$(".fade").click(); $("#forgetemail").val('');
            $("#sent-msg").html('');}, 1000);
          }
        },
        type: 'POST',
        dataType: 'json',
      });
    }
}








function valid_album()
{ 
	var isValid = true;
	//alert($('ul.select2-choices>li').length);
	$('#name_error').html("");
	$('#tags_error').html("");
	$('#textarea_error').html("");
	$('#tags_error').html("Please select atleast one Tag.");
	$('#images_error').html("");
	$("#album_name").css('border-color', '#dce4ec');
	$("#tags_error").css('border-color', '#dce4ec');
	$("#description").css('border-color', '#dce4ec');
	var total_image = $("#album_div").find('img').length;

	var des_id_array = [];
	var des_array =[];
	var des_id = $('#images_name').val();
	  des_id = des_id.replace(/^,|,$/g,'');
	  des_id_array = des_id.split(',');
	  
	for(var i=0; i<des_id_array.length; i++)
	{
	var unique_id = des_id_array[i].split('.');
	//alert(unique_id);
	if($('#des_'+unique_id[0]).val() == '')
	{
	  des_array[i] = '0';
	}
	else
	{
	  des_array[i] = $('#des_'+unique_id[0]).val();
	}
	}
	$('#images_des').val(des_array);

  
	//alert(des_array);
	var string = $('#description').val();
	string = string.replace(/(^\s*)|(\s*$)/gi,"");
	string = string.replace(/[ ]{2,}/gi," ");
	string = string.replace(/\n /,"\n");
	var length = string.split(' ').length;
	if(total_image >30)
	{   
		$("#images_error").css('color', '#FF0000');
		$('#images_error').html("Photos can't be more than 30.");
		isValid = false;
	} 

	if($.trim($("#album_name").val())=='')
	{   
		$("#album_name").css('border-color', '#FF0000');
		$("#name_error").css('color', '#FF0000');
		$('#name_error').html("Please enter album name.");
		isValid = false;
	} 

	if($('ul.select2-choices>li').length<=1){        
		$("#s2id_e2").css('border-color', '#FF0000');
		$("#tags_error").css('color', '#FF0000');
		$('#tags_error').html("Please select atleast one Tag.");
		isValid = false;
	}
	else
	{
		$("#s2id_e2").css('border-color', '');
		$('#tags_error').html("");
		//isValid = true;
	}
	if($.trim($("#description").val())==''  || parseInt(length) < 50) { 
		//alert(parseInt($('#about').val().length)); 
		$('#textarea_error').html(""); 
		$("#description").css('border-color', '#FF0000');
		$("#textarea_error").css('color', '#FF0000');
		$('#textarea_error').html("Album description should be min 50 words long.");
		isValid = false;
	}
	if(parseInt(length) > 500) { 
		 //alert(parseInt($('#about').val().length)); 
		$('#textarea_error').html(""); 
		$("#description").css('border-color', '#FF0000');
		$("#textarea_error").css('color', '#FF0000');
		$('#textarea_error').html("Album description can't be more than 500 words long.");
		isValid = false;
	}
	if(isValid==true){
		document.getElementById('home_search').submit();
	}
}


function delete_image(id, name)
{ //alert(name);
  //jQuery("#img_"+id).remove();
  var reg_url= site_url+'/user/photographer/deleteImg';
    $.ajax({
       url: reg_url,
       error: function() {
          $('#info').html('<p>An error has occurred</p>');
       },
       data: {id: id, name: name},
       beforeSend: function() {
            $('#loader').show();
       },
       success: function(data) {
      		$('#loader').hide();
			if(data.response === 'failed'){
			  alert('Image not deleted An error has occurred!');
			}else{
      var oldvalue = $.trim( $('#images_name').val() );
      var newstring = oldvalue.replace(name,"");
      newstring = newstring.replace(",,",",");
      newstring = newstring.replace(/^,|,$/g,'');
      $("#images_name").val("");
      $('#images_name').val(newstring);
              //alert('delete');
              jQuery("#img_"+id).remove();
          }
       },
       type: 'POST',
       dataType: 'json',
    });
}

 // make first char uppercase by jquery 
(function($) {
    $.ucfirst = function(str) {
        var text = str;
        var parts = text.split(' '),
            len = parts.length,
            i, words = [];
        for (i = 0; i < len; i++) {
            var part = parts[i];
            var first = part[0].toUpperCase();
            var rest = part.substring(1, part.length);
            var word = first + rest;
            words.push(word);
        }
        return words.join(' ');
    };
})(jQuery);


function shortlist(pg_id)
{
  //alert(pg_id);
  var ajax_url= site_url+'/user/photographer/shortlist';
  $.ajax({
        url: ajax_url,
        data: {pg_id: pg_id},
        success: function(data) { 
            if(data.response == 'success')
            {
              $("#shortstatus"+pg_id).css("display","none");
              $("#unshortstatus"+pg_id).css("display","inline-block");
              $("#unshortstatus"+pg_id+" a").css("color","#54ba4e");
            }
            else
            {
                alert('An error has occurred!');
            }
        },
        type: 'POST',
        dataType: 'json',
    });
};

function unshortlist(pg_id)
{
  //alert(pg_id);
  	var ajax_url= site_url+'/user/photographer/unShortlist';
  	$.ajax({
        url: ajax_url,
        error: function() {
            $('#info').html('<p>An error has occurred</p>');
        },
        data: {pg_id: pg_id},
        beforeSend: function() {
            //$('#loader_img').show();
        },
        success: function(data) 
        { 
            /*$('#loader_img').hide();*/
            if(data.response == 'success')
            {

              $("#unshortstatus"+pg_id).css("display","none");
              $("#shortstatus"+pg_id).css("display","inline-block");
              //$("#shortstatus a").html("<i class = 'fa fa-heart-o'></i> Short-List");
            }
            else
            {
                alert('An error has occurred!');
            }
        },
        type: 'POST',
        dataType: 'json',
    });
};

  $(document).ready(function() {
    
    $('#logn-form').keydown(function(event) {     
        if (event.keyCode == 13) {
           validate_login();
         }
    });
  });


  $(document).ready(function() {
    
    $('#reg-form').keydown(function(event) {     
        if (event.keyCode == 13) {
           validate_register();
         }
    });
  });

