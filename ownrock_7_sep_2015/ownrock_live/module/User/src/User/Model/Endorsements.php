<?php

namespace User\Model;

class Endorsements{

    public $id;
    public $project_id;
    public $project_shortcut;
    public $name;
    public $endorsetext;
    public $clientname;
   

    public function exchangeArray($data) {
	$this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->project_id = (!empty($data['project_id'])) ? $data['project_id'] : null;
        $this->project_shortcut = (!empty($data['project_shortcut'])) ? $data['project_shortcut'] : null;
        $this->name = (!empty($data['name'])) ? $data['name'] : null;
        $this->endorsetext= (!empty($data['endorsetext'])) ? $data['endorsetext'] : null;
        $this->clientname= (!empty($data['clientname'])) ? $data['clientname'] : null;
        
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}