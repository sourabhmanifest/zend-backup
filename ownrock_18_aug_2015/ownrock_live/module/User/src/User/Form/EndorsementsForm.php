<?php
// filename : module/Users/src/Users/Form/RegisterForm.php
namespace User\Form;

use Zend\Form\Form;

class EndorsementsForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('Add Endorsements');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype','multipart/form-data');
        
        $this->add(array(
        	'name' => 'id',
        	'attributes' => array(
                'type'  => 'hidden',
                'required' => 'required',
                'class' => 'form-control',
        	),
        ));
        
         $this->add(array(
        	'name' => 'name',
        	'attributes' => array(
                'type'  => 'hidden',
                'required' => 'required',
                'class' => 'form-control',

        	),
        ));
        $this->add(array(
        	'name' => 'project_id',
        	'attributes' => array(
                'type'  => 'hidden',
                'required' => 'required',
                'class' => 'form-control',
        	),
        ));
        
           $this->add(array(
        	'name' => 'project_shortcut',
        	'attributes' => array(
                'type'  => 'hidden',
                'required' => 'required',
                'class' => 'form-control',
        	),
        ));
               
        $this->add(array(
        	'name' => 'endorsetext',
        	'attributes' => array(
                'type'  => 'text',
                'required' => 'required',
                'class' => 'form-control',

        	),
        ));
        
        $this->add(array(
            'name' => 'clientname',
            'attributes' => array(
                'type'  => 'text',
                'class' => 'form-control',
                'placeholder' => 'Enter client name',

            ),
            'options' => array(
            ),
        ));

 
      
      
           
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Save'
            ),
        )); 
        
         $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Cancel'
            ),
        )); 
    }
    

}