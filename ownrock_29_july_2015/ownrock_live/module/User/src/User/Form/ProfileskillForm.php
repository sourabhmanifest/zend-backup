<?php
// filename : module/Users/src/Users/Form/RegisterForm.php
namespace User\Form;

use Zend\Form\Form;


class ProfileskillForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('Profileskill');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype','multipart/form-data');
        
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
           
        ));

        
        $this->add(array(
            'name' => 'user_shortcut',
            'attributes' => array(
                'type'  => 'text',
               
            ),
            'options' => array(
                'label' => 'user_shortcut',
                 
            ),
        )); 
        
          
        $this->add(array(
            'name' => 'skill',
            'type'  => 'Zend\Form\Element\Multicheckbox',
           
            'options' => array(
                'label' => 'Skill',
                'value_options' => array(
                    '1' => 'We Developer',
                    '2' => 'Web Designer',
                    '3' => 'Other',
                    
                ),
                ),
            'attributes' => array(
                'value' => 1
            )   
        )); 
             
        
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Upload Now'
            ),
        )); 
        
        
    }
    public function populateValues($data)
    {   
        foreach($data as $key=>$row)
        {
         
           if (is_array(@json_decode($row))){
           $data[$key] =   new \ArrayObject(\Zend\Json\Json::decode($row), \ArrayObject::ARRAY_AS_PROPS);
                      //  $data[$key] = \Zend\Json\Json::encode($row);
                      

        }       
        } 

        parent::populateValues($data);
    }
}
