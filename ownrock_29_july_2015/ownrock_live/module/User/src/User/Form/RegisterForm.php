<?php

namespace User\Form;

use Zend\Form\Form;

class RegisterForm extends Form{

    Public Function __construct($name = NULL) {
        parent:: __construct('Register');
        
        $this->add(array('name' => 'name',
            'attributes' => array(
                'type' => 'text',
                'required' => 'required',
                'placeholder' => 'Name',
                'class' => 'form-control',
            ),
            'options' => array(
                'label' => 'Full Name',
            )
        ));
        
         $this->add(array('name' => 'user_type',
            'attributes' => array(
                'type' => 'INT',
                'hidden' => 'true',
            ),
            'options' => array(
                'label' => 'User Type',
            )
        ));

        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type' => 'email',
                'required' => 'required',
                'class' => 'form-control',
                'placeholder' => 'someone@mail.com',
            ),
            'options' => array(
                'label' => 'Email',
            )
        ));

        $this->add(array(
            'name' => 'password',
            'attributes' => array(
                'type' => 'password',
                'placeholder' => 'Password',
                'class' => 'form-control',
                
               
            ),
            'options' => array(
                'label' => 'Password',
            )
        ));

        $this->add(array(
            'name' => 'confirm_password',
            'attributes' => array(
                'type' => 'password',
                 'placeholder' => 'Repeat password',
                'class' => 'form-control',
            ),
            'options' => array(
                'label' => 'Confirm Password',
            )
        ));
        
         $this->add(array(
            'name' => 'Submit',
            'attributes' => array(
                'type' => 'Submit',
               'value' => 'Register',
                'class'=> 'btn ls-dark-btn ladda-button col-md-12 col-sm-12 col-xs-12',
                'data-style'=> 'slide-down',
            ),
            'options' => array(
            )
        ));
    }

}
