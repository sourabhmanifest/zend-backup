<?php

namespace User\Model;

class AlbumTags {

    public $id;
    public $album_id;
    public $tags;
    

    public function exchangeArray($data) 
    {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->album_id = (!empty($data['album_id'])) ? $data['album_id'] : null;
        $this->tags = (!empty($data['tags'])) ? $data['tags'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}
