<?php

namespace User\Form;

use Zend\Form\Form;

class LoginForm extends Form {

    Public Function __construct() {
        parent:: __construct('form-login');

        $this->add(array('name' => 'email',
            'attributes' => array(
                'type' => 'email',
                'class'=>'form-control',
                'required' => 'required',
                'placeholder'=>'Email',
            ),
            'options' => array(
                'label' => 'Email',
            )
        ));

        $this->add(array('name' => 'password',
            'attributes' => array(
                'type' => 'password',
                'class'=>'form-control',
                'required' => 'required',
                 'placeholder'=>'Password',
            ),
            'options' => array(
                'label' => 'Password',
            )
        ));


        $this->add(array('name' => 'rememberme',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'label' => 'Remember Me',
                'use_hidden_element' => true,
                'checked_value' => 'yes',
                'unchecked_value' => 'no',
            ),
            'attributes' => array(
                'value' => 'yes'
            )
        ));
        
          $this->add(array('name' => 'user_type',
            'attributes' => array(
                'type' => 'text',
                'class'=>'form-control',
            ),
            'options' => array(
            )
        ));

          $this->add(array('name' => 'registration_date',
            'attributes' => array(
                'type' => 'date',
                'class'=>'form-control',
            ),
            'options' => array(
            )
        ));
        $this->add(array('name' => 'Login',
            'attributes' => array(
                'type' => 'Submit',
                'value' => 'Login',
                'data-style'=>'slide-down',
                'class' => 'btn ls-dark-btn ladda-button col-md-12 col-sm-12 col-xs-12',
            ),
            'options' => array(
            )
        ));
    }

}
