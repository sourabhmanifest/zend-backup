<?php
namespace User\Model;

use Zend\Text\Table\Row;

use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;

 
class RateAndReviewTable
{
    protected $tableGateway;
    
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    public function saveRateAndReview(RateAndReview $rate)
    {
        
		//echo "<pre>"; print_r($user); exit;
		$data = array(
            'user_id' => $rate->user_id,
            'star' => $rate->star,
			'review' => $rate->review,
            'name' => $rate->name,
            'email' => $rate->email,
            'phone' => $rate->phone,
            'otp' => $rate->otp,
            'otpverify' => $rate->otpverify,
            'status' => $rate->status
        );
        $this->tableGateway->insert($data); 
        return $this->tableGateway->getLastInsertValue();
    }
    public function phoneIsAlreadyExist($phone)
    {
        $phone = (int)$phone;
        $user_id = (int)$user_id; 
        $resultSet = $this->tableGateway->select(array('phone' => $phone,  'user_id' => $user_id));
        $row = $resultSet->current();
        if(!$row){
            return 0;
        }
        return 1;
    }
    public function getRateAndReview($phone)
    {
        $phone = (int)$phone; 
        $rowset = $this->tableGateway->select(array('phone' => $phone));
        $row = $rowset->current();
        return $row;
    }
    
    public function verifyOtp($id)
    {
        $id = (int)$id;    
        $data['otpverify'] = 1;
        $this->tableGateway->update($data,array('id'=>$id));
    }

    public function updateOtp($phone ,$otp)
    {
        $data['otp'] = $otp;
        $this->tableGateway->update($data,array('phone'=>$phone));
    }
    
    
}