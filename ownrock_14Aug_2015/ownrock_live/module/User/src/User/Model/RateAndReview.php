<?php

namespace User\Model;

class RateAndReview {

    public $id;
    public $user_id;
    public $star;
    public $review;
    public $name;
    public $email;
    public $phone;
    public $otp;
    public $otpverify;
    public $status;


    public function exchangeArray($data) {
        $this->id = (isset($data['id'])) ? $data['id'] : 0;
        $this->user_id = (isset($data['pg_id'])) ? $data['pg_id'] : 0;
        $this->star = (isset($data['star'])) ? $data['star'] : '';
        $this->review = (isset($data['review'])) ? $data['review'] : '';
        $this->name = (isset($data['name'])) ? $data['name'] : '';
        $this->email = (isset($data['email'])) ? $data['email'] : '';
        $this->phone = (isset($data['phone'])) ? $data['phone'] : '';
        $this->otp = (isset($data['otp'])) ? $data['otp'] : '';
        $this->otpverify = (isset($data['otpverify'])) ? $data['otpverify'] : 0;
        $this->status = (isset($data['status'])) ? $data['status'] : 0;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}
