<?php
// filename : module/Users/src/Users/Form/RegisterForm.php
namespace User\Form;

use Zend\Form\Form;


class SkillForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('Skill');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype','multipart/form-data');
        
        $this->add(array(
            'name' => 'skillid',
            'attributes' => array(
                'type'  => 'hidden',
            ),
           
        ));

        
        $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type'  => 'text',
               
            ),
            'options' => array(
                'label' => 'Title',
                 
            ),
        )); 
        
          
        $this->add(array(
            'name' => 'skill',
            'type'  => 'Zend\Form\Element\Multicheckbox',
           
            'options' => array(
                'label' => 'Skill',
                'value_options' => array(
                    '1' => 'We Developer',
                    '2' => 'Web Designer',
                    '3' => 'Other',
                    
                ),
                ),
            'attributes' => array(
                'value' => 1
            )   
        )); 
        
            
        $this->add(array(
            'name' => 'isPopular',
            'attributes' => array(
                'type'  => 'text',
               
            ),
            'options' => array(
                'label' => 'Is Popular',
                 
            ),
        ));
        
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Upload Now'
            ),
        )); 
        
        
    }
}
