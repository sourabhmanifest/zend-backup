<?php

namespace User\Model;

use Zend\Text\Table\Row;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class PhotographerTable 
{
    protected $tableGateway;
	private $offset;
	private $limit;
	private $keyword;

    public function __construct(TableGateway $tableGateway) 
    {
        $this->tableGateway = $tableGateway;
    }


    public function savePhotographer(photographer $photographer) 
    {   
        $data = array(
            'pg_id' => $photographer->pg_id,
            'user_id' => $photographer->user_id,
            'address' => $photographer->address,
            'location' => $photographer->location,
            'tagline' => $photographer->tagline,
            'description' => $photographer->description,
            'price' => $photographer->price,
            'equipment' => $photographer->equipment,
            'product_and_services' => $photographer->product_and_services,
            'status' => $photographer->status,
            'profile_progress' => $photographer->profile_progress
        );

        
        //echo '<pre>'; print_r($photographer);exit;
        $user_id = (int) $photographer->user_id;
        $pg_id = (int) $photographer->pg_id;
        
        if ($pg_id == 0) 
        {    //echo '<pre>'; print_r($data);exit;
            $this->tableGateway->insert($data);
            //echo '<pre>'; print_r($data);exit;
        } 
        else 
        {
            $this->tableGateway->update($data, array('pg_id' => $pg_id));
        } 
    }

    public function getPhotographerData($user_id)
    { 
        $user_id  = (int) $user_id;      
        $resultSet = $this->tableGateway->select(array('user_id' => $user_id)); 
        $row = $resultSet->current();
        return $row;
    }
    public function updateProfileImage($user_id,$profilepic) 
    {
        $data = array('profilepic'=>$profilepic);
        $this->tableGateway->update($data, array('user_id'=>$user_id));
    }

    public function fetchallProfiles() 
    {
        $resultSet = $this->tableGateway->select(); 
        return $resultSet->toArray();
    }

    public function getProfileByUserId($user_id) 
    {   
        $user_id  = (int) $user_id;      
        //echo $user_id;exit;
        $resultSet = $this->tableGateway->select(array('user_id' => $user_id)); 
        return $resultSet->current();
    }

    public function updateStatus($user_id) 
    {
        $user_id  = (int) $user_id;  
        $data = array('status'=>1);
        $this->tableGateway->update($data, array('user_id'=>$user_id));
    }

    /*public function getPhotographerByKeyword($keyword=array(),$offset=0,$limit=3,$location='',$sort_by='',$rate,$availability) 
    { 
        $this->keyword = array();   
        $response = array();
        $this->keyword = $keyword;
        
        $rowset=$this->tableGateway->select(function(Select $select) use ($limit,$offset,$location,$sort_by,$rate,$availability)
        {   
            $select->join('album', 'album.user_id = user_id',array('user_id' => 'user_id'));
            
            
            if($rowset)
            {
            
            $response['result'] = $rowset->toArray();      
            //$row = $rowset->current();
            echo '<pre>';print_r($response); die;
            }
            else
            {
                echo "else"; exit;
            }
        });
        
    }*/
    public function searchPhotographer($keyword=array(), $location='', $sort_by, $fromrate, $torate, $offset,$user_id = '') 
    { 
        //echo $sort_by; exit;
        $this->keyword = array();   
        // $this->$type = array();
        $response = array();
        $this->keyword = $keyword;
        $offset =(int)$offset;
        $sort_by  =(int)$sort_by;
        $torate  =(int)$torate;
        $fromrate  =(int)$fromrate;
        $user_id = (int)$user_id;

        $rowset = $this->tableGateway->select(function(Select $select) use ($limit, $offset, $location, $sort_by, $fromrate, $torate, $user_id  )
        {   

            $select->where->EqualTo('status', '2');

            
            if(trim($user_id) != ''){

                $select->where->notequalTo('user_id' ,$user_id);     
            }

            //if(!empty($this->keyword)){
             //       $select->where(array('profession' => $this->keyword));
            //}
            if(trim($location) != ''){

                $select->where->like('location', $location);               
            }
            //if(!empty($this->$type)){
            //        $select->where(array('type' => $this->type));
            //}
            
            $select->where->between('price', $fromrate, $torate);
            
            

            if(trim($sort_by) != '')
            {
                if(trim($sort_by)==0)
                {
                    $select->order('pg_id ASC');
                }
                if(trim($sort_by)==1)
                {
                    $select->order('price DESC');
                }
                if(trim($sort_by)==2)
                {
                    $select->order('price ASC');
                }
                if(trim($sort_by)==3)
                {
                    $select->order('pg_id DESC');
                }
            }

        });

        //$response['totalcount'] = $rowset->count(); 
        return $rowset->toArray();  
        //echo '<pre>';print_r($response['result'])  ;exit;  
        
    }

}

 