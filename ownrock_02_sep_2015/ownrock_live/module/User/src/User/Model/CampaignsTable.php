<?php

namespace User\Model;

use Zend\Text\Table\Row;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class CampaignsTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function saveCampaigns(Campaigns $campaigns) {
        $data = array(
            'preview' => $campaigns->preview,
            'payout' => $campaigns->payout,
            'categories' => $campaigns->categories,
            'MerchantName' => $campaigns->MerchantName,
            'phonenumber' => $campaigns->phonenumber,
            'websiteUrl' => $campaigns->websiteUrl,
            'description' => $campaigns->description,
            'email' => $campaigns->email,
        );

        $campaign_id = (int) $campaigns->campaign_id;
        if ($campaign_id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getCampaigns($campaign_id)) {
                $this->tableGateway->update($data, array('campaign_id' => $campaign_id));
            } else {
                throw new \Exception('Campaign id does not exist');
            }
        }
    }

    public function getCampaigns($id) {

        $campaign_id = (int) $id;
        $rowset = $this->tableGateway->select(array('campaign_id' => $campaign_id));
        $row = $rowset->current();

        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function fetchAll() {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function deleteCampaigns($campaign_id) {
        $this->tableGateway->delete(array('campaign_id' => (int) $campaign_id));
    }

}
