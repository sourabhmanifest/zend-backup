<script>
   $('#career a').css('background-color','#056ab2');
   $('#career a').css('color','#FFF');
</script>
<section class="page-cover career-cover-img">
   <div class="container-fluid">
      <div class="container">
         <div class="page-heading">
            <h3>CAREER</h3>
            <h4>We open the door of "HOPE"</h4>
            <p> Our career offerings make the way for the job seekers. Our career plan is to give opportunity for many people to expand our human resource base </p>
         </div>
      </div>
   </div>
</section>
<!--/page cover & career section-->
<section id="career" class="page-section theme-bg-gray">
   <div class="container-fluid">
      <div class="container">
         <div class="span7 pull-left margin-null txt-justify">
            <h1 class="heading-a border-theme-l"> Career at our Company</h1>
            <img src="assets/images/career/phpdeveloper.jpg" class="shadow-block" title="Career at our Company" />
            <div class="spacer-mini2"></div>
            <ul class="bullet_process check_right span border_box justify">
               <li class="bgcolor_jo">Job Summary</li>
               <li>Functional area :- IT/ Web programming</li>
               <li>Industry :- Computer/ IT</li>
               <li>Role Category :-PHP Developer/Web Developer/PHP/HTML/WordPress/MySQL.</li>
               <li>Vacancy :-2</li>
               <li>Job Location :- 105, Prakash Tower, Rani Sati Gate, YN Road Indore</li>
               <li>Job details :-</li>
               <li>Candidate should have more than 1 year of experience in PHP development.</li> 
               <li>Candidate should have good knowledge of Core PHP</li>
               <li>CodeIgniter, Drupal, Zend or Wordpress would be a plus point</li> 
               <li>Skills Needed :-</li>
               <li>Core PHP, Codeigniter, Drupal, or Wordpress, HTML, CSS, jQuery, Bootstrap, MVC, Payment Gateways, e-Commerce, Social media integrations etc.</li>
            </ul>
         </div>
         <!---/left panel-->
       <?php //include "career-news.php"; ?>
	   <?php include "portfolio/recent-post.php"; ?>
      </div>
   </div>
</section>
<?php include "current-opportunity.php"; ?>

