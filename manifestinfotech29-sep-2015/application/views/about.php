<script>
	$('#aboutus a').css('background-color','#056ab2');
	$('#aboutus a').css('color','#FFF');
</script>
<section class="page-cover About-cover-img">
  <div class="container-fluid">
    <div class="container">
      <div class="page-heading">
        <h3>ABOUT US</h3>
        <h4>We are the reason for "THINK BIG"</h4>
        <p> We make our customer to think ambitiously as they can and we are making that dream beyond their desire </p>
      </div>
    </div>
  </div>
</section>

<!--services end here about start -->
<section class="page-section coo_otr" >
  <div  class="container-fluid">
    <div class="container">
      <div class="page-heading"></div>
      <div class="spacer-mini"></div>
      <div class="container">
        <div class=" row-fluid">
          <div class="span12">
            <div class="border pull-right thumb-right"> 
				<a href="#" class="shadow"></a>
			</div>
            

            
            <h3>VISION</h3>
            
            <p class="justify"> "Vision without action is merely a dream. Action without Vision is just passes the time. Vision with Action can change the world".</p>
            <p><b> <h4 style="text-align:right">"Joel A.Barker"</h4></b></p>
            <br />
            <h3>MISSION</h3>
            
            <p class="justify"> Big Dreams are just Big Goal with step by step action. Anything is possible given enough Time, Passion, Persistence, and Dedication. </p>
            <p><b><h4 style="text-align:right">"Brooke Griffin"</h4></b></p>

            <p class="justify">We are helps our client's to convert their dreams into their goal, with the aim of client satisfaction at priority bases. We believe in quality services at low cost along with this using technology to gather, compose, clean, maintain, process and analysing data. </p>

            <p class="justify">Our company have principal to gain customer satisfaction by using all our delightful services. Our people's learnings, skills, and growth are also an important goal for us. There is relation between the happy people which lead to the happy customer and better innovative solution provided to customer. </p>

            <p class="justify">Manifest having culture of working with coordination to maintain our value system.  Our value alikeness words are Caring, Learning, Achieving, sharing and Social Responsibility. Every action of our company is fully dedicated to our stakeholder benefit and for gaining profitability for our company. </p>
            
            <p class="justify"><h3 >Introduction of Company </h3></p>
            
            <p class="justify"> The company was incorporated as a private Ltd company under Indian company act with the name Manifest Infotech Pvt. Ltd. </p>

            <p class="justify">Manifest Infotech founded in 2012 as a Software Development company having    expertise in creating technologies, Animation, Programming, Designing and many more. We are having professional team in web creative, development, system architecture, e-commerce and content management solutions with a wealth of experience in successfully deploying complex website projects in a wide variety of business sectors. </p>

            <p class="justify">We are doing Each and every project with full enthusiasm and effectiveness. Our experience and professional team work with fresh and innovative ideas. Our team has specialization in development and maintenance of desktop Applications, Mobile Application, websites, encompassing an array of technologies. We build imaginative and innovative solutions for web based on rising internet and web technologies in a moderate cost. </p>

            <p class="justify">We believe that client's success is our success. Great association with customer is the key piece of our association. When we provide services with full professionalism that makes our bonding more strong and lifelong. </p>

            <p class="justify">Our technical support and services are always with our client during project and also after completing the project in the form of technical support, maintenance and consulting services. </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
