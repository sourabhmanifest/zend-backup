<script type="text/javascript">
var loadFile = function(event) {
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
  };
var loadFile1 = function(event) {
    var output1 = document.getElementById('output1');
    output1.src = URL.createObjectURL(event.target.files[0]);
  };
var loadFile2 = function(event) {
    var output2 = document.getElementById('output2');
    output2.src = URL.createObjectURL(event.target.files[0]);
  };
var loadFile3 = function(event) {
    var output3 = document.getElementById('output3');
    output3.src = URL.createObjectURL(event.target.files[0]);
  };
var loadFile4 = function(event) {
    var output4 = document.getElementById('output4');
    output4.src = URL.createObjectURL(event.target.files[0]);
  };
</script>
<style>
#col-container1 
{
    width: 60%;
}
</style>
<?php
require 'ImageManipulator.php';
if($_SERVER['HTTP_HOST']=='localhost')
{
    $documentRootPath = $_SERVER['DOCUMENT_ROOT'].'/wordpress/';
}
else
{
    $documentRootPath = $_SERVER['DOCUMENT_ROOT'];
}
if (isset($_REQUEST['edit']))
{
    $edit_id = $_REQUEST['edit'];
    if(isset($_POST['update']))
    {
        $category_portfolio = $_POST["category_portfolio"];
        $project_name       = $_POST["project_name"];
        $hidden_name        = $_POST["hidden"];
        $url                = $_POST["url"];
        $project_one        = $_POST["project_one"];
        $desc               = $_POST["desc"];
        $prjct_tech         = $_POST["prjct_tech"];
        $folder_name        = (strtolower($_POST["project_name"]));
        $project_name1 = sanitize_title_with_dashes($folder_name);
        $path=$documentRootPath."/staging/assets/images/portfolio/".$project_name1;
        mkdir($path, 0777, true); 
        $active=0;
        if(isset($_POST['check'])) 
        {
            $active = 1;
        }
        $maxFileSize        = 2097152;
        $error = 0;
        if (isset($_FILES))
        {    
            $img=array();
            for ($i = 0; $i < 5; $i++)
            {   
                if ($_FILES['image']['error'][$i] == 0)
                {
                    if ($_FILES['image']['type'][$i] == 'image/jpg' || $_FILES['image']['type'][$i] == 'image/jpeg' || $_FILES['image']['type'][$i] == 'image/png' || $_FILES['image']['type'][$i] == 'image/gif')
                    {
                        if($i==0)
                        {
                            $file_name      = $_FILES['image']['name'][$i];
                            $file_name      = strtolower($file_name);
                            $file_name1     = explode(".", $file_name);
                            $img[]          = $file_name1[0]."thumb.".$file_name1[1];
                            $w = '420'; $h = '290';
                            $uploadfile = $path."/".$img[$i];
                            resizeqq($w, $h, $uploadfile, $_FILES['image'], $i);
                        }
                        else
                        {
                            $file_name = $_FILES['image']['name'][$i];
                            $file_name      = strtolower($file_name);
                            $file_name1 = explode(".", $file_name);
                            $img[] = $file_name1[0].$i.".".$file_name1[1];
                            $uploadfile = $path."/".$img[$i];
                            $w = 800; $h = 429;
                            resizeqq($w, $h, $uploadfile, $_FILES['image'], $i);        
                        }

                    }
                    else
                    {
                        $error=1;
                    }
                }
                else
                {
                    $error=1;
                }
            }       
        }
    $active=0;
    if(isset($_POST['check'])) 
    {
        $active = 1; 
    }
    if($error==0)
    {   
        global $wpdb;
       $sq = $wpdb->query("UPDATE `portfolio_project` SET category_id = $category_portfolio, project_name = '$project_name', project_url = '$url', 
            project_one_liner = '$project_one', project_description = '$desc', project_technologies = '$prjct_tech', thumb = '$img[0]', 
            image1 = '$img[1]', image2 = '$img[2]', image3 = '$img[3]' , image4 = '$img[4]', is_active = $active  WHERE project_id =" . $edit_id);
?>
            <div class="wrap">
            <div id="message" class="updated below-h2">
            <p><?php echo "Record Updated";?></p>
            </div></div>
<?php
        }
        else
        {
            ?><div class="wrap">
            <div id="message" class="updated below-h2">
            <p><?php echo "Record Not Updated";?></p>
            </div></div>
            <?php
        }
    }
}
?>
<script type="text/javascript">
    function show_alert() {
    var msg = "Project Inserted";
    alert(msg);
    }
</script>
<script type="text/javascript">
    function show_alert9() {
    var msg = "Format not supported\nPlease upload jpg, jpeg, gif, png format";
    alert(msg);
    }
</script>
<?php 
if (isset($_POST['submit']))
{
    $category_portfolio = $_POST["category_portfolio"];
    $project_name       = $_POST["project_name"];
    $hidden_name        = $_POST["hidden"];
    $url                = $_POST["url"];
    $project_one        = $_POST["project_one"];
    $desc               = $_POST["desc"];
    $prjct_tech         = $_POST["prjct_tech"];
    $folder_name        = (strtolower($_POST["project_name"]));  
    $project_name1 = sanitize_title_with_dashes($folder_name);
    
    $path=$documentRootPath."/staging/assets/images/portfolio/".$project_name1;
    mkdir($path, 0777, true); 
    $maxFileSize        = 2097152;
    $error = 0;
    if (isset($_FILES))
    {    
        $img=array();
        for ($i = 0; $i < 5; $i++)
        {   
            if ($_FILES['image']['error'][$i] == 0)
            {
                if ($_FILES['image']['type'][$i] == 'image/jpg' || $_FILES['image']['type'][$i] == 'image/jpeg' || $_FILES['image']['type'][$i] == 'image/png' || $_FILES['image']['type'][$i] == 'image/gif')
                {
                    if($i==0)
                    {
                        $file_name      = $_FILES['image']['name'][$i];
                        $file_name      = strtolower($file_name);
                        $file_name1     = explode(".", $file_name);
                        $img[]          = $file_name1[0]."thumb.".$file_name1[1];

                        $w = '420'; $h = '290';
                        $uploadfile = $path."/".$img[$i];
                        resizeqq($w, $h, $uploadfile, $_FILES['image'], $i);
                    }
                    else
                    {
                        $file_name = $_FILES['image']['name'][$i];
                        $file_name      = strtolower($file_name);
                        $file_name1 = explode(".", $file_name);
                        $img[] = $file_name1[0].$i.".".$file_name1[1];
                        $uploadfile = $path."/".$img[$i];
                        $w = 800; $h = 429;
                        resizeqq($w, $h, $uploadfile, $_FILES['image'], $i);        
                    }
                }
                else
                {
                    $error=1;
                }
            }
            else
            {
                $error=1;
            }
        }       
    }
    $active=0;
    if(isset($_POST['check'])) 
    {
        $active = 1; 
    }
    if($error==0)
    { 
        global $wpdb;
        $q = "INSERT INTO `portfolio_project`(project_id, category_id, project_name, project_url, project_one_liner, project_description, project_technologies, thumb, image1, image2, image3, image4, is_active )values(null,$category_portfolio, '$project_name', '$url', '$project_one' , '$desc', '$prjct_tech', '$img[0]','$img[1]','$img[2]','$img[3]','$img[4]', $active)"; 
        //die;
        $sql = $wpdb->query($q);
        if($sql)
        {
          ?>
            <div class="wrap">
                <div id="message" class="updated below-h2">
                <p><?php echo "Project Added Successfully";?></p>
                </div>
            </div>
            <?php
        }
        else
        {
            echo "Not Inserted";
        }
    }
} ?>
<div class = "wrap nosubsub">
    <h2><?php echo "Projects";?>
        <a class="add-new-h2" href="admin.php?page=views">View All Projects</a>
    </h2>
</div>
<div id="col-container1">
    <div id="">
        <div class="col-wrap">
            <div class="form-wrap">
<?php
global $wpdb;
//echo $path;
 $querystr   = "SELECT * FROM `portfolio_category` ";
$the_category   = $wpdb->get_results($querystr);
$category_id    = $category->category_id;
$querystring = "SELECT * FROM `portfolio_project` WHERE project_id = " . $edit_id;
$project_category = $wpdb->get_row($querystring);
$p_c_id     = $project_category->category_id;
$tech       = $project_category->project_technologies;
$_name      = $project_category->project_name;
$one_line = $project_category->project_one_liner;
$_url = $project_category->project_url;
$descp  = $project_category->project_description;
$chk = $project_category->is_active;
$thumb = $project_category->thumb;
$img_1 = $project_category->image1;
$img_2 = $project_category->image2;
$img_3 = $project_category->image3;
$img_4 = $project_category->image4;

$_name1 = sanitize_title_with_dashes($_name);
$p = 'http://manifestinfotech.com/';
$path1=$p."/staging/assets/images/portfolio/".strtolower($_name1);
$imageSrc = $path1  . '/' . $thumb;
?>
<h3><?php  echo "Add New Project";?></h3>
<form id="addtag" method="post" action="" class="validate" enctype="multipart/form-data">
    <table class="wp-list-table widefat fixed posts">
        <tr class="form-field form-required">
            <th scope="row"><label for="porfolio_category"><?php  _e('Select Portfolio Category');?><span class="description"><?php _e('(required)');?></span></label></th>
            <td><select id="porfolio_category" class="" value="<?php  echo esc_attr($new_user_login);?>" name="category_portfolio" required>
                <option value="">Select</option>
<?php
    foreach ($the_category as $category)
    {    
      $c_id = $category->category_id;?>
                <option value="<?php echo $category->category_id;?>" 
<?php
                if ($p_c_id == $category->category_id)
                {
?>                  selected="selected"
<?php
                }
?>>
<?php
                echo $category->category;
?>              </option>     
<?php
    }
?>
                </select>
        </tr>

         <tr class="form-field form-required">
            <th scope="row"><label for="project_name"><?php _e('Project Name');?><span class="description"><?php _e('(required)');?></span></label></th>
            <td><input name="project_name" type="text" id="project_name1" onKeypress="validate();" value="<?php if (isset($_REQUEST['edit']))echo $_name;?>"  required="required" /></td>
        </tr>
         <tr class="form-field">
            <th scope="row"><label for="url"><?php _e('Project URL');?></label></th>       
            <td><input name="url" type="url" id="url" class="code" value="<?php if (isset($_REQUEST['edit']))echo $_url;?>" required="required"  /></td>
        </tr>
        <tr class="form-field form-required">
            <th scope="row"><label for="project_one_liner"><?php _e('Project One Liner');?><span class="description"><?php _e('(required)');?></span></label></th>
            <td><input name="project_one" type="text" id="project_one1" value="<?php if (isset($_REQUEST['edit']))echo $one_line;?>" required="required" /></td>
        </tr>

        <tr class="form-field">
             <th scope="row"><label for="description"><?php _e('Project Description');?> </label></th>
             <td>
                <textarea name="desc" type="text" id="desc1" value="echo $descp;?>" required="required" rows="4" column="50"/><?php if (isset($_REQUEST['edit'])) echo $descp;?>
                </textarea>
             </td>
        </tr>
         <tr class="form-field">
             <th scope="row"><label for="technologies"><?php _e('Project Technologies');?> </label></th>
             <td><input name="prjct_tech" type="text" id="prjct_tech1" value="<?php if (isset($_REQUEST['edit']))echo $tech;?>" required="required"  /></td>
        </tr>
         <tr>
            <th scope="row"><label for="technologies"><?php _e('Thumbnail Image');?>  </label></th>
            <td>
                <input class='file' multiple="multiple" type="file" class="form-control" name="image[]" id="images" placeholder="Please choose your image" required="required" onchange="loadFile(event)">
            </td>
            <td>
                <?php if(isset($_REQUEST['edit'])){ ?><img src="<?php echo $imageSrc = $path1  . '/' . $thumb;?>" height="50px" width="50px"><?php } else { ?> <img id="output" width="100" height="100"/><?php }?>
            </td>

                <span class="help-block"></span>
                     <div id="loader" style="display: none;">Please wait...</div>
                        <div id="uploaded_images" class="uploaded-images">
                            <div id="error_div" style="color:red;">
                            </div>
                        <ul id="success_div" style="list-style-type: none;display: table-cell;"></ul>
         </tr>

         <tr>
              <th scope="row"><label for="technologies"><?php _e('Image 1');?>  </label></th>
              <td>
                <input class='file' multiple="multiple" type="file" class="form-control" name="image[]" id="images" placeholder="Please choose your image" required="required" onchange="loadFile1(event)">
              </td>                    
              <td>
                <?php if(isset($_REQUEST['edit'])){ ?><img src="<?php echo $imageSrc = $path1  . '/' . $img_1;?>" height="50px" width="50px"><?php } else { ?> <img id="output1" width="100" height="100"/><?php }?>
              </td>
         </tr>
          <tr>
               <th scope="row"><label for="technologies"><?php _e('Image 2');?> </label></th>
               <td>
                    <input class='file' multiple="multiple" type="file" class="form-control" name="image[]" id="images" placeholder="Please choose your image" required="required" onchange="loadFile2(event)">
               </td>
               <td>
                    <?php if(isset($_REQUEST['edit'])){ ?><img src="<?php echo $imageSrc = $path1  . '/' . $img_2;?>" height="50px" width="50px"><?php} else { ?> <img id="output2" width="100" height="100"/><?php }?>
               </td>
         </tr>
          <tr>
             <th scope="row"><label for="technologies"><?php  _e('Image 3');?> </label></th>
             <td>
                <input class='file' multiple="multiple" type="file" class="form-control" name="image[]" id="images" placeholder="Please choose your image" required="required" onchange="loadFile3(event)">
             </td>
             <td>
                <?php if(isset($_REQUEST['edit'])){ ?><img src="<?php echo $imageSrc = $path1  . '/' . $img_3;?>" height="50px" width="50px"><?php }?>
             </td>
          </tr>
           <tr>
                <th scope="row"><label for="technologies"><?php  _e('Image 4');?>  </label></th>
                    <td><input class='file' multiple="multiple" type="file" class="form-control" name="image[]" id="images" placeholder="Please choose your image" required="required" onchange="loadFile3(event)"></td>           
                <td>
                    <?php if(isset($_REQUEST['edit'])){ ?><img src="<?php echo $imageSrc = $path1  . '/' . $img_4;?>" height="50px" width="50px"><?php }?>
                </td>
           </tr>
            <tr class="form-field">
                <th scope="row"><label for="technologies"><?php _e('Active');?></label></th>
                <td>
                <input type="checkbox" value="" name="check" id="check" <?php if($chk == 1){?> checked="checked"<?php } ?>>Is Active
            </td>

            </tr>
            <tr class="form-field">
                <td><input name="<?php
                if (isset($_REQUEST['edit']))
                {
                    echo "update";
                }
                else
                {
                    echo "submit";
   