<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

<section class="page-cover blog-cover-img">
<div class="container-fluid">

<div class="container">
<div class="page-heading">
<h3>Blog</h3>
<h4>Creative thinking and experience .</h4>
<p>
We write on things that makes one strong thought and become opinion for many mind. Blogs needed thoughts, content, interest and comments.
</p>
</div>
</div>
</div>
</section>
<section><br />
	<div class="">
		<div class="container">
			<?php the_breadcrumb(); ?>
		</div>
	</div><br />
</section>

<section class="page-section theme-bg-gray">
<div class="container-fluid">
    <div class="container">
		 <h4 class="doc_dimand"><?php
			if ( is_single() ) :
				the_title( '<h2 class="entry-title">', '</h2>' );
			else :
				the_title( sprintf( '<h4 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h4>' );
			endif;
		?></h4><br />
	
		<div class="row-fluid">
			<div class="span8 pull-left">
	
	<?php echo '<span style="color:#1673B7;">Posted by:</span> '.get_the_author().',  '; ?>
	<?php echo the_date(); ?>
	
	<?php //the_author_meta('display_name'); ?>
	<?php edit_post_link( __( 'Edit', 'twentyfifteen' ), '[', ']' ); ?>
	<br /><br />
	<span class='st_sharethis' displayText=''></span>
	<span class='st_facebook' displayText=''></span>
	<span class='st_twitter' displayText=''></span>
	<span class='st_linkedin' displayText=''></span>
	<span class='st_pinterest' displayText=''></span>
	<span class='st_fblike' displayText=''></span>
     <div class="spacer-mini2"></div>
          
	<p class="" style="text-align: center; border: 1px dotted grey;"><?php the_post_thumbnail( 'full', array('class' => "img-responsive"))?>
		<span class="blog-content-des">
		<?php
			/* translators: %s: Name of current post */
			the_content( sprintf(
			__( 'Continue reading %s', 'twentyfifteen' ),
			the_title( '<span class="screen-reader-text">', '</span>', false )
			) );

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentyfifteen' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );
		?>
		</span>
	</p>
		
    <p class="txt-justify"> <b><?php
		// Author bio.
		if ( is_single() && get_the_author_meta( 'description' ) ) :
			get_template_part( 'author-bio' );
		endif;
		?> </b>
	</p>
     <br />
			 <div class="fb-comments" data-href="<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" data-numposts="5" data-colorscheme="light" ></div>		
			
			</div>
        <!----left pane-->	
		
		<div id="right-panel-cat" class="span3 pull-right shadow-block">
              <h3 class="cat-title heading-a border-theme-l"> Catagories</h3>
              <ul class="cat-list justify">
            <li> <a href="<?php echo $ciSiteUrl; ?>/portfolio/category/1"><i class="fa fa-chevron-right"></i>Design</a> </li>
            <li>
                <li> <a href="<?php echo $ciSiteUrl; ?>/portfolio/category/2"><i class="fa fa-chevron-right"></i>Developement</a> </li>
                <li>
                <li> <a href="<?php echo $ciSiteUrl; ?>/portfolio/category/3"><i class="fa fa-chevron-right"></i>SEO</a> </li>
				<!--<li> <a href="<?php echo $ciSiteUrl; ?>/mi/digital-marketing"><i class="fa fa-chevron-right"></i>Digital Marketing</a> </li> -->
          </ul>
            </div>
        <div class="span3 pull-right"> </div>
        <div id="right-panel-post" class="span3 pull-right shadow-block">
              <h3 class="cat-title heading-a border-theme-l"> Recent Posts</h3>
             <?php 
			 $args = array(
					'numberposts' => 6, 
					'suppress_filters' => true,
			) ;
			 $recent_posts = wp_get_recent_posts($args); 
			 ?>
			 <ul class="r-post-list">
			 <?php foreach( $recent_posts as $recent ){ ?>
			 <li>
				<a href="<?php echo get_permalink($recent["ID"]); ?>">
					<p><?php echo  $recent["post_title"]; ?></p>
					<small><?php echo date('j-F-Y',strtotime($recent["post_date"])) ;?></small> 
				</a> 
			 </li>
			 <?php } ?>          
          </ul>
            </div>
      </div>	
	
	

	</div>
	
</div>
</section>


<style>
.post-navigation {
display: none;
}

.blog-content-des p{

	color: #222;
font-size: 18px;
font-weight: 300;
line-height: 1.6em;
padding-top: 10px;
text-align: justify !important;
}
</style>