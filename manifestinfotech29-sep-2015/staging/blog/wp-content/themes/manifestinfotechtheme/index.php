<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */?>
<script>
var ajax_url = "<?php  echo admin_url('admin-ajax.php' , relative); ?>";
</script><?php
add_action( 'wp_ajax_loadmore_mail', 'loadmore_mail' );
/*function loadmore_mail() 
{
	echo "gdfgdfg";
	include "mail.php";
} */
get_header(); ?>

<section class="page-cover blog-cover-img">
<div class="container-fluid">

<div class="container">
<div class="page-heading">
<h3>Blog</h3>
<h4>Creative thinking and experience .</h4>
<p>
We write on things that makes one strong thought and become opinion for many mind. Blogs needed thoughts, content, interest and comments.
</p>
</div>
</div>
</div>
</section>

<?php 	$postslist = get_posts( array( 'posts_per_page' => 50, 'orderby'=> 'post_date' ) ); ?>

<section id="blog-thumb" class="page-section theme-bg-gray">
<div class="container-fluid">
<div class="container">
<div class="row">
<?php 
$counter=0;
foreach( $postslist as $post ) : setup_postdata($post); ?>
<?php  echo ($counter++ % 3 == 0) ? '</div><div class="spacer-mini"></div><div class="row">' : ''; ?>
<div class="col-lg-6 col-md-6 portfolio-item blog-thumb-bg">
	<a href="<?php the_permalink(); ?>">
		<div class="blog-thumb-date">
		<h2><?php  the_time('j'); ?></h2>
		<p><?php the_time('F') ?></p>
		</div>
		<div style="text-align:center;">
		<?php the_post_thumbnail( 'featured-img-thumb', array('class' => "img-responsive"))?></div>
	</a>
	<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
	<p><?php echo the_content_rss('', TRUE, '', 16); ?>.</p>
	<span class="blog-icons">
<span class='st_sharethis' displayText=''></span>
	<span class='st_facebook' displayText=''></span>
	<span class='st_twitter' displayText=''></span>
	<span class='st_linkedin' displayText=''></span>
	<span class='st_pinterest' displayText=''></span>
	<span class='st_fblike' displayText='' st_url="<?php the_permalink(); ?>" st_title="<?php the_title(); ?>"></span>
</span>
</div>

<?php  endforeach; ?>
</div>
</div>
</div>
</section>

<?php get_footer(); ?>

<style>
.col-lg-6 {
    margin-left: 0;
    margin-right: 4.5%;
    min-height: 366px!important;
    position: relative;
    width: 30.3%;
}
	
.blog-icons{ 
	 background-attachment: scroll;
    background-color: rgba(0, 0, 0, 0);
    background-image: url("../images/sep.png"); 
    background-position: 0 0;
    background-repeat: no-repeat;
	bottom: 5px;
    right: 5px;
    padding-bottom: 0;
    padding-left: 0;
    padding-right: 0;
    padding-top: 10px;
    position: absolute;
    text-align: right;
    width: 290px;
}
.stButton {
    vertical-align: text-top;
}



</style>
