<!-----top navigations section closed here...-------->
<section class="page-cover portfolio-cover-img">
  <div class="container-fluid">
    <div class="container">
      <div class="page-heading">
        <h3>Portfolio</h3>
        <h4>We help your make e marketing esier.</h4>
        <p> Contrary to popular belief, Lorem Ipsum is not simply random text. 
          It has roots in a piece of classical Latin literature from 45 BC, making
          it over 2000 years old. </p>
      </div>
    </div>
  </div>
</section>
<section >
  <div class="container-fluid bac_color border_gray">
    <div class="container prev_next">
      <ul class="mar_hide">
        <li><a href="javascript:void(0)"><img src="img/1426168176_basics-05-48.png" class="algin_verti_middile" />PREV</a></li>
        <li class="next_float"><a href="javascript:void(0)">NEXT<img src="img/1426168170_basics-06-48.png" class="algin_verti_middile" /></a></li>
      </ul>
    </div>
  </div>
</section>
<!--/page cover & portfolio section...
----------------------------------------------------------------------------------->
<section class="page-section theme-bg-gray">
  <div class="container-fluid">
    <div class="container">
      <h4 class="doc_dimand">ROBB REPORT</h4>
      <div class="row-fluid">
        <div class="span8 pull-left">
          <div class="flexslider">
            <ul class="slides img-hint">
              <li> <a href="javascript:void(0)"><img src="img/portfolio-img/p-large/design/webdesign-1.jpg"/></a>
                <p class="flex-caption">Web Desing Project</p>
                <a href="javascript:void(0)">
                <div class="hind-block"><i class="fa fa-link fa-2x"></i></div>
                </a> </li>
              <li> <a href="javascript:void(0)"><img src="img/portfolio-img/p-large/development/development.jpg" /></a>
                <p class="flex-caption">Web Development Project</p>
                <a href="javascript:void(0)">
                <div class="hind-block"><i class="fa fa-link fa-2x"></i></div>
                </a> </li>
              <li> <a href="javascript:void(0)"><img src="img/portfolio-img/p-large/seo/seo.jpg" /></a>
                <p class="flex-caption">SEO(Search Engine Optimazation)</p>
                <a href="javascript:void(0)">
                <div class="hind-block"><i class="fa fa-link fa-2x"></i></div>
                </a> </li>
              <li> <a href="javascript:void(0)"><img src="img/portfolio-img/p-large/design/webdesign-1.jpg" /></a>
                <p class="flex-caption">Web Desing Project</p>
                <a href="javascript:void(0)">
                <div class="hind-block"><i class="fa fa-link fa-2x"></i></div>
                </a> </li>
            </ul>
          </div>
          <div class="spacer-mini2"></div>
          <p class="txt-justify"><br />
            <b><a href="http://robbreport.com/" class="colo_url"> http://robbreport.com/</a></b></p>
          <p></p>
          <p class="txt-justify"> The Robb Report is an American luxury-lifestyle magazine featuring products including automobiles, real estate and watches. The Robb Report website shows the latest trend of jewelry, home & style and fashion. It is monthly published magazine and also an amazing website for all who love latest trends. </p>
          <p></p>
		  <hr/>
          <p class="txt-justify"> <b>Technologies used:</b> HTML5, CSS3, PHP, DRUPAL. </p>
        </div>
        <!----left pane----------------------->
        
		<!----portfolio-detail-categories start ----------------------->
        
		<?php include "category/portfolio-detail-categories.php"; ?>
		 
		<!----portfolio-detail-categories end ----------------------->

       	
		<!---------------- recent post start ----------------------->
        
		<?php include "recent-post.php"; ?>
		 
		<!----------- recent post end ----------------------->

      </div>
    </div>
  </div>
</section>