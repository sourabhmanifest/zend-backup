<script>
	if("<?php echo $title; ?>"!="Manifest Infotech")
	{
		$('#portfolio a').css('background-color','#056ab2');
		$('#portfolio a').css('color','#FFF');
	}
</script>

<section class="container-fluid page-cover portfolio-cover-img">
  <div class="container-fluid">
    <div class="container">
      <div class="page-heading">
        <h3>Featured Portfolio</h3>
        <h4>We are Glaring and Prominent in our work</h4>
        <p> We showcase our work which are done with full dedication and hardwork. Our designer team make an effort to show our portfolio better than the best </p>
      </div>
    </div>
  </div>
</section>
<!--/page cover & testimonail section...
----------------------------------------------------------------------------------->

<section  class="page-section theme-bg-gray">
  <div class="container-fluid">
    <div class="container">
      <div class="page-heading">
        <!--<h3>Featured Portfolio </h3>
        <p> Contrary to popular belief, Lorem Ipsum is not simply random text. </p>-->
      </div>
    </div>
    <!--page heading closed-->
    <div class="container">
      <div class="pagination portfolio-type">
        <ul class="filter nav nav-pills">
          <li class="" data-value="all"><a href="javascript:void(0)">All</a></li>
          <li data-value="design"><a href="javascript:void(0)">Designing</a></li>
          <li data-value="dev"><a href="javascript:void(0)">Development</a></li>
          <li data-value="seo"><a href="javascript:void(0)">SEO</a></li>
        </ul>
      </div>
      <ul class="thumbnails">
        <li data-type="design" data-id="1" class="span4 view view-tenth"> 
					<img src="img/portfolio-img/p-thumb/design/doctorondemand/doctor.jpg"  alt="doctorondemand">
          <div class="mask">
            <h2>DOCTOR ON DEMAND </h2>
            <p> An app that allow patient to get appointment, take prescription, and pay online fee to the doctors. </p>
            <a href="<?php echo site_url('portfolio/doctor-on-demand');?>" class="btn-light">More</a> 
					</div>
        </li>

        <li data-type="design" data-id="2" class="span4 view view-tenth"> 
					<img src="img/portfolio-img/p-thumb/design/bellanaija/bellanaija.jpg"  alt="bellanaija">
          <div class="mask">
            <h2> BELLA NAIJA</h2>
            <p> A website that provide all kind of news, features and updates exclusive from Nigeria.</p>
            <a href="<?php echo site_url('portfolio/bella-naija');?>" class="btn-light "> More </a> 
					</div>
        </li>

        <li data-type="seo" data-id="3" class="span4 view view-tenth"> 
					<img src="img/portfolio-img/p-thumb/design/publicstorage/public.jpg"  alt="">
          <div class="mask">
            <h2>PUBLIC STORAGE</h2>
            <p> It help for the extra space to the people who need to expand their work and life. </p>
            <a href="<?php echo site_url('portfolio/public-storage');?>" class="btn-light "> More </a> 
					</div>
        </li>

        <li data-type="seo" data-id="4" class="span4 view view-tenth"> 
					<img src="img/portfolio-img/p-thumb/design/robbreport/robberport.jpg"  alt="">
          <div class="mask">
            <h2> ROBB REPORT</h2>
            <p> Robb Report is magazine with awesome website showing latest trends of luxury lifestyle of America. </p>
            <a href="<?php echo site_url('portfolio/robb-report');?>" class="btn-light"> More </a> 
					</div>
        </li>

        <li data-type="dev" data-id="5" class="span4 view view-tenth"> 
					<img src="img/portfolio-img/p-thumb/design/shabbyyapple/shabbyyapple.jpg"  alt="">
          <div class="mask">
            <h2>SHABBY APPLE</h2>
            <p> This website is fully for women's dresses, fashion, and most important women empowerment.</p>
            <a href="<?php echo site_url('portfolio/shabby-apple');?>" class="btn-light"> More </a> 
					</div>
        </li>

        <li data-type="design " data-id="6" class="span4 view view-tenth"> 
					<img src="img/portfolio-img/p-thumb/design/stickerbanners/Screenshot_1.jpg"  alt="">
          <div class="mask">
            <h2>STICKERS BANNERS</h2>
            <p> This website is the best place for the banner, sticker, and magnet printing which based in UK. </p>
            <a href="<?php echo site_url('portfolio/stickers-banners');?>" class="btn-light"> More </a> 
					</div>
        </li>

        <li data-type="design" data-id="2" class="span4 view view-tenth"> 
					<img src="img/portfolio-img/p-thumb/design/cafepress/cafe.jpg"  alt="">
          <div class="mask">
            <h2> SPORT BIKE TRACK GEAR</h2>
            <p> Amazing website for bike lover. From bike parts to whole bike is available here. </p>
            <a href="<?php echo site_url('portfolio/sport-bike-track-gear');?>" class="btn-light "> More </a> 
					</div>
        </li>

        <li data-type="seo" data-id="3" class="span4 view view-tenth"> 
					<img src="img/portfolio-img/p-thumb/design/sportbiker/sport.jpg"  alt="">
          <div class="mask">
            <h2>CAFE PRESS</h2>
            <p> Shopping website for gift and surprises for all relations and customized your gift on your wish. </p>
            <a href="<?php echo site_url('portfolio/cafe-press');?>" class="btn-light "> More </a> 
					</div>
        </li>
        <li data-type="seo" data-id="4" class="span4 view view-tenth"> 
					<img src="img/portfolio-img/p-thumb/design/salary/salary.jpg"  alt="">
          <div class="mask">
            <h2> SALARY.COM</h2>
            <p> One website provides solution for the job seekers as well as for employees and employers as well. </p>
            <a href="<?php echo site_url('portfolio/salary');?>" class="btn-light"> More </a> 
					</div>
        </li>

				<!-- new code by sourabh start...-->

				<li data-type="seo" data-id="4" class="span4 view view-tenth"> 
					<img src="img/portfolio-img/p-thumb/design/enrouteaircanda/enroute.jpg"  alt="">
          <div class="mask">
            <h2> AIR CANADA ENROUTE</h2>
            <p> Air Canada is a magazine and app as a guide for Canada and also ranking the restaurant across Canada. </p>
            <a href="<?php echo site_url('portfolio/air-canada-enroute');?>" class="btn-light"> More </a> 
					</div>
        </li>

				<li data-type="seo" data-id="4" class="span4 view view-tenth"> 
					<img src="img/portfolio-img/p-thumb/design/fluencycoach/fluency.jpg"  alt="">
          <div class="mask">
            <h2> FLUENCYCOACH</h2>
            <p> One app help people in fluency in speech and improve the confidence in talking. </p>
            <a href="<?php echo site_url('portfolio/fluencycoach');?>" class="btn-light"> More </a> 
					</div>
        </li>

				<li data-type="seo" data-id="4" class="span4 view view-tenth"> 
					<img src="img/portfolio-img/p-thumb/design/starkey/star.jpg"  alt="">
          <div class="mask">
            <h2> HEAR COACH</h2>
            <p> This app is made for the person having hearing problem as well as give test to person having doubt on their hearing capability. </p>
            <a href="<?php echo site_url('portfolio/hear-coach');?>" class="btn-light"> More </a> 
					</div>
        </li>

				<li data-type="seo" data-id="4" class="span4 view view-tenth"> 
					<img src="img/portfolio-img/p-thumb/design/foodspotting/foodspotting.jpg"  alt="">
          <div class="mask">
            <h2> FOODSPOTTING</h2>
            <p> Foodspotting app is for the food lover, wants to share the recipe and passionate about new dishes.  </p>
            <a href="<?php echo site_url('portfolio/foodspotting');?>" class="btn-light"> More </a> 
					</div>
        </li>

				<li data-type="seo" data-id="4" class="span4 view view-tenth"> 
					<img src="img/portfolio-img/p-thumb/design/remind/remind.jpg"  alt="">
          <div class="mask">
            <h2> REMIND101</h2>
            <p> Best app for teachers, students and parents to directly communicate to each other with the privacy of information. </p>
            <a href="<?php echo site_url('portfolio/remind');?>" class="btn-light"> More </a> 
					</div>
        </li>
		
		<!-- new code by sourabh end...-->
      </ul>
    </div>
    <!---portfolio style end here..----------> 
  </div>
</section>

