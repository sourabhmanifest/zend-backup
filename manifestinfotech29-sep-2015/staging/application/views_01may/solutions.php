<script>
	$('#solutions a').css('background-color','#056ab2');
	$('#solutions a').css('color','#FFF');
</script>

<section class="page-cover solutions-cover-img">
  <div class="container-fluid">
    <div class="container">
      <div class="page-heading">
        <h3>SOLUTIONS</h3>
        <h4>Our solutions meets your demand</h4>
        <p> We are having solution range that convert our visitors into clients. Solutions that make answer to every questions of our clients </p>
      </div>
    </div>
  </div>
</section>

<!-----top navigations section closed here...-------->
 <!---top navigations section closed here...-------->
    <section class="page-section theme-bg-gray">
      <div class="container-fluid">
        <div class="container">
          <div class="spacer-mini2"></div>
          <div class="page-heading"> </div>
          <div class="spacer-mini"></div>
          <div id="services-block" class="container">
            <div class="row-fluid">
              <div class="span4 hvr-grow-shadow"> <i class=""><img src="assets/images/solutions/1426623159_pulse-box-32.png"></i>
                <h4>Health care</h4>
                <p> we provide a health care solution which reduces the cost and complexity and become user friendly software for our client.</p>
              </div>
              <div class="span4 hvr-grow-shadow"> <i class=""><img src="assets/images/solutions/1426607116_home_house_real_estate-32.png"/></i>
                <h4>Real estate</h4>
                <p> we offer excellent solution for real estate sector. Utilizing advancements which are significant to customer business with current business pattern.</p>
              </div>
              <div class="span4 hvr-grow-shadow"> <i class=""><img src="assets/images/solutions/1426607139_sports_tennis_ball_tennisball-32.png"/></i>
                <h4>Sports</h4>
                <p> we produce sports solution which give boost to the business and give amazing and realistic feel to the client. </p>
              </div>
            </div>
          </div>
        </div>
      </div>
       <!--------------------------------start Second---------------------------> 
    </section>
    <section class="page-section">
      <div class="container-fluid">
        <div class="container">
          <div class="spacer-mini2"></div>
          <div class="page-heading"> </div>
          <div class="spacer-mini"></div>
          <div id="services-block" class="container">
            <div class="row-fluid">
              <div class="span4 hvr-grow-shadow"> <i><img src="assets/images/solutions/forbidden_document-32.png"/></i>
                <h4>Classified</h4>
                <p> we offer a classified solution which give more information from the customer, find about their products, services or other solutions.</p>
              </div>
              <div class="span4 hvr-grow-shadow"> <i class=""><img src="assets/images/solutions/1426622946_aiga_water_transportation-32.png"/></i>
                <h4>Shipping</h4>
                <p> we committed our client to provide most reliable and flexible shipping solution. We enjoy providing client requirement that can meet the complex business demand.
              </div>
              <div class="span4 hvr-grow-shadow"> <i class=""><img src="assets/images/solutions/1426607458_Facebook-32.png"/></i>
                <h4>Social Networking</h4>
                <p> we create the social networking solutions which are most popular and helpful for the business growth and expansion </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--------------------------------Second closed---------------------------> 
      <!--------------------------------Start thierd---------------------------> 
      </section>
    <section class="page-section theme-bg-gray">
      <div class="container-fluid">
        <div class="container">
          <div class="spacer-mini2"></div>
          <div class="page-heading"> </div>
          <div class="spacer-mini"></div>
          <div id="services-block" class="container">
            <div class="row-fluid">
              <div class="span4 hvr-grow-shadow"> <i class=""><img src="assets/images/solutions/elearning-32.png"/></i>
                <h4>Startup</h4>
                <p> we offer helping hand for the people who want to take a first step for their business idea. If any people not clear about their idea then our team is ready to create platform for your business</p>
              </div>
              <div class="span4 hvr-grow-shadow"> <i class=""><img src="assets/images/solutions/vacation_25-512.fw.png"/></i>
                <h4>Travel</h4>
                <p> with the travel solution we identified the problem and come up with better solution. And also providing better customized option for the client, user, and agents.
              </div>
              <div class="span4 hvr-grow-shadow"> <i class=""><img src="assets/images/solutions/1426607569_108-32.png"/></i>
                <h4>Restaurant</h4>
                <p> our restaurant solution met the scale of each type and size of restaurant. This Solution reduces cost and improves business performance without sacrificing quality or customer experience.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--third Closed--> 
      <!--Fourth Start--> 
    </section>
    <section class="page-section">
      <div class="container-fluid">
        <div class="container">
          <div class="spacer-mini2"></div>
          <div class="page-heading"> </div>
          <div class="spacer-mini"></div>
          <div id="services-block" class="container">
            <div class="row-fluid">
              <div class="span4 hvr-grow-shadow"> <i class=""><img src="assets/images/solutions/1426607371_Streamline-81-32.png"/></i>
                <h4>E-commerce</h4>
                <p> we are expert in building and design. E-commerce solution provides all type of internet business and websites provides space and scope to make product online.</p>
              </div>
              <div class="span4 hvr-grow-shadow"> <i class=""><img src="assets/images/solutions/1426623437_internt_web_technology-05-32.png"/></i>
                <h4>E-learning</h4>
                <p> we provide E-learning solution for the business. Now a day E-learning is most popular learning way to gain knowledge and profit. So we offer the platform to earn the money through providing knowledge</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--Fourth Closed-->