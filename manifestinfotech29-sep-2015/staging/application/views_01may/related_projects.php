<section class="page-section">
  <div class="container-fluid">
    <div class="container">
      <div class="page-heading">
        <h3>Other Projects </h3>
        <h4>We are Glaring and Prominent in our work</h4>
      </div>
      
      <!--page heading-->
      <div id="related-p"  class="well">
        <div id="myCarousel" class="carousel slide hidden-phone">
          <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
          </ol>
          
          <!-- Carousel items -->
          <div class="carousel-inner">
            <div class="item active">
              <div class="row-fluid">
                <div class="testimonial-thumb-group">
                  <div class="span3 border"> <a href="JavaScript:Void(0)" class="thumbnail shadow">
                    <div class="rp-dec"> Web Design</div>
                    <img src="img/portfolio-img/p-thumb/design/webdesign.jpg" border="0"/></a> </div>
                  <div class="span3 border"> <a href="JavaScript:Void(0)" class="thumbnail shadow">
                    <div class="rp-dec"> Web Development</div>
                    <img src="img/portfolio-img/p-thumb/development/development.jpg" border="0"/></a> </div>
                  <div class="span3 border"><a href="JavaScript:Void(0)" class="thumbnail shadow">
                    <div class="rp-dec"> SEO(Search Engine Optimizations)</div>
                    <img src="img/portfolio-img/p-thumb/seo/seo.jpg" border="0"/> </a> </div>
                  <div class="span3 border"><a href="JavaScript:Void(0)" class="thumbnail shadow">
                    <div class="rp-dec"> Web Design</div>
                    <img src="img/portfolio-img/p-thumb/design/webdesign.jpg" border="0"/></a> </div>
                </div>
                <div class="spacer-mini2"></div>
              </div>
              <!--/row-fluid--> 
            </div>
            <!--/item-->
            
            <div class="item">
              <div class="row-fluid">
                <div class="testimonial-thumb-group">
                  <div class="span3 border"> <a href="JavaScript:Void(0)" class="thumbnail shadow">
                    <div class="rp-dec"> Web Design</div>
                    <img src="img/portfolio-img/p-thumb/design/webdesign.jpg" border="0"/></a> </div>
                  <div class="span3 border"> <a href="JavaScript:Void(0)" class="thumbnail shadow">
                    <div class="rp-dec"> Web Development</div>
                    <img src="img/portfolio-img/p-thumb/development/development.jpg" border="0"/></a> </div>
                  <div class="span3 border"><a href="JavaScript:Void(0)" class="thumbnail shadow">
                    <div class="rp-dec"> SEO(Search Engine Optimizations)</div>
                    <img src="img/portfolio-img/p-thumb/seo/seo.jpg" border="0"/> </a> </div>
                  <div class="span3 border"><a href="JavaScript:Void(0)" class="thumbnail shadow">
                    <div class="rp-dec"> Web Design</div>
                    <img src="img/portfolio-img/p-thumb/design/webdesign.jpg" border="0"/></a> </div>
                </div>
                <div class="spacer-mini2"></div>
              </div>
            </div>
            <!--/item-->
            <div class="item">
              <div class="row-fluid">
                <div class="testimonial-thumb-group">
                  <div class="span3 border"> <a href="JavaScript:Void(0)" class="thumbnail shadow">
                    <div class="rp-dec"> Web Design</div>
                    <img src="img/portfolio-img/p-thumb/design/webdesign.jpg" border="0"/></a> </div>
                  <div class="span3 border"> <a href="JavaScript:Void(0)" class="thumbnail shadow">
                    <div class="rp-dec"> Web Development</div>
                    <img src="img/portfolio-img/p-thumb/development/development.jpg" border="0"/></a> </div>
                  <div class="span3 border"><a href="JavaScript:Void(0)" class="thumbnail shadow">
                    <div class="rp-dec"> SEO(Search Engine Optimizations)</div>
                    <img src="img/portfolio-img/p-thumb/seo/seo.jpg" border="0"/> </a> </div>
                  <div class="span3 border"><a href="JavaScript:Void(0)" class="thumbnail shadow">
                    <div class="rp-dec"> Web Design</div>
                    <img src="img/portfolio-img/p-thumb/design/webdesign.jpg" border="0"/></a> </div>
                </div>
                <div class="spacer-mini2"></div>
              </div>
            </div>
          </div>
          <!--/carousel-inner--> 
          <a class="left carousel-control" data-slide="prev" href="#myCarousel"><i class="fa fa-chevron-left"></i></a> <a class="right carousel-control" data-slide="next" href="#myCarousel"><i class="fa fa-chevron-right"></i></a> </div>
        <!--/myCarousel-->
        
        <div id="myCarouselPhone" class="carousel slide visible-phone"> 
          <!-- Carousel items -->
          <div class="carousel-inner">
            <div class="item active">
              <div class="span3 border"> <a href="JavaScript:Void(0)" class="thumbnail shadow">
                <div class="rp-dec"> Web Design</div>
                <img src="img/portfolio-img/p-thumb/design/webdesign.jpg" border="0"/></a> </div>
            </div>
            <div class="item">
              <div class="span3 border"> <a href="JavaScript:Void(0)" class="thumbnail shadow">
                <div class="rp-dec"> Web Development</div>
                <img src="img/portfolio-img/p-thumb/development/development.jpg" border="0"/></a> </div>
            </div>
            <div class="item">
              <div class="span3 border"><a href="JavaScript:Void(0)" class="thumbnail shadow">
                <div class="rp-dec"> SEO(Search Engine Optimizations)</div>
                <img src="img/portfolio-img/p-thumb/seo/seo.jpg" border="0"/> </a> </div>
            </div>
            <div class="item">
              <div class="span3 border"><a href="JavaScript:Void(0)" class="thumbnail shadow">
                <div class="rp-dec"> Web Design</div>
                <img src="img/portfolio-img/p-thumb/design/webdesign.jpg" border="0"/></a> </div>
            </div>
          </div>
          <!--/carousel-inner--> <a class="left carousel-control" href="#myCarouselPhone" data-slide="prev">�</a> <a class="right carousel-control" href="#myCarouselPhone" data-slide="next">�</a> </div>
        <!--/myCarouselPhone--> 
      </div>
      <!--/well--> 
    </div>
  </div>
</section>