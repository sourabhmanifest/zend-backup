<script>
	$('#career a').css('background-color','#056ab2');
	$('#career a').css('color','#FFF');
</script>

<section class="page-cover career-cover-img">
  <div class="container-fluid">
    <div class="container">
      <div class="page-heading">
        <h3>CAREER</h3>
        <h4>We open the door of "HOPE"</h4>
        <p> Our career offerings make the way for the job seekers. Our career plan is to give opportunity for many people to expand our human resource base </p>
      </div>
    </div>
  </div>
</section>
<!--/page cover & career section...
----------------------------------------------------------------------------------->

<section id="career" class="page-section theme-bg-gray">
<div class="container-fluid">
	<div class="container">
		<div class="span7 pull-left margin-null txt-justify">
			<h1 class="heading-a border-theme-l"> Career at our Company</h1>
			<img src="assets/images/career/bdevelopments.jpg" class="shadow-block" title="Career at our Company" />
			<div class="spacer-mini2"></div>
			<p class="lead-mini"> <strong>Job Summary:</strong><br />
				<strong>Functional area: </strong> IT/ Web programming<br/>
				<strong>Industry:</strong> Computer/ IT<br />
				<strong>Role Category:</strong> Business Development Officer<br />
				<strong>Vacancy:</strong> 2</br>
				<strong>Job Location:</strong> 105, Prakash Tower, Rani Sati Gate, YN Road Indore 
			</p>
			<p> <strong>Job details:</strong><br />
				Candidate has experience of more than 1yr for the post of  Business Development officer
			</p>
			<p> >   Knowledge of IT products and services<br />
				>  Excellent communication skills, good knowledge of MS office and its tools </p>
			<p > <strong>Skills Needed:</strong><br />Excellent communication skills </p>
		</div>
		<!---/left panel-->
		 <?php //include "career-news.php"; ?>
		 <?php include "portfolio/recent-post.php"; ?>
	</div>
</div>
</section>

<?php include "current-opportunity.php"; ?>