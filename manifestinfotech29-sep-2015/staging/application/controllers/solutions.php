<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Solutions extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('commonmodel','mimodel'));
	}

	public function index()
	{	
		$data['all_project_data'] = $this->commonmodel->getRecords('portfolio_project');
		$data['title']= 'Manifest Infotech| Enterprise Solutions ';
		$data['description']= "We are IT Solution Company taking projects in Healthcare, Real estate, Sports, Classified, Social, Networking, Shipping, Travel, Restaurant, etc";
		$data['keywords']= "";
		$this->load->view('includes/header',$data);
		$this->load->view('solutions');
		$this->load->view('portfolio/related_projects');
		$this->load->view('includes/footer');
	}
	
	
}


