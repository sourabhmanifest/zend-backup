<!doctype html>
<style>
dd, li {
    margin-bottom: 6px;
    list-style: disc;
}
</style>
<html lang="en">
<meta http-equiv="Content-type" content="text/html;charset=utf-8"> 
<div class="wrap nosubsub">
    <h2><?php
echo "Policies";
?>
   </h2>
</div>
<br/>
<br/>

<!DOCTYPE html>
<html>
<body>

<div id="col-container" >
    <div class="col-wrap">
        <div class="form-wrap">
<h2><strong>1. Code of Business Conduct and Ethics</strong></h2>
<p>This Code of Business Conduct and Ethics applies to all employees and officers of the subsidiaries and affiliates of Manifest Infotech Pvt Ltd Company, which are referred to in this Code as Company or the Company.</p>
<p>The Company is proud of its reputation for integrity and honesty and is committed to these core values. Personal responsibility is at the core of the Company’s principles and culture. The Company’s reputation depends on you maintaining the highest standards of conduct in all business endeavors. You have a personal responsibility to protect this reputation, to “do the right thing,” and to act with honesty and integrity in all dealings with customers, business partners and each other. You should not take unfair advantage of anyone through manipulation, concealment, abuse of privileged information, misrepresentation of material facts, or any other unfair-dealing practice.</p>
<p>The principles set forth in this document describe how you should conduct yourself. This Code does not address every expectation or condition regarding proper and ethical business conduct. Good common sense is your best guide. It does not substitute for Company policies and procedures. In every business-related endeavour, you must follow the ethics and compliance principles set forth in this Code as well as all other applicable corporate policies and procedures.</p>
<p>You are accountable for reading, understanding and adhering to this Code. Further, compliance with all laws, rules and regulations related to Company activities is mandatory and your conduct must be such as to avoid even the appearance of impropriety. Failure to do so could result in disciplinary action, up to and including termination of employment.
If you are uncertain about what to do, refer to the relevant section of this Code. If you are still unsure, speak with your supervisor or, if you prefer, communicate with any of the other points of contact indicated. If you have any doubt, ask for help.</p>
<br/><br/>
<h3><strong>1.1 In the Workplace</strong></h3>
<p>Company is committed to providing a diverse and inclusive work environment, free of all forms of unlawful discrimination, including any type of harassment.</p>
<h4><strong>1.1.1   Respect</strong></h4>
<p>The Company’s greatest strength lies in the talent and ability of its associates. Since working in partnership is vital to the Company’s continued success, mutual respect must be the basis for all work relationships. Engaging in behaviour that ridicules, belittles, intimidates, threatens or demeans, affects productivity, can negatively impact the Company’s reputation. You are expected to treat others with the same respect and dignity that any reasonable person may wish to receive, creating a work environment that is inclusive, supportive and free of harassment and unlawful discrimination.</p>
<h4><strong>1.1.2    Equal Employment Opportunity</strong></h4>
<p>The talents and skills needed to conduct business successfully are not limited to any particular group of people. Company has a long-standing commitment to a meaningful policy of equal employment opportunity. The Company’s policy is to ensure equal employment and advancement opportunity for all qualified individuals without distinction or discrimination because of race, color, religion, gender, sexual orientation, age, national origin, disability, covered veteran status, marital status or any other unlawful basis. As part of this commitment, the Company will make reasonable accommodations for applicants and qualified employees.</p>
<h4><strong>1.1.3    Sexual Harassment and Other Discriminatory Harassment</strong></h4>
<p>Sexual harassment and other discriminatory harassment are illegal and violate Company policies. Actions or words of a sexual nature that harass or intimidate others are prohibited. Similarly, actions or words that harass or intimidate based on race, color, religion, gender, sexual orientation, age, national origin, disability, covered veteran status, marital status or any other unlawful basis are also prohibited. </p>
<br/><br/>
<h3><strong>1.2 Business Conduct Certification Program</strong></h3>
<p>The responsibility for maintaining the Company’s reputation for integrity and compliance rests in large measure on associates who guide its operations and others in particularly sensitive positions. The Business Conduct Certification Program is designed to have you affirm your compliance with the standards contained in this Code and to help identify situations that may in fact, or in appearance, involve conflicts of interest or other improper conduct. If you are required to complete or update a Business Conduct Certificate, you must do so in a timely and forthright manner with accurate responses. Above all, you must remember that any act that gives the appearance of being improper can damage Company’s reputation and impair the public’s confidence in the Company. All such acts must be avoided.</p>
<p>You must acknowledge that you have read and understand this Employee Code of Business Conduct and Ethics. In addition, management-level associates must periodically disclose on Business Conduct Certificate information that is considered to be directly relevant to avoiding problems with compliance obligations, self-dealing and impropriety. In certain circumstances, disclosure is required even if appropriate approval is obtained. An investigation may be conducted to resolve potential problems. All associates are required to cooperate in reaching a resolution of any issues found.</p>

<br/><br/>
<h3>1.3 Conflicts of Interest</h3>
<p>Company policy prohibits conflicts of interest. A “conflict of interest” occurs when your private interest interferes in any way with the interests of Company. In addition to avoiding conflicts of interest, you should also avoid even the appearance of a conflict. </p>
<h4><strong>1.3.1 Corporate Opportunities</strong></h4>
<p>You owe a duty to Company to advance its legitimate interests. You are prohibited from competing with the Company and from using corporate property, information or position for personal opportunities or gain.  </p>
<h4><strong>1.3.2 Outside Activities - Officer or Director of another business</strong></h4>
<p>You may not serve as a director, officer, trustee, and partner or in any other principal position of another for-profit or publicly held organization or company without the prior approval of Company’s Chief Executive Officer (or a designee). You should obtain approval from Company’s Chief Executive Officer (or a designee) before agreeing to serve on the board or in a principal position of a trade or professional association or of a non-profit organization. In any event, these outside activities must not impact in any way your daily job responsibilities in your current position. </p>
<h4><strong>1.3.3   Second Job</strong></h4>
<p>Unless the Company otherwise consents in its sole discretion, you will devote your entire resources and full and undivided attention exclusively to the business of the Company during the term of your employment with the Company and shall not accept any other employment or engagement (honorary or otherwise).</p>
<h4><strong>1.3.4   Vendors, Suppliers and Consultants</strong></h4>
<p>All vendors, suppliers and consultants shall be approved in accordance with Company policies and procedures. Company’s business relationships must be totally based on their ability to competitively meet the Company’s business needs. If your association with a current or prospective Company vendor, supplier or consultant is of a nature that gives rise, or potentially gives rise, to a conflict of interest, the Company may have to refrain from entering into the relationship and, in any event, you must not be involved in any way with approving, managing or influencing the Company’s business relationship.</p>
<h4><strong>1.3.5  Communication of Conflicts</strong></h4>
<p>All potential and actual conflicts of interest or material transactions or relationships that reasonably could be expected to give rise to such a conflict or the appearance of such a conflict must be disclosed. If you have any doubt about whether a conflict of interest exists after consulting this Code, you should seek assistance from the appropriate persons or entities identified in the Resources section, so that you can make that determination. </p>
<p>Company and its associates will not directly or indirectly engage in bribery, kickbacks, payoffs or other corrupt business practices, in their relations with governmental agencies or customers.</p>

<br/><br/>
<h3>1.4 Protection and Proper Use of Company Assets</h3>
<p>Safeguarding and appropriately using Company assets, whether those assets take the form of paper files, electronic data, computer resources, trademarks or otherwise, is critical.</p>
<h4><strong>1.4.1 Technology</strong></h4>
<p>Safeguarding computer resources is critical because the Company relies on technology to conduct daily business. Software is provided to enable you to perform your job and is covered by federal copyright laws. You cannot duplicate, distribute or lend software to anyone unless permitted by the license agreement.</p>
<p>In addition, Company meetings are confidential. You may not use audio or video equipment to record these meetings without the specific prior authorization of the head of your department.</p>
<h4><strong>1.4.2 Confidentiality</strong></h4>
<p>customer or employee-related, must be treated in a confidential manner, and disclosing it is limited to those people who have an appropriate business or legal reason to have access to the information. You need to take special precautions when transmitting information via e-mail, fax, the Internet or other media. Remember to treat all such communications as if they were public documents and printed on letterhead.</p>
<p>Company provides electronic mail (e-mail) and Internet access to assist and facilitate business communications. All information stored, transmitted, received, or contained in these systems is the Company’s sole property and is subject to its review at any time. All e-mail and Internet use must be consistent with Company’s policies, practices and commitment to ensuring a work environment where all persons are treated with respect and dignity. Because these systems provide access to a worldwide audience, you should act at all times as if you are representing Company to the public, and should preserve Company’s system security and protect its name and trademarks.</p>
<p>You must act responsibly and adhere to all laws and Company policies when using e-mail or the Internet.</p>
<p>You must use your computer appropriately in accordance with Company standards and be sure to secure both the computer and all data from loss, damage or unauthorized access, reporting all instances of unauthorized access to the Information Technology Department.</p>

<br/><br/>
<h3>1.5 Administration</h3>
<h4><strong>1.5.1 Reporting of Any Illegal or Unethical Behavior; Points of Contact</strong></h4>
<p>If you are aware of any illegal or unethical behavior or if you believe that an applicable law, rule or regulation or this Code has been violated, the matter must be promptly reported to your supervisor or company executives.  </p>
<p>Your supervisor is normally the first person you should contact if you have questions about anything in this Code or if you believe Company or an associate is violating the law or Company policy or engaging in conduct that appears unethical. Under some circumstances, it may be impractical or you may feel uncomfortable raising a matter with your supervisor. In those instances, you may contact the head of your department or any other company executives. Furthermore, you should take care to report violations to a person who you believe is not involved in the alleged violation. All reports of alleged violations will be promptly investigated and, if appropriate, remedied, and if legally required, immediately reported to the proper governmental authority.</p>
<p>You will be expected to cooperate in assuring that violations of this Code are promptly addressed. Company has a policy of protecting the confidentiality of those making reports of possible misconduct to the maximum extent permitted by law. <strong>In no event will there be any retaliation against someone for reporting an activity that he or she in good faith believes to be a violation of any law, rule, regulation, internal policy or this Code.</strong> Any supervisor intimidating or imposing sanctions on someone for reporting a matter will be disciplined up to and including termination.</p>
<br/>
<h2><strong>2.Terms of Employment</strong></h2>

<h3>2.1 Terms of employment</h3>
<p>Terms of employment are as set out in the appointment letter. </p>

<h4><strong>2.1.1 The terms of employment</strong></h4>
<p>The terms of employment are as per the details contained in the appointment letter.  The company reserves the right to amend, alter, change any or all the terms and conditions governing employment.  The company will also be the sole judge of the meaning and interpretation of all or any of these terms and conditions and its decision thereon shall be binding on all employees.</p>
<h4><strong>2.1.2 The employment contract</strong></h4>
<p>The employment contract is a contract between the individual employee and the company and the terms of contract are individual to each employee. Hence, all employees are required not to share the terms of contract with others including fellow employees</p>
<br/>
<h3>2.2 Joining process </h3>
<p> The copies of the following documents shall be submitted by an employee on the date of joining:</p>
<ul>
	<li>
		1.) Proof of age (birth certificate/school leaving certificate/passport copy);
	</li>
	<li>
		2.) Duly Attested Educational and other qualification certificates;
	</li>
	<li>
		3.) Release letter from the previous employer (if applicable);
	</li>
	<li>
		4.) Acknowledgement for receipt of the HR Policies and Code of Conduct guidelines;
	</li>
</ul>
<br/>
<h2><strong>3. General Administrative Matters</strong></h2>

<h3>3.1 Working days </h3>
<p>The working days at the Company will be from Monday through Friday.<br/>
Unless otherwise stated, work hours would be as follows: </p>
<table border="1px">
  <tr>
    <th>Days</th>
    <th>Timings</th>
  </tr>
  <tr>
    <td width="200" align="center">Monday – Friday</td>
    <td width="200" align="center">0900 hrs To 1800 hrs</td>
  </tr>
  <tr>
    <td align="center">Saturday</td>
    <td align="center">0900 hrs To 1400 hrs.</td>
  </tr>
</table>
<p>Owing to work exigencies, an employee’s working hours maybe different from the timings mentioned above.</p>
<h3>3.2 Weekly off </h3>
<p>Saturday(half day) and Sunday will be the holidays. Employee who completed 3 years or more work experience in MI will get benefit of Saturday full day holiday with 8 hours working time in week days. Fresher who are in probation period will work full day also on Saturday.</p>
<h3>3.3 Late arrival</h3>
<p>Employees are expected to arrive at work and for meetings on time.  If an employee anticipates late arrival he/she must inform the immediate manager (or a colleague in case the immediate manager is not available) in advance to allow for schedule changes and to handle coverage of working hours. Repeat challenges with late arrivals will be recorded as misconduct in the employee’s file. All employees working with customers must ensure that all meeting commitments are met on time. Lapses in punctuality will not be acceptable. </p>
<h3>3.4 Absence from office</h3>
<ul>
	<li>
		1.)  Any employee, who is outside the office during working hours, should ensure that the immediate manager (or a colleague, if the immediate manager is not available) is aware of his/her whereabouts.
	</li>  
	<li>
		2.) Unauthorized absence from office, or absence from office without prior approval from the immediate manager, will be recorded as misconduct in the employee’s file. 
	</li>
	<li>
		3.) Unauthorized absence will be treated as Loss of Pay (LOP).</p>
	</li>	
</ul>
<h3>3.5 Housekeeping </h3>
<p>It will be the responsibility of all employees to ensure that the offices of the company are kept neat and tidy at all times.  The work area should be cleared of all files and papers every evening prior to leaving the office. Computers and any lights in the work area need to be switched off.</p>
<h3>3.6 Smoking</h3>
<p>Smoking is prohibited within office premises. In order to maintain a clean and healthy atmosphere in the workplace and arising out of our concern for fellow employees, smoking is prohibited within the office premises.</p>
<br/>
<h2><strong>4. Employee Development</strong></h2>
<h3>4.1 General</h3>
<ol>
	<li>It is the policy of the Company that the work of each employee will be evaluated periodically by the employee’s manager/supervisor, in order to monitor individual performance on the job, assess training needs and to identify future leaders.</li>

	<li>The process of employee development is covered by:</li>
	
<ol>
	<li>Performance Evaluation</li>
	<li>Training and Development and</li>
	<li>Career Planning</li>
</ol>
</ol>
<h3>4.2 Performance Evaluation</h3>
<ol>
	<li>The process of performance evaluation provides a systematic approach for communicating goals, expectations and objectives to each employee as well as documenting individual performance.</li>
</ol>
	<li>The process of performance evaluation is covered in three steps:</li>
	<ol>
	<li>Goal setting</li>
	<ol>
		<li>The supervisor and employee discuss and set performance expectations for the assessment period and sign off individual performance contracts.</li>
	</ol>
	<li>Performance review:</li>
	<ol>
		<li>Performance review is conducted periodically (semi annually) to assess individual performance and to take necessary action to remove bottlenecks and to provide suggestions for improvement. The outcome of this review would result in the identification of training needs, rewards and recognition and career development.</li>
	</ol>
	<li> Performance appraisal:</li>
		<ol>
			<li>
			Performance Appraisal is done based on careful consideration of employee performance for the assessment period. 
			</li>
		</ol>
	</ol>
</ol>	
<br/>
<h2><strong>5. Compensation</strong></h2>
<h3>5.1 Salary Administration</h3>
<p>All employees will be paid their salary on a monthly basis on the first working week of the subsequent month through an account payee cheque or internet banking.</p>
<h3>5.2 Salary Increases</h3>
<p>Compensation review is an annual exercise, which determines the increment in salary.  The increment is done on the cost of living adjustments and market trends in compensation levels.  However, increment in the employee's salary is not automatic and will be subject to the employee's performance and the company's performance.</p>
<h2><strong>6. Employee Termination</strong></h2>
<ol>
An employee will be separated from the company in the following events:
<ol><li>On his/her resignation from the services of the company</li>
<li>On resignation of employee's he/she must survive notice period of 1month for less then 2years experience in MI or 1.5 month for more then 2 year experience employee's.</li>
<li>On being removed from the services or on being dismissed by the company</li>
<li>On the expiry of any fixed contract period</li>
<li>On being found medically unfit to continue working in his/her present responsibility</li>
</ol>
</ol>
<br/>
<h2><strong>7. Leave Policy</strong></h2>
<h3>7.1 General</h3>
<ol>
	<li>For the purpose of calculating leave accounts, “year” shall mean the calendar year commencing on the first day of January and ending on the last day of December of the next year.</li>
	<li>Leave, other than maternity leave, cannot be claimed as a matter of right.  Discretion is reserved with the authority empowered to sanction leave, to refuse or revoke leave at any time, depending on exigencies of the company’s work.</li>
	<li>All leave must be applied for at least 7 days prior for approval to immediate manager, with the exception of sick leave, which may be intimated verbally and post facto approval sought upon resumption of work.</li>
	<li>Leave records are being maintained on the common share. It will be the employee’s responsibility to enter their leave for the month and keep the record updated</li>
	<li>here is no provision at this time to carry forward any unused leave into the next year or encash it at the time of termination. </li>
</ol>

<h3>7.2 Earned Leave</h3>
<p>An employee will be entitled to earned leave up to 12 working days in a year (exclusive of intervening weekends or public holidays).</p>
<ol>
	<li>Employees desirous of availing earned leave in excess of 4 consecutive working days will need to submit a leave application to their immediate manager, at least two weeks in advance. </li>
	<li>Employees may take leave only after obtaining permission.  In the event an employee goes on leave without notifying the company, it will be deemed that the employee has been absent from work without permission, and the period of absence will be treated as leave without pay. </li>
	<li>Earned leave can be added on to sick leave or maternity leave. </li>
	<li>Earned leave entitlement will be on a pro rated basis for employees joining during the year.</li>
	<li>Earned leave will increase to 14 days after 3 years of employment in the company</li>
</ol>
<h3>7.3 Sick Leave 5 days</h3>
<ol>
	<li>All employees may avail of sick leave up to 5 days in a given year.</li>
	<li>Submission of medical certificates of sickness as well as fitness will be required in case of sick leave exceeding three days.</li>
	<li>An employee may take sick leave keeping the immediate supervisor informed. The day the employee reports back to work, leave records need to be updated</li>
</ol>
<h3>7.4 Public or Festive holidays: 7 days</h3>
<ol>
	<li>In case the working of the office is likely to be hampered on account of strike, power cut, etc, the company may declare, a public or a weekly holiday (except national holidays) to be a normal working day and declare the affected day to be a holiday.</li>
	
</ol>
<h3>7.5 Unauthorized absence</h3>
<ol>
	<li>Unauthorized absence refers to absence from work without requisite approval. </li>
	<li>The employee will need to offer an explanation to the immediate manager in the event of any unauthorized absence.</li>
	<li>The employee will not be eligible for payment of salary for this period of absence.</li>
</ol>
<h3>7.6 Paternity Leave</h3>
<ol>
	<li>Is applicable to all male employees who are married (the employee should be married as per company's records, with information having been provided at the time of joining or at the time of marriage)
is to be availed as soon as the child is born (within a week's time) is available for two children only
cannot be carried forward or added to other leave categories
an employee is entitled to 5 continuous working days of leave
this leave can be availed even when the child is adopted</li>
</ol>
<h3>7.7 Leave during Notice Period</h3>
<p>Employees are not eligible to take any leave when they are serving their notice period. Any leave taken during the notice period will be considered as leave on loss of pay.</p>
<br/>
<h2><strong>8. Internet Use Policy</strong></h2>
<h3>8.1 The Policy</h3>
<ol>
	<li>
	The Company recognizes that the Internet can be a helpful tool in dealing with family and other personal matters; however, its use must not interfere with work responsibilities, conflict with business needs, or violate any Company policy or law.  <strong>Company reserves the right at all times to monitor, access and decrypt associates' use of the Internet, Company property, equipment, phone lines, computers (including disks, drives, storage media, electronic mail, etc.) and information.</strong>
	</li>
	<li>
		All users are expected to use good judgment when using the Internet. Company strictly prohibits: 
	</li>	
	<ol>
		<li>
		In accordance with the Company's standards of business conduct, hacking or other attempts to penetrate non-public systems or any dishonest, defamatory, fraudulent, immoral, illegal and/or unethical activities; and 
		</li>
		<li>
		using Company's name or property or a Company-provided Internet access ID to conduct business on behalf of an entity other than Company or on behalf of any individual, including yourself; to represent yourself as someone else; or to solicit Company associates. 
		</li>
	</ol>
</ol>
<p>All users must respect Company's, its <b>affiliate's</b> and third parties' intellectual property rights (patents, copyrights, trademarks, trade secrets, as well as rights of privacy and publicity) and must take precautions to protect software, information and data that are owned, licensed or managed by Company.</p>
<p>No software, information or data may be used or distributed in a manner that infringes upon any intellectual property right or violates a license agreement or jeopardizes Company's trade secrets. </p>
<p>No one may conduct business by or on behalf of Company with third parties using personal access accounts or IDs. </p>
<p><b>Misuse of Company resources and conduct in violation of Company policy will result in disciplinary action in accordance with the Company policy, up to and including termination.</b></p>
</div>
</div>
</div>
</body>