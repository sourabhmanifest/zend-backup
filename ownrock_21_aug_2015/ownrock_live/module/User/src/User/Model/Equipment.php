<?php

namespace User\Model;

class Equipment {

    public $id;
    public $equipment;

    public function exchangeArray($data) {

        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->equipment = (!empty($data['equipment'])) ? $data['equipment'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}
