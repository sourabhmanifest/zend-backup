<?php

namespace User\View\Helper;

use Zend\View\Helper\AbstractHelper;
 use Zend\Form\Element;

class ViewImage extends AbstractHelper {

    protected $_baseurl = null;
    protected $_exists = array();

    /**
     * Constructor
     */
    public function __construct() {
        
    }

    /*     * Vie
     * Output the <img /> tag
     *
     * @param string $path
     * @param array $params
     * @return string
     */

    public function __invoke($path, $params = array()) {
      
        $element = new Element\Image('Merchant logo');
        $element->setAttribute('src', $path);
        $element->setAttribute('height', $params['height']);
        $element->setAttribute('width', $params['width']);
// <input type="image" name="my-image" src="/img/my-pic.png">
// Within your view...

        return $element;
        
}
}