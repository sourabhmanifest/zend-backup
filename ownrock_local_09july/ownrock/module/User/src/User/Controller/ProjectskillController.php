<?php

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use User\Form\UsereditForm;
use User\Form\LoginForm;
use User\Model\User;
use User\Model\UserTable;
use User\Model\Profileskill;
use User\Model\ProfileskillTable;
use User\Model\Projectskill;
use User\Model\ProjectskillTable;
use User\Model\Merchant;
use User\Model\MerchantTable;
use User\Model\Skills;
use User\Model\SkillsTable;
use User\Model\Project;
use User\Model\ProjectTable;

use User\Model\Categorylist;
use User\Model\CategorylistTable;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;
use User\Form\RegisterForm;
use Zend\Db\Sql\Select;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\Iterator as paginatorIterator;
use Zend\View\Model\JsonModel;

class ProjectskillController extends AbstractActionController {

    protected $authservice;

    public function getAuthService() {
        $this->authservice = $this->getServiceLocator()->get('AuthService');
        return $this->authservice;
    }
    
    public function ViewAction(){
        $this->layout('layout/common-layout');
        
        $project_id = $this->params()->fromRoute('project_id');
        
        $projectskillTable = $this->getServiceLocator()->get('projectskilltable');
        $result = $projectskillTable->selectProjectskill($project_id);
        
        $skillsTable = $this->getServiceLocator()->get('skillstable');
        $skills_result = $skillsTable->getAllSkills();

        $projectTable = $this->getServiceLocator()->get('projecttable');
        $result_project = $projectTable->getProjectById($project_id);
        
        $viewModel = new Viewmodel(array('result' => $result,'project_id'=>$project_id,'skills_result'=>$skills_result,'result_project'=>$result_project));

        return $viewModel;
    }
    
    public function deleteAction(){
        $this->layout('layout/common-layout');
        $projectskillTable = $this->getServiceLocator()->get('projectskilltable');
        $project_id = $this->params()->fromRoute('project_id');
        $projectskillTable->deleteProjectSkillAll($project_id);
        return $this->redirect()->toRoute('user/projectskill', array('action' => 'index'));
    }
    
    public function deletesAction(){
        $this->layout('layout/common-layout');
        $projectskillTable = $this->getServiceLocator()->get('projectskilltable');
        $project_id = $this->params()->fromRoute('project_id');
        $projectskillTable->deleteProjectSkillById($project_id);
        return $this->redirect()->toRoute('user/projectskill', array('action' => 'index'));
    }
   
    
    /*public function AddAction() {
        $this->layout('layout/common-layout');  
       
        $form = $this->getServiceLocator()->get('SkilleditForm');    
        $form->get('submit')->setValue('Add');     
        $request = $this->getRequest();
        if ($request->isPost()) {
            $skill = new Skill();
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $skill->exchangeArray($form->getData());
               $this->getServiceLocator()->get('SkillTable')->saveSkill($skill);
               return $this->redirect()->toRoute('user/projectmanager', array('action' => 'index'));
            }
        }
      return array('form' => $form);
    }
    */
}