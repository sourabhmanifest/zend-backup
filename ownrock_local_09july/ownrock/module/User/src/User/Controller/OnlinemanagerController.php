<?php

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use User\Form\UsereditForm;
use User\Form\LoginForm;
use User\Model\User;
use User\Model\UserTable;
use User\Model\Profileskill;
use User\Model\ProfileskillTable;
use User\Model\Merchant;
use User\Model\MerchantTable;
use User\Model\Project;
use User\Model\ProjectTable;

use User\Model\Categorylist;
use User\Model\CategorylistTable;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;
use User\Form\RegisterForm;
use Zend\Db\Sql\Select;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\Iterator as paginatorIterator;
use Zend\View\Model\JsonModel;
use User\Model\Uploads;
use User\Model\ImageUpload;

class OnlinemanagerController extends AbstractActionController {

    protected $authservice;

    public function getAuthService() {
        $this->authservice = $this->getServiceLocator()->get('AuthService');
        return $this->authservice;
    }
    
     public function IndexAction() {
        $this->layout('layout/common-layout');
        $merchantTable = $this->getServiceLocator()->get('merchanttable');        
        
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            $searchtxt = $this->params()->fromPost('search');
            $paginator = $merchantTable->fetchAll(true,$searchtxt);
        }else{
             $paginator = $merchantTable->fetchAll(true);
             $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
        }
        $paginator->setItemCountPerPage(5);
        
        $all_merchants = $merchantTable->fetchAll(false);
        
        foreach($all_merchants as $key=>$val){
            $ProjectTable = $this->getServiceLocator()->get('projecttable');
            $temp_total_projects = $ProjectTable->getProjectByMerchant($val->name);
            $array_cnt[$val->name] = count($temp_total_projects->toArray());
        }
        
        $viewModel = new Viewmodel(array('paginator' => $paginator,'array_cnt'=>$array_cnt));
        return $viewModel;
    }
    
     public function EditAction() {
        $this->layout('layout/common-layout');
        $MerchantTable = $this->getServiceLocator()->get('MerchantTable');
        $merchant = $MerchantTable->getMerchant(str_replace('-',' ',$this->params()->fromRoute('id')));
        $form = $this->getServiceLocator()->get('MerchanteditForm');
        $form->bind($merchant);
        $viewModel = new ViewModel(array('form' => $form, 'user_id' => $this->params()->fromRoute('id')));
        return $viewModel;
    }

    public function DeleteAction() {
         $this->layout('layout/common-layout');
        $this->getServiceLocator()->get('UserTable')->deleteUser($this->params()->fromRoute('id'));
        return $this->redirect()->toRoute('user/usermanager');
    }

    public function AddAction() {
        $this->layout('layout/common-layout');  
       
        $form = $this->getServiceLocator()->get('MerchanteditForm');    
        $form->get('submit')->setValue('Add');     
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $config = $this->getServiceLocator()->get('config');
            $uploadPath = $config['module_config']['upload_location'];

            $uploadFile = $this->params()->fromFiles('profilepic');
            $adapter = new \Zend\File\Transfer\Adapter\Http();
            $adapter->setDestination($uploadPath);
            $adapter->receive($uploadFile['name']);
           
            $rand = rand(2000,100000);
            rename($uploadPath.'/'.$uploadFile['name'],$uploadPath.'/'.$rand.'_'.$uploadFile['name']);
            
            $sourceImageFileName = $uploadPath . '/' . $rand.'_'.$uploadFile['name'];
            $thumbnailFileName = $rand.'_'.'tn150x150_'.$uploadFile['name'];
            $imageThumb = $this->getServiceLocator()->get('WebinoImageThumb');
            $thumb = $imageThumb->create($sourceImageFileName, $options = array());
            $thumb->resize(150, 150);
            $thumb->save($uploadPath . '/' . $thumbnailFileName);
            
            $merchant = new Merchant();
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $temp_data = $form->getData();
                
                foreach($temp_data as $key=>$val){
                    $exc_arr[$key] = $val; 
                }
                $exc_arr['profilepic'] = $rand . '_pp_' .$uploadFile['name'];
               //$merchant->exchangeArray($form->getData());
                $merchant->exchangeArray($exc_arr);
               $this->getServiceLocator()->get('MerchantTable')->saveMerchant($merchant);
               return $this->redirect()->toRoute('user/onlinemanager', array('action' => 'index'));
            }
        }
      return array('form' => $form);
    }
    
      public function AddskillAction() {
       $this->layout('layout/test-layout');  
     
        $form = $this->getServiceLocator()->get('ProfileskillForm');    
        $form->get('submit')->setValue('Add');     
        $request = $this->getRequest();
       
        if ($request->isPost()) {
            $profileskills = new Profileskill();
            $form->setData($request->getPost());

            if ($form->isValid()) {
             
                $profileskills->exchangeArray($form->getData());
               $this->getServiceLocator()->get('ProfileskillTable')->saveProfileskill($profileskills);
               die;
               return $this->redirect()->toRoute('user/onlinemanager', array('action' => 'index'));
            }
        }
      return array('form' => $form);
    }

    public function ProcessAction() {
       
        if (!$this->request->isPost()) {
            return $this->redirect()->toRoute('user/onlinemanager', array('action' => 'edit'));
        }

        $post = $this->request->getPost();
        $merchantTable = $this->getServiceLocator()->get('merchantTable');
        $merchant = $merchantTable->getMerchant($post->name);

        $form = $this->getServiceLocator()->get('MerchantEditForm');
        $form->bind($merchant);
        $form->setData($post);

        if (!$form->isValid()) {
            $model = new ViewModel(array(
                'error' => true,
                'form' => $form,
            ));
            $model->setTemplate('user/onlinemanager/edit');
            return $model;
        }

        $this->getServiceLocator()->get('MerchantTable')->saveMerchant($merchant);

        return $this->redirect()->toRoute('user/onlinemanager');
    }

    public function UpdateAction() {
       
        $MerchantTable = $this->getServiceLocator()->get('MerchantTable');
        $merchant_id = $this->params()->fromRoute('merchant_id');
        $merchant_details = $MerchantTable->getMerchantById($this->params()->fromRoute('merchant_id'));
        $merchant_status = $this->params()->fromRoute('merchant_status');
        
        $merchant = array('id'=>$merchant_id,'status'=>$merchant_status);
        $MerchantTable->updateMerchantstatus($merchant);
        return $this->redirect()->toRoute('user/onlinemanager');
    }
    
    public function Index3Action() {
        $this->layout('layout/online-layout');
        $merchantTable = $this->getServiceLocator()->get('merchanttable');
        $merchants = $merchantTable->fetchAll();
        $merchants2 = $merchantTable->selectMerchant("Shopping");
        $merchants3 = $merchantTable->selectMerchant("Food");
        $merchants4 = $merchantTable->selectMerchant("Recharge");
        $merchants5 = $merchantTable->selectMerchant("Travel");
        $user = $this->getAuthService()->getStorage()->read();
        $dbadapter = $this->getServiceLocator()->get('dbAuthService');
        $categorylistTable = $this->getServiceLocator()->get('categorylisttable');
        $category = $categorylistTable->selectuniquecategory($dbadapter);     
//foreach ($category as $cat) :
//{
//    echo ($cat->category);
//}
//endforeach;


        if($user==NULL)
        {
            $username = 'guest';
        }
        else
        {
            $username = $user->name;
        }
        $form = new LoginForm();
        $viewModel = new ViewModel(array('merchants' => $merchants,
            'merchants2' => $merchants2,
            'merchants3' => $merchants3,
            'merchants4' => $merchants4,
            'merchants5' => $merchants5,
            'username' => $username, 
            'form' => $form,
            'category'=> $category,
            'categorylistTable' => $categorylistTable,
        ));

        return $viewModel;
    }

    public function StoreAction() {
        $this->layout('layout/online-layout');
        $merchant = $this->getEvent()->getRouteMatch()->getParam('id');
        $merchantTable = $this->getServiceLocator()->get('merchanttable');
        $merchanttable = $merchantTable->selectMerchantname($merchant);

        $select = new Select();
        $order_by = $this->params()->fromRoute('order_by') ? $this->params()->fromRoute('order_by') : 'entrydate';
        $order = $this->params()->fromRoute('order') ? $this->params()->fromRoute('order') : Select::ORDER_ASCENDING;
        $page = $this->params()->fromRoute('page') ? (int) $this->params()->fromRoute('page') : 1;
        $onlinemanagertable = $this->getServiceLocator()->get('onlinecouponTable');
        $onlinemanager = $onlinemanagertable->fetchAll($select->order($order_by . ' ' . $order));
        $itemsPerPage = 10;
        $onlinemanager->current();
        $paginator = new Paginator(new paginatorIterator($onlinemanager));
        $paginator->setCurrentPageNumber($page)
                ->setItemCountPerPage($itemsPerPage)
                ->setPageRange(10);



        return new ViewModel(array('merchanttable' => $merchanttable,
            'onlinecoupons' => $onlinemanagertable->selectOnlinecoupon($merchant, $select),
            'order_by' => $order_by,
            'order' => $order,
            'page' => $page,
            'paginator' => $paginator,
        ));
    }
    
     public function CategoriesAction() {
         $this->layout('layout/online-layout');       
        $subcategory = $this->getEvent()->getRouteMatch()->getParam('id');      
        $onlinecouponTable = $this->getServiceLocator()->get('onlinecoupontable');
       
        $onlinecoupontable = $onlinecouponTable->selectsubcategoryname($subcategory);
//        foreach ($onlinecoupontable as $cat) :
//        {
//           echo ($cat->longdescription);
//}
//endforeach;
//die;
        $merchantTable = $this->getServiceLocator()->get('merchanttable');

        $select = new Select();
        $order_by = $this->params()->fromRoute('order_by') ? $this->params()->fromRoute('order_by') : 'entrydate';
        $order = $this->params()->fromRoute('order') ? $this->params()->fromRoute('order') : Select::ORDER_ASCENDING;
        $page = $this->params()->fromRoute('page') ? (int) $this->params()->fromRoute('page') : 1;
        $onlinemanagertable = $this->getServiceLocator()->get('onlinecouponTable');
        $onlinemanager = $onlinemanagertable->fetchAll($select->order($order_by . ' ' . $order));
        $itemsPerPage = 10;
        $onlinemanager->current();
        $paginator = new Paginator(new paginatorIterator($onlinemanager));
        $paginator->setCurrentPageNumber($page)
                ->setItemCountPerPage($itemsPerPage)
                ->setPageRange(10);



        return new ViewModel(array(
            //'onlinecoupons' => $onlinemanagertable->selectOnlinecoupon($merchant, $select),
            'order_by' => $order_by,
            'order' => $order,
            'page' => $page,
            'paginator' => $paginator,
            'onlinecoupontable' => $onlinecoupontable,
            'merchantTable' => $merchantTable,
        ));
    }

}
