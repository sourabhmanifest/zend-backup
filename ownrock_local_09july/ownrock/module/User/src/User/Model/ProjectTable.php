<?php

namespace User\Model;

use Zend\Text\Table\Row;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class ProjectTable {

    protected $tableGateway;
    private $mname;
    private $sby;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function saveProject(project $project) {
        $data = array(
            'project_id' => $project->project_id,
            'user_name' => $project->user_name,
            'about' => $project->about,
            'website_url' => $project->website_url,
            'industry' => $project->industry,
            'role' => $project->role,
            'time' => $project->time,
            'cost' => $project->cost,
            'endorsement' => $project->endorsement,
            'stars' => $project->stars,
            'quality' => $project->quality,
            'effective_cost' => $project->effective_cost,
            'effective_time' => $project->effective_time,
            'experience' => $project->experience,
            'communication' => $project->communication,
            'project_name' => $project->project_name,
            'project_shortcut' => $project->project_shortcut,
            'added_by' => $project->added_by,
        );
        
        
        $id = (int) $project->project_id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            return $this->tableGateway->getLastInsertValue();
        } else {
            if ($this->getProject($project->project_id)) {
                $this->tableGateway->update($data, array('project_id' => $id));
            } else {
                throw new \Exception('Project id does not exist');
            }
        }
    }

    public function updateProjectOrder($project_id,$i,$mid) {
        
            if ($this->getProject($project_id)) {
                $data = array('project_order'=>$i);
                $this->tableGateway->update($data, array('user_name' => $mid,'project_id'=>$project_id));
            } else {
                throw new \Exception('Project id does not exist');
            }
    }
    
    public function getProject($project_shortcut) {
        //$id = (int) $id;
        //$rowset = $this->tableGateway->select(array('project_shortcut' => $project_shortcut));
        $rowset = $this->tableGateway->select(array('project_id' => $project_shortcut));
        $row = $rowset->current();
        
        if (!$row) {
            throw new \Exception("Could not find row $project_shortcut");
        }
        return $row;
    }

    public function fetchAll($paginated=false,$searchtext="") {
   
        if ($paginated) {
             // create a new Select object for the table album
             $select = new Select('project');
             
             if($searchtext != ""){
                 $select->where->like('project_name', "%" . "$searchtext" . "%");
             }
             
             // create a new result set based on the Album entity
             $resultSetPrototype = new ResultSet();
             $resultSetPrototype->setArrayObjectPrototype(new Project());
             // create a new pagination adapter object
             $paginatorAdapter = new DbSelect(
                 // our configured select object
                 $select,
                 // the adapter to run it against
                 $this->tableGateway->getAdapter(),
                 // the result set to hydrate
                 $resultSetPrototype
             );
        
             $paginator = new Paginator($paginatorAdapter);
             return $paginator;
         }
        $resultSet = $this->tableGateway->select();
        
        /*$resultSet = $this->tableGateway->select(function(Select $select) {
                $select->join('projectskill', 'project.project_id = projectskill.project_id', array('skill'));
            });
         */
        
        $resultSet->buffer();
        return $resultSet;
    }

    public function deleteProject($project_id) {
        $this->tableGateway->delete(array('project_id' =>  $project_id));
    }

    public function selectProject($header) {      
        $rowset = $this->tableGateway->select(array('header' => $header));
        return $rowset;    
    }
    
    public function selectProjectname($name) {     
        $rowset = $this->tableGateway->select(array('name' => $name));
        return $rowset;    
    }

    public function searchProject($byName) {     
        $rowset = $this->tableGateway->select(array('project_name LIKE ' => $byName .'%'));
        return $rowset;    
    }
    
    /*public function getProjectByMerchant($merchant_name,$sort_by) {     
        $rowset = $this->tableGateway->select(array('user_name' => $merchant_name));
        $rowset->buffer();
        return $rowset;    
    }*/
    
    public function getProjectByMerchant($merchant_name,$sort_by=''){
            
        if($sort_by != ""){
        $this->mname = $merchant_name;
        $this->sby = $sort_by;
        
        $resultSet = $this->tableGateway->select(function(Select $select) {
                $select->where->equalTo('user_name', $this->mname);
                $select->order(array("$this->sby ASC"));
            });
        }else{
            $resultSet = $this->tableGateway->select(function(Select $select) {
                    $select->where->equalTo('user_name', $this->mname);
                });
        }
        
        
        /*
        $resultSet = $this->tableGateway->select(function(Select $select){
            $select->where(array('user_name'=>"$merchant_name"));
            $select->order('project_order ASC');
        });*/
        $resultSet->buffer();
        return $resultSet;
        /*
        $rowset = $this->tableGateway->select(array('user_name' => $merchant_name));
        $rowset->buffer();
        return $rowset;
            */
    }
    public function getTotalProjectByMerchant($merchant_name)
    {
        $this->mname = $merchant_name;
        $resultSet = $this->tableGateway->select(function(Select $select) {
                    $select->where->equalTo('user_name', $this->mname);
                });
        return $resultSet->count(); 

    }
    
    public function getProjectById($project_id) {
        
        $rowset = $this->tableGateway->select(array('project_id' => $project_id));
        $row = $rowset->current();
        
        if (!$row) {
            throw new \Exception("Could not find row $project_id");
        }
        return $row;
    }
    
}