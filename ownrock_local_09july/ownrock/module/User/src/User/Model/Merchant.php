<?php

namespace User\Model;

class Merchant{
    public $name;
    public $profession;
    public $country;
    public $city;
    public $rate;
    public $availability;
    public $smalldescription;
    public $longdescription;
    public $profilepic;
    public $coverpic;
    public $email;
    public $phone;
    public $id;
    public $status;
    
    public function exchangeArray($data)
     {
  
         $this->name    = (!empty($data['name'])) ? $data['name'] : null;   
         $this->profession = (!empty($data['profession'])) ? $data['profession'] : null;
         $this->country  = (!empty($data['country'])) ? $data['country'] : null;
         $this->city  = (!empty($data['city'])) ? $data['city'] : null;
         $this->rate   = (!empty($data['rate'])) ? $data['rate'] : null;   
         $this->availability  = (!empty($data['availability'])) ? $data['availability'] : null;
         $this->smalldescription  = (!empty($data['smalldescription'])) ? $data['smalldescription'] : null;
         $this->longdescription  = (!empty($data['longdescription'])) ? $data['longdescription'] : null;
         $this->profilepic  = (!empty($data['profilepic'])) ? $data['profilepic'] : null;
         $this->coverpic  = (!empty($data['coverpic'])) ? $data['coverpic'] : null;
         $this->email  = (!empty($data['email'])) ? $data['email'] : null;
         $this->phone  = (!empty($data['phone'])) ? $data['phone'] : null;
         $this->id  = (!empty($data['id'])) ? $data['id'] : null;
         $this->status  = (!empty($data['status'])) ? $data['status'] : null;
         $this->website_url  = (!empty($data['website_url'])) ? $data['website_url'] : null;
     }
     
     public function getArrayCopy()
     {
         return get_object_vars($this);
     }
            
}