-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 08, 2015 at 08:03 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ownrockc_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `endorsements`
--

CREATE TABLE IF NOT EXISTS `endorsements` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `project_id` int(255) NOT NULL,
  `endorsetext` varchar(255) NOT NULL,
  `clientname` varchar(255) NOT NULL,
  `project_shortcut` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `enquiry`
--

CREATE TABLE IF NOT EXISTS `enquiry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` text NOT NULL,
  `budget` varchar(255) NOT NULL,
  `skills` varchar(255) NOT NULL,
  `startdate` date NOT NULL,
  `enddate` date NOT NULL,
  `description` varchar(255) NOT NULL,
  `merchant_id` bigint(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `imageupload`
--

CREATE TABLE IF NOT EXISTS `imageupload` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `project_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `filename` (`filename`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=68 ;

--
-- Dumping data for table `imageupload`
--

INSERT INTO `imageupload` (`id`, `filename`, `thumbnail`, `label`, `project_id`) VALUES
(1, '4-fashion-photography.jpg', 'tn_4-fashion-photography.jpg', 'red', 'great-proj'),
(3, '40df3894936683bd0fb63ba7e06c0373.jpg', 'tn_40df3894936683bd0fb63ba7e06c0373.jpg', 'fashion', 'tulikaproject'),
(58, '19328_webdev.jpg', '19328_tn_webdev.jpg', 'Project Pic1', '3'),
(59, '58982_webcms.jpg', '58982_tn_webcms.jpg', 'Project Pic2', '3'),
(60, '24654_webdev.jpg', '24654_tn_webdev.jpg', 'Project Pic1', '4'),
(61, '11176_webcms.jpg', '11176_tn_webcms.jpg', 'Project Pic2', '4'),
(62, '10474_999869_10202045919088366_670552254_n.jpg', '10474_tn_999869_10202045919088366_670552254_n.jpg', 'Project Pic1', '5'),
(63, '78560_1175471_10202045649121617_209376105_n.jpg', '78560_tn_1175471_10202045649121617_209376105_n.jpg', 'Project Pic2', '5'),
(64, '97873_AS_85638new.jpg', '97873_tn_AS_85638new.jpg', 'Project Pic3', '5'),
(65, '71800_0816_Lips_ukflag (1).jpg', '71800_tn_0816_Lips_ukflag (1).jpg', 'Project Pic4', '5'),
(66, '57550_AS_85709.JPG', '57550_tn_AS_85709.JPG', 'Project Pic5', '5'),
(67, '68646_seo.jpg', '68646_tn_seo.jpg', 'Project Pic3', '3');

-- --------------------------------------------------------

--
-- Table structure for table `merchant`
--

CREATE TABLE IF NOT EXISTS `merchant` (
  `name` varchar(250) NOT NULL,
  `profession` varchar(250) NOT NULL,
  `country` varchar(250) NOT NULL,
  `city` varchar(250) NOT NULL,
  `rate` int(250) NOT NULL,
  `availability` int(255) NOT NULL,
  `smalldescription` varchar(255) NOT NULL,
  `longdescription` text NOT NULL,
  `profilepic` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` int(255) NOT NULL,
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `status` enum('incomplete','pending','verified','rejected') NOT NULL DEFAULT 'pending',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `merchant`
--

INSERT INTO `merchant` (`name`, `profession`, `country`, `city`, `rate`, `availability`, `smalldescription`, `longdescription`, `profilepic`, `email`, `phone`, `id`, `status`) VALUES
('Nikita', 'Web Design', 'IN', 'MUM', 12, 6, 'i am great', 'lllllllllllllllllllllllllllllllllllllllllllllllllllll', 'nikita.jpg', '', 0, 1, 'pending'),
('Tulika', 'Web Design', 'IN', 'THA', 10, 5, 'Best developer in world', 'I am an awesome developer', 'tulika.jpg', 'tulika@gmail.com', 2147483647, 2, 'incomplete'),
('akshu', 'Web Design', 'IN', 'RAJ', 10, 15, 'Awesome Designer', 'I am an awesome developer', 'tulika.jpg', 'hkhk@gmail.com', 2147483647, 3, 'pending'),
('Tahir Khan', 'Web Development', 'IN', 'THA', 50, 5, 'This is small', 'I am a web developer having 6 years of experience in web development. I have worked for various kind of projects in various domains like E-learning, Media, E-commerce, Social Media, Advertising, Real Estate etc. I am proficient in PHP, Mysql, CakePHP, Zen', '45254_pp_pic1.jpg', 'webdeveloper24x7@gmail.com', 1234567890, 4, 'verified'),
('John Budro', 'Responsive', 'IN', 'MUM', 250, 265, 'small description', 'long description', '', 'john@budro.com', 123456789, 5, 'verified'),
('Nicks', 'Web Design', 'IN', 'KOL', 12, 6, 'This is a great website!', 'We are testing long description functionality! Great great website.', '9241_pp_AS_85636.JPG', 'nikitaeck@gmail.com', 2147483647, 6, 'verified');

-- --------------------------------------------------------

--
-- Table structure for table `profileskill`
--

CREATE TABLE IF NOT EXISTS `profileskill` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_shortcut` varchar(255) NOT NULL,
  `skill` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `profileskill`
--

INSERT INTO `profileskill` (`id`, `user_shortcut`, `skill`) VALUES
(25, 'tulika', '["1","3"]'),
(26, 'tulika', '["1","3"]'),
(27, 'akshu', '["1","3"]'),
(28, 'akshu', '["1","3"]'),
(29, 'akshu', '["1","3"]'),
(30, 'akshu', '["1","3"]'),
(31, 'niki', '["1","2"]');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `project_id` int(255) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) DEFAULT NULL,
  `about` varchar(255) DEFAULT NULL,
  `website_url` varchar(255) DEFAULT NULL,
  `industry` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `time` varchar(255) DEFAULT NULL,
  `cost` varchar(255) DEFAULT NULL,
  `endorsement` varchar(255) DEFAULT NULL,
  `stars` int(255) DEFAULT NULL,
  `quality` int(255) DEFAULT NULL,
  `effective_cost` int(255) DEFAULT NULL,
  `effective_time` int(255) DEFAULT NULL,
  `experience` int(255) DEFAULT NULL,
  `communication` int(255) DEFAULT NULL,
  `project_name` varchar(255) DEFAULT NULL,
  `project_shortcut` varchar(255) DEFAULT NULL,
  `project_order` int(10) DEFAULT '0',
  `added_by` enum('admin','user') DEFAULT 'user',
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`project_id`, `user_name`, `about`, `website_url`, `industry`, `role`, `time`, `cost`, `endorsement`, `stars`, `quality`, `effective_cost`, `effective_time`, `experience`, `communication`, `project_name`, `project_shortcut`, `project_order`, `added_by`) VALUES
(1, 'nikita', 'test project', 'www', 'fashion', 'designer', '3-4 days', '30', 'good website', 0, 1, 2, 3, 4, 5, 'nikita project', 'nikita-project', 0, 'user'),
(2, 'Nikita', 'great project!!', 'www.great.com', 'service ', 'Designer', '10 weeks', '50', 'nfm,f', 2, 3, 4, 5, 6, 7, 'new project', 'excited', 0, 'admin'),
(3, 'Tahir Khan', 'This project was outsourced to a third party service provider because of less resource and less expertise.', 'http://tahir-khan.com', 'Arts', 'Manager', '15 hrs', '150000', 'I have no endorsement', 5, 1, 13, 4, 7, 8, 'My Project', 'my-project', 2, 'admin'),
(4, 'Tahir Khan', 'This project is for a manufacturing company which is a mumbai based firm.', 'http://www.ilovenerul.com', 'Manufacturing', 'Programmer', '15 days', '20000', 'Amazing work done by Tahir Khan', 5, 1, 2, 4, 1, 1, 'ManufacturePro', 'manu-pro', 1, 'admin'),
(5, 'Nicks', 'My first project', 'www.cashvasool.com', 'Arts', 'Designer', '10', '2-3', 'fmmdf', 1, 2, 3, 4, 5, 6, 'nicks project 1', 'nicks-project-1', 0, 'admin'),
(6, 'Nicks', 'great great project', 'www.odd.com', 'photography', NULL, 'less_than_2_months', '2000-3000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nikita123', 'nikita third project', 0, NULL),
(7, 'Nicks', 'great great project', 'www.odd.com', 'photography', NULL, 'less_than_2_months', '2000-3000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nikita123', 'nikita third project', 0, NULL),
(8, 'Nicks', NULL, 'rest', 'web_designing', NULL, 'less_than_2_months', '2000-3000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(9, 'Nicks', NULL, 'rest', 'web_designing', NULL, 'less_than_2_months', '2000-3000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `projectskill`
--

CREATE TABLE IF NOT EXISTS `projectskill` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `project_shortcut` varchar(255) NOT NULL,
  `project_id` int(255) NOT NULL,
  `skill` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `projectskill`
--

INSERT INTO `projectskill` (`id`, `project_shortcut`, `project_id`, `skill`) VALUES
(1, 'my-project', 3, '5'),
(2, 'my-project', 3, '7'),
(3, 'nikita-project', 1, '19'),
(4, 'nikita-project', 1, '23'),
(6, 'manu-pro', 4, '5'),
(7, 'manu-pro', 4, '4'),
(8, 'manu-pro', 4, '10'),
(9, 'nicks-project-1', 5, '19'),
(10, 'nicks-project-1', 5, '23'),
(11, 'nicks-project-1', 5, '3'),
(12, 'nicks-project-1', 5, '4'),
(13, 'nicks-project-1', 5, '5'),
(14, 'nicks-project-1', 5, '10'),
(15, 'nicks-project-1', 5, '1'),
(16, 'nikita third project', 6, '27'),
(17, 'nikita third project', 6, '50'),
(18, 'nikita third project', 6, '51'),
(19, 'nikita third project', 6, '52'),
(20, 'nikita third project', 6, '75'),
(21, 'nikita third project', 6, '77'),
(22, 'nikita third project', 6, '108'),
(23, 'nikita third project', 7, '27'),
(24, 'nikita third project', 7, '50'),
(25, 'nikita third project', 7, '51'),
(26, 'nikita third project', 7, '52'),
(27, 'nikita third project', 7, '75'),
(28, 'nikita third project', 7, '77'),
(29, 'nikita third project', 7, '108');

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE IF NOT EXISTS `skills` (
  `title` varchar(255) NOT NULL,
  `skill` varchar(255) NOT NULL,
  `skillId` int(255) NOT NULL AUTO_INCREMENT,
  `ispopular` int(16) NOT NULL,
  PRIMARY KEY (`skillId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=109 ;

--
-- Dumping data for table `skills`
--

INSERT INTO `skills` (`title`, `skill`, `skillId`, `ispopular`) VALUES
('Designer Skills', 'CSS', 3, 0),
('Designer Skills', 'HTML', 4, 0),
('Designer Skills', 'Photoshop', 5, 0),
('Popular Skills', 'Logo Design', 7, 0),
('Designer Skills', 'Responsive', 8, 0),
('Designer Skills', 'Graphic', 9, 0),
('Designer Skills', 'PHP', 10, 0),
('Designer Skills', '3D Modeling & CAD', 11, 0),
('Popular Skills', 'After Effects', 12, 0),
('Popular Skills', 'Responsive', 13, 0),
('Popular Skills', 'Graphic', 14, 0),
('Popular Skills', 'PHP', 15, 0),
('Popular Skills', 'Brand Identity', 16, 0),
('Popular Skills', 'Wordpress Maintenance', 17, 0),
('Designer Skills', 'Web Development', 51, 0),
('Designer Skills', 'Wordpress', 53, 0),
('Popular Skills', 'Ecommerce', 75, 0),
('Designer Skills', 'Web Design', 108, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT 'Name',
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `user_type` int(10) NOT NULL,
  `verification_code` varchar(255) NOT NULL,
  `registration_date` date NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=56 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `password`, `user_type`, `verification_code`, `registration_date`, `status`) VALUES
(46, 'Tahir khan', 'webdeveloper24x7@gmail.com', '3eef2c925da519119d7b2756e553cb72', 1, 'mU7yPwUPIj2Qep13Eijs', '2015-05-19', '1'),
(48, NULL, 'nikitaeck@gmail.com', 'e807f1fcf82d132f9bb018ca6738a19f', 1, 'NBtAsycYsGodLPKYOyYt', '2015-05-28', '1'),
(50, NULL, 'manifvbvb@gmail.com', '4693fbb215b8ca15a6900f0cfa164cdc', 1, 'pcZeHgRF5WM39hmtyBz3', '2015-06-01', '0'),
(53, 'Rishi Choudhary', 'rishikumarchoudhary@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', 1, '', '2015-06-06', '0'),
(55, 'Manifest Infotech', 'manifestest2@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', 1, '', '2015-06-06', '0');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
