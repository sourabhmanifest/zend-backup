<?php

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use User\Form\LoginForm;
use User\Model\User;
use User\Model\UserTable;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;
use Zend\Mail;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\View\Model\JsonModel;
//use Facebook\FacebookSession;

class LoginController extends AbstractActionController {

    protected $authservice;
    public function IndexAction() {
        
        include 'Plugin/fb/src/facebook.php';
        
        $facebook = new \Facebook(array(
  'appId'  => '344617158898614',
  'secret' => '6dc8ac871858b34798bc2488200e503d',
));

// Get User ID
$user = $facebook->getUser();

echo $user;

$user_profile = $facebook->api('/me');

print_r($user_profile);
exit;
        
        //Facebook Login Code Ends
        /*
        //define('FACEBOOK_SDK_V4_SRC_DIR', 'Plugin/facebook-php-sdk-v4-4.0-dev/src/Facebook');
        //require 'Plugin/facebook-php-sdk-v4-4.0-dev/autoload.php';
        include 'Plugin/facebook-php-sdk-v4-4.0-dev/src/Facebook/FacebookRequest.php';
        include 'Plugin/facebook-php-sdk-v4-4.0-dev/src/Facebook/FacebookSession.php';
        //\FacebookSession::setDefaultApplication('1426095174376131', '43a911fa79ed3711d37163216a1d5a9f');
        echo '<pre>';
        print_r($_SERVER);
        
        print_r($_GET);
        
        
        $session = new \FacebookSession('access token here');
        
        // Add `use Facebook\FacebookRequest;` to top of file
        $request = new \FacebookRequest($session, 'GET', '/me');
        $response = $request->execute();
        $graphObject = $response->getGraphObject();
        
        echo 'Object:'.$graphObject;
        */
        /*
        $facebook = new \Facebook(array(
            'appId'  => '1426095174376131',
            'secret' => '43a911fa79ed3711d37163216a1d5a9f',
        ));*/

        exit;
        //Facebook Login Code Ends
        
        
        //Google Login Code Starts
        require_once 'Plugin/trunk/src/Google_Client.php';
        require_once 'Plugin/trunk/src/contrib/Google_Oauth2Service.php';
        $user_session = new Container('user');
        
        if (isset($_GET['state']) && $user_session->state != $_GET['state']) {
          header('HTTP/ 401 Invalid state parameter');
          exit;
        }
        
        $client = new \Google_Client();
        $client->setApplicationName('Google+ server-side flow');
        $client->setClientId('643271913404-j53l8b046qoq8m1b6o351mcs54thein5.apps.googleusercontent.com');
        $client->setClientSecret('O_rSeZ0_T9Ps2F0g1NdoblxN');
        $client->setRedirectUri('http://ownrock.com/zend/public/user/login');
        $client->setDeveloperKey('AIzaSyDjNeO5ppCF8Apauxb26vLGwzaZqicJHEc');
        //$plus = new \Google_PlusService($client);
        $oauth2 = new \Google_Oauth2Service($client);

        if (isset($_GET['code'])) {
          $client->authenticate();
          $jsonTokens = $client->getAccessToken();
          $temp_data = json_decode($jsonTokens,true);
          $user_session->access_token = $temp_data['access_token'];          
          $user = $oauth2->userinfo->get();
        }
        
        exit;
        $this->layout('layout/register');
        $form = new LoginForm();
        return new ViewModel(array('form' => $form));
    }

    public function getAuthService() {
        $this->authservice = $this->getServiceLocator()->get('AuthService');
        return $this->authservice;
    }

    public function ProcessAction() {
        if (!$this->request->getPost()) {
            return $this->redirect()->toRoute('user/login');
        }
        $post = $this->request->getPost();
        $form = new LoginForm();
        $form->setData($post);
        if (!$form->isValid()) {
            $model = new ViewModel(array(
                'error' => true,
                'form' => $form,
            ));
            $model->setTemplate('user/login/index');
            return $model;
        } else {
            //check authentication...
            $this->getAuthService()->getAdapter()
                    ->setIdentity($this->request->getPost('email'))
                    ->setCredential($this->request->getPost('password'));
            $result = $this->getAuthService()->authenticate();
            $auth = $this->getAuthService();
            $authAdapter = $this->getAuthService()->getAdapter();
            if ($result->isValid()) {
                $storage = $auth->getStorage();
                $storage->write($authAdapter->getResultRowObject(
                                null, 'password'));
                $time = 1209600;
                if ($post['rememberme'] = "yes") {
                    $sessionManager = new \Zend\Session\SessionManager();
                    $sessionManager->rememberMe($time);
                }

                $mail = new Mail\Message();

                $mail->setBody('This is the text of the email.');
                $mail->setFrom('nikitaeck@gmail.com', 'Sender\'s name');
                $mail->addTo('nikitaeck@gmail.com', 'Name of recipient');
                $mail->setSubject('TestSubject');

                $transport = new Mail\Transport\Sendmail();
                
                //$transport->send($mail);
                //   $this->getAuthService()->getStorage()->write($this->request->getPost('email'));
                //return $this->redirect()->toRoute('user/login', array('action' => 'confirm'));
                return $this->redirect()->toRoute('user/login', array('action' => 'confirm'));
            } else {
                $model = new ViewModel(array(
                    'error' => true,
                    'form' => $form,
                ));
                $model->setTemplate('user/login/index');
                return $model;
            }
        }
    }

    public function ajaxAction() {
        $form = new LoginForm();
        return new ViewModel(array('form' => $form));
        $post = $this->request->getPost();
        $form = new LoginForm();
        $form->setData($post);
        $response = $this->getResponse();
        if (!$form->isValid()) {
            echo 'inajax';
            die;
            // email is invalid; print the reasons
            $json = $form->getMessages();
            $response->setContent(\Zend\Json\Json::encode($json));
            return $response;
        }

        $this->getAuthService()->getAdapter()->setIdentity(
                $this->request->getPost('email'))->setCredential(
                $this->request->getPost('password'));
        $result = $this->getAuthService()->authenticate();
        switch ($result->getCode()) {
//            case Result::FAILURE_IDENTITY_NOT_FOUND:
//                $json = 'No such email found';
//                $response->setContent(\Zend\Json\Json::encode($json));
//                print_r($result->getCode());die;
//
//                return $response;
//                break;
//
//            case Result::FAILURE_CREDENTIAL_INVALID:
//                $json = 'Invalid password';
//                $response->setContent(\Zend\Json\Json::encode($json));
//                return $response;
//                break;
        }

        $dbTableAuthAdapter = $this->getServiceLocator()->get('AuthService');
        if ($result->isValid()) {
            $authAdapter = $this->getAuthService()->getAdapter();
            $result = $this->getAuthService()->getStorage();
            $result->write($authAdapter->getResultRowObject(array(
                        'email',
            )));
            ; // Writes email and name to the storage
            $result->write($authAdapter->getResultRowObject(
                            null, 'password'
            ));

            $user_session = new Container('user');
            $user_session->user_name = $this->getAuthService()->getStorage()->read()->name;
            $user_session->user_email = $this->getAuthService()->getStorage()->read()->email; // gets email from storage
            $user_session->login_session = true;
        }
    }

    public function validateformAction() {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->getHelper('layout')->disableLayout();
        $form = new LoginForm();
        $form->isValid($this->_getAllParams);
        $json = $form->getMessages();
        header('Content-type: user/json');
        echo Zend_Json::encode($json);
    }

    public function ConfirmAction() {
        $this->layout('layout/common-layout');
        $user = $this->getAuthService()->getStorage()->read();
        $viewModel = new ViewModel(array(
            'user' => $user
        ));
        return $viewModel;
    }

    public function LogoutAction() {
        $form = new LoginForm();
        return new ViewModel(array('form' => $form));
    }

}
