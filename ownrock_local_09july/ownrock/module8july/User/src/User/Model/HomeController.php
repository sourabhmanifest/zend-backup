<?php

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use User\Form\LoginForm;
use User\Model\User;
use User\Model\UserTable;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;
use Zend\Mail;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\View\Model\JsonModel;

use Zend\Session\Container; // We need this when using sessions

use Zend\Mail\Message;

use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;

class HomeController extends AbstractActionController
{
    protected $authservice;
    public function IndexAction()
    {
        $this->layout('layout/home-layout');
        
        $error_id = $this->params()->fromRoute('error_id');
        $error_msg = "";
        if(isset($error_id) && $error_id != ""){
            if($error_id == "25486"){
                $error_msg = 'Invalid login credentials ! Please try again.' ;
            }
            if($error_id == "55600"){
                $error_msg = 'Email address already registered !. Please try with different one.' ;
            }
        }
        $this->layout()->error_msg = $error_msg;
        
        return new ViewModel(array());
    }
   
    public function getAuthService() {
        $this->authservice = $this->getServiceLocator()->get('AuthService');
        return $this->authservice;
    }
    
    public function LoginAction()
    {       

        $this->getAuthService()->getAdapter()
                ->setIdentity($this->request->getPost('loginemail'))
                ->setCredential(md5($this->request->getPost('loginpassword')));
        $result = $this->getAuthService()->authenticate();
        $auth = $this->getAuthService();
        $authAdapter = $this->getAuthService()->getAdapter();
        if ($result->isValid()) {
            $storage = $auth->getStorage();
            $storage->write($authAdapter->getResultRowObject(
                            null, 'password'));
            
            // Store username in session
            $user_session = new Container('user');
            $result1 = $authAdapter->getResultRowObject();
            
            $user_session->id = $result1->id;
            $user_session->first_name = $result1->name;
            $user_session->email = $result1->email;
            
            /*$time = 1209600;
            if ($post['rememberme'] = "yes") {
                $sessionManager = new \Zend\Session\SessionManager();
                $sessionManager->rememberMe($time);
            }*/
            $response = 'success';
            //return $this->redirect()->toRoute('user/home', array('action' => 'dashboard'));           
        } else {
            $response = 'failed';
        }
        echo json_encode(array('response'=>$response));
        die();
    }
    
    public function RegisterAction(){
        $post = array();
        $post = $this->request->getPost();
        $post['user_type'] = 1;
        $post['registration_date'] = date("Y-m-d");
        
        $UserTable = $this->getServiceLocator()->get('usertable');
        $email_exists = $UserTable->getUserByEmail($post['email']);
        if(!$email_exists){
            $this->createUser($post);
        
            $confirm_link = '/zend/public/user/home/verifyaccount/param1/'.$this->randomNumber(20);
            $message_body = 'Thank you for regsitering with Ownrock!!!';
            
            $this->sendEmail(trim($post['email']),'Welcome to OwnRock!!',$message_body,$confirm_link);
        
            $response = 'success';
            
            $user_session = new Container('user');
            $user_session->email = trim($post['email']);
            
            //return $this->redirect()->toRoute('user/home', array('action' => 'dashboard'));
        }else{
            $response = 'failed';
            //return $this->redirect()->toRoute('user/home',array('error_id'=>'55600'));
        }
        echo json_encode(array('response'=>$response));
        die();
    
    }
    
    protected function createUser($data) {
        $user = new User();
        $user->exchangeArray($data);
        $user->setPassword($data['password']);
        $userTable = $this->getServiceLocator()->get('UserTable');
        $userTable->saveUser($user);
        return true;
    }
    
    protected function dashboardAction() {
        //$this->layout('layout/home-layout');
        $user_session = new Container('user');
        
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/home');
        }
        
        $fname = $user_session->first_name;
        $lname = $user_session->last_name;
        
        return new ViewModel(array('fname'=>$fname,'lname'=>$lname));
    }
    
    public function googleloginAction() {
         
        require_once 'Plugin/trunk/src/Google_Client.php';
        //require_once 'Plugin/trunk/src/contrib/Google_Oauth2Service.php';
        require_once 'Plugin/trunk/src/contrib/Google_PlusService.php';
        $user_session = new Container('user');
        
        /*
        if (isset($_GET['state']) && $user_session->state != $_GET['state']) {
          header('HTTP/ 401 Invalid state parameter');
          exit;
        }*/
        
        $client = new \Google_Client();
        $client->setApplicationName('Google+ server-side flow');
        $client->setClientId('643271913404-j53l8b046qoq8m1b6o351mcs54thein5.apps.googleusercontent.com');
        $client->setClientSecret('SCi1w2gCvYcjx-MBhN5DaWc7');
        $client->setRedirectUri('http://ownrock.com/zend/public/user/home/googlelogin');
        $client->setDeveloperKey('AIzaSyA2gkZh34yULNSDbjn087KpEbGWXa5cwxE');
        $plus = new \Google_PlusService($client);
        //$oauth2 = new \Google_Oauth2Service($client);
        
        $me = $plus->people->get('107194039893970907338');
        session_start();
        if (isset($_GET['code'])) {
            $client->authenticate($_GET['code']);
            $at = $client->getAccessToken();
            $_SESSION['token'] = $at;
        }
        if (isset($_SESSION['token'])) {
          $client->setAccessToken($_SESSION['token']);
        }
        
        $access_details = json_decode($at,TRUE);       
        $atkn = $access_details['access_token'];
        if ($client->getAccessToken()) {
  
            $google_data_temp = file_get_contents('https://www.googleapis.com/userinfo/v2/me/?access_token='.$atkn);
            $google_data = json_decode($google_data_temp,true);
            $name = $google_data['name'];
            $googleid = $google_data['id'];
            $email = $google_data['email'];
            
            $UserTable = $this->getServiceLocator()->get('usertable');
            $email_exists = $UserTable->getUserByEmail($email);
            
            if(!$email_exists){
                
                $create_user_array = array(
                    'name'=>$name,
                    'email'=>$email,
                    'password'=>$this->randomNumber(),
                    'user_type'=>1,
                    'registration_date'=>date("Y-m-d")
                );
                $this->createUser($create_user_array);
                $user_session->email = $email;               
                return $this->redirect()->toRoute('user/home', array('action' => 'dashboard'));
            }else{
                return $this->redirect()->toRoute('user/home',array('error_id'=>'55600'));
            }
          
          $_SESSION['token'] = $client->getAccessToken();
        } else {
          //$authUrl = $client->createAuthUrl();
        }         
        return $this->redirect()->toRoute('user/home', array('action' => 'dashboard'));
          
        }
    
    public function facebookloginAction() {
        
    }
    
    public function logoutAction(){
        
        $user_session = new Container('user');
        
        if ($user_session->access_token != "") {
            //$user_session->access_token = "";
            $user_session->getManager()->getStorage()->clear();
            
            require_once 'Plugin/trunk/src/Google_Client.php';
            $client = new \Google_Client();
            
            $client->revokeToken();
        }
        $user_session->email = '';
        
        return $this->redirect()->toRoute('user/home');
    }
    
    protected function randomNumber($length=8) {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }
    
    protected function sendEmail($to,$subject,$body){
        require_once 'Plugin/PHPMailerMain/src/class.phpmailer.php';
        $mail = new \PHPMailer();
        $mail->IsSMTP();                                      // set mailer to use SMTP
        $mail->Host = "ownrock.com";  // specify main and backup server
        $mail->SMTPAuth = true;     // turn on SMTP authentication
        $mail->Username = "info@ownrock.com";  // SMTP username
        $mail->Password = "info@123"; // SMTP password
        $mail->From = "info@ownrock.com";
        $mail->FromName = "Ownrock";
        $mail->AddAddress($to, "Tahir Khan");
        //$mail->AddAddress("ellen@example.com");                  // name is optional
        //$mail->AddReplyTo("info@example.com", "Information");
        $mail->WordWrap = 50;                                 // set word wrap to 50 characters
        //$mail->AddAttachment("/var/tmp/file.tar.gz");         // add attachments
        //$mail->AddAttachment("/tmp/image.jpg", "new.jpg");    // optional name
        $mail->IsHTML(true);                                  // set email format to HTML
        $mail->Subject = $subject;
        $mail->Body    = $body;
        $mail->AltBody = "This is the body in plain text for non-HTML mail clients";
        if(!$mail->Send())
        {
           echo "Message could not be sent. <p>";
           echo "Mailer Error: " . $mail->ErrorInfo;
           $res = 'ERROR_SENDING_MAIL';
        }else{
           $res = 'SUCCESS_SENDING_MAIL';
        }
        return $res;
    }
    
    public function verifyaccountAction(){
        $vericode = $this->params()->fromRoute('vericode');
        
        $UserTable = $this->getServiceLocator()->get('usertable');
        $veriresponse = $UserTable->verifyUser($vericode);
        
        return $this->redirect()->toRoute('user/home', array('action' => 'dashboard'));
        
    }
    
}