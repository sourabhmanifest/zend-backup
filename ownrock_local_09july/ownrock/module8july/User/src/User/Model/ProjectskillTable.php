<?php

namespace User\Model;

use Zend\Text\Table\Row;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class ProjectskillTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function saveProjectskill(Projectskill $projectskill) {
        /*$data = array(
            'id' => $projectskill->id,
            'project_shortcut' => $projectskill->project_shortcut,
            'project_id' => $projectskill->project_id,
            'skill' => $projectskill->skill,
        );*/
        
        $id = (int) $projectskill->id;
        if ($id == 0) {
            foreach ( $projectskill->skill as $skillId)
            {
                $data= array ( 
                            'id' => $projectskill->id , 'project_shortcut' => $projectskill->project_shortcut,
                            'project_id'=> $projectskill->project_id,'skill'=>$skillId
                        ) ;
                /*$data[] = array ( 
                            'id' => $projectskill->id , 'project_shortcut' => $projectskill->project_shortcut,
                            'project_id'=> $projectskill->project_id,'skill'=>$skillId
                        ) ;*/
                $this->tableGateway->insert($data);
            }
            
        } else {
                if ($this->getProject($projectskill->project_id)) {
                    $this->tableGateway->update($data, array('id' => $id));
                } else {
                    throw new \Exception('id does not exist');
                }
        }
    }
   
    public function fetchAll($paginated=false) {
   
        if ($paginated) {
             // create a new Select object for the table album
             $select = new Select('projectskill');
                
             // create a new result set based on the Album entity
             $resultSetPrototype = new ResultSet();
             $resultSetPrototype->setArrayObjectPrototype(new Projectskill());
             // create a new pagination adapter object
             $paginatorAdapter = new DbSelect(
                 // our configured select object
                 $select,
                 // the adapter to run it against
                 $this->tableGateway->getAdapter(),
                 // the result set to hydrate
                 $resultSetPrototype
             );
        
             $paginator = new Paginator($paginatorAdapter);
             return $paginator;
         }
        $resultSet = $this->tableGateway->select();
        $resultSet->buffer();
        return $resultSet;
    }

    public function deleteProjectSkillById($projskillid) {
        $this->tableGateway->delete(array('id' =>  $projskillid));
    }

    public function deleteProjectSkillAll($project_id) {
        $this->tableGateway->delete(array('project_id' => $project_id));
    }
    
    public function getProjectSkill($id) {
        //$id = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }   
    
    public function selectProjectskill($project_id) {     
        
        $rowset = $this->tableGateway->select(array('project_id' => $project_id));
        return $rowset;    
    }
                
    public function allProjectskill($project_ids) {     
        
        foreach($project_ids as $v){
            $rowset = $this->tableGateway->select(array('project_id' => $v));
            $arr[$v] = $rowset->toArray();
        }
        return $arr;
        /*
        $select = $this->tableGateway->getSql()->select();
        $select->columns(array('skill'));
        $select->where("project_id IN ($project_ids)");
        
        $resultSet = $this->tableGateway->select($select);
        $resultSet->buffer(); 
        return $resultSet;
        */
    }

    public function saveProjectskillFront($projectid,$project_shortcut,$skillid) {
        
        $data= array ( 
                    'project_shortcut' => $project_shortcut,
                    'project_id'=> $projectid,'skill'=>$skillid
                ) ;
        $this->tableGateway->insert($data);
    }
        
}