<?php

namespace User\Model;

use Zend\Text\Table\Row;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class EnquiryTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function saveEnquiry(Enquiry $enquiry) {

        $data= array ( 
                    'name' => $enquiry->name , 'email' => $enquiry->email,
                    'phone'=> $enquiry->phone,'budget'=>$enquiry->budget,
                    'skills'=> $enquiry->skills,'startdate'=>$enquiry->startdate,
                    'enddate'=> $enquiry->enddate,'description'=>$enquiry->description,
                ) ;
        $this->tableGateway->insert($data);
    }
   
    public function fetchAll($paginated=false,$filter_col) {
   
        if ($paginated) {
             // create a new Select object for the table album
             $select = new Select('enquiry');
                
             if($filter_col != ""){
                 $select->where->equalTo('merchant_id', $filter_col);
             }
             
             // create a new result set based on the Album entity
             $resultSetPrototype = new ResultSet();
             $resultSetPrototype->setArrayObjectPrototype(new Enquiry());
             // create a new pagination adapter object
             $paginatorAdapter = new DbSelect(
                 // our configured select object
                 $select,
                 // the adapter to run it against
                 $this->tableGateway->getAdapter(),
                 // the result set to hydrate
                 $resultSetPrototype
             );
        
             $paginator = new Paginator($paginatorAdapter);
             return $paginator;
         }
        $resultSet = $this->tableGateway->select();
        $resultSet->buffer();
        return $resultSet;
    }

    public function getEnquiryByMerchantId($id) {
        //$id = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        return $rowset;
    }   
    
}