<?php

namespace User\Model;

class ImageUpload {

    public $id;
    public $filename;
    public $label;
    public $user_id;
    public $thumbnail;
    
    public function exchangeArray($data) {
        $this->id = (isset($data['id'])) ? $data['id'] : null;
        $this->filename = (isset($data['filename'])) ? $data['filename'] : null;
        $this->label = (isset($data['label'])) ? $data['label'] : null;
        $this->project_id = (isset($data['project_id'])) ? $data['project_id'] : null;
        $this->thumbnail = (isset($data['thumbnail'])) ? $data['thumbnail'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}
