<?php

return array(
    'acl' => array(
        'roles' => array(
            'guest' => null,
            'member' => 'guest'
        ),
        'resources' => array(
            'allow' => array(
                'user' => array(
                    'login' => 'guest',
                    'index' => 'guest',
                    'index' => 'guest'
                ),
                'User\Controller\Login' => array(
                    'index' => 'guest',
                    'all' => 'member',
                    'process' => 'guest',
                    'confirm' => 'guest',
                    'logout' => 'guest',
                    'ajax' => 'guest',
                    'confirm2' => 'guest',
                    'update'=>'guest'
                ),
                'User\Controller\Campaigns' => array(
                    'index' => 'guest',
                    'add' => 'guest',
                    'edit' => 'guest',
                    'delete' => 'guest',
                ),
                'User\Controller\Usermanager' => array(
                    'index' => 'guest',
                    'add' => 'guest',
                    'edit' => 'guest',
                    'delete' => 'guest',
                ),
                'User\Controller\Onlinemanager' => array(
                    'index' => 'guest',
                    'store' => 'guest',
                    'index1' => 'guest',
                    'gethtmlresponse' => 'guest',
                    'processajaxrequest' => 'guest',
                    'process' => 'guest',
                    'categories' => 'guest',
                    'add' => 'guest',
                    'edit' => 'guest',
                    'process' => 'guest',
                    'delete' => 'guest',
                    'addskill' => 'guest',
                    'update' => 'guest',
                ),
                'User\Controller\Mediamanager' => array(
                    'showimage' => 'guest',
                    'index' => 'guest',
                    'upload' => 'guest',
                    'processupload' => 'guest',
                ),
                'User\Controller\Uploadmanager' => array(
                    'index' => 'guest',
                    'upload' => 'guest',
                    'processupload' => 'guest',
                ),
                'User\Controller\Projectmanager' => array(
                    'index' => 'guest',
                    'add' => 'guest',
                    'edit' => 'guest',
                    'delete' => 'guest',
                    'process' => 'guest',
                    'addprojectskill'=>'guest',
                    'addendorsements' => 'guest',
                ),
                'User\Controller\Projectskill' => array(
                    'index' => 'guest',
                    'add' => 'guest',
                    'edit' => 'guest',
                    'delete' => 'guest',
                    'deletes' => 'guest',
                    'process' => 'guest',
                    'view' => 'guest',
                ),
                'User\Controller\Search' => array(
                    'index' => 'guest',
                    'generateindex' => 'guest',
                    
                ),
               'User\Controller\Photographer' => array(
                    'index' => 'guest',                    
                    
                ),
                'User\Controller\Register' => array(
                    'index' => 'guest',
                    'publish' => 'guest',
                    'confirm' => 'guest',
                    'process' => 'guest',
                ),
                'User\Controller\Frontportfolio' => array(
                    'index' => 'guest', 
                    'notify' => 'guest',
                ),
                'User\Controller\Index' => array(
                    'index' => 'guest',                    
                ),
                'User\Controller\Home' => array(
                    'index' => 'guest',
                    'login' => 'guest',
                    'register' => 'guest',
                    'logout' => 'guest',
                    'dashboard' => 'guest',
                    'googlelogin'=>'guest',
                    'facebooklogin'=>'guest',
                    'verifyaccount'=>'guest',
                    'addnewproject'=>'guest',
                    'viewproject'=>'guest',
                    'enquiry'=>'guest',
                    'updateprojectorder'=>'guest',
                    'editprofileimage'=>'guest',
                    'editproject'=>'guest',
                    'deleteproject'=>'guest',
                    'addprojectimage'=>'guest',
                    'forgot' => 'guest',
                    'search' => 'guest',
					'reset' => 'guest',
                )
                
            )
        )
    )
);