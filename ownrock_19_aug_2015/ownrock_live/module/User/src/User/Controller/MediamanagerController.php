<?php

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Http\Headers;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;
use User\Form\RegisterForm;
use User\Form\RegisterFilter;
use User\Model\User;
use User\Model\UserTable;
use User\Model\Uploads;
use User\Model\UploadsTable;
use User\Form\UploadForm;
use User\Model\ImageUpload;
use User\Model\ImageUploadTable;

class MediamanagerController extends AbstractActionController {

    protected $storage;
    protected $authservice;

    public function generateThumbnail($imageFileName) {
        $path = $this->getFileUploadLocation();
        $sourceImageFileName = $path . '/' . $imageFileName;
        $thumbnailFileName = 'tn_' . $imageFileName;
        $imageThumb = $this->getServiceLocator()->get('WebinoImageThumb');
        $thumb = $imageThumb->create($sourceImageFileName, $options = array());
        $thumb->resize(100, 100);
        $thumb->save($path . '/' . $thumbnailFileName);
        return $thumbnailFileName;
    }

    public function getAuthService() {
        if (!$this->authservice) {
            $this->authservice = $this->getServiceLocator()->get('AuthService');
        }
        return $this->authservice;
    }

    public function getFileUploadLocation() {
        // Fetch Configuration from Module Config
        $config = $this->getServiceLocator()->get('config');
        return $config['module_config']['upload_location'];
    }

    public function index1Action() {

        $this->layout('layout/common-layout');

        $uploadTable = $this->getServiceLocator()->get('ImageuploadTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        $user = $this->getAuthService()->getStorage()->read();

        $user = $userTable->getUserByEmail($user->email);
        $viewModel = new ViewModel(
                array(
            'myUploads' => $uploadTable->getUploadsByUserId($user->id),
                // 'sharedUploadsList' => $sharedUploadsList,
                )
        );

        return $viewModel;
    }

    public function indexAction() {

        $this->layout('layout/common-layout');
        $ImageuploadTable = $this->getServiceLocator()->get('ImageUploadTable');
        $viewModel = new ViewModel(
                array(
            'Uploads' => $ImageuploadTable->fetchAll(),
                // 'sharedUploadsList' => $sharedUploadsList,
                )
        );
        return $viewModel;
    }

    public function showimageAction() {
        $uploadId = $this->params()->fromRoute('id');
        $uploadTable = $this->getServiceLocator()->get('ImageUploadTable');
        $upload = $uploadTable->getUpload($uploadId);
       
        //$upload = $uploadTable->fetchAll();
        // Fetch Configuration from Module Config
        $uploadPath = $this->getFileUploadLocation();
        if ($this->params()->fromRoute('subaction') == 'thumb') {
            $filename = $uploadPath . "/" . $upload->thumbnail;
        } else {
            $filename = $uploadPath . "/" . $upload->filename;
        }
       
        $file = file_get_contents($filename);

        // Directly return the Response 
        $response = $this->getEvent()->getResponse();
        $response->getHeaders()->addHeaders(array(
            'Content-Type' => 'application/octet-stream',
            'Content-Disposition' => 'attachment;filename="' . $upload->filename . '"',
        ));
        $response->setContent($file);
        return $response;
    }

    public function processuploadAction() {
        $this->layout('layout/common-layout');
        
//        $userTable = $this->getServiceLocator()->get('UserTable');
//        $user_email = $this->getAuthService()->getStorage()->read();
//        $user = $userTable->getUserByEmail($user_email->email);
        $form = $this->getServiceLocator()->get('UploadForm');
        $request = $this->getRequest();
        if ($request->isPost()) {

            $upload = new ImageUpload();
            
            $uploadFile = $this->params()->fromFiles('fileupload');
            $form->setData($request->getPost());

            if ($form->isValid()) {
                // Fetch Configuration from Module Config
                $uploadPath = $this->getFileUploadLocation();
                // Save Uploaded file    	
                $adapter = new \Zend\File\Transfer\Adapter\Http();
                $adapter->setDestination($uploadPath);

                if ($adapter->receive($uploadFile[0]['name'])) {

                    $exchange_data = array();
                    $exchange_data['label'] = $request->getPost()->get('label');
                    $exchange_data['filename'] = $uploadFile[0]['name'];
                    $exchange_data['thumbnail'] = $this->generateThumbnail($uploadFile[0]['name']);
                   
                   //$exchange_data['user_id'] = $user->id;

                    $upload->exchangeArray($exchange_data);

                    $uploadTable = $this->getServiceLocator()->get('ImageUploadTable');
                    $uploadTable->saveUploads($upload);

                    return $this->redirect()->toRoute('user/media', array(
                                'action' => 'index'
                    ));
                }
            }
        }
        die;
        return array('form' => $form);
    }

    public function uploadAction() {
        $this->layout('layout/common-layout');
        $form = new UploadForm();
        $viewModel = new ViewModel(array('form' => $form));
        return $viewModel;
    }

    public function deleteAction() {
        $this->layout('layout/common-layout');
        $uploadId = $this->params()->fromRoute('id');
        $uploadTable = $this->getServiceLocator()->get('UploadsTable');
        $upload = $uploadTable->getUploads($uploadId);
        $uploadPath = $this->getFileUploadLocation();
        // Remove File

        unlink($uploadPath . "/" . $upload->filename);
        // Delete Records
        $uploadTable->deleteUploads($uploadId);

        return $this->redirect()->toRoute('user/uploadmanager');
    }

    public function editAction() {
        $this->layout('layout/common-layout');
        $uploadId = $this->params()->fromRoute('id');
        $uploadTable = $this->getServiceLocator()->get('UploadsTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        // Upload Edit Form
        $upload = $uploadTable->getUploads($uploadId);
        $form = $this->getServiceLocator()->get('UploadEditForm');
        $form->bind($upload);
        $viewModel = new ViewModel(array(
            'form' => $form,
            'upload_id' => $uploadId,
//            'sharedUsers' => $sharedUsers,
//            'uploadShareForm' => $uploadShareForm,
                )
        );
        return $viewModel;
    }

    public function fileDownloadAction() {
        $uploadId = $this->params()->fromRoute('id');
        $uploadTable = $this->getServiceLocator()->get('UploadTable');
        $upload = $uploadTable->getUpload($uploadId);

        // Fetch Configuration from Module Config
        $uploadPath = $this->getFileUploadLocation();
        $file = file_get_contents($uploadPath . "/" . $upload->filename);

        // Directly return the Response 
        $response = $this->getEvent()->getResponse();
        $response->getHeaders()->addHeaders(array(
            'Content-Type' => 'application/octet-stream',
            'Content-Disposition' => 'attachment;filename="' . $upload->filename . '"',
        ));
        $response->setContent($file);

        return $response;
    }

}
