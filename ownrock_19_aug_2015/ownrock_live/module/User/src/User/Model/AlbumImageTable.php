<?php

namespace User\Model;

use Zend\Text\Table\Row;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class AlbumImageTable
{
    protected $tableGateway;
    //protected $uploadsSharingTableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
      //  $this->uploadSharingTableGateway = $uploadSharingTableGateway;
    }

    public function saveImages(AlbumImage $albumimage)
    {   
        //echo '<pre>'; print_r($albumimage); exit;
        $data = array(
            'id' => $albumimage->id,
            'album_id' => $albumimage->album_id,
            'imagename'  => $albumimage->imagename,
            'thumbname'  => $albumimage->thumbname,
            'description'  => $albumimage->description,
        );
        
            $this->tableGateway->insert($data); 
    }
    
    public function getImagesByAlbumId($album_id)
    { 
    	$album_id  = (int) $album_id;      
    	$resultSet = $this->tableGateway->select(array('album_id' => $album_id)); 
    	return $resultSet->toArray();
    }
    
    public function deleteAlbumImagesByAlbumId($album_id)
    {
        $album_id  = (int) $album_id;     
    	$this->tableGateway->delete(array('album_id' => $album_id));
    }

    public function deleteByName($imagename)
    {
        $id  = (int) $id;      
        $this->tableGateway->delete(array('imagename' => $imagename));
    }
	
    
}