<?php

namespace User\Model;

class Onlinecoupon {

    public $couponid;
    public $merchant;
    public $couponname;
    public $shortdescription;
    public $longdescription;
    public $couponcode;
    public $expirydate;
    public $couponurl;
    public $entrydate;
    public $category;
    public $subcategory;

    public function exchangeArray($data) {
        $this->couponid = (!empty($data['couponid'])) ? $data['couponid'] : null;
        $this->merchant = (!empty($data['merchant'])) ? $data['merchant'] : null;
        $this->couponname = (!empty($data['couponname'])) ? $data['couponname'] : null;
        $this->shortdescription = (!empty($data['shortdescription'])) ? $data['shortdescription'] : null;
        $this->longdescription = (!empty($data['longdescription'])) ? $data['longdescription'] : null;
        $this->couponcode = (!empty($data['couponcode'])) ? $data['couponcode'] : null;
        $this->expirydate = (!empty($data['expirydate'])) ? $data['expirydate'] : null;
        $this->couponurl = (!empty($data['couponurl'])) ? $data['couponurl'] : null;
        $this->entrydate = (!empty($data['entrydate'])) ? $data['entrydate'] : null;
        $this->category = (!empty($data['category'])) ? $data['category'] : null;
        $this->subcategory = (!empty($data['subcategory'])) ? $data['subcategory'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}
