<?php

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use User\Form\RegisterForm;
use User\Model\User;
use User\Model\UserTable;


class RegisterController extends AbstractActionController {

    public function IndexAction() {
        $this->layout('layout/register');
        $form = new RegisterForm();
        $form->get('user_type')->setValue('1');
        return new ViewModel(array('form' => $form));
    }

    public function PublisherAction() {
        $this->layout('layout/register');
        $form = new RegisterForm();
        $form->get('user_type')->setValue('3');
        return new ViewModel(array('form' => $form));
    }

    public function ProcessAction() {

        $post = $this->request->getPost();

        $form = new RegisterForm();
        $form->setData($post);
        if (!$form->isValid()) {
            $model = new ViewModel(array(
                'error' => true,
                'form' => $form,
            ));
            $model->setTemplate('');
            return $model;
        }
        $this->createUser($form->getData());

        return $this->redirect()->toRoute('user/register', array('action' => 'confirm'));
    }

    public function confirmAction() {

        return new ViewModel();
    }

    protected function createUser(array $data) {
        $user = new User();
        $user->exchangeArray($data);
        $user->setPassword($data['password']);
        $userTable = $this->getServiceLocator()->get('UserTable');
        $userTable->saveUser($user);
        return true;
    }

}
