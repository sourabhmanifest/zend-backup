<?php

namespace User\Model;

class Categorylist {

    public $category;
    public $subcategory;

    public function exchangeArray($data) {
        $this->category = (!empty($data['category'])) ? $data['category'] : null;
        $this->subcategory = (!empty($data['subcategory'])) ? $data['subcategory'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}
