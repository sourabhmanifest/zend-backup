<?php

namespace User\Model;

class AlbumTag {

    public $id;
    public $album_id;
    public $tag_id;
    

    public function exchangeArray($data) 
    {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->album_id = (!empty($data['album_id'])) ? $data['album_id'] : null;
        $this->tag_id = (!empty($data['tag_id'])) ? $data['tag_id'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}
