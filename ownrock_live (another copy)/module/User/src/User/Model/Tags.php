<?php

namespace User\Model;

class Tags {

    public $tag_id;
    public $tags;

    public function exchangeArray($data) {

        $this->tag_id = (!empty($data['tag_id'])) ? $data['tag_id'] : null;
        $this->tags = (!empty($data['tags'])) ? $data['tags'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}
