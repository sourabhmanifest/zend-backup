<?php

namespace User\Model;

use Zend\Text\Table\Row;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class TagsTable 
{
    protected $tableGateway;
    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }
    public function getAllTags() 
    {     
        $rowset = $this->tableGateway->select();
        $temp_data = $rowset->toArray();
        foreach($temp_data as $v)
        {
            $final_result[$v['skillId']] = $v;
        }
        return $final_result;    
    }
}