echo $this->headLink(array('rel' => 'shortcut icon', 'type' => 'image/vnd.microsoft.icon', 'href' => $this->basePath() . '/img/favicon.ico'))
                ->prependStylesheet($this->basePath() . '/css/style_fickle.css')
                ->prependStylesheet($this->basePath() . '/css/pace.css')
                ->prependStylesheet($this->basePath() . '/css/responsive.css')
                ->prependStylesheet($this->basePath() . '/css/bootstrap-theme.min_fickle.css')
                ->prependStylesheet($this->basePath() . '/css/bootstrap-switch.min_fickle')
                ->prependStylesheet($this->basePath() . '/css/bootstrap.min_1.css')
                ->prependStylesheet($this->basePath() . '/css/ladda-themeless.min_fickle.css')
                ->prependStylesheet($this->basePath() . '/css/humane_themes/bigbox.css')
                ->prependStylesheet($this->basePath() . '/css/humane_themes/jackedup.css')
                ->prependStylesheet($this->basePath() . '/css/humane_themes/libnotify.css')
        
		
        <link rel="stylesheet" href="<?php$this->basePath().?>/css/style_fickle.css">
        <link rel="stylesheet" href="<?php$this->basePath().?>/css/pace.css">
        <link rel="stylesheet" href="<?php$this->basePath().?>/css/responsive.css">
		
			->prependStylesheet($this->basePath() . '/css/style_fickle.css')
			->prependStylesheet($this->basePath() . '/css/pace.css')
			->prependStylesheet($this->basePath() . '/css/responsive.css')
             
			 
			 body class="login-screen">
        <section>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="login-box">
                            <div class="login-content">
                                <div class="login-user-icon">
                                    <i class="glyphicon glyphicon-user"></i>

                                </div>
                                <h3>Identify Yourself</h3>
                                <div class="social-btn-login">
                                    <ul>
                                        <li><a href="javascript:void(0)"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-github"></i></a></li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-bitbucket"></i></a></li>
                                    </ul>
                                    <!--<button class="btn ls-dark-btn rounded"><i class="fa fa-facebook"></i></button>
                                    <button class="btn ls-dark-btn rounded"><i class="fa fa-twitter"></i></button>
                                    <button class="btn ls-dark-btn rounded"><i class="fa fa-linkedin"></i></button>
                                    <button class="btn ls-dark-btn rounded"><i class="fa fa-google-plus"></i></button>
                                    <button class="btn ls-dark-btn rounded"><i class="fa fa-github"></i></button>
                                    <button class="btn ls-dark-btn rounded"><i class="fa fa-bitbucket"></i></button>-->
                                </div>
                            </div>

                            <div class="login-form">
                                <form id="form-login" action="#" class="form-horizontal ls_form">
                                    <div class="input-group ls-group-input">
                                        <input class="form-control" type="text" placeholder="Username">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    </div>


                                    <div class="input-group ls-group-input">

                                        <input type="password" placeholder="Password" name="password"
                                               class="form-control" value="">
                                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    </div>

                                    <div class="remember-me">
                                        <input class="switchCheckBox" type="checkbox" checked data-size="mini"
                                               data-on-text="<i class='fa fa-check'><i>"
                                               data-off-text="<i class='fa fa-times'><i>">
                                        <span>Remember me</span>
                                    </div>
                                    <div class="input-group ls-group-input login-btn-box">
                                        <button class="btn ls-dark-btn ladda-button col-md-12 col-sm-12 col-xs-12" data-style="slide-down">
                                            <span class="ladda-label"><i class="fa fa-key"></i></span>
                                        </button>

                                        <a  class="forgot-password" href="javascript:void(0)">Forgot password</a>
                                    </div>
                                </form>
                            </div>
                            <div class="forgot-pass-box">
                                <form action="#" class="form-horizontal ls_form">
                                    <div class="input-group ls-group-input">
                                        <input class="form-control" type="text" placeholder="someone@mail.com">
                                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    </div>
                                    <div class="input-group ls-group-input login-btn-box">
                                        <button class="btn ls-dark-btn col-md-12 col-sm-12 col-xs-12">
                                            <i class="fa fa-rocket"></i> Send
                                        </button>

                                        <a class="login-view" href="javascript:void(0)">Login</a> & <a class="" href="registration.html">Registration</a>

                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <p class="copy-right big-screen hidden-xs hidden-sm">
                <span>&#169;</span>  <span class="footer-year">2014</span>
            </p>
        </section>

    </body>
   
        
		
		