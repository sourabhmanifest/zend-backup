<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MI extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('html');
		$this->load->model(array('commonmodel','mimodel'));
	}
	public function index()
	{	
		$orderby = 'project_id';
		$data['all_project_data'] = $this->commonmodel->getRecords('portfolio_project','*','', $orderby);
		$data['title']= 'Manifest Infotech';
		$data['description']= "Manifest Infotech is an IT solution company. We offers development of PHP, Codeigniter, Drupal Module, Android app, iPhone  app, CMS Customization";
		$data['keywords'] = "Web Design, Web Development, SEO, Digital Marketing, IT Services, IT Solution";
		//echo '<pre>';print_r($data);exit;
		$this->load->view('includes/header',$data);
		$this->load->view('home');
		$this->load->view('includes/footer');
	}
	public function testimonial()
	{
		$data['all_project_data'] = $this->commonmodel->getRecords('portfolio_project');
		$data['title']= 'Testimonial | Manifest Infotech';
		$data['description']= "";
		$data['keywords']= "";
		$this->load->view('includes/header',$data);		
		$this->load->view('testimonials');
		$this->load->view('portfolio/related_projects');
		$this->load->view('includes/footer');
	}
	public function career()
	{
		$data['all_project_data'] = $this->commonmodel->getRecords('portfolio_project');
		$data['title']= 'Career | Manifest Infotech';
		$data['keywords']= "";
		$data['description']= "We are give offer of jobs in Design, Development, & Management to explore your knowledge base and enhance your experience";
		$this->load->view('includes/header',$data);		
		$this->load->view('career');
		$this->load->view('portfolio/related_projects');
		$this->load->view('includes/footer');
	}
	public function contact()
	{
		$data['all_project_data'] = $this->commonmodel->getRecords('portfolio_project');
		$data['title']= 'Contact | Manifest Infotech';
		$data['description']= "You can contact us for any inquiry by fill out the form, or email or call us. Feel free to contact us";
		$data['keywords']= "";
		$this->load->view('includes/header',$data);		
		$this->load->view('contact');
		$this->load->view('gmap');
		$this->load->view('portfolio/related_projects');
		$this->load->view('includes/footer');

	}
	public function pageMissing()
	{
		$data['title']= 'Error 404 (Page Not Found) | Manifest Infotech';
		$data['description']= "";
		$data['keywords']= "";
		$this->load->view('includes/header',$data);		
		$this->load->view('page-missing');
		$this->load->view('includes/footer');

	}
	public function process()
	{
		$data['all_project_data'] = $this->commonmodel->getRecords('portfolio_project');
		$data['title']= 'Process | Manifest Infotech';
		$data['description']= "";
		$data['keywords']= "";
		$this->load->view('includes/header',$data);		
		$this->load->view('process');
		$this->load->view('portfolio/related_projects');
		$this->load->view('includes/footer');
	}
	public function experties()
	{
		$data['all_project_data'] = $this->commonmodel->getRecords('portfolio_project');
		$data['title']= 'Experties | Manifest Infotech';
		$data['description']= "QA & Deploy is the last step for SDLC after this project deploy to the client for use on browser";
		$data['keywords']= "";
		$this->load->view('includes/header',$data);		
		$this->load->view('experties');
		$this->load->view('portfolio/related_projects');
		$this->load->view('includes/footer');
	}
	public function offering()
	{
		$data['all_project_data'] = $this->commonmodel->getRecords('portfolio_project');
		$data['title']= 'Offering | Manifest Infotech';
		$data['description']= "";
		$data['keywords']= "";
		$this->load->view('includes/header',$data);		
		$this->load->view('offering');
		$this->load->view('portfolio/related_projects');
		$this->load->view('includes/footer');
	}
	public function whatwedo()
	{
		$data['all_project_data'] = $this->commonmodel->getRecords('portfolio_project');
		$data['title']= 'What We Do | Manifest Infotech';
		$data['description']= "";
		$data['keywords']= "";
		$this->load->view('includes/header',$data);		
		$this->load->view('what-we-do');
		$this->load->view('portfolio/related_projects');
		$this->load->view('includes/footer');
	}
	public function whyus()
	{
		$data['all_project_data'] = $this->commonmodel->getRecords('portfolio_project');
		$data['title']= 'Why Us | Manifest Infotech';
		$data['description']= "";
		$data['keywords']= "";
		$this->load->view('includes/header',$data);		
		$this->load->view('why-us');
		$this->load->view('portfolio/related_projects');
		$this->load->view('includes/footer');
	}
	public function mission()
	{
		$data['all_project_data'] = $this->commonmodel->getRecords('portfolio_project');
		$data['title']= 'Mission | Manifest Infotech';
		$data['description']= "";
		$data['keywords']= "";
		$this->load->view('includes/header',$data);		
		$this->load->view('mission');
		$this->load->view('portfolio/related_projects');
		$this->load->view('includes/footer');
	}
	public function analysisandplanning()
	{
		$data['all_project_data'] = $this->commonmodel->getRecords('portfolio_project');
		$data['title']= 'Analysis and Planning | Manifest Infotech';
		$data['description']= "Analyze and Planning is the first step for software development life cycle through agile model";
		$data['keywords']= "";
		$this->load->view('includes/header',$data);		
		$this->load->view('analysis-and-planning');
		$this->load->view('portfolio/related_projects');
		$this->load->view('includes/footer');
	}
	public function designandhtml()
	{
		$data['all_project_data'] = $this->commonmodel->getRecords('portfolio_project');
		$data['title']= 'Design and Html | Manifest Infotech';
		$data['description']= "Design & HTML is second step in MI for SDLC which make the basic layout of project";
		$data['keywords']= "";
		$this->load->view('includes/header',$data);		
		$this->load->view('design-and-html');
		$this->load->view('portfolio/related_projects');
		$this->load->view('includes/footer');
	}
	public function developmentandimplimentation()
	{
		$data['all_project_data'] = $this->commonmodel->getRecords('portfolio_project');
		$data['title']= 'Development and Implimentation | Manifest Infotech';
		$data['description']= "Development and Implementation is third step which make the project user interface and implement the code";
		$data['keywords']= "";
		$this->load->view('includes/header',$data);		
		$this->load->view('development-and-implimentation');
		$this->load->view('portfolio/related_projects');
		$this->load->view('includes/footer');
	}
	public function qaanddeploy()
	{
		$data['all_project_data'] = $this->commonmodel->getRecords('portfolio_project');
		$data['title']= 'QA and Deploy | Manifest Infotech';
		$data['description']= "QA & Deploy is the last step for SDLC after this project deploy to the client for use on browser";
		$data['keywords']= "";
		$this->load->view('includes/header',$data);		
		$this->load->view('qa-and-deploy');
		$this->load->view('portfolio/related_projects');
		$this->load->view('includes/footer');
	}
	public function businessDevelopmentOfficer()
	{
		$data['all_posts'] = $this->mimodel->getallpost();
		$data['all_project_data'] = $this->commonmodel->getRecords('portfolio_project');
		$data['title']= 'Business Development Officer | Manifest Infotech';
		$data['description']= "Job opening for Business development officer with 3 years of experience in bidding, business planning etc.";
		$data['keywords']= "";
		$this->load->view('includes/header',$data);		
		$this->load->view('business-development-officer');
		$this->load->view('portfolio/related_projects');
		$this->load->view('includes/footer');
	}
	public function webDesigner()
	{
		$data['all_posts'] = $this->mimodel->getallpost();
		$data['all_project_data'] = $this->commonmodel->getRecords('portfolio_project');
		$data['title']= 'Web Designer | Manifest Infotech';
		$data['description']= "Creative Web designer having a golden opportunity to show their skills and talent.";
		$data['keywords']= "";
		$this->load->view('includes/header',$data);		
		$this->load->view('web-designer');
		$this->load->view('portfolio/related_projects');
		$this->load->view('includes/footer');
	}
	public function phpDevelopment()
	{	
		$data['all_posts'] = $this->mimodel->getallpost();
		$data['all_project_data'] = $this->commonmodel->getRecords('portfolio_project');
		$data['title']= 'Php Development | Manifest Infotech';
		$data['description']= "We are hiring PHP developer with more than 2 years of experience in programming";
		$data['keywords']= "";
		$this->load->view('includes/header',$data);		
		$this->load->view('php-development');
		$this->load->view('portfolio/related_projects');
		$this->load->view('includes/footer');
	}
	public function projectManager()
	{
		$data['all_posts'] = $this->mimodel->getallpost();
		$data['all_project_data'] = $this->commonmodel->getRecords('portfolio_project');
		$data['title']= 'Project Manager. | Manifest Infotech';
		$data['description']= "We need a dynamic candidate with managerial skills to manage projects and give reason for future growth.";
		$data['keywords']= "";
		$this->load->view('includes/header',$data);		
		$this->load->view('project-manager');
		$this->load->view('portfolio/related_projects');
		$this->load->view('includes/footer');
	}
	function emailsender()
	{
		if($this->input->is_ajax_request())
		{
			// load form validation class
			$this->load->library('form_validation');
			// set form validation rules
			$this->form_validation->set_rules('fname', 'Fname', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			$this->form_validation->set_rules('phone', 'Phone Number', 'trim');
			$this->form_validation->set_rules('subject', 'Subject',  'trim');
			$this->form_validation->set_rules('msg', 'msg',  'trim');
			//call the email configuration funtion 
			$this->commonmodel->setMailConfig();
			// set email data
			$this->email->from($this->input->post('email'), $this->input->post('fname'));
			$email=$this->input->post('email');
			$this->email->to(TO_EMAIL);
			$this->email->reply_to($this->input->post('email'), $this->input->post('fname'));
			$this->email->subject($this->input->post('subject'));
			if($this->input->post('phone'))
			{
				$phone=$this->input->post('phone');
				$message_contact=$this->input->post('msg')."<br /><br />"."Email : ".$email."<br />"."Contact no. : ".$phone;
			}
			else
				$message_contact=$this->input->post('msg')."<br /><br />"."Email : ".$email;
			$this->email->message($message_contact);
			// send email
			if($this->commonmodel->sendEmail())
			{
				echo 'Thank you for contacting us.';
			}
			else
			{
				echo show_error($this->email->print_debugger());
			}
		}
	}
	public function sitemap()
	{
		$data['title']= 'Sitemap | Manifest Infotech';
		$data['description']= "";
		$data['keywords']= "";
		$this->load->view('includes/header',$data);		
		$this->load->view('sitemap');
		$this->load->view('includes/footer');
	}
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
