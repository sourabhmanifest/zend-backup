<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Services extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('commonmodel','mimodel'));
	}
	public function index()
	{	
		$data['all_project_data'] = $this->commonmodel->getRecords('portfolio_project');
		$data['title']= 'Services | Manifest Infotech';
		$data['description']= "Manifest Infotech offers services of Web Design, Web Development, SEO and Digital Marketing, which give support and help to our clients in their business";
		$data['keywords']= "";
		$this->load->view('includes/header',$data);
		$this->load->view('services');
		$this->load->view('portfolio/related_projects');
		$this->load->view('includes/footer');
	}
	public function webDesign()
	{
		$data['all_project_data'] = $this->commonmodel->getRecords('portfolio_project');
		$data['title']= 'Web Design | Manifest Infotech';
		$data['description']= "We are provides web design services which are affordable and best web design, with responsive and unique ideas";
		$data['keywords']= "Web design company, web design services,website redesign, responsive web design services, mobile website design";
		$this->load->view('includes/header',$data);		
		$this->load->view('web-design');
		$this->load->view('portfolio/related_projects');
		$this->load->view('includes/footer');
	}
	public function webDevelopment()
	{
		$data['all_project_data'] = $this->commonmodel->getRecords('portfolio_project');
		$data['title']= 'Web Development | Manifest Infotech';
		$data['description']= "We are web development company having professional web developers making very cost effective and  provide flexible solutions.";
		$data['keywords']= "Keywords : php web development, website development company, custom website development, web application development, web development website.";
		$this->load->view('includes/header',$data);		
		$this->load->view('web-development');
		$this->load->view('portfolio/related_projects');
		$this->load->view('includes/footer');
	}
	public function seo()
	{
		$data['all_project_data'] = $this->commonmodel->getRecords('portfolio_project');
		$data['title']= 'Seo | Manifest Infotech';
		$data['description']= "Search Engine Optimization Services - We are using white hat technique to improve page ranking of websites for better traffic on website";
		$data['keywords']= "search engine optimisation services, seo website, website seo services,website seo analysis, seo services, organic seo services";
		$this->load->view('includes/header',$data);		
		$this->load->view('seo');
		$this->load->view('portfolio/related_projects');
		$this->load->view('includes/footer');
	}
	public function digitalMarketing()
	{
		$data['all_project_data'] = $this->commonmodel->getRecords('portfolio_project');
		$data['title']= 'Digital Marketing | Manifest Infotech';
		$data['description']= "Digital Marketing is the medium to attract and manage customers through various channels";
		$data['keywords']= "Affiliate Marketing, Email marketing, Pay per click marketing, Search engine optimization";
		$this->load->view('includes/header',$data);		
		$this->load->view('digital-marketing');
		$this->load->view('portfolio/related_projects');
		$this->load->view('includes/footer');
	}
}