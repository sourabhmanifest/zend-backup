<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Solutions extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('commonmodel','mimodel'));
	}
	public function index()
	{	
		$data['all_project_data'] = $this->commonmodel->getRecords('portfolio_project');
		$data['title']= 'Solutions | Manifest Infotech';
		$data['description']= "You can find your solutions here";
		$data['keywords']= "";
		$this->load->view('includes/header',$data);
		$this->load->view('solutions');
		$this->load->view('portfolio/related_projects');
		$this->load->view('includes/footer');
	}
}