<script>
	$('#portfolio a').css('background-color','#056ab2');
	$('#portfolio a').css('color','#FFF');
</script>

<!-----top navigations section closed here...-------->
<section class="page-cover portfolio-cover-img">
  <div class="container-fluid">
    <div class="container">
      <div class="page-heading">
        <h3>Featured Portfolio</h3>
        <h4>We are Glaring and Prominent in our work</h4>
        <p> <?php echo $project_data['project_one_liner'];?> </p>
      </div>
    </div>
  </div>
</section>
<section >
  <div class="container-fluid bac_color border_gray">
    <div class="container prev_next">
	  <?php
	  //echo '<pre>'; print_r($all_project_data);
	  //$project_id_name_array=array();
	  foreach($all_project_data as $project_record)
	  {
		  $idarr[]=$project_record['project_id'];
		  $namearr[]=$project_record['project_name'];
	  }
	  $project_id_name_array=array_combine($idarr,$namearr);
	  //echo '<pre>'; print_r($project_id_name_array);

	  $prev_id = $project_data['project_id'] == 1 ? count($all_project_data):$project_data['project_id']-1;
	  $next_id = $project_data['project_id'] == count($all_project_data) ? 1:$project_data['project_id']+1;
		/*echo '<pre>'; print_r($idarr);*/
		echo "<ul class='mar_hide'>
    <li>";
	  if(in_array($prev_id++,$idarr))
	  {
		  $project_name=$project_id_name_array[$prev_id++];?>
		<li>
			<a href="<?php echo site_url('portfolio/project/'.$prev_id.'/'.$project_data['project_name']);?>"><img src="assets/images/1426168176_basics-05-48.png" class="algin_verti_middile" />PREV</a>
		</li>
		<?php
	  }
	  
		if(in_array($next_id--,$idarr))
		{
			$project_name=$project_id_name_array[$next_id--];?>
			<li class="next_float">
			<a href="<?php echo site_url('portfolio/project/'.$next_id.'/'.$project_data['project_name']);?>">NEXT<img src="assets/images/1426168170_basics-06-48.png" class="algin_verti_middile" /></a>
		</li><?php

		}
	  
	  ?>
    
      </ul>
    </div>
  </div>
</section>
<!--/page cover & portfolio section...
----------------------------------------------------------------------------------->
<section class="page-section theme-bg-gray">
  <div class="container-fluid">
    <div class="container">
      <h4 class="doc_dimand"><?php echo $project_data['project_name'];?></h4>
      <div class="row-fluid">
        <div class="span8 pull-left">
          <div class="flexslider">
			<?php $imagePath="assets/images/portfolio/".strtolower(str_replace(' ','-',$project_data['project_name']));?>
            <ul class="slides img-hint">
              <li> <a href="javascript:void(0)"><img src="<?php echo $imagePath.'/'.$project_data['image1'];?>"/></a>
                <p class="flex-caption">Web Desing Project</p>
                <a href="javascript:void(0)">
                <div class="hind-block"><i class="fa fa-link fa-2x"></i></div>
                </a> 
			  </li>
              <li> <a href="javascript:void(0)"><img src="<?php echo $imagePath.'/'.$project_data['image2'];?>" /></a>
                <p class="flex-caption">Web Development Project</p>
                <a href="javascript:void(0)">
                <div class="hind-block"><i class="fa fa-link fa-2x"></i></div>
                </a> 
			  </li>
              <li> <a href="javascript:void(0)"><img src="<?php echo $imagePath.'/'.$project_data['image3'];?>" /></a>
                <p class="flex-caption">SEO(Search Engine Optimazation)</p>
                <a href="javascript:void(0)">
                <div class="hind-block"><i class="fa fa-link fa-2x"></i></div>
                </a> 
			  </li>
              <li> <a href="javascript:void(0)"><img src="<?php echo $imagePath.'/'.$project_data['image4'];?>" /></a>
                <p class="flex-caption">Web Desing Project</p>
                <a href="javascript:void(0)">
                <div class="hind-block"><i class="fa fa-link fa-2x"></i></div>
                </a> 
			  </li>
            </ul>
          </div>
          <div class="spacer-mini2"></div>
          <!--<h4 class="heading-a">Website Name :- Doctor on Demand</h4>--->
          <p class="justify"><br />
            <b><a href="<?php echo $project_data['project_url'];?>" class="colo_url"><?php echo $project_data['project_url'];?></a></b></p>
          <p></p>
          <p class="justify"><?php echo $project_data['project_discription'];?></p>
          <p></p>
		  <hr/>
          <p class="justify"> <b>Technologies used:</b><?php echo $project_data['project_technologies'];?></p>
        </div>
        <!----left pane----------------------->
		
		<!----portfolio-detail-categories start ----------------------->
        
		<?php include "portfolio-detail-categories.php"; ?>
		 
		<!----portfolio-detail-categories end ----------------------->
        
			
		<!---------------- recent post start ----------------------->
        
		<?php include "recent-post.php"; ?>
		 
		<!----------- recent post end ----------------------->

      </div>
    </div>
  </div>
</section>