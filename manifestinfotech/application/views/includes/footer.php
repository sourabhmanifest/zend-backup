 <!--footer start--> 
 <!--call to action pop up code-->
 <div id="myModal" class="modal fade popup_padding" tabindex="-1" role="dialog" aria-hidden="true">
    <button type="button" id="toggle" class="close cutumize_close" data-dismiss="modal" aria-hidden="true"> &times; </button>
    <div class="span6 padd_top_action">
        <div class="page-heading">
            <h3 class="">Send Us Message !</h3>
        </div>
        <div class="spacer-mini"><p id="popupsuccessmsg" class="successmsg"></p></div>
        <form class="">
            <div class="controls controls-row">
                <input type="text" name='fname' id='fullname' onBlur='checkFnameValidation("fullname",1)' class="span3 colo_relal fa fa-user" placeholder="Full Name">
                <input type="text" name='mail' id='email' value='' onBlur='checkEmailValidation("email",1)' class="span3 margin_left" placeholder="Email Address">
                <input type="text" name='phone' id='telphone' onBlur='checkPhoneValidation("telphone",1)' class="span3 margin-null" placeholder="Phone Number">
                <input type="text" name='subject' id='subject' class="span3" placeholder="Subject">
            </div>
            <div class="controls">
                <textarea id="message" name="msg" class="span6 colr_chage_all_field no_resize no_resize" placeholder="Your Message" rows="5"></textarea>
            </div>
            <div class="controls">
                <button id="contact-submit" onclick="send_msg('fullname','email','telphone','subject','message',1,'popupsuccessmsg')" type="button" class="color_back_button btn   pull-right">Send Now</button>
            </div>
        </form>
    </div>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body"> </div>
        </div>
    </div>
</div>
<section class="page-section">
<div class="container-fluid">
	<div class="container">
		<div class="row-fluid">
			<div class="span3 footer_grid pull-left" style="max-width: 23%;">
				<ul class="unstyled">
				  <li class="mar_bootum bac_image3">Navigation</li>
				  <li><a href="<?php echo site_url('overview');?>">Overview</a></li>
				  <li><a href="<?php echo site_url('mi/process');?>">Process</a></li>
				  <li><a href="<?php echo site_url('services');?>">Services</a></li>
				  <li><a href="<?php echo site_url('solutions');?>">Solutions</a></li>
				  <li><a href="<?php echo site_url('mi/experties');?>">Experties</a></li>
				  
				  <!--<li><a href="<?php echo site_url('mi/what-we-do');?>">what we do</a></li>
				  <li><a href="<?php echo site_url('mi/offering');?>">offerings</a></li>-->
				</ul>
			</div>

			<div class="span3 footer_grid pull-left" style="max-width: 23%;">
				<ul class="unstyled">
					<li class="mar_bootum bac_image1">Strategies</li>
					<li><a href="<?php echo site_url('mi/analysis-and-planning');?>">Analyse & Planning</a></li>
					<li><a href="<?php echo site_url('mi/design-and-html');?>">Design & Html</a></li>
					<li><a href="<?php echo site_url('mi/development-and-implimentation');?>">Development & Implementation</a></li>
					<li><a href="<?php echo site_url('mi/qa-and-deploy');?>">QA & Deploy</a></li>
				</ul>
			</div>
			<div class="span3 footer_grid pull-left" style="max-width: 23%;">
				<ul class="unstyled">
				  <li class="mar_bootum bac_image2">Technologies</li>
					<li><a href="<?php echo base_url('blog/python-technology/');?>">Python</a></li>
					<li><a href="<?php echo base_url('blog/php-technology/');?>">PHP</a></li>
					<li><a href="<?php echo base_url('blog/asp-net-technology/');?>">ASP.Net</a></li>
					<li><a href="<?php echo base_url('blog/android-app-development/');?>">Android</a></li>
					<li><a href="<?php echo base_url('blog/ipad-app-development/');?>">iOS</a></li>
					<li><a href="<?php echo base_url('blog/web-design/');?>">Web Design</a></li>
				</ul>
			</div>
			
			<div class="span3 footer_grid pull-left" style="max-width: 23%;">
				<ul class="unstyled">
					<li class="mar_bootum bac_image">Open Source</li>
					<li><a href="<?php echo base_url('blog/cakephp-development/');?>">Cake PHP</a></li>
					<li><a href="<?php echo base_url('blog/codeigniter-development/');?>">Codeigniter</a></li>
					<li><a href="<?php echo base_url('blog/yii-development/');?>">Yii</a></li>
					<li><a href="<?php echo base_url('blog/zend-development/');?>">Zend</a></li>
					<li><a href="<?php echo base_url('blog/wordpress-customization/');?>">Wordpress</a></li>
					<li><a href="<?php echo base_url('blog/drupal-customization/');?>">Drupal</a></li>
					<li><a href="<?php echo base_url('blog/joomla-customization/');?>">Joomla</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
</section>

 
 <section>
  <div id="footer" class="footer">
    <div class="container-fluid">
      <div class="container">
        <div class="row-fluid">
          <div class="span6">Copyright &copy; <strong class="color-mark"><img src="assets/images/common-img/16x16.png" class="log_foooter" alt="16x16"/>&nbsp;Manifest Infotech Pvt. Ltd.</strong>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; All rights reserved.</div>
          <div class="span6">
            <ul class="inline-list pull-right">
              
              <li><a href="<?php echo site_url('overview');?>">Overview</a></li>
              <li><a href="<?php echo site_url('services');?>">Services</a></li>
              <li><a href="<?php echo site_url('portfolio');?>">Portfolio</a></li>
              <li><a href="blog">Blog</a></li>
              <li><a href="<?php echo site_url('mi/contact');?>">Contact</a></li>
              <li><a href="<?php echo site_url('mi/sitemap');?>">Site Map</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
<!--</div>-->
<!--footer end--> 

</section>

<!--wrapper closed here-->
<div ><a href="#" class="scrollup"><i class="fa fa-chevron-up"></i></a> </div>
<script type="text/javascript" src="assets/js/jquery.quicksand.js"></script> 
<script>
	function gallery(){}
		var $itemsHolder = $('ul.thumbnails');
		var $itemsClone = $itemsHolder.clone(); 
		var $filterClass = "";
		$('ul.filter li').click(function(e) {
			e.preventDefault();
			$filterClass = $(this).attr('data-value');
			if($filterClass == 'all'){ var $filters = $itemsClone.find('li'); }
			else { var $filters = $itemsClone.find('li[data-type='+ $filterClass +']'); }
			$itemsHolder.quicksand(
			$filters,
			{ duration: 1000 },
			gallery
			);
		});
		$(document).ready(gallery);
</script> 

<!---map api script--> 
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAo1Fedo-w_WP4ie4kAu3DdKacKFe_jOyY&amp;sensor=false"></script> 
<script>
	var myCenter=new google.maps.LatLng(22.727009,75.880990);

function initialize()
{
	var mapProp = {
	center:myCenter,
	zoom:10,
	scrollwheel:false,
  
	mapTypeId:google.maps.MapTypeId.ROADMAP
  };

var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

var marker=new google.maps.Marker({
  position:myCenter,
  animation:google.maps.Animation.BOUNCE
  });
	marker.setMap(map);
	var infowindow = new google.maps.InfoWindow({
	content:'<div id="content">'+
      '<div id="siteNotice">'+
      '</div>'+
      '<h4>Manifest Infotech</h4>'+
      '<div id="bodyContent">'+
      '<p><b>Address</b>105 Prakash Tower,YN Road Near <br /> Rani Sati Gate, Indore, INDIA</p>'+
	  '<p><b>Office</b> (+91)9770368611</p>'+
      '</div>'+
      '</div>'

  });
	infowindow.open(map,marker);
	google.maps.event.addListener(marker, 'click', function() {
    infowindow.open(map,marker);
});

}
google.maps.event.addDomListener(window, 'load', initialize);
//window.onload = loadScript;
</script> 
<script>
 $(window).scroll(function(){
		
        if ($(this).scrollTop() > 50) {
            $('.scrollup').fadeIn('slow');
        } else {
            $('.scrollup').fadeOut('slow');
        }
    });
    $('.scrollup').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 500);
        return false;
    });

</script>



<script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-58518073-1', 'auto');
  ga('send', 'pageview');
</script>

</body>
</html>
