

<script>
   $('#career a').css('background-color','#056ab2');
   $('#career a').css('color','#FFF');
</script>
<section class="page-cover career-cover-img">
   <div class="container-fluid">
      <div class="container">
         <div class="page-heading">
            <h3>CAREER</h3>
            <h4>We open the door of "HOPE"</h4>
            <p> Our career offerings make the way for the job seekers. Our career plan is to give opportunity for many people to expand our human resource base </p>
         </div>
      </div>
   </div>
</section>
<!--/page cover & career section-->
<section id="career" class="page-section theme-bg-gray">
   <div class="container-fluid">
      <div class="container">
         <div class="span7 pull-left margin-null txt-justify">
            <h1 class="heading-a border-theme-l"> Career at our Company</h1>
            <img src="assets/images/career/webdesiner.jpg" class="shadow-block" title="Career at our Company" />
            <div class="spacer-mini2"></div>
            <ul class="bullet_process justify border_box">
               <li class="bgcolor_jo">Job Summary</li>
               <li>Functional area: IT/ Web programming</li>
               <li>Industry :- Computer/ IT</li>
               <li>Role Category :- Web designer/ XHTML/CSS3<li>
               <li>Vacancy :- 2</li>
               <li>Job Location :- 105, Prakash Tower, Rani Sati Gate, YN Road Indore </li>
               <li >Job details :-</li>
               <li>Candidate has experience of more than 1yr for the post of web designer </li>
               <li>Strong conceptual thinking with great attention to detail</li>
               <li>Ability to work on multiple projects under tight deadlines with solid organizational and time management skills </li>
               <li>Skills Needed: fireworks & coreldraw </li>
            </ul>
         </div>
         <!---/left panel-->
         <?php //include "career-news.php"; ?>
		 <?php include "portfolio/recent-post.php"; ?>
      </div>
   </div>
</section>
<?php include "current-opportunity.php"; ?>

