<script>
	$('#testimonial a').css('background-color','#056ab2');
	$('#testimonial a').css('color','#FFF');
</script>
<section class="page-cover testimonial-cover-img">
  <div class="container-fluid">
    <div class="container">
      <div class="page-heading">
        <h3>TESTIMONIALS</h3>
        <h4>We venerate our clients and their feedback</h4>
		<P>Our client and imparting their experience with us is more valuable for our company</P>
      </div>
    </div>
  </div>
</section>
<!--/page cover & testimonail section-->
<section id="testimonial" class="page-section theme-bg-gray">
  <div class="container-fluid">
    <div class="container">
      <h1 class="heading-a border-theme-l"> What people are saying ? </h1>
      <div  class="row-fluid shadow-block">
        <div class="span12">
			<div class="border pull-left thumb-right"> <a href="JavaScript:Void(0)" class="">
				<img  class="testimonia-thumb log_foooter border_raduis_tumb" src="assets/images/testimonial/Kayode.png" alt="Kayode" class="span4"/> </a>
			</div>
          <h4>Kayode</h4>
		  
          <p class="justify"><span class="qautes"></span> The price is right at all level for service offers, but the best thing is your technical support. Whenever I need help or made mistakes or damage something, your team are with me to fix it. I hope to see them for all the coming year </p>
        </div>
      </div>
      <!---1testimonial-->
      
      <div  class=" row-fluid shadow-block">
        <div class="span12">
          <div class="border pull-left thumb-right"> <a href="JavaScript:Void(0)" class="">
			<img class="testimonia-thumb log_foooter border_raduis_tumb"  src="assets/images/testimonial/Maria.png" alt="Maria"> </a> 
		</div>
          <h4>Maria</h4>
		  
          <p class="justify"><span class="qautes"></span> Thank you very much to create a website that truly shows my company motto. My site is now clear, clean and concise. Your company is doing very great job. All the best for future</p>
        </div>
      </div>
      <!---2testimonial-->
      
      <div  class=" row-fluid shadow-block">
        <div class="span12">
			<div class="border pull-left thumb-right"> <a href="JavaScript:Void(0)" class=""> 
				<img class="testimonia-thumb log_foooter border_raduis_tumb"  src="assets/images/testimonial/Rued.png" alt="Rued"> </a> 
			</div>
          <h4>Rued</h4>
		  
          <p class="justify"><span class="qautes"></span> My experience with Manifest Infotech is amazing. I proud to have such a beautiful & responsive website, from the responsive team. This team is experienced, knowledgeable, skilled and patient. I can't wait to start new project with them </p>
        </div>
      </div>
      <!---3testimonial-->
      
      <div  class="row-fluid shadow-block">
        <div class="span12">
			<div class="border pull-left thumb-right"> <a href="JavaScript:Void(0)" class=""> 
				<img  class="testimonia-thumb log_foooter border_raduis_tumb"  src="assets/images/testimonial/shiv.png" alt="shiv"> </a> 
			</div>
          <h4>shiv</h4>
		  
          <p class="justify"><span class="qautes"></span> I would like to say you are very positive, proactive, helpful. I am very comfortable in communication with Manifest Infotech team. All the best from my side </p>
        </div>
      </div>
	  
      <div  class="row-fluid shadow-block">
        <div class="span12">
			<div class="border pull-left thumb-right"> <a href="JavaScript:Void(0)" class=""> 
				<img  class="testimonia-thumb log_foooter border_raduis_tumb"  src="assets/images/testimonial/Shailendra.png" alt="Shailendra"> </a> 
			</div>
          <h4>Shailendra</h4>
		  
          <p class="justify"><span class="qautes"></span> I just want to thank you and your staff for the all you support. That is why I choose to work with your company. My website is running far better with your technical support and help. Keep up with Great Customer Services </p>
        </div>
      </div>
    </div>
  </div>
</section>
<!--4testimonial--> 
