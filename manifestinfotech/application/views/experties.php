<section class="page-cover About-cover-img">
  <div class="container-fluid">
    <div class="container">
      <div class="page-heading">
        <h3>EXPERTISE</h3>
        <h4>Our work define our Competence</h4>
        <p>We are having years of Industry experience in dealing various projects and providing exceptional & cost effective solutions</p>
      </div>
    </div>
  </div>
</section>


<section class="page-section coo_otr" >
  <div  class="container-fluid">
    <div class="container">
      <div class="page-heading"> </div>
      <div class="spacer-mini"></div>
      <div class="container">
        <div class=" row-fluid">
          <div class="span12">
            <div class="border pull-right thumb-right"> <a href="JavaScript:Void(0)" class="shadow"> 
              <!--<img  width="250" src="img/home-about/about-us.jpg" title="About Us">--> 
              </a> </div>
            <h3>Experties</h3>
            <br />
            <p class="process_prg justify"> Many companies can build web applications - but if you need a web application 
              that can scale seamlessly to serve millions of users, integrate with multiple technology platforms and can 
              be extended to provide interface to various mobile devices, you would need the expertise of someone like us.</p> 
          <br />
			</div>
			<div>
        <h4>Technology Expertise</h4>
        <ul class="bullet_process check_right check_ic span12 pull-left">
          <li class="li_cheak">Open Source - PHP, Perl, Python, AJAX, Ruby on Rails</li>
          <li class="li_cheak">Microsoft - VB.Net, ASP.Net, Silver light, SOAP, XML, MFC SDK </li>
          <li class="li_cheak">Social Networking - Face book API, OpenSocial API, Twitter, LinkedIn, Goggle + </li>
          <li class="li_cheak">Mobile - iPhone, Android, BlackBerry </li>
          <li class="li_cheak">Databases - mySQL, SQL Server, Oracle, Postgres </li>
        </ul>
			</div>
			<div>
        <h4>Domain Expertise</h4>
        <ul class="bullet_process check_right check_ic span8 pull-left">
          <li class="li_cheak">Media Application </li>
          <li class="li_cheak">Ad Servers</li>
          <li class="li_cheak">Customer Relationship Management</li>
          <li class="li_cheak">Social Networking</li>
          <li class="li_cheak">Health Care</li>
          <li class="li_cheak">Hotel Management </li>
          <li class="li_cheak">Sports Management </li>
          <li class="li_cheak">Education</li>
          <li class="li_cheak">Web 2.0, Collaboration </li>
          <li class="li_cheak">Mobile Application</li>
          <li class="li_cheak">Recruitment and HR </li>
        </ul>
			</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>