<!--/page cover & career section...
-----------------------------------------------------------------------------------> 

<!--/career  & current opportunity section...
----------------------------------------------------------------------------------->
<section id="opportunity" class="page-section bac_color_career">
  <div class="container-fluid ">
    <div id="curent_oppt"  class="container">
      <h1 class="heading-a border-theme-l">Current Opportunity</h1>
      <div class="spacer-mini2"></div>
      <div  class="row-fluid">
        <div class="span6 shadow-block">
          <div class="border pull-left thumb-right"> <a href="<?php echo site_url('mi/phpdevelopment');?>" class="shadow"> <img  src="img/career/Opp-Thumb/opp-thumb-1.png"> </a> </div>
          <a href="<?php echo site_url('mi/phpdevelopment');?>">
          <h3>PHP Developer : </h3>
          </a>
          <p>Build your career with us in the field of technology you are dreaming about</p>
          <span ><a href="<?php echo site_url('mi/phpdevelopment');?>" class="btn pull-left">More</a></span> </div>
        <div class="span6 shadow-block">
          <div class="border pull-left thumb-right"> <a href="<?php echo site_url('mi/webdesigner');?>" class="shadow"> <img  src="img/career/Opp-Thumb/opp-thumb-2.png"> </a> </div>
          <a href="<?php echo site_url('mi/webdesigner');?>">
          <h3>Web Designer :</h3>
          </a>
          <p>You can plan your career with us, that's the right place to make step for it </p>
          <span ><a href="<?php echo site_url('mi/webdesigner');?>" class="btn pull-left">More</a></span> </div>
      </div>
      <!---row-1---->
      <div class="spacer-mini"></div>
      <div  class="row-fluid">
        <div class="span6 shadow-block">
          <div class="border pull-left thumb-right"> <a href="<?php echo site_url('mi/businessdevelopmentofficer');?>" class="shadow"> <img  src="img/career/Opp-Thumb/opp-thumb-3.png"> </a> </div>
          <a href="<?php echo site_url('mi/businessdevelopmentofficer');?>">
          <h3>Business Development officer :</h3>
          </a>
          <p>If you have a talent then get the opportunity for being a part of our organization</p>
          <span ><a href="<?php echo site_url('mi/businessdevelopmentofficer');?>" class="btn pull-left">More</a></span> </div>
        <div class="span6 shadow-block">
          <div class="border pull-left thumb-right"> <a href="javascript:void(0)" class="shadow"> <img  src="img/career/Opp-Thumb/opp-thumb-1.png"> </a> </div>
          <a href="javascript:void(0)">
          <h3>Graphics UI Designer :</h3>
          </a>
          <p> ravida pellentesque urna varius vitae. Sed dui lorem, adipiscing </p>
          <span ><a href="javascript:void(0)" class="btn pull-left">More</a></span> </div>
      </div>
      <!---row-2----> 
      
    </div>
  </div>
</section>
<!--/opportunity & footer...
---------------------------------------------------------------------------------------------------------------------------->