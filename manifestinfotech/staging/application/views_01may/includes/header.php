<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" type="image/ico" href="assets/images/common-img/favicon.ico" alt="manifest Infotech"/>
<link href="https://plus.google.com/113088163173614486012" rel="publisher" />
<meta charset="utf-8">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta name="application-name" content="Software development company">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php echo $title; ?></title>
<meta name="description" content="<?php if($description!="")echo $description; ?>">
<meta name="keywords" content="Web Design, Web Development, SEO, Digital Marketing, IT Services, IT Solution">
<meta name="author" content="Manifest Infotech Pvt Ltd">

<base href="<?php echo base_url();?>">

<!-- CSS Files -->

<link rel="stylesheet" href="assets/css/bootstrap.css"/>
<link rel="stylesheet" href="assets/css/bootstrap-responsive.css"/>
<link rel="stylesheet" href="assets/css/manifest.css"/>
<link rel="stylesheet" href="assets/icon-font/css/font-awesome.css"/>
<link rel="stylesheet" href="assets/css/responsive.css"/>
<link rel="stylesheet" href="assets/css/style_common.css"/>
<link rel="stylesheet" href="assets/css/style10.css"/>
<link rel="stylesheet" href="assets/css/simpleslider.css"/>
<link rel="stylesheet" href="assets/css/slider.css"/>
<link rel="stylesheet" href="assets/css/flexslider.css">
<link rel="stylesheet" href="assets/css/submenu.css"/>
<link rel="stylesheet" href="assets/css/footernavigation.css">
<link rel="stylesheet" href="assets/css/developer.css">

<!-- End CSS Files -->



<script type="text/javascript" src="assets/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.js"></script>
<script type="text/javascript" src="assets/js/submenu.js"></script>
<script type="text/javascript" src="assets/js/function.js"></script>
<script src="assets/js/jquery.js"></script>
<script src="assets/js/jquery_003.js"></script>
<script src="assets/js/jquery.flexslider-min.js"></script>
<script type="text/javascript">
		$(window).load(function() {
			$('.flexslider').flexslider();
		});
		site_url="<?php echo site_url(); ?>";
</script>
<!--testmoniyal script--->
<script>
$(document).ready(function( ) {
var offsetHeight =100;
$('.nav-menu').scrollspy({
offset: offsetHeight
});


$('.nav li a').click(function (event) {
var scrollPos = $('body > #wrapper').find($(this).attr('href')).offset().top - offsetHeight;
$('body,html').animate({
scrollTop: scrollPos
}, 700, function () {
//$(".btn-navbar").click();
});
return false;
});
});
</script>
<script>
$(document).ready(function(){
	$('#banner').simpleSlider({ height: 400});
});
</script>


</script>
<script type="text/javascript">
	var $zoho= $zoho || {salesiq:{values:{},ready:function(){}}};
	var d=document;
	s=d.createElement("script"); s.type="text/javascript"; s.defer=true; s.src="https://salesiq.zoho.com/manifestinfotech/float.ls?embedname=manifestinfotech"; t=d.getElementsByTagName("script")[0]; t.parentNode.insertBefore(s,t);
</script>


</head>

<body  data-spy="scroll" data-target=".nav-menu" data-offset="300" >

<!-----------facebook share start --------------->
<div id="fb-root"></div>
<script>
	(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>
<!-----------facebook share end--------------->

<header class="top-fixed" id="navigation">
<div class="container-fluid top-social-block">
  <div class="container">
    <div class="pagetop visible-tablet visible-desktop">
      <ul  class="pull-right">
        <li><a href="mailto:info@manifestinfotech.com"><i class="fa fa-envelope"></i>&nbsp;info@manifestinfotech.com</a></li>
        <li><a href="JavaScript:Void(0)"><i class="fa fa-phone"></i>&nbsp;+91 9770368611</a></li>
        <li><a href="<?php echo site_url('mi/sitemap');?>">Site Map</a></li>
        <li class="hailight">
		   <a href="javascript:void(0)"  class="" data-toggle="modal" data-target="#myModal">Call To Action</a>
			<div id="myModal" class="modal fade popup_padding" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<button type="button" id="toggle" class="close cutumize_close" data-dismiss="modal" aria-hidden="true"> &times; </button>
				<div class="span6 padd_top_action">
					<div class="page-heading">
						<h3 class="">Send Us Message !</h3>
					</div>
					<div class="spacer-mini"><p id="popupsuccessmsg" class="successmsg"></p></div>
					<form class="">
						<div class="controls controls-row">
						  <input type="text" name='fname' id='fullname' onBlur='checkFnameValidation("fullname",1)' class="span3 colo_relal fa fa-user" placeholder="Full Name">

						  <input type="text" name='mail' id='email' value='' onBlur='checkEmailValidation("email",1)' class="span3 margin_left" placeholder="Email Address">

						  <input type="text" name='phone' id='telphone' onBlur='checkPhoneValidation("telphone",1)' class="span3 margin-null" placeholder="Phone Number">

						  <input type="text" name='subject' id='subject' class="span3" placeholder="Subject">
						</div>
						<div class="controls">
							<textarea id="message" name="msg" class="span6 colr_chage_all_field no_resize no_resize" placeholder="Your Message" rows="5"></textarea>
						</div>
						<div class="controls">
							<button id="contact-submit" onclick="send_msg('fullname','email','telphone','subject','message',1,'popupsuccessmsg')" type="button" class="color_back_button btn   pull-right">Send Now</button>
						</div>
					</form>
				</div>
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-body"> </div>
					</div>
				</div>
			</div>

          <script>
			function centerModal() 
			{
				$(this).css('display', 'block');
				var $dialog = $(this).find(".modal-dialog");
				var offset = ($(window).height() - $dialog.height()) / 2;
				// Center modal vertically in window
				$dialog.css("margin-top", offset);
			}

			$('.modal').on('show.bs.modal', centerModal);
			$(window).on("resize", function () {
				$('.modal:visible').each(centerModal);
			});

			</script> 
        </li>
      </ul>
      <ul class="pull-left social-icon">
		<li><a href="https://www.facebook.com/manifestinfotech" target="_blank" Title="Facebook"><i class="fa  fa-facebook"></i></a></li>
		<li><a href="https://twitter.com/ManifestInfotec" target="_blank" Title="Twitter"><i class="fa fa-twitter"></i></a></li>
		<li><a href="https://plus.google.com/+Manifestinfotech/about" target="_blank" Title="Google+"><i class="fa fa-google-plus"></i></a></li>
		<li><a href="https://www.linkedin.com/pub/manifest-infotech/6a/974/601" target="_blank" Title="Linkedin"><i class="fa  fa-linkedin"></i></a></li>
		<li><a href="https://www.pinterest.com/archananchoukse/" target="_blank" Title="Pinterest"><i class="fa fa-pinterest" target="_blank" Title="Pinterest"></i></a></li>
		<li>&nbsp;&nbsp;<div class="fb-like log_foooter" data-href="https://www.facebook.com/manifestinfotech" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div></li>
	  </ul>
	 </div>
    <!---/tablet and destop view------>
    
	<div class="dropdown visible-phone"> <a class="dropdown-toggle" id="" role="button" data-toggle="dropdown" data-target="JavaScript:Void(0)" href="/page.html"> <small>Social</small> <b class="caret"></b> </a>
	  <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
		<li><a href="mailto:info@manifestinfotech.com"><i class="fa fa-envelope"></i>&nbsp info@manifestinfotech.com</a></li>
		<li><a href="JavaScript:Void(0)"><i class="fa fa-phone"></i>&nbsp;</span>+919770368611</a></li>
		<li><a href="JavaScript:Void(0)">Site Map</a></li>
		<div class="divider"></div>
		<li><a href="https://www.facebook.com/manifestinfotech" target="_blank" Title="Facebook"><i class="fa  fa-facebook"></i></a></li>
		<li><a href="https://twitter.com/ManifestInfotec" target="_blank" Title="Twitter"><i class="fa fa-twitter"></i></a></li>
		<li><a href="https://plus.google.com/+Manifestinfotech/about" target="_blank" Title="Google+"><i class="fa fa-google-plus"></i></a></li>
		<li><a href="https://www.linkedin.com/pub/manifest-infotech/6a/974/601" target="_blank" Title="Linkedin"><i class="fa  fa-linkedin"></i></a></li>
		<li><a href="https://www.pinterest.com/archananchoukse/" target="_blank" Title="Pinterest"><i class="fa fa-pinterest" target="_blank" Title="Pinterest"></i></a></li>
		<a href="JavaScript:Void(0)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="assets/images/fb-icons1.png" class="log_foooter"/></a>
	  </ul>
	</div>
    <!---/mobile social block----> 
  </div>
</div>

<!--top social icon bar closed-->
<div class="inner navigation" >
	<a class="scroll" href="<?php echo site_url();?>">
		<img src="assets/images/common-img/logo.png" class="logo" alt="Manifest Infotech" title="Manifest Infotech">
	</a> 
	<!--logo closed & Nav Menu open... -->

	<div class="nav-menu">
      <ul class="nav main-nav navigate_menu">
		<!--<li id="mi-home" class="navigation_menu"><a class="scroll navi_menu" href="<?php echo site_url();?>">home</a></li>-->
 
	<!----------------------------------About Sub menu start------------------------------------>

        <li id="aboutus" class="dropdown mega-dropdown navigation_menu"> <a  data-rel="aboutmenu" class="dropdown-toggle navi_menu" data-toggle="dropdown" href="<?php echo site_url('aboutus');?>">overview</a>
          <!--<div class="dropdown-menu mega-dropdown-menu cutumize_menu size_cutomize ">
          <div class="container" style="max-width:100%;">
            <div  class="span3" style="max-width: 29%;">
              <div class="about_menu co_back_about">
                <ul>
                  <li class="About_bootum">About us</li>
                  <li><i class="fa fa-angle-down"></i><a href="#">Vision</a></li>
                  <li><i class="fa fa-angle-down"></i><a href="#">Misson</a></li>
                  <li><i class="fa fa-angle-down"></i><a href="#">About comany</a></li>
                </ul>
              </div>
            </div>
            <div class="span3" style="max-width: 29%">WHY US</div>
            <div class="span3" style="max-width: 29%;">
              <div class="about_menu">
                <ul>
                  <li class="About_bootum">PROCESS</li>
                  <li><i class="fa fa-angle-down"></i><a href="#">Agile</a></li>
                  <li><i class="fa fa-angle-down"></i><a href="#">Scrum</a></li>
                </ul>
              </div>
            </div>
          </div>-->
        </li>
        <!------------------------------------About sub menu ----------------------------> 


 <!--------------------------------Services Sub menu Start------------------------------------>
        
        <li id="mi-services" class="dropdown mega-dropdown navigation_menu"> <a  data-rel="aboutmenu" class="dropdown-toggle navi_menu" data-toggle="dropdown" href="<?php echo site_url('services');?>">services</a>
         <!--- <div class="dropdown-menu mega-dropdown-menu cutumize_menu">
          <div class="container" style="max-width: 100%;">
            <div class="span3 [class*="span"]" style="max-width: 22.33%;">
              <div class="[ info-card ]"><img style="min-width:100%;" src="assets/images/home-slider-img/web-design.png"/>
                <div class="[ info-card-details ] animate">
                  <div class="[ info-card-header ] algin_h1h3">
                    <h1> WEBDESIGN</h1>
                    <h3> aka not a real person study Loream isum </h3>
                  </div>
                  <div class="[ info-card-detail ]"> 
                   
                    <p>Services beauty is realizing that you are the beholder. This empowers us to find beauty in places where others have not dared to look including inside ourselves.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="span3 class_span" style="max-width: 22.33%;">
              <div class="[ info-card ]"><img style="min-width:100%;" src="assets/images/home-slider-img/web-development.png"/>
                <div class="[ info-card-details ] animate">
                  <div class="[ info-card-header ] algin_h1h3">
                    <h1> WEB DEVELOPMENT</h1>
                    <h3> aka not a real person study Loream isum </h3>
                  </div>
                  <div class="[ info-card-detail ]"> 
                   
                    <p>Services beauty is realizing that you are the beholder. This empowers us to find beauty in places where others have not dared to look including inside ourselves.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="span3 class_span" style="max-width: 22.33%;">
              <div class="[ info-card ]"><img style="min-width:100%;" src="assets/images/home-slider-img/monitor.png"/>
                <div class="[ info-card-details ] animate">
                  <div class="[ info-card-header ] algin_h1h3">
                    <h1> SEO</h1>
                    <h3> aka not a real person study Loream isum </h3>
                  </div>
                  <div class="[ info-card-detail ]"> 
                    
                    <p>Services beauty is realizing that you are the beholder. This empowers us to find beauty in places where others have not dared to look including inside ourselves.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="span3 class_span" style="max-width: 22.33%;">
              <div class="[ info-card ]"><img style="min-width:100%;" src="assets/images/home-slider-img/Digital.fw.png"/>
                <div class="[ info-card-details ] animate">
                  <div class="[ info-card-header ] algin_h1h3">
                    <h1>DIGITAL MARKETING</h1>
                    <h3> aka not a real person study Loream isum </h3>
                  </div>
                  <div class="[ info-card-detail ]"> 
                   
                    <p>Services beauty is realizing that you are the beholder. This empowers us to find beauty in places where others have not dared to look including inside ourselves.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>-->
        </li>
        <!---------------------------------servicess Sub menu closed--------------------------------> 


		<!-----------------------------Solutions Sub menu Start----------------------------------->
        
        <li id="solutions" class="dropdown mega-dropdown navigation_menu"> <a  data-rel="solutionmenu"  class="dropdown-toggle navi_menu" data-toggle="dropdown" href="<?php echo site_url('solutions');?>">Solutions</a>
          <!--<div class="dropdown-menu mega-dropdown-menu cutumize_menu">
          <div class="container"  style="max-width: 100%;">
            <div class="span3" style="max-width: 22.33%;">solution</div>
            <div class="span3" style="max-width: 22.33%;">solution</div>
            <div class="span3" style="max-width: 22.33%;">solution</div>
          </div>-->
        </li>
		

		<li id="portfolio" class="navigation_menu">
			<a class="scroll navi_menu" href="<?php echo site_url('portfolio');?>">Portfolio</a>
		</li>
        <li id="mi-blog" class="navigation_menu"><a class="scroll navi_menu" href="blog">Blog</a></li>
        <li id="testimonial" class="navigation_menu">
			<a class="scroll navi_menu" href="<?php echo site_url('mi/testimonial');?>">testimonial</a>
		</li>
        <li id="career" class="navigation_menu">
			<a class="scroll navi_menu" href="<?php echo site_url('mi/career');?>">Career</a>
		</li>
        <li id="contact-us" class="navigation_menu">
			<a class="scroll navi_menu" href="<?php echo site_url('mi/contact');?>">Contact</a>
		</li>
      </ul>
    </div>
    <!-- Dropdown Menu For Mobile Devices-->
    <div class="dropdown mobile-drop"> <a data-toggle="dropdown" class="mobile-menu" href="JavaScript:Void(0)"><i class="fa fa-bars"></i></a>
      <ul class="nav dropdown-menu fullwidth" role="menu">
        <li class="active"><a class="scroll" href="<?php echo site_url();?>">Home</a></li>
        <li><a class="scroll" href="<?php echo site_url('aboutus');?>">About Us</a></li>
        <li><a class="scroll" href="<?php echo site_url('services');?>">Services</a></li>
        <li><a class="scroll" href="<?php echo site_url('solutions');?>">Solutions</a></li>
        <li><a class="scroll" href="<?php echo site_url('portfolio');?>">Portfolio</a></li>
        <li><a class="scroll" href="blog">Blog</a></li>
        <li><a class="scroll" href="<?php echo site_url('mi/testimonial');?>">Testimonial</a></li>
        <li><a class="scroll" href="<?php echo site_url('mi/career');?>">Career</a></li>
        <li><a class="scroll" href="<?php echo site_url('mi/contact');?>">Contact Us</a></li>
      </ul>
    </div>
  </div>
</header>