<script>
	$('#mi-home a').css('background-color','#056ab2');
	$('#mi-home a').css('color','#FFF');
</script>

<div id="wrapper">
<section id="home" class="home-slider">
  <div class="container-fluid">
    <div class="container">
      <div class="simpleSlider">
        <div id="banner">
          <section id="first-slide">
            <div class="mainSlider" rel="one">
              <div class="sliderItem"> <a href="<?php echo site_url('services/web-design');?>"><img src="assets/images/home-slider-img/web-design.png" rel="0" class="monitor" alt="Web Design"></a></div>
            </div>
            <div class="subSlider">
              <div  class="sliderItem">
                <h3 class="envatoHeader animate0 rollOut"> Web Design </h3>
                <p class="envatoText animate1 rollOut"> Great design is all about making other person feel good and impressive </p>
                <p class="envatoText animate1 rollOut"><a href="<?php echo site_url('services/web-design');?>" class="btn-light btn-large"> More </a></p>
              </div>
            </div>
          </section>
          <section id="second-slide">
            <div class="mainSlider" rel="one">
              <div class="sliderItem"> <a href="<?php echo site_url('services/web-development');?>"><img src="assets/images/home-slider-img/web-development.png" rel="0" class="monitor" alt="Web Development"></a></div>
            </div>
            <div class="subSlider">
              <div  class="sliderItem">
                <h3 class="envatoHeader animate0 rollOut"> Web Development</h3>
                <p class="envatoText animate1 rollOut"> We are not just build a website we are building dreams </p>
                <p class="envatoText animate1 rollOut"><a href="<?php echo site_url('services/web-development');?>" class="btn-light btn-large"> More </a></p>
              </div>
            </div>
          </section>
          <section id="third-slide">
            <div class="mainSlider" rel="one">
              <div class="sliderItem"> <a href="<?php echo site_url('services/seo');?>"><img src="assets/images/home-slider-img/monitor.png" rel="0" class="monitor" alt="Search Engine Optimazation"></a></div>
            </div>
            <div class="subSlider">
              <div  class="sliderItem">
                <h3 class="envatoHeader animate0 rollOut"> SEO </h3>
                <p class="envatoText animate1 rollOut"> We convert our visitors into a clients </p>
                <p class="envatoText animate1 rollOut"><a href="<?php echo site_url('services/seo');?>" class="btn-light btn-large"> More </a></p>
              </div>
            </div>
          </section>
          <section id="second-slide">
            <div class="mainSlider" rel="one">
              <div class="sliderItem"> <a href="<?php echo site_url('services/digital-marketing');?>"><img src="assets/images/home-slider-img/Digital.fw.png" rel="0" class="monitor" alt="Digital Marketing"></a></div>
            </div>
            <div class="subSlider">
              <div  class="sliderItem">
                <h3 class="envatoHeader animate0 rollOut"> Digital Marketing </h3>
                <p class="envatoText animate1 rollOut"> It is boundless and vast area for promoting business</p>
                <p class="envatoText animate1 rollOut"><a href="<?php echo site_url('services/digital-marketing');?>" class="btn-light btn-large"> More </a></p>
              </div>
            </div>
          </section>
          <div style="opacity:1;" class="arrowButton">
            <div class="prevArrow"></div>
            <div class="nextArrow"> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--home section closed...
------------------------------------------------------------------------------>

<section id="about-home">
  <div class="container-fluid">
  <div class="container">
    <div class="row">
      <div class="border pull-left"> <a href="<?php echo site_url('services/web-design');?>" class="shadow span4 "><img src="assets/images/home-about/web-design.jpg" title="Web Design" alt="Web Design" /></a> </div>
      <div class="row">
				<div class="span7 pull-left process_grid">
					<h3 class="heading-b">Web Design</h3>
					<div class="pull-left">
						<ul class="check_ic">
							<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;Logo Design</a> </li>
							<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;Responsive Website Design </a> </li>
							<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;Content Management System Theme</a> </li>
						</ul>
					</div>
					<div class="pull-left span4">
						<ul class="check_ic">
							<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;Mobile Web Design</a> </li>
							<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;Custom Web Design</a> </li>
							<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;Mobile App Design</a> </li>
						</ul>
					</div>
				</div>
				<span class="pull-left span"><a href="<?php echo site_url('services/web-design');?>" class="btn">More</a></span>
			</div>	
    </div>
    <!--webdesign block---->
    <div class="spacer-mini"></div>
    <div class="row">
      <div class="border pull-right"> <a href="<?php echo site_url('services/web-development');?>" class="shadow span4 "><img src="assets/images/home-about/web-dev.jpg"  title="Web Development"  alt="Web Development"/></a> </div>
			<div class="row">
				<div class="span7 pull-left process_grid">
					<h3 class="heading-b">Web Development </h3>
					<div class="pull-left">
						<ul class="check_ic">
							<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;CMS  Development</a> </li>
							<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;PHP Development</a> </li>
							<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;ASP.NET Development</a> </li>
						</ul>
					</div>
					<div class="pull-left span5">
						<ul class="check_ic">
							<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;Mobile web Development</a> </li>
							<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;Mobile App Development</a> </li>
						</ul>
					</div>
				</div>
				<span class="pull-left span"><a href="<?php echo site_url('services/web-development');?>" class="btn">More</a></span>
			</div>
    </div>
    <!--webdesign block---->
    <div class="spacer-mini"></div>
    <div class="row">
      <div class="border pull-left"> <a href="<?php echo site_url('services/seo');?>" class="shadow span4"><img src="assets/images/home-about/seo.jpg" title="Search Engine Optimazation" alt="Search Engine Optimazation" /></a> </div>
			<div class="row">
				<div class="span7 pull-left process_grid">
					<h3 class="heading-b">SEO</h3>
					<div class="pull-left">
						<ul class="check_ic">
							<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;On Page & Off Page Optimization</a> </li>
							<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;Organic/Inorganic optimization </a> </li>
							<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;Link Building </a> </li>
						</ul>
					</div>
					<div class="pull-left span4">
						<ul class="check_ic">
							<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;Social Sharing </a> </li>
							<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;Creating Sitemaps</a> </li>
							<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;Maintaining & Editing Website</a> </li>
						</ul>
					</div>
				</div>
				<span class="pull-left span"><a href="<?php echo site_url('services/seo');?>" class="btn">More</a></span>
			</div>
    </div>
      <!----------------------- SEO--------------------->
      <div class="spacer-mini"></div>
      <div class="row">
        <div class="border pull-right"> <a href="<?php echo site_url('services/digital-marketing');?>" class="shadow span4 "><img src="assets/images/home-about/Digi_mar.jpg"  title="Web Development" alt="Digital Marketing"/></a> </div>
				<div class="row">
					<div class="span7 pull-left process_grid">
						<h3 class="heading-b"> Digital Marketing </h3>
						<div class="pull-left">
							<ul class="check_ic">
								<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;&nbsp;Search Engine Optimization </a> </li>
								<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;&nbsp;Social Media Marketing</a> </li>
								<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;&nbsp;Social Media Optimization </a> </li>
							</ul>
						</div>
						<div class="pull-left span4">
							<ul class="check_ic">
								<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;&nbsp;Pay Per Click</a> &nbsp;&nbsp;&nbsp;</li>
								<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;&nbsp;Reputation Management &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a> </li>
								<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;&nbsp;Email Marketing &nbsp;&nbsp;&nbsp;</a> </li>
							</ul>
						</div>
					</div>
					<span class="pull-left span"><a href="<?php echo site_url('services/digital-marketing');?>" class="btn">More</a></span>
				</div>
      </div>
    </div>
	</div>
 </div> 
</section>
<!--- - - - -  - - -  - -  -about us section closed...- - - - - - - - - - - - --->

<section id="portfolio-home">
  <div class="container-fluid">
    <div class="container">
      <div class="page-heading">
		<a href="<?php echo site_url('portfolio');?>"><h3>FEATURED PORTFOLIO</h3></a>
        <h4>We are Glaring and Prominent in our work</h4>
        <p> We showcase our work which are done with full dedication and hardwork. Our designer team make an effort to show our portfolio better than the best </p>
      </div>
    </div>
    <!--page heading closed-->
    <div class="container">
      <div class="pagination portfolio-type">
        <ul class="filter nav nav-pills">
          <li class="active" data-value="all"><a href="javascript:void(0)">All</a></li>
          <li data-value="1"><a href="javascript:void(0)">Design</a></li>
          <li data-value="2"><a href="javascript:void(0)">Development</a></li>
          <li data-value="3"><a href="javascript:void(0)">SEO</a></li>
        </ul>
      </div>
      <ul class="thumbnails">
        <?php foreach($all_project_data as $project)
		{
			$dashedProjectName = strtolower(str_replace(' ','-',$project['project_name']));
			$thumbPath="assets/images/portfolio/".$dashedProjectName;

		?>
        <li data-type="<?php echo $project['category_id'];?>" data-id="1" class="span4 view view-tenth"> 
			<img src="<?php echo $thumbPath.'/'.$project['thumb'];?>"  alt="<?php echo strtolower($project['project_name']);?>">
			<div class="mask">
				<h2><?php echo strtoupper($project['project_name']);?></h2>
				<p> <?php echo $project['project_one_liner'];?> </p>
				<a href="<?php echo site_url('portfolio/project/'.$project['project_id'].'/'.$dashedProjectName);?>" class="btn-light">More</a> 
			</div>
        </li>
		<?php
		}
		?>
      </ul>
    </div>
  </div>
</section>

<!----------------------process home-------------------------------------->

<section class="page-section theme-bg-gray">
	<div class="container-fluid">
		<div class="container">
		<div class="page-heading">
			<h3>PROCESS</h3>
			<h4>We keep up transparency policy in our network</h4>
			<p class="mar_bottom">Our process methodology build a smooth communication path for clients as well as our team members</p>
		</div>
      
		<div class="row">
        <div class="border pull-left"> <a href="JavaScript:Void(0)" class="shadow span5 ">
			<img src="assets/images/process/agile.jpg" title="Agile Model" alt="Agile Model"/></a> 
		</div>
		<div class="span6 pull-right process_grid">
			<h2 class="heading-b">Agile Model</h2>
			<ul class="check_ic">
				<div class="menu_left">
					<ul class="check_ic">
						<li> <a href="JavaScript:Void(0)"><i class="fa fa fa-check"></i>&nbsp;&nbsp;Requirement Gathering</a> </li>
						<li> <a href="JavaScript:Void(0)"><i class="fa fa fa-check"></i>&nbsp;&nbsp;Analyze and Planning </a> </li>
						<li> <a href="JavaScript:Void(0)"><i class="fa fa fa-check"></i>&nbsp;&nbsp;Design and HTML</a> </li>
						<li> <a href="JavaScript:Void(0)"><i class="fa fa fa-check"></i>&nbsp;&nbsp;Development and Implementation</a> </li>
					<a href="<?php echo site_url('blog/agile-methodology/');?>" class="btn pull-left mar_top">More</a>
					</ul>
				</div>
           
				<div class="menu_left pull-right">
					<ul class="check_ic">
						<li> <a href="JavaScript:Void(0)"><i class="fa fa fa-check"></i>&nbsp;&nbsp;QA and Debugging </a> </li>
						<li> <a href="JavaScript:Void(0)"><i class="fa fa fa-check"></i>&nbsp;&nbsp;Deployment and Maintenance  </a> </li>
						<li> <a href="JavaScript:Void(0)"><i class="fa fa fa-check"></i>&nbsp;&nbsp;Documentation of Changes</a> </li>
						<li> <a href="JavaScript:Void(0)"><i class="fa fa fa-check"></i>&nbsp;&nbsp;Drafting Changes </a> </li>
					</ul>
				</div>
			</ul>
		</div>
	</div>
      <!--webdesign block---->
     
      <!--webdesign block---->
	<div class="spacer-mini"></div>
		<div class="row">
			<div class="border pull-left"> <a href="JavaScript:Void(0)" class="shadow span5">
				<img src="assets/images/process/Scraum.png" title="Scrum Model" alt="Scrum Model"/></a> 
			</div>
			<div class="span6 pull-right process_grid">
				<h3 class="heading-b">Scrum Model</h3>
				<div class="menu_left">
					<ul class="check_ic">
						<li> <a href="JavaScript:Void(0)"><i class="fa fa fa-check"></i>&nbsp;&nbsp;Planning for Sprint Meeting</a> </li>
						<li> <a href="JavaScript:Void(0)"><i class="fa fa fa-check"></i>&nbsp;&nbsp;Scrum Meeting	</a> </li>
						<a href="<?php echo site_url('blog/scrum-model/');?>" class="btn pull-left mar_top">More</a>
					</ul>
				</div>
				<div class="menu_left pull-right">
				<ul class="check_ic">
					<li> <a href="JavaScript:Void(0)"><i class="fa fa fa-check"></i>&nbsp;&nbsp;Sprint Review </a> </li>
					<li> <a href="JavaScript:Void(0)"><i class="fa fa fa-check"></i>&nbsp;&nbsp;Sprint Retrospective</a> </li>
				</ul>
			</div>
		</div>
      <!----------------------- SEO--------------------->
	</div>

</div>
</section>

<!--------------------------------process home closed---------------------------> 


 <!---portfolio style end here..----------> 
  
  <!-------------------------------- Testimonial carusel start----------------------------------->

	<?php include 'Testimonials-carousel.php'; ?>

  <!---------------------------------- Testimonial section closed-------------------------------->
  
  <!-------------------------------- Contact start ----------------------------------->

	<?php include 'contact.php'; ?>
  
 <!-------------------------------- Contact End ----------------------------------->


  