<section id="" class="page-section">
  <div class="container-fluid">
    <div class="container">
      <div class="page-heading">
        <h3>TESTIMONIALS</h3>
        <h4>We venerate our clients and their feedback</h4>
		<P>Our client and imparting their experience with us is more valuable for our company</P>
      </div>
      
      <!--page heading-->
      <div class="well">
        <div id="myCarousel" class="carousel slide">
          <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
          </ol>
          
          <!-- Carousel items -->
          <div class="carousel-inner">
            <div class="item active">
              <div class="row-fluid">
				<div class="span2 border"><a href="testimonial.htm" class=" "><img src="img/avatar-4.jpg" border="0"class="rou_images log_foooter bor_fade" /></a></div>
				<div style="">
					<h3 class="top_margin">Client Name</h3>
					<p class="page-desc" style="text-align:left;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi</p>
				  </div>
              </div>
              <!--/row-fluid--> 
            </div>
            <!--/item-->
            
            <div class="item">
              <div class="row-fluid">
				<div class="span2 border"><a href="testimonial.htm" class=" "><img src="img/avatar-6.jpg" border="0" class="rou_images log_foooter bor_fade"/></a></div>
				<div style="">
					<h3 class="top_margin">Client Name</h3>
					<p class="page-desc" style="text-align:left;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi</p>
				  </div>
              </div>
            </div>

			<div class="item">
              <div class="row-fluid">
				<div class="span2 border"><a href="testimonial.htm" class=" "><img src="img/avatar-2.jpg" border="0" class="rou_images log_foooter bor_fade"/></a></div>
				<div style="">
					<h3 class="top_margin" >Client Name</h3>
					<p class="page-desc" style="text-align:left;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus. doloremque</p>
				  </div>
              </div>
            </div>
            <!--/item--> 
            
          </div>
          <!--/carousel-inner--> 
        </div>
        <!--/myCarousel-->
        
        <div id="myCarouselPhone" class="carousel slide visible-phone"> 
          <!-- Carousel items -->
          <div class="carousel-inner">
            </div>
          </div
      ></div>
      <!--/well--> 
    </div>
  </div>
</section>

<script>
$('#myCarousel').carousel({
    interval: 3000
});
</script>