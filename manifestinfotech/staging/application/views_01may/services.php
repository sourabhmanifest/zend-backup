<script>
	$('#mi-services a').css('background-color','#056ab2');
	$('#mi-services a').css('color','#FFF');
</script>

<section class="page-cover services-cover-img">
  <div class="container-fluid">
    <div class="container">
      <div class="page-heading">
        <h3>SERVICES</h3>
        <h4>Our services make us different in industry</h4>
        <p> Our services shows our enthusiasm in our work and value clients thoughts. Our services performance make clients stay long with us </p>
      </div>
    </div>
  </div>
</section>
<!-----top navigations section closed here...

<!--services end here about start ....
--------------------------------------------------------------------------------------------------------------------------> 
<!--------------------Web Design start------------------------------------>
<section class="page-section coo_otr" >
<div  class="container-fluid">
	<div class="container">
		<div class="page-heading">
			
			<a href="<?php echo site_url('services/web-design');?>"><h3>Web Design</h3></a>
			<h4> Great design is all about making other person feel good and impressive </h4>
		</div>
		<div class="spacer-mini"></div>
		<div class="container">
			<div class=" row-fluid">
				<div class="span12 process_grid">
					<div class="border pull-left thumb-right"> 
						<a href="<?php echo site_url('services/web-design');?>" class="shadow"> <img  width="250" src="assets/images/home-slider-img/web-design1.png" title="About Us"> </a>
					</div>
					<div class="row">
						<div class="span9">
							<div class="span6 pull-left margin_sub_services">
								<ul class="check_ic">
									<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;Logo Design</a></li>
									<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;Responsive Website Design </a> </li>
									<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;Content Management System Theme</a> </li>
								</ul>
							</div>
							<div class="span6 pull-left margin_sub_services">
								<ul class="check_ic">
									<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;Mobile Web Design</a> </li>
									<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;Custom Web Design</a> </li>
									<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;Mobile App Design</a> </li>
								</ul>
							</div>
						</div>
						<span><a href="<?php echo site_url('services/web-design');?>"class="btn pull-left">More</a></span>
						<div>
							<div class="span9">
								<p class="justify">We are Manifest Infotech make delightful and eye pleasing web design for your business.
								In our web design unit we provide Logo Design, Web Design, Mobile app design, Template design,
								Graphic Design as per requirements of our clients. It is so important for your business to show
								your work over it. We are focus on client's brand and business goal. We are always 
								emphasis on quality work, innovative design, and flexibility of our work. </p>
							</div>
						</div>
					</div>
					
					
					
				</div>
			</div>
		</div>
	</div>  
  </div>
  
  <!--------------------Web Design closed------------------------------------> 
  
  <!--------------------Web Development start------------------------------------> 
  
</section>
<section class="page-section">
<div  class="container-fluid">
	<div class="container">
		<div class="page-heading">
			<a href="<?php echo site_url('services/web-development');?>"><h3>Web Development</h3></a>
			<h4>We are not just build a website... we are building dreams</h4>
		</div>
		<div class="spacer-mini"></div>
		
		<div class="container">
			<div class=" row-fluid">
				<div class="span12 process_grid">
					<div class="border pull-left thumb-right"> 
						<a href="<?php echo site_url('services/web-development');?>" class="shadow"> <img  width="250" src="assets/images/home-slider-img/web-developments.png" title="About Us"> </a> 
					</div>
					<div class="row">
						<div class="span9">
							<div class="span6 pull-left margin_sub_services">
								<ul class="check_ic">
									<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;CMS  Development</a> </li>
									<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;PHP Development</a> </li>
									<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;ASP.NET Development</a> </li>
								</ul>
							</div>
							<div class="span6 pull-left butt_left margin_sub_services">
							  <ul class="check_ic">
								<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;Mobile web Development</a> </li>
								<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;Mobile web Development</a> </li>
							  </ul>
							</div>
						</div>	
						<span><a href="<?php echo site_url('services/web-development');?>" class="btn pull-left">More</a></span>
						<div>
							<div class="span9">
								<p class="justify">Manifest infotech having professional team of web development for better creation and 
								experience of website. We are facilitate our client with the CMC development, PHP development,
								Mobile websites development etc. web development is quite important for the growth of business. 
								With the improvement of business website must be looks dynamic and innovative along with that it
								must be responsive and user friendly. Web development services including the surety of proper 
								testing, clear and smooth work flow, and latest technologies used.</p>
							</div>
						</div>
					</div>			
				</div>
			</div>
		</div>
	</div>
 </div>
  <!--------------------Web Development closed------------------------------------> 
  
  <!--------------------Web Seo start------------------------------------> 
</section>
<section class="page-section coo_otr">
<div  class="container-fluid">
	<div class="container">
		<div class="page-heading">
			
			<a href="<?php echo site_url('services/seo');?>"><h3>SEO</h3></a>
			<h4>We convert our visitors into a clients</h4>
		</div>
		<div class="spacer-mini"></div>
		<div class="container">
			<div class=" row-fluid">
				<div class="span12 process_grid">
					<div class="border pull-left thumb-right"> 
						<a href="<?php echo site_url('services/seo');?>" class="shadow"> <img  width="250" src="assets/images/home-slider-img/monitors.png" title="About Us"> </a> 
					</div>
					<div class="row">
						<div class="span9">
							<div class="span6 pull-left margin_sub_services">
								<ul class="check_ic">
									<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;On Page & Off Page Optimization</a> </li>
									<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;Organic/Inorganic optimization </a> </li>
									<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;Link Building </a> </li>
								</ul>
							</div>
							<div class="span6 pull-left margin_sub_services">
								<ul class="check_ic">
									<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;Social Sharing </a> </li>
									<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;Creating Sitemaps</a> </li>
									<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;Maintaining & Editing Website</a> </li>
								</ul>
							</div>
						</div>
						<span><a href="<?php echo site_url('services/seo');?>" class="btn pull-left">More</a></span>
						<div>
							<div class="span9">
								<p class="justify">Manifest infotech having professional team of web development for better creation and 
								experience of website. We are facilitate our client with the CMC development, PHP development,
								Mobile websites development etc. web development is quite important for the growth of business. 
								With the improvement of business website must be looks dynamic and innovative along with that it
								must be responsive and user friendly. Web development services including the surety of proper 
								testing, clear and smooth work flow, and latest technologies used.</p>
							</div>
						</div>						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
  <!--------------------Web seo closed-----------------------------------> 
  
  <!--------------------Web Digital start------------------------------------> 
</section>
<section class="page-section">
<div  class="container-fluid">
	<div class="container">
		<div class="page-heading">
			<a href="<?php echo site_url('services/digital-marketing');?>"><h3>Digital Marketing</h3></a>
			<h4>It is boundless and vast area for promoting business</h4>
		</div>
		<div class="spacer-mini"></div>
		<div class="container">
			<div class=" row-fluid">
				<div class="span12 process_grid">
					<div class="border pull-left thumb-right"> 
						<a href="<?php echo site_url('services/digital-marketing');?>" class="shadow"> <img  width="250" src="assets/images/home-slider-img/Digital.png" title="About Us"> </a> 
					</div>
					<div>
						<div class="span5 pull-left margin_sub_services">
							<ul class="check_ic">
								<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;Search Engine Optimization </a> </li>
								<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;Social Media Marketing</a> </li>
								<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;Social Media Optimization </a> </li>
							</ul>
						</div>
						<div class="span4 pull-left margin_sub_services">
							<ul class="check_ic">
								<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;Pay Per Click</a></li>
								<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;Reputation Management </a> </li>
								<li> <a href="javascript:void(0)"><i class="fa fa-check"></i>&nbsp;Email Marketing</a> </li>
							</ul>
						</div>
					</div>
					<span class="pull-left"><a href="<?php echo site_url('services/digital-marketing');?>" class="btn">More</a></span>
					<div>
						<div class="span9">
							<p class="justify"> Digital marketing now a days playing role of blood for the website. We are creating digital marketing strategy for
							the clients website for better improvement of the business. With the changing scenario we provides various services under
							digital marketing like SEO, SMM, SMO, PPC, Reputation management, Email marketing. Our professional expert having experience
							in making strategy for marketing for every domain we serve. Digital marketing may affected by social networking, search and
							channel marketing etc.</p>
						</div>	
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>
</div>
</section>
