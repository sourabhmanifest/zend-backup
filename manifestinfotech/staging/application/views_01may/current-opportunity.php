<!--/page cover & career section...
-----------------------------------------------------------------------------------> 

<!--/career  & current opportunity section...
----------------------------------------------------------------------------------->
<section id="opportunity" class="page-section bac_color_career">
  <div class="container-fluid ">
    <div id="curent_oppt"  class="container">
      <h1 class="heading-a border-theme-l">Current Opportunity</h1>
      <div class="spacer-mini2"></div>
      <div  class="row-fluid">
        <div class="span6 shadow-block">
          <div class="border pull-left thumb-right"> <a href="<?php echo site_url('mi/php-development');?>" class="shadow"> <img  src="assets/images/career/phpdev.jpg"> </a> </div>
          <a href="<?php echo site_url('mi/php-development');?>">
          <h3>PHP Developer : </h3>
          </a>
          <p>Build your career with us in the field of technology you are dreaming about</p>
          <span ><a href="<?php echo site_url('mi/php-development');?>" class="btn pull-left">More</a></span> </div>
        <div class="span6 shadow-block">
          <div class="border pull-left thumb-right"> <a href="<?php echo site_url('mi/web-designer');?>" class="shadow"> <img  src="assets/images/career/designer.jpg"> </a> </div>
          <a href="<?php echo site_url('mi/web-designer');?>">
          <h3>Web Designer :</h3>
          </a>
          <p>You can plan your career with us, that's the right place to make step for it </p>
          <span ><a href="<?php echo site_url('mi/web-designer');?>" class="btn pull-left">More</a></span> </div>
      </div>
      <!---row-1---->
      <div class="spacer-mini"></div>
      <div  class="row-fluid">
        <div class="span6 shadow-block">
          <div class="border pull-left thumb-right"> <a href="<?php echo site_url('mi/business-development-officer');?>" class="shadow"> <img  src="assets/images/career/bdevelopment.jpg"> </a> </div>
          <a href="<?php echo site_url('mi/business-development-officer');?>">
          <h3>Business Development officer :</h3>
          </a>
          <p>If you have a talent then get the opportunity for being a part of our organization</p>
          <span ><a href="<?php echo site_url('mi/business-development-officer');?>" class="btn pull-left">More</a></span> </div>
        <div class="span6 shadow-block">
          <div class="border pull-left thumb-right"> <a href="<?php echo site_url('mi/project-manager');?>" class="shadow"> <img  src="assets/images/career/projectmg.jpg"> </a> </div>
          <a href="<?php echo site_url('mi/project-manager');?>">
          <h3>Project Manager :</h3>
          </a>
          <p> Our Company provide opportunity for the best Management skills </p>
          <span ><a href="<?php echo site_url('mi/project-manager');?>" class="btn pull-left">More</a></span> </div>
      </div>
      <!---row-2----> 
      
    </div>
  </div>
</section>
<!--/opportunity & footer...
---------------------------------------------------------------------------------------------------------------------------->