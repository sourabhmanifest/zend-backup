<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" type="image/ico" href="img/common-img/favicon.ico"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Testimonial</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- CSS Files -->

<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/bootstrap-responsive.css">
<link rel="stylesheet" href="css/manifest.css">
<link rel="stylesheet" href="icon-font/css/font-awesome.css">
<link rel="stylesheet" href="css/responsive.css">
<!-- End CSS Files -->
<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script>
</head>
<body>
<section class="page-cover testimonial-cover-img">
  <div class="container-fluid">
    <div class="container">
      <div class="page-heading">
        <h3>Testimonial</h3>
        <h4>We help your make e marketing esier.</h4>
        <p> Contrary to popular belief, Lorem Ipsum is not simply random text. 
          It has roots in a piece of classical Latin literature from 45 BC, making
          it over 2000 years old. </p>
      </div>
    </div>
  </div>
</section>
<!--/page cover & testimonail section...
----------------------------------------------------------------------------------->
<section id="testimonial" class="page-section theme-bg-gray">
  <div class="container-fluid">
    <div class="container">
      <h1 class="heading-a border-theme-l"> What people are saying ? </h1>
      <div  class="row-fluid shadow-block">
        <div class="span12">
          <div class="border pull-left thumb-right"> <a href="#" class="shadow"> <img  class="testimonia-thumb" src="img/testimonial/testimonial-3.png"  width="200" class="span4"/> </a> </div>
          <h4>John Deo.</h4>
          <p><span class="qautes"></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing 
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing 
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing 
        </div>
      </div>
      <!---1testimonial---->
      <div class="spacer-mini"></div>
      <div  class=" row-fluid shadow-block">
        <div class="span12">
          <div class="border pull-left thumb-right"> <a href="#" class="shadow"> <img class="testimonia-thumb"  src="img/testimonial/testimonial-4.png"> </a> </div>
          <h4>John Deo.</h4>
          <p><span class="qautes"></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing 
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing 
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing 
        </div>
      </div>
      <!---2testimonial---->
      <div class="spacer-mini"></div>
      <div  class=" row-fluid shadow-block">
        <div class="span12">
          <div class="border pull-left thumb-right"> <a href="#" class="shadow"> <img class="testimonia-thumb"  src="img/testimonial/testimonial-5.png"> </a> </div>
          <h4>John Deo.</h4>
          <p><span class="qautes"></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing 
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing 
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing 
        </div>
      </div>
      <!---3testimonial---->
      <div class="spacer-mini"></div>
      <div  class="row-fluid shadow-block">
        <div class="span12">
          <div class="border pull-left thumb-right"> <a href="#" class="shadow"> <img  class="testimonia-thumb"  src="img/testimonial/testimonial-6.png"> </a> </div>
          <h4>John Deo.</h4>
          <p><span class="qautes"></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing 
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing 
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing 
        </div>
      </div>
    </div>
  </div>
</section>
<!---4testimonial----> 
