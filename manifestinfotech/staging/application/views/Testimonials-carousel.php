<section class="page-section">
	<div class="container-fluid">
	    <div class="container">
			<div class="page-heading">
				<a href="http://<?php echo $_SERVER['HTTP_HOST'];?>/mi/testimonial"><h3>TESTIMONIALS</h3></a>
	        	<h4>We venerate our clients and their feedback</h4>
				<P>Our client and imparting their experience with us is more valuable for our company</P>
	      	</div>
	      
			<div class="well">
				<div id="myCarousel" class="carousel slide">
					<ol class="carousel-indicators margin_top_testmonial">
						<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
						<li data-target="#myCarousel" data-slide-to="1"></li>
						<li data-target="#myCarousel" data-slide-to="2"></li>
					</ol>
					<div class="carousel-inner">
						<div class="item active">
							<div class="row-fluid">
								<div class="span2 border">
									<div class="page-heading">
										<a href="JavaScript:Void(0)" class=" ">
											<img src="http://<?php echo $_SERVER['HTTP_HOST'];?>/assets/images/testimonial/Kayode.png" class="rou_images log_foooter bor_fade" alt="Kayode"/>
										</a>
									</div>
								</div>
								<p class="page-desc" style="text-align:left;">"The price is right at all level for service offers, but the best thing is your technical support. Whenever I need help or made mistakes or damage something, your team are with me to fix it. I hope to see them for all the coming year"</p>
							</div>
							<div class="text-center center-block">
								<h4>- Kayode -</h4>
								<br />
								<!--<a href="JavaScript:Void(0)"><i id="social" class="fa fa-facebook-square fa-2x social-fb"></i></a>
								<a href="JavaScript:Void(0)"><i id="social" class="fa fa-linkedin-square fa-2x social-in"></i></a>-->
							</div>
						</div>
						<div class="item">
							<div class="row-fluid">
								<div class="span2 border">
									<div class="page-heading">
										<a href="JavaScript:Void(0)" class=" ">
											<img src="http://<?php echo $_SERVER['HTTP_HOST'];?>/assets/images/testimonial/Maria.png" class="rou_images log_foooter bor_fade" alt="Maria"/>
										</a>
									</div>
								</div>
								<p class="page-desc" style="text-align:left;">"Thank you very much to create a website that truly shows my company motto. My site is now clear, clean and concise.  Your company is doing very great job. All the best for future" </p>
							</div>
							<div class="text-center center-block">
								<h4>- Maria -</h4>
								<br />
								<!--<a href="JavaScript:Void(0)"><i id="social" class="fa fa-facebook-square fa-2x social-fb"></i></a>
								<a href="JavaScript:Void(0)"><i id="social" class="fa fa-linkedin-square fa-2x social-in"></i></a>-->
							</div>
						</div>
						<div class="item">
							<div class="row-fluid">
								<div class="span2 border">
									<div class="page-heading">
										<a href="JavaScript:Void(0)" class=" ">
											<img src="http://<?php echo $_SERVER['HTTP_HOST'];?>/assets/images/testimonial/Rued.png" class="rou_images log_foooter bor_fade" alt="Rued"/>
										</a>
									</div>
								</div>
								<p class="page-desc" style="text-align:left;"> "My experience with Manifest Infotech is amazing. I proud to have such a beautiful & responsive website, from the responsive team. This team is experienced, knowledgeable, skilled and patient. I can't wait to start new project with them"</p>
							</div>
							<div class="text-center center-block ">
								<h4>- Rued -</h4>
								<br />
								<!--<a href="JavaScript:Void(0)"><i id="social" class="fa fa-facebook-square fa-2x social-fb"></i></a>
								<a href="JavaScript:Void(0)"><i id="social" class="fa fa-linkedin-square fa-2x social-in"></i></a>-->
							</div>
						</div>
					</div>
				</div>
				<div id="myCarouselPhone" class="carousel slide visible-phone"> 
					<div class="carousel-inner"> </div>
				</div>     
	        <!--/carousel-inner--> 
	        </div>
	        <!--/myCarousel
	        
	        <div id="myCarouselPhone" class="carousel slide visible-phone"> -->
	          <!-- Carousel items 
	        	<div class="carousel-inner"></div>
	        </div>-->
		</div>
	      <!--/well--> 
	</div>
</section>
<script>
$('#myCarousel').carousel({
    interval: 3000
});
</script>
