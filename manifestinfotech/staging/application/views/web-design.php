<!--Heading Start-->
<section class="container-fluid page-cover portfolio-cover-img">
<div class="container-fluid">
    <div class="container">
        <div class="page-heading">
            <h3>Web Design </h3>
            <h4>Great design is all about making other person feel good and impressive. </h4>
         <!--<p> Contrary to popular belief, Lorem Ipsum is not simply random text. 
              It has roots in a piece of classical Latin literature from 45 BC, making
              it over 2000 years old. </p>-->
        </div>
	</div>
</div>
</section>
<!--Heading closed-->

<!--webdesign block----> 


<section class="page-section coo_otr" >
<div  class="container-fluid">
    <div class="container">
		<div class="page-heading"><h3>Logo Design</h3></div>
		<div class="spacer-mini"></div>
		<div class="container">
			<div class=" row-fluid">
				<div class="span12 process_grid">
					<div class="border pull-left thumb-right">
						<a href="JavaScript:Void(0)" class="shadow"> 
							<img width="250"src="assets/images/detail-pages-images/webdesign/logo.jpg" title="About Us">
						</a>
					</div>
					<p class="webdesin_prg justify">Any logo of company is the identity for their products and services. We are creating logo that makes your business more visible and attractive.</p>
					<ul class="bullet_detail check_ic check_right span8 margin_left_ul">
						<li class="li_cheak">We are having an expert and creative team which give shape to your requirement and desire.</li>
						<li class="li_cheak">We design your logo with high quality and original look that present your business in market. If you are not clear what you want and what's your
							logo ideas then come and discuss with our team.</li> 										
						<li class="li_cheak">We provide you an endless solution for your website.</li>
					</ul>
				</div>	
			</div>
		</div>
	</div>
</div>	
</section>
<section class="page-section " >
<div  class="container-fluid">
    <div class="container">
		<div class="page-heading"><h3>Responsive Website Design</h3></div>
		<div class="spacer-mini"></div>
		<div class="container">
			<div class=" row-fluid">
				 <div class="span12 process_grid">
					<div class="border pull-right thumb-left">
						<a href="JavaScript:Void(0)" class="shadow"> 
							<img  width="250" src="assets/images/detail-pages-images/webdesign/responsive.jpg" title="About Us">
						</a> 
					</div>
					<p class="webdesin_prg justify">We create web page which responsive for	every device like desktop, laptop, and iPod. We have an expert and yearly	experienced web designer team for making an exclusive and systematic website for our client. Responsive website having following feature:</p>
					<ul class="bullet_detail check_ic check_right span8 margin_left_ul">
						<li class="li_cheak">There is no need to make separate website for small devices.</li>
						<li class="li_cheak">There are user friendly because no need to scroll vertically or horizontally.</li>
						<li class="li_cheak">Responsive website is very affordable and effective.</li>
					</ul>
        	 	</div>
			</div>
		</div>
	</div>
</div>
</section>
<section class="page-section coo_otr" >
<div  class="container-fluid">
	<div class="container">
		<div class="page-heading"><h3>Content Management System Theme</h3></div>
		<div class="spacer-mini"></div>
		<div class="container">
			<div class=" row-fluid">
				<div class="span12 process_grid">
					<div class="border pull-left thumb-right"
						<a href="JavaScript:Void(0)" class="shadow"> 
							<img  width="250" src="assets/images/detail-pages-images/webdesign/cms.jpg" title="About Us"> 
						</a> 
					</div>
					<p class="webdesin_prg justify">For the better presentation of website CMS themes and template are the best option for use.Theme	and template of CMS are available and also customized by web designer.</p>
					<ul class="bullet_detail check_ic check_right span8 margin_left_ul">
						<li class="li_cheak">We offer various customized theme and designs after analyzing and complete study of business background.</li>
						<li class="li_cheak">We provide CMS themes having authentic design which give unique and personalized view for website.</li>
						<li class="li_cheak">We also support website after completing the work as technical support and maintenance.</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
</section>
<section class="page-section">
<div class="container-fluid">
    <div class="container">
		<div class="page-heading"><h3>Mobile web Design</h3></div>
		<div class="spacer-mini"></div>
		<div class="container">
			<div class=" row-fluid">
				<div class="span12 process_grid">
					<div class="border pull-right thumb-left"> 
						<a href="JavaScript:Void(0)" class="shadow"> 
							<img  width="250" src="assets/images/detail-pages-images/webdesign/moresp.jpg" title="About Us"> 
						</a>  
					</div>
					<p class="webdesin_prg justify">As of now mobile is most using device everywhere. We are creating mobile friendly website which responsive for the mobile devices like iPods, Smartphone etc.</p>
					<ul class="bullet_detail check_ic  check_right span8 margin_left_ul">
						<li class="li_cheak">We provides mobile site for user with the quality of high definition display and no scrolling page. </li>
						<li class="li_cheak">Our mobile websites are fully tested with exclusive experience for our client and their user. </li>
						<li class="li_cheak">We have Very capable and experience designer team which produce a website beyond the expectation of our clients.</li>
					</ul>
       			</div>         
      		</div>
   		</div>
	</div>
</div>
</section>
<section class="page-section coo_otr">
<div  class="container-fluid">
    <div class="container">
			<div class="page-heading"><h3>Custom Web Design</h3></div>
			<div class="spacer-mini"></div>
			<div class="container">
			<div class=" row-fluid">
				<div class="span12 process_grid">
					<div class="border pull-left thumb-right"> 
						<a href="JavaScript:Void(0)" class="shadow"> 
							<img  width="250" src="assets/images/detail-pages-images/webdesign/custom.jpg" title="About Us"> 
						</a>
					</div>
					<p class="webdesin_prg justify">We have year of experience web designer for designing the best and unique for our client in every sector like health care, real estate, sports, classified etc. </p>
					<ul class="bullet_detail check_ic check_right span8 margin_left_ul">
						<li class="li_cheak">We focus on designing website that must be user friendly, interactive and affordable in all manner.</li>
						<li class="li_cheak">Our designs are working as a brand for your company that is important to success the business online.</li>
						<li class="li_cheak">We provides flexible and standard solution for all requirements that make our client bond with us for the long term.</li>
					</ul>
				</div>
			</div>
		</div>
	</div>	
</div>	
</section>
<section class="page-section " >
<div  class="container-fluid">
	<div class="container">
		<div class="page-heading"><h3>Mobile App Design</h3> </div>
		<div class="spacer-mini"></div>
		<div class="container">
			<div class=" row-fluid">
				<div class="span12 process_grid">
					<div class="border pull-right thumb-left"> 
						<a href="JavaScript:Void(0)" class="shadow"> 
							<img  width="250" src="assets/images/detail-pages-images/webdesign/mobileapp.jpg" title="About Us"> 
						</a>
					</div>
					<p class="webdesin_prg justify"> We have professional designer for designing mobile app which ensure about quality service and user friendly app. We also provides</p> 
					<ul class="bullet_detail check_ic  check_right span8 margin_left_ul">
						<li class="li_cheak">Applications related to the business, E-commerce, social networking, education etc. </li>
						<li class="li_cheak">Quality services and app must be fully tested before deployed to client.</li>
						<li class="li_cheak">Easy, quick, and interactive way to communicate with the user.</li>
					</ul>
          		</div> 
			</div>
		</div>
	</div>
</div>
</section>
    <!--webdesign block----> 