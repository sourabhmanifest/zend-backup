<script>
	$('#solutions a').css('background-color','#056ab2');
	$('#solutions a').css('color','#FFF');
</script>

<section class="page-cover solutions-cover-img">
  <div class="container-fluid">
    <div class="container">
      <div class="page-heading">
        <h3>SOLUTIONS</h3>
        <h4>Our solutions meets your demand</h4>
        <p> We are having solution range that convert our visitors into clients. Solutions that make answer to every questions of our clients </p>
      </div>
    </div>
  </div>
</section>

<!-----top navigations section closed here...-------->
 <!---top navigations section closed here...-------->
    <section class="page-section theme-bg-gray">
      <div class="container-fluid">
        <div class="container">
          <div class="spacer-mini2"></div>
          <div class="page-heading"> </div>
          <div class="spacer-mini"></div>
          <div id="services-block" class="container">
            <div class="row-fluid">
              <div class="span4 hvr-grow-shadow"> <i class=""><img src="assets/images/solutions/1426623159_pulse-box-32.png"></i>
                <h4>Health care</h4>
                <p> We provide a Health Care Website which reduces the cost and complexity and become user friendly software for our client.</p>
              </div>
              <div class="span4 hvr-grow-shadow"> <i class=""><img src="assets/images/solutions/1426607116_home_house_real_estate-32.png"/></i>
                <h4>Real estate</h4>
                <p> We offer excellent Real Estate Web Design which are significant to customers of business with current business pattern.</p>
              </div>
              <div class="span4 hvr-grow-shadow"> <i class=""><img src="assets/images/solutions/1426607139_sports_tennis_ball_tennisball-32.png"/></i>
                <h4>Sports</h4>
                <p> We produce Sports Solution which give boost to the business and give amazing and realistic feel to the client. </p>
              </div>
            </div>
          </div>
        </div>
      </div>
       <!--start Second--> 
    </section>
    <section class="page-section">
      <div class="container-fluid">
        <div class="container">
          <div class="spacer-mini2"></div>
          <div class="page-heading"> </div>
          <div class="spacer-mini"></div>
          <div id="services-block" class="container">
            <div class="row-fluid">
              <div class="span4 hvr-grow-shadow"> <i><img src="assets/images/solutions/forbidden_document-32.png"/></i>
                <h4>Classified</h4>
                <p> We offer a Classified Website designing and development which provide two way communication, products,or services.</p>
              </div>
              <div class="span4 hvr-grow-shadow"> <i class=""><img src="assets/images/solutions/1426622946_aiga_water_transportation-32.png"/></i>
                <h4>Shipping</h4>
                <p> We committed our client to provide most reliable and flexible shipping solution which meet the complex business demand.</p>
              </div>
              <div class="span4 hvr-grow-shadow"> <i class=""><img src="assets/images/solutions/1426607458_Facebook-32.png"/></i>
                <h4>Social Networking</h4>
                <p> We make your own social network solutions which are most popular and helpful for the business growth and expansion. </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--Second closed--> 
      <!--Start thierd--> 
      </section>
    <section class="page-section theme-bg-gray">
      <div class="container-fluid">
        <div class="container">
          <div class="spacer-mini2"></div>
          <div class="page-heading"> </div>
          <div class="spacer-mini"></div>
          <div id="services-block" class="container">
            <div class="row-fluid">
              <div class="span4 hvr-grow-shadow"> <i class=""><img src="assets/images/solutions/elearning-32.png"/></i>
                <h4>Startup</h4>
                <p> We offer helping hand for Startup Website who want to take a first step for their business idea. Our team is ready to create platform for your business.</p>
              </div>
              <div class="span4 hvr-grow-shadow"> <i class=""><img src="assets/images/solutions/vacation_25-512.fw.png"/></i>
                <h4>Travel</h4>
                <p> With the Travel Website Development we identified the problem providing better customized option for the client, user, and agents.</p>
              </div>
              <div class="span4 hvr-grow-shadow"> <i class=""><img src="assets/images/solutions/1426607569_108-32.png"/></i>
                <h4>Restaurant</h4>
                <p> Our Restaurant Web Design met the scale of each type and size of restaurant also reduces cost and improves business performance & customer experience. </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--third Closed--> 
      <!--Fourth Start--> 
    </section>
    <section class="page-section">
      <div class="container-fluid">
        <div class="container">
          <div class="spacer-mini2"></div>
          <div class="page-heading"> </div>
          <div class="spacer-mini"></div>
          <div id="services-block" class="container">
            <div class="row-fluid">
              <div class="span4 hvr-grow-shadow"> <i class=""><img src="assets/images/solutions/1426607371_Streamline-81-32.png"/></i>
                <h4>E-commerce</h4>
                <p> Our Solution provides all type of E-commerce website development and websites provides space and scope to make product online.</p>
              </div>
              <div class="span4 hvr-grow-shadow"> <i class=""><img src="assets/images/solutions/1426623437_internt_web_technology-05-32.png"/></i>
                <h4>E-learning</h4>
                <p> We provide E-learning solution for the business. So we offer the platform to earn the money through providing knowledge.</p>
              </div>
              <div class="span4 hvr-grow-shadow"> <i class=""><img src="assets/images/solutions/crm.png"/></i>
                <h4>CRM</h4>
                <p>We provide CRM solutions for the clients for better customer data management, interaction tracking and so on. </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--Fourth Closed-->