<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Portfolio extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('commonmodel','mimodel'));
	}

	public function index()
	{	
		

		$data['all_project_data'] = $this->commonmodel->getRecords('portfolio_project','', array('is_active'=>1, 'del_status'=>0));
		//$data['all_project_data'] = $this->commonmodel->getRecords('portfolio_project','', array('is_active'=>1));
		//echo '<pre>';print_r($data['all_project_data']);exit;

		$data['title']= 'Manifest Infotech| Company Portfolio';
		$data['description']= "Take a look for our Portfolio showcase for Web Designing, Development, SEO and Logo design. We convert your dreams into your achievement";
		$data['keywords']="Web design company, Web Development Company, Web Development Portfolio, Web Design Portfolios, Web Design, Web Development, IT Solution Company";
		$this->load->view('includes/header',$data);
		$this->load->view('portfolio/portfolio');
		$this->load->view('Testimonials-carousel');
		$this->load->view('includes/footer');
	}
	
	public function project($project_id)
	{	
		$data['all_project_data'] = $this->commonmodel->getRecords('portfolio_project');
		$data['all_category_data'] = $this->commonmodel->getRecords('portfolio_category','', array('is_active'=>1, 'del_status'=>0));
		$data['all_posts'] = $this->mimodel->getallpost();
		$data['project_data'] = $this->commonmodel->getRecords('portfolio_project', '*', array('project_id'=>$project_id), '', true);
		//echo '<pre>';print_r($data['project_data']);exit;
		//$data['title']= $data['project_data']['project_name'].' | Website Design by Manifest';
		$data['title']= $data['project_data']['title'];
		//echo '<pre>';print_r($data['title']);exit;
		$data['description']= $data['project_data']['meta_description'];
		//echo '<pre>';print_r($data['description']);exit;
		$data['keywords']= "";
		$this->load->view('includes/header',$data);
		$this->load->view('portfolio/project-detail');
		$this->load->view('portfolio/related_projects');
		$this->load->view('includes/footer');
	}

	public function category($category_id)
	{
		$data['all_project_data'] = $this->commonmodel->getRecords('portfolio_project');
		$data['all_category_data'] = $this->commonmodel->getRecords('portfolio_category','', array('is_active'=>1, 'del_status'=>0));
		$data['all_posts'] = $this->mimodel->getallpost();
		
																																										
		$data['category_data'] = $this->commonmodel->getRecords('portfolio_category', '*', array('category_id'=>$category_id), '', true);
		//echo '<pre>';print_r($data['category_data']);exit;

		$data['all_project_data'] = $this->commonmodel->getRecords('portfolio_project','*', array('category_id'=>$data['category_data']['category_id']));
		//echo '<pre>';print_r($data['category_data']);exit;

		$data['title']= $data['category_data']['category'].' | Manifest Infotech';
		//echo '<pre>';print_r($data['title']);exit;
		$data['description']= "Take a look for our Portfolio showcase for Web Designing, Development, SEO and Logo design. We convert your dreams into your achievement";
		$data['keywords']= "";
		$this->load->view('includes/header',$data);		
		$this->load->view('portfolio/category');
		$this->load->view('portfolio/related_projects');
		$this->load->view('includes/footer');
	}

	






}


