<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Aboutus extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('commonmodel','mimodel'));
	}

	public function index()
	{	
		$data['all_project_data'] = $this->commonmodel->getRecords('portfolio_project');
		$data['title']= 'Manifest Infotech | About the Company';
		$data['description']= "We are having team of expert in designing and development who build and create the websites and mobile applications using latest technologies";
		$data['keywords']= "";
		$this->load->view('includes/header',$data);
		$this->load->view('about');
		$this->load->view('portfolio/related_projects');
		$this->load->view('includes/footer');
	}
	
	
}


