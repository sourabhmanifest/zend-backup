
<?php
function resizeqq($width, $height, $path, $imgarr, $i){
  /* Get original image x y*/
  
  $img_tmp = $imgarr['tmp_name'][$i];

  list($w, $h) = getimagesize($img_tmp);
  /* calculate new image size with ratio */
  $ratio = max($width/$w, $height/$h);
  $h = ceil($height / $ratio);
  $x = ($w - $width / $ratio) / 2;
  $w = ceil($width / $ratio);
  /* new file name */
  //$path = 'uploads/'.$width.'x'.$height.'_'.$_FILES['image']['name'];
  /* read binary data from image file */
  
  $imgString = file_get_contents($img_tmp);
  
  

  /* create image from string */
  $image = imagecreatefromstring($imgString);
  $tmp = imagecreatetruecolor($width, $height);
  imagecopyresampled($tmp, $image,
    0, 0,
    $x, 0,
    $width, $height,
    $w, $h);
  /* Save image */
  switch ($imgarr
    ['type'][$i]) {
    case 'image/jpeg':
      imagejpeg($tmp, $path, 100);
      break;
    case 'image/png':
      imagepng($tmp, $path, 0);
      break;
    case 'image/gif':
      imagegif($tmp, $path);
      break;
    default:
      exit;
      break;
  }
  return  $path;
  /* cleanup memory */
  imagedestroy($image);
  imagedestroy($tmp);
}