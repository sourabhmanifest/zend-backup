<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<div class="content">
	<!--start-plans-404page---->
	<div class="error-page">
		<p>Page Not Found!</p>
		<h3>404</h3>
	</div>
	<!--End-plans-404page---->
	<div class="clear"> </div>
</div>

<?php //echo '<pre>'; print_r($_SERVER);
	if($_SERVER['HTTP_HOST']=="localhost")
	{
		//include_once('.././././application/views/Testimonials-carousel.php');
	}
	else
	{
		//include_once('/home/mnifesti/public_html/application/views/Testimonials-carousel.php');
	}  
?>

<!--call to action pop up code-->
<div id="myModal" class="modal fade popup_padding" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <button type="button" id="toggle" class="close cutumize_close" data-dismiss="modal" aria-hidden="true"> &times; </button>
    <div class="span6 padd_top_action">
        <div class="page-heading">
            <h3 class="">Send Us Message !</h3>
        </div>
        <div class="spacer-mini"><p id="popupsuccessmsg" class="successmsg"></p></div>
        <form class="">
            <div class="controls controls-row">
                <input type="text" name='fname' id='fullname' onBlur='checkFnameValidation("fullname",1)' class="span3 colo_relal fa fa-user" placeholder="Full Name">
                <input type="text" name='mail' id='email' value='' onBlur='checkEmailValidation("email",1)' class="span3 margin_left" placeholder="Email Address">
                <input type="text" name='phone' id='telphone' onBlur='checkPhoneValidation("telphone",1)' class="span3 margin-null" placeholder="Phone Number">
                <input type="text" name='subject' id='subject' class="span3" placeholder="Subject">
            </div>
            <div class="controls">
                <textarea id="message" name="msg" class="span6 colr_chage_all_field no_resize no_resize" placeholder="Your Message" rows="5"></textarea>
            </div>
            <div class="controls">
            
                <button id="contact-submit" onclick="send_msg('fullname','email','telphone','subject','message',1,'popupsuccessmsg')" type="button" class="color_back_button btn   pull-right">Send Now</button>
            </div>
        </form>
    </div>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body"> </div>
        </div>
    </div>
</div>




<section class="page-section">
<div class="container-fluid">
	<div class="container">
		<div class="row-fluid">
			<div class="span3 footer_grid pull-left" style="max-width: 23%;">
				<ul class="unstyled">
					<li class="mar_bootum bac_image3">Navigation</li>
					<li><a href="<?php echo $ciSiteUrl; ?>/about-us">Overview</a></li>
					<li><a href="<?php echo $ciSiteUrl; ?>/mi/process">Process</a></li>
					<li><a href="<?php echo $ciSiteUrl; ?>/services">Services</a></li>
					<li><a href="<?php echo $ciSiteUrl; ?>/solutions">Solution</a></li>
					<li><a href="<?php echo $ciSiteUrl; ?>/mi/experties">Experties</a></li>
				</ul>
			</div>
			<div class="span3 footer_grid pull-left" style="max-width: 23%;">
				<ul class="unstyled">
					<li class="mar_bootum bac_image1">Strategies</li>
					<li><a href="<?php echo $ciSiteUrl; ?>/mi/analysis-and-planning">Analyse & Planning</a></li>
					<li><a href="<?php echo $ciSiteUrl; ?>/mi/design-and-html">Design & Html</a></li>
					<li><a href="<?php echo $ciSiteUrl; ?>/mi/development-and-implimentation">Development & Implementation</a></li>
					<li><a href="<?php echo $ciSiteUrl; ?>/mi/qa-and-deploy">Qa & Deploy</a></li>
				</ul>
			</div>
			<div class="span3 footer_grid pull-left" style="max-width: 23%;">
				<ul class="unstyled">
					<li class="mar_bootum bac_image2">Technologies</li>
					<li><a href="<?php echo $ciSiteUrl; ?>/blog/python-technology/">Python</a></li>
					<li><a href="<?php echo $ciSiteUrl; ?>/blog/php-technology/">PHP</a></li>
					<li><a href="<?php echo $ciSiteUrl; ?>/blog/asp-net-technology/">ASP.Net</a></li>
					<li><a href="<?php echo $ciSiteUrl; ?>/blog/android-app-development/">Android</a></li>
					<li><a href="<?php echo $ciSiteUrl; ?>/blog/ipad-app-development/">iOS</a></li>
					<li><a href="<?php echo $ciSiteUrl; ?>/blog/web-design/">Web Design</a></li>
					
				</ul>
			</div>
			<div class="span3 footer_grid pull-left" style="max-width: 23%;">
				<ul class="unstyled">
					<li class="mar_bootum bac_image">Open Source</li>
					<li><a href="<?php echo $ciSiteUrl; ?>/blog/cakephp-development/">Cake PHP</a></li>
					<li><a href="<?php echo $ciSiteUrl; ?>/blog/codeigniter-development/">Codeigniter</a></li>
					<li><a href="<?php echo $ciSiteUrl; ?>/blog/yii-development/">Yii</a></li>
					<li><a href="<?php echo $ciSiteUrl; ?>/blog/zend-development/">Zend</a></li>
					<li><a href="<?php echo $ciSiteUrl; ?>/blog/wordpress-customization/">Wordpress</a></li>
					<li><a href="<?php echo $ciSiteUrl; ?>/blog/drupal-customization/">Drupal</a></li>
					<li><a href="<?php echo $ciSiteUrl; ?>/blog/joomla-customization/">Joomla</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
</section>

<section>
<div id="footer" class="footer">
	<div class="container-fluid">
		<div class="container">
			<div class="row-fluid">
				<div class="span6">Copyright &copy; <strong class="color-mark">Manifest Infotech Pvt. Ltd.</strong>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; All rights reserved.</div>
				<div class="span6">
					<ul class="inline-list pull-right">
						<li><a href="<?php echo $ciSiteUrl; ?>/aboutus">Overview</a></li>
						<li><a href="<?php echo $ciSiteUrl; ?>/services">Services</a></li>
						<li><a href="<?php echo $ciSiteUrl; ?>/portfolio">Portfolio</a></li>
						<li><a href="<?php echo $ciSiteUrl; ?>/blog">Blog</a></li>
						<li><a href="<?php echo $ciSiteUrl; ?>/mi/contact">Contact</a></li>
						<li><a href="<?php echo $ciSiteUrl; ?>/mi/sitemap">Site Map</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!--footer strip-->
</section>
<!---contact us end...