function checkFnameValidation(id,borderwidth)
{ 
	/*$('#e_from').html('');*/
	$('#'+id).css("border","");
	strRegExp = "[^A-Za-z\ ]";
	var fname = $.trim($('#'+id).val());
	this.fname=fname;
	//alert(fname);
	if(fname.length >= 3)
	{
		charpos = fname.search(strRegExp);
		if (charpos >=0)
		{
			$('#'+id).css("border",borderwidth+"px solid red");
			$('#'+id).addClass("intro");
			$('#'+id).val("");
			$('#'+id).attr("placeholder", "Only alphabets are allow");
			err1=1;
			return err1;
		}
		else
		{
			return 0;
		}
		
	}
	else
	{
		if(fname.length == 0)
		{
			$('#'+id).css("border",borderwidth+"px solid red");
			$('#'+id).val("");
			$('#'+id).attr("placeholder", "Please enter name");
			err1=1;
			return err1;
		}
		else
		{
			charpos = fname.search(strRegExp);
			if (charpos >=0)
			{
				$('#'+id).css("border",borderwidth+"px solid red");
				$('#'+id).addClass("intro");
				$('#'+id).val("");
				$('#'+id).attr("placeholder", "Only alphabets are allow");
				err1=1;
				return err1;
			}
			else
			{
				$('#'+id).css("border",borderwidth+"px solid red");
				$('#'+id).val("");
				$('#'+id).attr("placeholder", "Enter atleast 3 characters.");
				err1=1;
				return err1;
			}
		}
	}
}

function checkEmailValidation(id,borderwidth)
{
	/*alert("hi");*/
	/*$('#e_email').html('');*/
	$('#'+id).css("border","");
	var email = $.trim($('#'+id).val());
	//alert(email);
	this.email=email;
	var atpos=email.indexOf("@");
	var dotpos=email.lastIndexOf(".");
	
	if(email.length <=0)
	{ 		
		$('#'+id).css("border",borderwidth+"px solid red");
		$('#'+id).attr("placeholder", "Please enter email.");
		err1=1;
		return err1;
	}
	else if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)
	{
		$('#'+id).css("border",borderwidth+"px solid red");
		$('#'+id).val("");
		$('#'+id).attr("placeholder", "Invalid email.");
		err1=1;
		return err1;
	}
	else
	{
		return 0;
	}
	
}

function checkPhoneValidation(id,borderwidth)
{ 
	//alert("hi")
	/*$('#e_phone').html('');*/
	$('#'+id).css("border","");
	strRegExp = "[^0-9\]";
	var phone = $.trim($('#'+id).val());
	this.phone=phone;
	//alert(phone);
	if(phone.length==0)
	{
		$('#'+id).attr("placeholder", "Phone Number");
		return 0;
	}
	else
	{
		if(phone.length == 10)
		{
			charpos = phone.search(strRegExp);
			if (charpos >=0)
			{
				$('#'+id).css("border",borderwidth+"px solid red");
				$('#'+id).attr("placeholder", "Invalid phone no.");
				err1=1;
				return err1;
			}
			else
			{
				return 0;
			}
			
		}
		else
		{
			$('#'+id).css("border",borderwidth+"px solid red");
			$('#'+id).val("");
			$('#'+id).attr("placeholder", "Invalid phone no.");
			err1=1;
			return err1;
		}
	}
	
}

/*function checksubjectValidation(id,borderwidth)
{ 
	$('#'+id).css("border","");
	var subject = $.trim($('#'+id).val());
	this.subject=subject;
	//alert(fname);
}

function checkMessageValidation()
{ 
	$('#msg').css("border","");
	strRegExp = "[^A-Za-z 0-9 .\ ]";
	var msg = $.trim($('#msg').val());
	this.msg=msg;
	if(msg.length <= 10)
	{
		$('#msg').css("border","2px solid red");
		$('#msg').attr("placeholder", "Message should have atleast 10 characters long.");
		err1=1;
		return err1;
	}
	else
		if
	{
		return 0;
	}
}*/

function send_msg(fnameid,emailid,phoneid,subid,msgid,borderwidth,successmsgid)
{
	//alert(successmsgid);
	$("#"+successmsgid).text("");
	var err1=checkFnameValidation(fnameid,borderwidth);
	var err2=checkEmailValidation(emailid,borderwidth);
	var err3=checkPhoneValidation(phoneid,borderwidth);
	var subject = $.trim($('#'+subid).val());
	var msg = $.trim($('#'+msgid).val());
	//var phone=1234567890;
	//alert(fname+" "+email+" "+phone+" "+msg);
	//alert(msg);
	if(err1==0 && err2==0 && err3==0 )
	{
		data ='fname='+fname+'&email='+email+'&phone='+phone+'&subject='+subject+'&msg='+msg;
		//alert(data);
		$.ajax({
			type: "POST",
			async: false,
			url: site_url + "/mi/emailsender",
			data: data,	
				
			success: function(data)
			{
				//alert(data);	
				$("#"+fnameid).val("");
				$("#"+emailid).val("");
				$("#"+phoneid).val("");
				$("#"+subid).val("");
				$("#"+msgid).val("");
				$("#"+successmsgid).text(data);
				setTimeout( function() {$("#"+successmsgid).css('display','none');},2000);
				if(successmsgid=="popupsuccessmsg")
				setTimeout( function() {$("#toggle").trigger('click');},1000);

			}
		});
	}

}
