/* global screenReaderText */
/**
 * Theme functions file.
 *
 * Contains handlers for navigation and widget area.
 */

( function( $ ) {
	var $body, $window, $sidebar, adminbarOffset, top = false,
	    bottom = false, windowWidth, windowHeight, lastWindowPos = 0,
	    topOffset = 0, bodyHeight, sidebarHeight, resizeTimer;

	// Add dropdown toggle that display child menu items.
	$( '.main-navigation .page_item_has_children > a, .main-navigation .menu-item-has-children > a' ).after( '<button class="dropdown-toggle" aria-expanded="false">' + screenReaderText.expand + '</button>' );

	$( '.dropdown-toggle' ).click( function( e ) {
		var _this = $( this );
		e.preventDefault();
		_this.toggleClass( 'toggle-on' );
		_this.next( '.children, .sub-menu' ).toggleClass( 'toggled-on' );
		_this.attr( 'aria-expanded', _this.attr( 'aria-expanded' ) === 'false' ? 'true' : 'false' );
		_this.html( _this.html() === screenReaderText.expand ? screenReaderText.collapse : screenReaderText.expand );
	} );

	// Enable menu toggle for small screens.
	( function() {
		var secondary = $( '#secondary' ), button, menu, widgets, social;
		if ( ! secondary ) {
			return;
		}

		button = $( '.site-branding' ).find( '.secondary-toggle' );
		if ( ! button ) {
			return;
		}

		// Hide button if there are no widgets and the menus are missing or empty.
		menu    = secondary.find( '.nav-menu' );
		widgets = secondary.find( '#widget-area' );
		social  = secondary.find( '#social-navigation' );
		if ( ! widgets.length && ! social.length && ( ! menu || ! menu.children().length ) ) {
			button.hide();
			return;
		}

		button.on( 'click.twentyfifteen', function() {
			secondary.toggleClass( 'toggled-on' );
			secondary.trigger( 'resize' );
			$( this ).toggleClass( 'toggled-on' );
		} );
	} )();

	// Sidebar scrolling.
	function resize() {
		windowWidth   = $window.width();
		windowHeight  = $window.height();
		bodyHeight    = $body.height();
		sidebarHeight = $sidebar.height();

		if ( 955 > windowWidth ) {
			top = bottom = false;
			$sidebar.removeAttr( 'style' );
		}
	}

	function scroll() {
		var windowPos = $window.scrollTop();

		if ( 955 > windowWidth ) {
			return;
		}

		if ( sidebarHeight + adminbarOffset > windowHeight ) {
			if ( windowPos > lastWindowPos ) {
				if ( top ) {
					top = false;
					topOffset = ( $sidebar.offset().top > 0 ) ? $sidebar.offset().top - adminbarOffset : 0;
					$sidebar.attr( 'style', 'top: ' + topOffset + 'px;' );
				} else if ( ! bottom && windowPos + windowHeight > sidebarHeight + $sidebar.offset().top && sidebarHeight + adminbarOffset < bodyHeight ) {
					bottom = true;
					$sidebar.attr( 'style', 'position: fixed; bottom: 0;' );
				}
			} else if ( windowPos < lastWindowPos ) {
				if ( bottom ) {
					bottom = false;
					topOffset = ( $sidebar.offset().top > 0 ) ? $sidebar.offset().top - adminbarOffset : 0;
					$sidebar.attr( 'style', 'top: ' + topOffset + 'px;' );
				} else if ( ! top && windowPos + adminbarOffset < $sidebar.offset().top ) {
					top = true;
					$sidebar.attr( 'style', 'position: fixed;' );
				}
			} else {
				top = bottom = false;
				topOffset = ( $sidebar.offset().top > 0 ) ? $sidebar.offset().top - adminbarOffset : 0;
				$sidebar.attr( 'style', 'top: ' + topOffset + 'px;' );
			}
		} else if ( ! top ) {
			top = true;
			$sidebar.attr( 'style', 'position: fixed;' );
		}

		lastWindowPos = windowPos;
	}

	function resizeAndScroll() {
		resize();
		scroll();
	}

	$( document ).ready( function() {
		$body          = $( document.body );
		$window        = $( window );
		$sidebar       = $( '#sidebar' ).first();
		adminbarOffset = $body.is( '.admin-bar' ) ? $( '#wpadminbar' ).height() : 0;

		$window
			.on( 'scroll.twentyfifteen', scroll )
			.on( 'resize.twentyfifteen', function() {
				clearTimeout( resizeTimer );
				resizeTimer = setTimeout( resizeAndScroll, 500 );
			} );
		$sidebar.on( 'click keydown', 'button', resizeAndScroll );

		resizeAndScroll();

		for ( var i = 1; i < 6; i++ ) {
			setTimeout( resizeAndScroll, 100 * i );
		}
	} );

} )( jQuery );


//Call to action js

function checkFnameValidation(id,borderwidth)
{ 
	/*$('#e_from').html('');*/
	$('#'+id).css("border","");
	strRegExp = "[^A-Za-z\ ]";
	var fname = $.trim($('#'+id).val());
	this.fname=fname;
	//alert(fname);
	if(fname.length >= 3)
	{
		charpos = fname.search(strRegExp);
		if (charpos >=0)
		{
			$('#'+id).css("border",borderwidth+"px solid red");
			$('#'+id).addClass("intro");
			$('#'+id).val("");
			$('#'+id).attr("placeholder", "Only alphabets are allow");
			err1=1;
			return err1;
		}
		else
		{
			return 0;
		}
		
	}
	else
	{
		if(fname.length == 0)
		{
			$('#'+id).css("border",borderwidth+"px solid red");
			$('#'+id).val("");
			$('#'+id).attr("placeholder", "Please enter name");
			err1=1;
			return err1;
		}
		else
		{
			charpos = fname.search(strRegExp);
			if (charpos >=0)
			{
				$('#'+id).css("border",borderwidth+"px solid red");
				$('#'+id).addClass("intro");
				$('#'+id).val("");
				$('#'+id).attr("placeholder", "Only alphabets are allow");
				err1=1;
				return err1;
			}
			else
			{
				$('#'+id).css("border",borderwidth+"px solid red");
				$('#'+id).val("");
				$('#'+id).attr("placeholder", "Enter atleast 3 characters.");
				err1=1;
				return err1;
			}
		}
	}
}

function checkEmailValidation(id,borderwidth)
{
	/*alert("hi");*/
	/*$('#e_email').html('');*/
	$('#'+id).css("border","");
	var email = $.trim($('#'+id).val());
	//alert(email);
	this.email=email;
	var atpos=email.indexOf("@");
	var dotpos=email.lastIndexOf(".");
	
	if(email.length <=0)
	{ 		
		$('#'+id).css("border",borderwidth+"px solid red");
		$('#'+id).attr("placeholder", "Please enter email.");
		err1=1;
		return err1;
	}
	else if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)
	{
		$('#'+id).css("border",borderwidth+"px solid red");
		$('#'+id).val("");
		$('#'+id).attr("placeholder", "Invalid email.");
		err1=1;
		return err1;
	}
	else
	{
		return 0;
	}
	
}

function checkPhoneValidation(id,borderwidth)
{ 
	//alert("hi")
	/*$('#e_phone').html('');*/
	$('#'+id).css("border","");
	strRegExp = "[^0-9\]";
	var phone = $.trim($('#'+id).val());
	this.phone=phone;
	//alert(phone);
	if(phone.length==0)
	{
		$('#'+id).attr("placeholder", "Phone Number");
		return 0;
	}
	else
	{
		if(phone.length == 10)
		{
			charpos = phone.search(strRegExp);
			if (charpos >=0)
			{
				$('#'+id).css("border",borderwidth+"px solid red");
				$('#'+id).attr("placeholder", "Invalid phone no.");
				err1=1;
				return err1;
			}
			else
			{
				return 0;
			}
			
		}
		else
		{
			$('#'+id).css("border",borderwidth+"px solid red");
			$('#'+id).val("");
			$('#'+id).attr("placeholder", "Invalid phone no.");
			err1=1;
			return err1;
		}
	}
	
}

/*function checksubjectValidation(id,borderwidth)
{ 
	$('#'+id).css("border","");
	var subject = $.trim($('#'+id).val());
	this.subject=subject;
	//alert(fname);
}

function checkMessageValidation()
{ 
	$('#msg').css("border","");
	strRegExp = "[^A-Za-z 0-9 .\ ]";
	var msg = $.trim($('#msg').val());
	this.msg=msg;
	if(msg.length <= 10)
	{
		$('#msg').css("border","2px solid red");
		$('#msg').attr("placeholder", "Message should have atleast 10 characters long.");
		err1=1;
		return err1;
	}
	else
		if
	{
		return 0;
	}
}*/

function send_msg(fnameid,emailid,phoneid,subid,msgid,borderwidth,successmsgid)
{
	//alert(successmsgid);
	$("#"+successmsgid).text("");
	var err1=checkFnameValidation(fnameid,borderwidth);
	var err2=checkEmailValidation(emailid,borderwidth);
	var err3=checkPhoneValidation(phoneid,borderwidth);
	var subject = $.trim($('#'+subid).val());
	var msg = $.trim($('#'+msgid).val());
	//var phone=1234567890;
	//alert(fname+" "+email+" "+phone+" "+msg);
	//alert(msg);
	if(err1==0 && err2==0 && err3==0 )
	{
		data ='fname='+fname+'&email='+email+'&phone='+phone+'&subject='+subject+'&msg='+msg;
		//alert(data);
		$.ajax({
			type: "POST",
			async: false,
			url: site_url + "/mi/emailsender",
			data: data,	
				
			success: function(data)
			{
				//alert(data);	
				$("#"+fnameid).val("");
				$("#"+emailid).val("");
				$("#"+phoneid).val("");
				$("#"+subid).val("");
				$("#"+msgid).val("");
				$("#"+successmsgid).text(data);
				setTimeout( function() {$("#"+successmsgid).css('display','none');},2000);
				if(successmsgid=="popupsuccessmsg")
				setTimeout( function() {$("#toggle").trigger('click');},1000);

			}
		});
	}
}
