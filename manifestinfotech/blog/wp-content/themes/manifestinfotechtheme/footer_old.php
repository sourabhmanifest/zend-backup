<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>


<?php include_once('/home/manifestinfotech/public_html/application/views/Testimonials-carousel.php'); ?>


<section class="page-section">
<div class="container">
	<div class="row-fluid">
		<div class="span3 footer_grid" style="max-width: 23%;">
			<ul class="unstyled">
				<li class="mar_bootum">NAVIGATION</li>
				<li><a href="<?php echo $ciSiteUrl; ?>/mi/process">PROCESS</a></li>
				<li><a href="<?php echo $ciSiteUrl; ?>/services">SERVICES</a></li>
				<li><a href="<?php echo $ciSiteUrl; ?>/mi/experties">EXPERTIES</a></li>
				<li><a href="<?php echo $ciSiteUrl; ?>/solutions">SOLUTION</a></li>
				<li><a href="<?php echo $ciSiteUrl; ?>/mi/what-we-do">WHAT WE DO</a></li>
				<li><a href="<?php echo $ciSiteUrl; ?>/mi/offering">OFFERINGS</a></li>
			</ul>
		</div>
		<div class="span3 footer_grid" style="max-width: 23%;">
			<ul class="unstyled">
				<li class="mar_bootum">EXPLORE</li>
				<li><a href="<?php echo $ciSiteUrl; ?>/about-us">ABOUT US</a></li>
				<li><a href="<?php echo $ciSiteUrl; ?>/mi/why-us">WHY US</a></li>
				<!--<li><a href="JavaScript:Void(0)">VISSION</a></li>-->
				<li><a href="<?php echo $ciSiteUrl; ?>/mi/mission">MISSION</a></li>
				<li><a href="<?php echo $ciSiteUrl; ?>/mi/contact">CONTACT US</a></li>
				<li><a href="JavaScript:Void(0)" data-toggle="modal" data-target="#myModal">QUOTE REQUEST</a></li>
			</ul>
		</div>
		<div class="span3 footer_grid" style="max-width: 23%;">
			<ul class="unstyled">
				<li class="mar_bootum">STRATEGIES</li>
				<li><a href="<?php echo $ciSiteUrl; ?>/mi/analysis-and-planning">ANALYZE AND PLANNING</a></li>
				<li><a href="<?php echo $ciSiteUrl; ?>/mi/design-and-html">DESIGN AND HTML</a></li>
				<li><a href="<?php echo $ciSiteUrl; ?>/mi/development-and-implimentation">DEVELPOMENT & IMPLEMENTATION</a></li>
				<li><a href="<?php echo $ciSiteUrl; ?>/mi/qa-and-deploy">QA AND DEPLOY</a></li>
			</ul>
		</div>
		<div class="span3 footer_grid" style="max-width: 23%;">
			<ul class="unstyled">
				<li class="mar_bootum">TECHNLOGIES</li>
				<li><a href="<?php echo $ciSiteUrl; ?>/blog/cakephp-development/">CAKE PHP</a></li>
				<li><a href="<?php echo $ciSiteUrl; ?>/blog/codeigniter-development/">CODEIGNIGTER</a></li>
				<li><a href="<?php echo $ciSiteUrl; ?>/blog/yii-development/">YII</a></li>
				<li><a href="<?php echo $ciSiteUrl; ?>/blog/zend-development/">ZEND</a></li>
				<li><a href="<?php echo $ciSiteUrl; ?>/blog/wordpress-customization/">WORDPRESS</a></li>
				<li><a href="<?php echo $ciSiteUrl; ?>/blog/drupal-customization/">DRUPAL</a></li>
				<li><a href="<?php echo $ciSiteUrl; ?>/blog/joomla-customization/">JOOMLA</a></li>
			</ul>
		</div>
	</div>
</div>
</section>

<section>
<div id="footer" class="footer">
	<div class="container-fluid">
		<div class="container">
			<div class="row-fluid">
				<div class="span6">Copyright &copy; <strong class="color-mark">Manifest Infotech Pvt. Ltd.</strong>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; All rights reserved.</div>
				<div class="span6">
					<ul class="inline-list pull-right">
						<li><a href="<?php echo $ciSiteUrl; ?>/">Home</a></li>
						<li><a href="<?php echo $ciSiteUrl; ?>/aboutus">About Us</a></li>
						<li><a href="<?php echo $ciSiteUrl; ?>/services">Services</a></li>
						<li><a href="<?php echo $ciSiteUrl; ?>/portfolio">Portfolio</a></li>
						<li><a href="<?php echo $ciSiteUrl; ?>/blog">Blog</a></li>
						<li><a href="<?php echo $ciSiteUrl; ?>/mi/contact">Contact</a></li>
						<li><a href="<?php echo $ciSiteUrl; ?>/mi/sitemap">Site Map</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!--footer strip-->
</section>
<!---contact us end...
----------------------------------------------------------------------------------------------->	
<div ><a href="#" class="scrollup"><i class="fa fa-chevron-up"></i></a> </div>

<script>
 $(window).scroll(function(){
		
        if ($(this).scrollTop() > 50) {
            $('.scrollup').fadeIn('slow');
        } else {
            $('.scrollup').fadeOut('slow');
        }
    });
    $('.scrollup').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 500);
        return false;
    });

</script>
<style>
/* CSS for removing Social Icons */
.stButton .chicklets{
padding:0px;
padding-left: 15px;
margin:0px;
}
.stButton{
	margin:0px;
}
.st_fblike	 > span	{
	display: inline-table!important;
}

#fb_button{
    overflow:hidden;
}


.fb_edge_comment_widget {
  display: none !important;
}

</style>
<?php wp_footer(); ?>
</body>
</html>
