<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<?php $ciSiteUrl = "http://manifestinfotech.com"; ?>
  <title>
       <?php
       if($_SERVER['REQUEST_URI']=='/staging/blog/'){
               //echo "Blog | "; wp_title();
                echo the_title()." | Technology Blogs | Manifest Infotech";
       }else{
               //wp_title('|', true, 'right');
                echo the_title()." | Technology Blogs | Manifest Infotech";
       }?>
  </title>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="description" content="Take a look for the latest blog for technologic al knowledge and increase your experience. <?php the_title(); ?>" />
  <meta name="author" content="Manifest Infotech" />
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="shortcut icon" type="image/ico" href="<?php echo get_template_directory_uri() ?>/img/common-img/favicon.ico"/>

	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<script>(function(){document.documentElement.className='js'})();</script>
	<?php //wp_head(); ?>

<!-- CSS Files -->
<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/awesomefont.css';  ?>"/>
<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.css';  ?>"/>
<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap-responsive.css';   ?>"/>
<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/manifest.css';   ?>"/>
<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/icon-font/css/font-awesome.css'; ?>"/>
<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/responsive.css'; ?>"/>
<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/style_common.css';?>"/>
<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/style10.css';?>"/>
<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/simpleslider.css';?>"/>
<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/slider.css';?>"/>
<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/footernavigation.css';?>"/>
<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/developer.css';?>"/>
<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/facebook.css';?>"/>
<!-- End CSS Files -->


<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "46f8eaf8-272a-47ef-9a5e-14ad19ff45b8", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>

<script type="text/javascript" src="<?php echo get_template_directory_uri().'/js/jquery-1.8.3.min.js';?>"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri().'/js/jquery.js';?>"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri().'/js/bootstrap.js';?>"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri().'/js/jquery_003.js';?>"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri().'/js/submenu.js';?>"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri().'/js/functions.js';?>"></script>

<script>
$(document).ready(function( ) {
var offsetHeight =100;
$('.nav-menu').scrollspy({
offset: offsetHeight
});


$('.nav li a').click(function (event) {
var scrollPos = $('body > #wrapper').find($(this).attr('href')).offset().top - offsetHeight;
$('body,html').animate({
scrollTop: scrollPos
}, 700, function () {
//$(".btn-navbar").click();
});
return false;
});
});
</script>
<script>
$(document).ready(function(){
	$('#banner').simpleSlider({ height: 400});
});
</script>

<script type="text/javascript">
	var $zoho= $zoho || {salesiq:{values:{},ready:function(){}}};
	var d=document;
	s=d.createElement("script"); s.type="text/javascript"; s.defer=true; s.src="https://salesiq.zoho.com/manifestinfotech/float.ls?embedname=manifestinfotech"; t=d.getElementsByTagName("script")[0]; t.parentNode.insertBefore(s,t);
</script>
</head>

<body <?php body_class(); ?> data-spy="scroll" data-target=".nav-menu" data-offset="300">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<header class="top-fixed" id="navigation" >
  <div class="container-fluid top-social-block" id="upperheader">
    <div class="container">
      <div class="pagetop visible-tablet visible-desktop">
        <ul  class="pull-right">
          <li><a href="mailto:info@manifestinfotech.com"><span class="fa fa-envelope"></span> Email</a></li>
          <li><a href="javascript:void(0)"><span class="fa fa-phone"></span> +91 9770368611</a></li>
          <li><a href="<?php echo $ciSiteUrl; ?>/mi/sitemap">Site Map</a></li>
					<li class="hailight"><a href="javascript:void(0)"  id = "call-to-action" class="" data-toggle="modal" data-target="#myModal">Call To Action</a>                    
                </li> 
        </ul>
         <ul class="pull-left social-icon">
                <li><a href="https://www.facebook.com/manifestinfotech" target="_blank" Title="Facebook"><i class="fa  fa-facebook"></i></a></li>
                <li><a href="https://twitter.com/ManifestInfotec" target="_blank" Title="Twitter"><i class="fa fa-twitter"></i></a></li>
                <li><a href="https://plus.google.com/+Manifestinfotech/about" target="_blank" Title="Google+"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="https://www.linkedin.com/pub/manifest-infotech/6a/974/601" target="_blank" Title="Linkedin"><i class="fa  fa-linkedin"></i></a></li>
                <li><a href="https://www.pinterest.com/archananchoukse/" target="_blank" Title="Pinterest"><i class="fa fa-pinterest" target="_blank" Title="Pinterest"></i></a></li>
                <li class="facebook">&nbsp;&nbsp;<div class="fb-like log_foooter" data-href="https://www.facebook.com/manifestinfotech" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div></li>
            </ul>
      </div>
      <!--tablet and destop view-->
      
     <div class="dropdown visible-phone" id="mobile-social"><a class="dropdown-toggle"  role="button" data-toggle="dropdown" data-target="javascript: void main(0);" href="javascript:void(0);"> <small>Social</small> <b class="caret"></b> </a>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                <li><a href="mailto:info@manifestinfotech.com"><i class="fa fa-envelope"></i>&nbsp info@manifestinfotech.com</a></li>
                <li><a href="JavaScript:Void(0)"><i class="fa fa-phone"></i>&nbsp;</span>+919770368611</a></li>
                <li><a href="JavaScript:Void(0)">Site Map</a></li>
                <div class="divider"></div>
                <li><a href="https://www.facebook.com/manifestinfotech" target="_blank" Title="Facebook"><i class="fa  fa-facebook"></i></a></li>
                <li><a href="https://twitter.com/ManifestInfotec" target="_blank" Title="Twitter"><i class="fa fa-twitter"></i></a></li>
                <li><a href="https://plus.google.com/+Manifestinfotech/about" target="_blank" Title="Google+"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="https://www.linkedin.com/pub/manifest-infotech/6a/974/601" target="_blank" Title="Linkedin"><i class="fa  fa-linkedin"></i></a></li>
                <li><a href="https://www.pinterest.com/archananchoukse/" target="_blank" Title="Pinterest"><i class="fa fa-pinterest" target="_blank" Title="Pinterest"></i></a></li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class="fb-like log_foooter" data-href="https://www.facebook.com/manifestinfotech" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div></li>
            </ul>
        </div>
      <!---/mobile social block----> 
    </div>
  </div>
  
  <!--top social icon bar closed-->
  <div class="inner navigation fixed-header">
    <div>
      <a class="scroll" id="themify-logo img" href="<?php echo $ciSiteUrl;?>">
        <img src="<?php echo get_template_directory_uri(); ?>/img/common-img/logo.png" class="logo" alt="Manifest Infotech" title="Manifest Infotech">
      </a> 
    </div>
    <!--logo closed & Nav Menu open... -->

    <div class="nav-menu" id="desktop-menu">
      <ul class="nav main-nav navigate_menu"> 
        <li id="aboutus" class="dropdown mega-dropdown navigation_menu"> <a  data-rel="aboutmenu" class="dropdown-toggle navi_menu" data-toggle="dropdown" href="<?php echo $ciSiteUrl; ?>/aboutus">overview</a></li>
        <li id="mi-services" class="dropdown mega-dropdown navigation_menu"> <a  data-rel="aboutmenu" class="dropdown-toggle navi_menu" data-toggle="dropdown" href="<?php echo $ciSiteUrl; ?>/services">services</a></li>
        <li id="solutions" class="dropdown mega-dropdown navigation_menu"> <a  data-rel="solutionmenu"  class="dropdown-toggle navi_menu" data-toggle="dropdown" href="<?php echo $ciSiteUrl; ?>/solutions">Solutions</a></li>
        <li id="portfolio" class="navigation_menu"><a class="scroll navi_menu" href="<?php echo $ciSiteUrl; ?>/portfolio">Portfolio</a></li>
        <li id="mi-blog" class="navigation_menu"><a class="scroll navi_menu" href="<?php echo $ciSiteUrl; ?>/blog">Blog</a></li>
        <li id="testimonial" class="navigation_menu"><a class="scroll navi_menu" href="<?php echo $ciSiteUrl; ?>/mi/testimonial">testimonial</a></li>
        <li id="career" class="navigation_menu"><a class="scroll navi_menu" href="<?php echo $ciSiteUrl; ?>/mi/career">Career</a></li>
        <li id="contact-us" class="navigation_menu"><a class="scroll navi_menu" href="<?php echo $ciSiteUrl; ?>/mi/contact">Contact</a></li>
      </ul>
    </div>
    
    <!-- Dropdown Menu For Mobile Devices-->

    <div class="dropdown mobile-drop"> <a data-toggle="dropdown" class="mobile-menu" href="JavaScript:Void(0)"><i class="fa fa-bars"></i></a>
      <ul class="nav dropdown-menu fullwidth" role="menu">
        <li><a class="scroll" href="<?php echo $ciSiteUrl; ?>/aboutus">Overview</a></li>
        <li><a class="scroll" href="<?php echo $ciSiteUrl; ?>/services">Services</a></li>
        <li><a class="scroll" href="<?php echo $ciSiteUrl; ?>/solutions">Solutions</a></li>
        <li><a class="scroll" href="<?php echo $ciSiteUrl; ?>/portfolio">Portfolio</a></li>
        <li><a class="scroll" href="<?php echo $ciSiteUrl; ?>/blog">Blog</a></li>
        <li><a class="scroll" href="<?php echo $ciSiteUrl; ?>/mi/testimonial">Testimonial</a></li>
        <li><a class="scroll" href="<?php echo $ciSiteUrl; ?>/mi/career">Career</a></li>
        <li><a class="scroll" href="<?php echo $ciSiteUrl; ?>/mi/contact">Contact</a></li>
      </ul>
    </div>
    <!--Drop down Menu Desktop after scroll-->
    <div class="dropdown mobile-drop scroll-menu-display" id="mobile_hide"> 
      <div class="bs-example">
        <ul class="btn_two">
          <li><a href="<?php echo $ciSiteUrl; ?>/portfolio" class="btn Desktop_button">Portfolio</a></li>
          <li><a href="javascript:void(0)" class="btn Desktop_button1" data-toggle="modal" data-target="#myModal">Call to Action</a></li>
          <li><a data-toggle="dropdown" class="mobile-menu dropdown" href="javascript:void(0)"><i class="fa fa-bars fa-1x"></i></a></li>
        </ul>
      </div>
      <ul class="nav dropdown-menu_scroll" role="menu">
        <li><a class="scroll" href="<?php echo $ciSiteUrl; ?>/aboutus">Overview</a></li>
        <li><a class="scroll" href="<?php echo $ciSiteUrl; ?>/services">Services</a></li>
        <li><a class="scroll" href="<?php echo $ciSiteUrl; ?>/solutions">Solutions</a></li>
        <li><a class="scroll" href="<?php echo $ciSiteUrl; ?>/portfolio">Portfolio</a></li>
        <li><a class="scroll" href="<?php echo $ciSiteUrl; ?>/blog">Blog</a></li>
        <li><a class="scroll" href="<?php echo $ciSiteUrl; ?>/mi/testimonial">Testimonial</a></li>
        <li><a class="scroll" href="<?php echo $ciSiteUrl; ?>/mi/career">Career</a></li>
        <li><a class="scroll" href="<?php echo $ciSiteUrl; ?>/mi/contact">Contact</a></li>
      </ul>
    </div>
  </div>
</header>
<script>
  $('#mi-blog a').css('background-color','#056ab2');
  $('#mi-blog a').css('color','#FFF');
</script>

