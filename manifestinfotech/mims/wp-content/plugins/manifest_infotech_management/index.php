<?php
/*
Plugin Name: Manifest Infotech Management
Plugin URI: http://www.manifestinfotech.com/
Description: Manifest Infotech Management
Version: 0.2 BETA
Author: Rakhi
Author URI: http://www.manifestinfotech.com
*/
//initialize the plugin
function time_add($time1, $time2)
{
 $e_time1=explode(':',$time1);
 $e_time2=explode(':',$time2);
 $sec_carry=0;
 $min_carry=0;
 $sec = $e_time2[2] + $e_time1[2];
 if($sec/60>=1)
 {
  	$sec_carry = Round($sec/60);
  	$sec = $sec%60;
 }
 $min = $e_time2[1] + $e_time1[1] + $sec_carry;
 if($min/60>=1)
 {
  	$min_carry = Round($min/60);
  	$min = $min%60;
 }
 $hr = $e_time2[0] + $e_time1[0] + $min_carry;
 echo '<br/>';
 $total_time[0]=$hr;
 $total_time[1]=$min;
 $total_time[2]=$sec;
 return $total_time=implode(':',$total_time);
}
wp_enqueue_script('jquery-1.5.2.min.js', plugins_url() . '/manifest_infotech_management/js/jquery-1.5.2.min.js');
wp_enqueue_script('timer.js', plugins_url() . '/manifest_infotech_management/js/timer.js');
add_action('admin_menu','Manifest_Infotech_Management');
add_action( 'wp_ajax_loadmore', 'loadmore_callback' );
function Manifest_Infotech_Management()
{
	add_menu_page('MI Management', 'MI Management','read','add_time','punch_in_out','',$_wp_last_object_menu);
	add_submenu_page('add_time', '','','read','add_time','punch_in_out');
	add_submenu_page('add_time','Log_in','Punch-In/Punch-Out','read','Log_in_details','log_details');
	add_submenu_page('add_time','Leave_Management','Leave Management','read','Leave','leave_mgmt');
	add_submenu_page('add_time','Policy_Management','Policy Management','read','Policy','policy_mgmt');
	add_submenu_page( 'add_time', '','', 'read', 'view_application', 'application');
	add_submenu_page( 'add_time', '', '', 'read', 'admin_page', 'admin-page');
	add_submenu_page( 'add_time', '', '', 'read', 'log_in', 'login-page');
	add_submenu_page( 'add_time', '', '', 'read', 'ajax_more', 'ajax_page');
}
function log_details()
{
	$user_ID = get_current_user_id(); 
	$current_user = wp_get_current_user();
	$username=$current_user->user_login;
	date_default_timezone_set("Asia/Kolkata");
	$current_date1 = date('Y-m-d');
	$start_time = date('Y-m-d h:i:s');
	if($username == 'manifestinfotech')
	{
	include "log_page.php";
	}
	else
	{
?>	<div>
<h1>LOG IN</h1>
<br/>
<br/>
<?php
if(isset($_POST['START']))
{
		$current_date1 = date('Y-m-d');
		$start_time = date('Y-m-d H:i:s');
		global $wpdb;
	    $q = "INSERT INTO `" . $wpdb->prefix . "login` (user_id, username, date1, start_time, log_in_status) VALUES($user_ID,'$username','$current_date1', '$start_time', 1)";
	    $sql = $wpdb->query($q);
 }
    if(isset($_POST['STOP']))
	{
		  $current_date2 = date('Y-m-d');
		  $end_time = date('Y-m-d H:i:s');
		    global $wpdb;			   
		    $dwd="UPDATE `wp_login` SET end_time = '$end_time', log_in_status = 0 WHERE id=".$_POST['log_id'];
		    $wpdb->query($dwd);               
			$querystring = "SELECT start_time, end_time FROM `" . $wpdb->prefix . "login` WHERE  id=".$_POST['log_id'];
            $total_hour = $wpdb->get_row($querystring);
		    $_time = $total_hour->start_time;			  
		    $e_time = $total_hour->end_time;
            $total = date_create ($e_time)->diff(date_create($_time))->format('%H:%i:%s');			   
			$dw="UPDATE `wp_login` SET total_working_hour = '$total' WHERE id=".$_POST['log_id'];
            $wpdb->query($dw);            
			$querystring1 = "SELECT * FROM `" . $wpdb->prefix . "login` WHERE user_id = " .$user_ID." AND  id=".$_POST['log_id'];
                 $total_hour1 = $wpdb->get_row($querystring1);
		          $_time1 = $total_hour1->total_working_hour;
		          
			     $_id1 = $total_hour1->user_id;
			     $_name1 = $total_hour1->username;
                 
               $qrey ="SELECT * FROM `wp_log_master` WHERE current_date1 = '$current_date2' AND user_id_master = ".$_id1;
			   $log1 = $wpdb->get_row($qrey);
			   $hour_total = $log1->total_working_hours;
			  if($wpdb->num_rows == 0)
			  {
				 $qr = "INSERT INTO `" . $wpdb->prefix . "log_master` (user_id_master, username_master, current_date1, total_working_hours) VALUES($_id1,'$_name1','$current_date2', '$_time1')";
	             $sql = $wpdb->query($qr);
			  }
				  elseif($wpdb->num_rows == 1)
				  {   
				  	//time add function
				  	$time_totl = time_add($total, $hour_total);
                     $dw="UPDATE `wp_log_master` SET total_working_hours = '$time_totl' WHERE  current_date1 = '$current_date2' AND user_id_master = ".$_id1;
                     $wpdb->query($dw);
				  }
	}          
?>
<form method="post">		 
<?php 
       global $wpdb;
        $q1 ="SELECT * FROM `wp_login` WHERE date1 = '$current_date1' AND user_id = '$user_ID' ORDER BY `wp_login`.`id` DESC LIMIT 1";
      
		$log = $wpdb->get_row($q1);
		//echo "<pre>"; print_r($log); //die;
		?>
			<input type="hidden" name="log_id" value="<?php echo $log->id; ?>">
	   <?php
	   if($wpdb->num_rows == 0 || $log->log_in_status == 0)
	   {
		?>
		
		<input type="submit" value="Start Login" id="" name="START"/>
		 <?php 
	   }
       else
	   {?>
		 
		<input type="submit" value="Stop Login" id="" name="STOP" /> 
		 <?php 
	   }?>

</form>
<br/>
<br/>
</div>
			<!-----Show total working hour------>
<div id="col-container" >
<div class="col-wrap">
	<div class="form-wrap">
		<table class="wp-list-table widefat fixed posts">
			<thead>
				<tr>
					<th class="manage-column column-categories" style="" scope="col">Date</th>
					<th class="manage-column column-categories" style="" scope="col">Start Time</th>
					<th class="manage-column column-categories" style="" scope="col">End Time</th>
					<th class="manage-column column-author" style="" scope="col">Total Working Hour</th>
				</tr>
			</thead>
		</div>
	</div>
</div>
<?php $querystr1 = "SELECT * FROM `wp_log_master` ORDER BY current_date1 DESC limit 5";	 
	$_login = $wpdb->get_results($querystr1);
?>
<tfoot>
	<tr>
		<th class="manage-column column-categories" style="" scope="col">Date</th>
		<th class="manage-column column-categories" style="" scope="col">Start Time</th>
		<th class="manage-column column-categories" style="" scope="col">End Time</th>
	    <th class="manage-column column-author" style="" scope="col">Total Working Hour</th>
	</tr>
</tfoot>
<?php
	global $wpdb;
	  $querystr1 = "SELECT * FROM `wp_login` WHERE username = '$username' and user_id =" . $user_ID. " ORDER BY 'date1' DESC";
	  $_login = $wpdb->get_results($querystr1);
?>
<tbody id="the-list">
<?php
		//$tim = 0;
		foreach ($_login as $login)
			{
			$r_  = $login->total_working_hour;
			$d_  = $login->date1;
			$st_time = $login->start_time;
			$en_time = $login->end_time;
			if($en_time == '0000-00-00 00:00:00')
			{
				$en_time = '---------- --:--:--';
			}
						
?>
	<tr id="post-1" class="post-1 type-post status-publish format-standard hentry category-uncategorized alternate iedit author-self level-0">
        <td><?php echo $d_; ?></td>
        <td><?php echo $st_time; ?></td>
        <td><?php echo $en_time; ?></td>
		<td><?php echo $r_;?></td>
	</tr>
 </tbody>
<?php
			}		
	}
}
function leave_mgmt()
{
	include "leave.php";
}
function policy_mgmt()
{
	include "policy.php";
}
function application()
{
	include "view_application.php";
}
function ajax_page()
{
	include "ajax_more.php";
}
function loadmore_callback() 
{
	include "ajax_more.php";
}
