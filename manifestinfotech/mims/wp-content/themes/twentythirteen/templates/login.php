<script type="text/javascript">	
	var plugins_url = "<?php echo plugins_url(); ?>";
</script>

<?php

/*
Template Name:Log In


*/
get_header();

    $user_ID = get_current_user_id(); 
	$current_user = wp_get_current_user();
	$username=$current_user->user_login;
?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			
</head>
<body>
<div class="panel panel-default">
 <div class="panel-heading">
  <h3 class="panel-title">Login</h3>
 </div>
  <div class="panel-body">
 <form class="form-horizontal" role="form">
  <div class="form-group">
    <label for="name" class="col-sm-5 control-label">Username</label>
    <div class="col-sm-3">
      <input type="email" class="form-control" id="name"
       placeholder="Enter username">
    </div>
  </div>
   
    <div class="form-group">
    <label for="password" class="col-sm-5 control-label" placeholder="Enter password">Password</label>
    <div class="col-sm-3">
      <input type="password" class="form-control" id="pass">
    </div>
  </div>
</form>
  </div>
  <div class="panel-footer" style="overflow:hidden;text-align:right;">
    <div class="form-group">
    <div class="col-sm-offset-2 col-sm-3">
      <button type="submit" class="btn btn-success btn-sm">Submit</button>
      
    </div>
  </div>  
  </div>
</div>
</body>
</html>
	</div><!-- #content -->
</div><!-- #primary -->
<?php
get_sidebar();
//get_footer();

?>



