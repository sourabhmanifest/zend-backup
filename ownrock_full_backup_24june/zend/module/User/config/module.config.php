<?php

return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/',
                    'defaults' => array(
                        'controller' => 'User\Controller\Home',
                        'action' => 'index',
                    ),
                ),
            ),

			
			
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'user' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/user',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),               
					
									
					'register' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/register[/:action]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'controller' => 'User\Controller\Register',
                                'action' => 'index',
                            ),
                        ),
                    ),
                    'login' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/login[/:action]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'controller' => 'User\Controller\Login',
                                'action' => 'index',
                            ),
                        ),
                    ),
                    'campaigns' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/campaigns[/:action][/:id]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'User\Controller\Campaigns',
                                'action' => 'index',
                            ),
                        ),
                    ),
                    'uploadmanager' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/uploadmanager[/:action][/:id]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'User\Controller\Uploadmanager',
                                'action' => 'index',
                            ),
                        ),
                    ),
                    
                     'media' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/media[/:action[/:id[/:subaction]]]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[a-zA-Z0-9_-]*',
                                'subaction' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'controller' => 'User\Controller\Mediamanager',
                                'action' => 'index',
                            ),
                        ),
                    ),
                    'usermanager' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/usermanager[/:action][/:id]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'User\Controller\Usermanager',
                                'action' => 'index',
                            ),
                        ),
                    ),
                     'onlinemanager' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/onlinemanager[/:action][/:id][/page/:page][/order_by/:order_by][/:order]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '(?!\bpage\b)(?!\border_by\b)[a-zA-Z][a-zA-Z0-9_-]*',
                                'page' => '[0-9]+',
                                'id' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'order_by' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'order' => 'ASC|DESC',
                            ),
                            'defaults' => array(
                                'controller' => 'User\Controller\Onlinemanager',
                                'action' => 'index',
                            ),
                        ),
                    ),
                    'onlinemanager' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/onlinemanager[/:action][/:merchant_id][/:merchant_status]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '(?!\bpage\b)(?!\border_by\b)[a-zA-Z][a-zA-Z0-9_-]*',
                                'merchant_id' => '[0-9]+',
                                'merchant_status' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'controller' => 'User\Controller\Onlinemanager',
                                'action' => 'index',
                            ),
                        ),
                    ),
                    'onlinemanager' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/onlinemanager[/:action][/:id]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '(?!\bpage\b)(?!\border_by\b)[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'controller' => 'User\Controller\Onlinemanager',
                                'action' => 'index',
                            ),
                        ),
                    ),
                    'projectmanager' => array(
                        'type' => 'Segment',
                        'options' => array(
                            //'route' => '/projectmanager[/:action][/:project_shortcut][/page/:page][/order_by/:order_by][/:order]',
                            'route' => '/projectmanager[/:action][/:user_id][/:user_name][/:project_id][/page/:page][/order_by/:order_by][/:order]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '(?!\bpage\b)(?!\border_by\b)[a-zA-Z][a-zA-Z0-9_-]*',
                                'user_id' => '[0-9]+',
                                'user_name' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'project_id' => '[0-9]+',
                                'page' => '[0-9]+',
                                'id' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'order_by' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'order' => 'ASC|DESC',
                            ),
                            'defaults' => array(
                                'controller' => 'User\Controller\Projectmanager',
                                'action' => 'index',
                            ),
                        ),
                    ),
                    'projectmanager' => array(
                        'type' => 'Segment',
                        'options' => array(
                            //'route' => '/projectmanager[/:action][/:project_shortcut][/page/:page][/order_by/:order_by][/:order]',
                            'route' => '/projectmanager[/:action][/:project_id][/:project_shortcut]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '(?!\bpage\b)(?!\border_by\b)[a-zA-Z][a-zA-Z0-9_-]*',
                                'project_id' => '[0-9]+',
                                'project_shortcut' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'controller' => 'User\Controller\Projectmanager',
                                'action' => 'index',
                            ),
                        ),
                    ),
                    'projectmanager' => array(
                        'type' => 'Segment',
                        'options' => array(
                            //'route' => '/projectmanager[/:action][/:project_shortcut][/page/:page][/order_by/:order_by][/:order]',
                            'route' => '/projectmanager[/:action][/:user_id][/:user_name][/:project_id][/:project_shortcut]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '(?!\bpage\b)(?!\border_by\b)[a-zA-Z][a-zA-Z0-9_-]*',
                                'user_id' => '[0-9]+',
                                'user_name' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'controller' => 'User\Controller\Projectmanager',
                                'action' => 'index',
                            ),
                        ),
                    ),
                    'projectskill' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/projectskill[/:action][/:project_id][/:project_shortcut]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '(?!\bpage\b)(?!\border_by\b)[a-zA-Z][a-zA-Z0-9_-]*',
                                'project_id' => '[0-9]+',
                                'project_shortcut' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'controller' => 'User\Controller\Projectskill',
                                'action' => 'index',
                            ),
                        ),
                    ),
                    //Front End Controller
                    'frontportfolio' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/frontportfolio[/:action][/:merchant_id][/:merchant_name]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '(?!\bpage\b)(?!\border_by\b)[a-zA-Z][a-zA-Z0-9_-]*',
                                'merchant_id' => '[0-9]+',
                                'merchant_name' => '[a-zA-Z][a-zA-Z0-9_-]*',
                ),
                            'defaults' => array(
                                'controller' => 'User\Controller\Frontportfolio',
                                'action' => 'index',
                            ),
                        ),
                    ),
                    
                    //Front End Controller
                    
                    //Front End Controller
                    'frontproject' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/frontproject[/:action][/:merchant_id][/:merchant_name]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '(?!\bpage\b)(?!\border_by\b)[a-zA-Z][a-zA-Z0-9_-]*',
                                'merchant_id' => '[0-9]+',
                                'merchant_name' => '[a-zA-Z][a-zA-Z0-9_-]*',
                               
                ),
                            'defaults' => array(
                                'controller' => 'User\Controller\Frontproject',
                                'action' => 'index',
                            ),
                        ),
                    ),
                    
                    //Front End Controller 
                    
                    'home' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/home[/:action]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '(?!\bpage\b)(?!\border_by\b)[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'controller' => 'User\Controller\Home',
                                'action' => 'index',
                            ),
                        ),
                    ),
                    /*'home' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/home[/:action][/:error_id]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '(?!\bpage\b)(?!\border_by\b)[a-zA-Z][a-zA-Z0-9_-]*',
                                'error_id' => '[0-9]+'
                            ),
                            'defaults' => array(
                                'controller' => 'User\Controller\Home',
                                'action' => 'index',
                            ),
                        ),
                    ),*/
                    'home' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/home[/:action][/:param1][/:param2][/]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '(?!\bpage\b)(?!\border_by\b)[a-zA-Z][a-zA-Z0-9_-]*',
                                'param1' => '[0-9]+',
                                'param2' => '[a-zA-Z][a-zA-Z0-9_-]*'
                            ),
                            'defaults' => array(
                                'controller' => 'User\Controller\Home',
                                'action' => 'index',
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'User\Controller\Index' => 'User\Controller\IndexController',
            'User\Controller\Register' => 'User\Controller\RegisterController',
            'User\Controller\Login' => 'User\Controller\LoginController',
            'User\Controller\Usermanager' => 'User\Controller\UsermanagerController',
            'User\Controller\Campaigns' => 'User\Controller\CampaignsController',
            'User\Controller\Uploadmanager' => 'User\Controller\UploadmanagerController',
            'User\Controller\Search' => 'User\Controller\SearchController',
            'User\Controller\Accheck' => 'User\Controller\AccheckController',
            'User\Controller\Onlinemanager' => 'User\Controller\OnlinemanagerController',
            'User\Controller\Mediamanager' => 'User\Controller\MediamanagerController',
            'User\Controller\Projectmanager' => 'User\Controller\ProjectmanagerController',
            'User\Controller\Projectskill' => 'User\Controller\ProjectskillController',
            'User\Controller\Frontportfolio' => 'User\Controller\FrontportfolioController',
            'User\Controller\Home' => 'User\Controller\HomeController',		
            'User\Controller\Frontproject' => 'User\Controller\FrontprojectController',
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => array(
            'layout/layout' => __DIR__ . '/../view/layout/default-layout.phtml',
            'layout/register' => __DIR__ . '/../view/layout/register-layout.phtml',
            'layout/login' => __DIR__ . '/../view/layout/login-layout.phtml',
            'user/index/index' => __DIR__ . '/../view/user/index/index.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
            'paginator-slide' => __DIR__ . '/../view/layout/paginator-layout.phtml',
            'strategies'=>array(
                'ViewJsonStrategy'
            )

        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'view_helpers' => array(
        'invokables' => array(
            'test_helper' => 'User\View\Helper\Testhelper',
            'img' => 'User\View\Helper\ViewImage'
        )
    ),
    'module_config' => array(
        'image_location' => __DIR__ . '/data/uploads',
        'upload_location' => 'public/images/advertiserimage',
        'search_index' => __DIR__ . '/data/search_index',
    ),
    'ZfcDatagrid' => array(
        'settings' => array(
            'default' => array(
                'renderer' => array(
                    'http' => 'bootstrapTable',
                    'console' => 'zendTable'
                )
            ),
            'export' => array(
                'enabled' => true,
                // currently only A formats are supported...
                'papersize' => 'A4',
                // landscape / portrait (we preferr landscape, because datagrids are often wide)
                'orientation' => 'landscape',
                'formats' => array(
                    // renderer -> display Name (can also be HTML)
                    'PHPExcel' => 'Excel',
                    'tcpdf' => 'PDF'
                ),
                // The output+save directory
                'path' => 'public/download',
                'mode' => 'direct'
            )
        ),
        'cache' => array(
            'adapter' => array(
                'name' => 'Filesystem',
                'options' => array(
                    'ttl' => 720000, // cache with 200 hours,
                    'cache_dir' => 'data/ZfcDatagrid'
                )
            ),
            'plugins' => array(
                'exception_handler' => array(
                    'throw_exceptions' => false
                ),
                'Serializer'
            )
        ),
        'renderer' => array(
            'jqGrid' => array(
                'templates' => array(
                    'layout' => 'zfc-datagrid/renderer/jqGrid/layout'
                )
            )
        )
    ),
    // Placeholder for console routes
      'di' => array(
        'instance' => array(
            'alias' => array(
                'user' => 'User\Controller\UserController'
            ),
            'user' => array(
                'parameters' => array(
                    'broker' => 'Zend\Mvc\Controller\PluginBroker'
                )
            ),
            'User\Event\Authentication' => array(
                'parameters' => array(
                    'userAuthenticationPlugin' => 'User\Controller\Plugin\UserAuthentication',
                   'aclClass'                 => 'User\Acl\Acl'
                )
            ),
           'User\Acl\Acl' => array(
                'parameters' => array(
                    'config' => include __DIR__ . '/acl.config.php'
                )
           ),
            
            'Zend\Mvc\Controller\PluginLoader' => array(
                'parameters' => array(
                    'map' => array(
                        'userAuthentication' => 'User\Controller\Plugin\UserAuthentication'
                    )
                )
            ),
            'Zend\View\PhpRenderer' => array(
                'parameters' => array(
                    'options' => array(
                        'script_paths' => array(
                            'user' => __DIR__ . '/../views'
                        )
                    )
                )
            )
        )
    ),
);