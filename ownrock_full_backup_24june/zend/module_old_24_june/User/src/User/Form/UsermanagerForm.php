<?php

namespace User\Form;

use Zend\Form\Form;

class UsermanagerForm extends Form{

    Public Function __construct($name = NULL) {
        parent:: __construct('userAdd');
        
        $this->add(array('name' => 'name',
            'attributes' => array(
                'type' => 'text',
                'required' => 'required',
            ),
            'options' => array(
                'label' => 'Full Name',
            )
        ));

        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type' => 'email',
                'required' => 'required',
            ),
            'options' => array(
                'label' => 'Email',
            )
        ));
        
         $this->add(array(
            'name' => 'Save',
            'attributes' => array(
                'type' => 'Submit',
                'value' => 'Save',
            ),
            'options' => array(
            )
        ));
    }

}
