<?php

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use User\Model\Campaigns;
use User\Model\CampaignsTable;
use User\Form\CampaignForm;
use User\Form\MapForm;
use User\Form\LocatestoreForm;
use ZfcDatagrid\Column;
use Zend\Session\Container;
use Zend\Session\SessionManager;
use User\Model\User;
use User\Model\UserTable;
use User\Model\Locatestore;
use User\Model\LocatestoreTable;
use Zend\Http\Headers;

class CampaignsController extends AbstractActionController {
private static $_fromAction1 = false;

    public function IndexAction() {
        $this->layout('layout/common-layout');

//          $auth = new \Zend\Authentication\AuthenticationService();
//        // $auth = $this->getServiceLocator()->get('AuthService');
//            
//        
//        $config = $this->getServiceLocator()->get('Config');
//        $acl = new \User\Acl\Acl($config);
//        $role = \User\Acl\Acl::DEFAULT_ROLE;
//               
//
//        
//        if ($auth->hasidentity()){
//                
//            $user = $auth->getIdentity();
//            $userTable = $this->getServiceLocator()->get('UserTable');
//            $usrId = $userTable->getUserByEmail($user->email);
//            $usr1Id = $usrId->id;
//            //echo $usr1Id;
//            switch($usr1Id) {
//                case 1:
//                    $role = \User\Acl\Acl::DEFAULT_ROLE;
//                     break;
//                case 6:
//                    $role = 'member';
//                    break;
//                default:
//                    $role = \User\Acl\Acl::DEFAULT_ROLE;
//                     break;
//                
//            }    
//            
//            }
//            
//            $controller = $this->params()->fromRoute('controller');
//            $action = $this->params()->fromRoute('action');
//            
//            if(!$acl->hasResource($controller)) {
//                throw new \Exception('Resource'.$controller.'not defined');
//            }
//            echo $role;
//            if(!$acl->isAllowed($role,$controller,$action)){
//                
//                
//            return $this->redirect()->toRoute('user/campaigns', array(
//                        'action' => 'add'
//            ));
//            }






        $campaigns = $this->getCampaignsTable()->fetchAll();
        $d = array();



        foreach ($campaigns as $campaign) {
            $data = (array) $campaign;
            array_push($d, $data);
        }

        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        $grid->setTitle('Minimal grid');
        $grid->setDataSource($d);

        $col = new Column\Select('campaign_id');
        $col->setLabel('Id');
        $col->setIdentity(1);
        $grid->addColumn($col);


        $col = new Column\Select('MerchantName');
        $col->setLabel('Merchant Name');
        $grid->addColumn($col);

        $col = new Column\Select('payout');
        $col->setLabel('Payout');
        $grid->addColumn($col);


        $col = new Column\Select('categories');
        $col->setLabel('Categories');
        $grid->addColumn($col);

        $btn2 = new Column\Action\Button();
        $btn2->setLabel('Delete');
        $rowId1 = $btn2->getRowIdPlaceholder();
        $btn2->setLink('campaigns/delete/' . $rowId1);

        $actionsDelete = new Column\Action();
        $actionsDelete->setLabel('Delete');
        $actionsDelete->addAction($btn2);
        $grid->addColumn($actionsDelete);

        $btn = new Column\Action\Button();
        $btn->setLabel('Edit');
        $rowId = $btn->getRowIdPlaceholder();
        $btn->setLink('campaigns/edit/' . $rowId);

        $actions = new Column\Action();
        $actions->setLabel('Edit');
        $actions->addAction($btn);
        $grid->addColumn($actions);

        $grid->setDefaultItemsPerPage(10);
        $grid->render();

        return $grid->getResponse();
    }

    public function editAction() {
        $this->layout('layout/common-layout');

        $id = (int) $this->params()->fromRoute('id');
        if (!$id) {

            return $this->redirect()->toRoute('user/campaigns', array(
                        'action' => 'add'
            ));
        }

        try {

            $campaign = $this->getCampaignsTable()->getCampaigns($id);
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute('user/campaigns', array(
                        'action' => 'index'
            ));
        }

        $form = new CampaignForm();
        $form->bind($campaign);
        $form->get('submit')->setAttribute('value', 'Edit');
        $request = $this->getRequest();

        if ($request->isPost()) {
            // $form->setInputFilter($album->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $this->getCampaignsTable()->saveCampaigns($campaign);
                return $this->redirect()->toRoute('user/campaigns', array(
                            'action' => 'index'));
            }
        }
        return array(
            'id' => $id,
            'form' => $form,
        );
    }

    public function deleteAction() {

        $id = (int) $this->params()->fromRoute('id', 0);

        if (!$id) {
            return $this->redirect()->toRoute('user/campaigns');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                $this->getCampaignsTable()->deleteCampaigns($id);
            }
            return $this->redirect()->toRoute('user/campaigns');
        }

        return array(
            'id' => $id,
            'campaign' => $this->getCampaignsTable()->getCampaigns($id)
        );
    }

    public function addAction() {
       
        $this->layout('layout/common-layout');
        $form = new CampaignForm();
        $form->get('submit')->setValue('Add');
        
        $request = $this->getRequest();
        if ($request->isPost()) {
           

            $campaign = new Campaigns();
//$form->setInputFilter($campaign->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {

                $campaign->exchangeArray($form->getData());
                $this->getCampaignsTable()->saveCampaigns($campaign);
                
// Redirect to list of albums
                
                return $this->redirect()->toRoute('user/campaigns', array('action' => 'index'));
            }
        }
                 

        
        return array('form' => $form);
    }
    
       public function viewpicsAction() {
        $this->layout('layout/test-layout');
        $uploadTable = $this->getServiceLocator()->get('UploadsTable');
        $campaignsTable = $this->getServiceLocator()->get('CampaignsTable');
        $viewModel = new ViewModel(array('myUploads' => $uploadTable->fetchAll(),
            'myCampaigns' => $campaignsTable->fetchAll()));
        return $viewModel;
    }

    public function addaddressAction() {

        $this->layout('layout/test-layout');
        $form = $this->getServiceLocator()->get('LocatestoreForm');
        
        $request = $this->getRequest();                
        if ($request->isPost()) {
            CampaignsController::$_fromAction1 = true;

            $locatestore = new locatestore();
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $locatestore->exchangeArray($form->getData());
                $coords = $this->getCoordinates($locatestore->address);
                $locatestore->lng = $coords[0];
                $locatestore->lat = $coords[1];
                $locatestore->campaign_id = 1;
                $locatestore->name = 'nikita';
                $locatestore->type = 'storelocation';
                
                $this->getLocatestoreTable()->saveLocatestore($locatestore);
                $fromaddress = true;
                
             
                return $this->redirect()->toRoute('user/campaigns', array('action' => 'add'));
                
            }
        }
        return array('form' => $form);
    }

    public function ProcessAction() {
        
    }

    public function MapAction() {
        $this->layout('layout/test-layout');
        $form = new mapForm();

        $request = $this->getRequest();

        if ($request->isPost()) {
            echo 'nikita';
            die;
        }
        return array('form' => $form);
    }

    public function LocationAction() {
        $data['address'] = "Wall Street, New York";
        $coords = $this->getCoordinates("Wall Street, New York");
        $data['lng'] = $coords[0];
        $data['lat'] = $coords[1];
        $data['id'] = 1;


        $locatestore->exchangeArray($data);
        $this->getLocatestoreTable()->saveLocatestore($locatestore);
        return $this->redirect()->toRoute('user/campaigns', array('action' => 'add'));
    }

    public function getCoordinates($address) {
        $address = urlencode($address);
        $url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=" . $address;
        $response = file_get_contents($url);
        $json = json_decode($response, true);

        $lat = $json['results'][0]['geometry']['location']['lat'];
        $lng = $json['results'][0]['geometry']['location']['lng'];

        return array($lat, $lng);
    }

    protected $campaignsTable;

    public function getCampaignsTable() {
        if (!$this->campaignsTable) {
            $this->campaignsTable = $this->getServiceLocator()->get('CampaignsTable');
        }
        return $this->campaignsTable;
    }

    protected $locatestoreTable;

    public function getLocatestoreTable() {
        if (!$this->locatestoreTable) {
            $this->locatestoreTable = $this->getServiceLocator()->get('LocatestoreTable');
        }
        return $this->locatestoreTable;
    }

}
