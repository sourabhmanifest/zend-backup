<?php

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use User\Form\UsereditForm;
use User\Form\LoginForm;
use User\Model\User;
use User\Model\UserTable;
use User\Model\Profileskill;
use User\Model\ProfileskillTable;
use User\Model\Merchant;
use User\Model\MerchantTable;

use User\Model\Categorylist;
use User\Model\CategorylistTable;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;
use User\Form\RegisterForm;
use Zend\Db\Sql\Select;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\Iterator as paginatorIterator;
use Zend\View\Model\JsonModel;

class SkillmanagerController extends AbstractActionController {

    protected $authservice;

    public function getAuthService() {
        $this->authservice = $this->getServiceLocator()->get('AuthService');
        return $this->authservice;
    }
    
     public function IndexAction() {
        $this->layout('layout/common-layout');
        $skillTable = $this->getServiceLocator()->get('SkillTable');        
        $paginator = $skillTable->fetchAll(true);
        $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
     // set the number of items per page to 10
        $paginator->setItemCountPerPage(5);
        $viewModel = new Viewmodel(array('paginator' => $paginator));

        //$this->getServiceLocator()->get('log')->debug("A Debug Log Message");
        return $viewModel;
    }
    
     public function EditAction() {
        $this->layout('layout/common-layout');
        $SkillTable = $this->getServiceLocator()->get('ProjectSkillTable');
        $skill = $SkillTable->getSkill($this->params()->fromRoute('id'));
        $form = $this->getServiceLocator()->get('SkilleditForm');
        $form->bind($skill);
        $viewModel = new ViewModel(array('form' => $form, 'user_id' => $this->params()->fromRoute('id')));
        return $viewModel;
    }

    public function DeleteAction() {
        $this->layout('layout/common-layout');
        $this->getServiceLocator()->get('ProjectSkillTable')->deleteSkill($this->params()->fromRoute('id'));
        return $this->redirect()->toRoute('user/projectmanager');
    }

    public function AddAction() {
        $this->layout('layout/common-layout');  
       
        $form = $this->getServiceLocator()->get('SkilleditForm');    
        $form->get('submit')->setValue('Add');     
        $request = $this->getRequest();
        if ($request->isPost()) {
            $skill = new Skill();
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $skill->exchangeArray($form->getData());
               $this->getServiceLocator()->get('SkillTable')->saveSkill($skill);
               return $this->redirect()->toRoute('user/projectmanager', array('action' => 'index'));
            }
        }
      return array('form' => $form);
    }
    
      public function AddskillAction() {
       $this->layout('layout/test-layout');  
     
        $form = $this->getServiceLocator()->get('ProfileskillForm');    
        $form->get('submit')->setValue('Add');     
        $request = $this->getRequest();
       
        if ($request->isPost()) {
            $profileskills = new Profileskill();
            $form->setData($request->getPost());

            if ($form->isValid()) {
             
                $profileskills->exchangeArray($form->getData());
               $this->getServiceLocator()->get('ProfileskillTable')->saveProfileskill($profileskills);
               die;
               return $this->redirect()->toRoute('user/onlinemanager', array('action' => 'index'));
            }
        }
      return array('form' => $form);
    }

    public function ProcessAction() {
       
        if (!$this->request->isPost()) {
            return $this->redirect()->toRoute('user/projectmanager', array('action' => 'edit'));
        }

        $post = $this->request->getPost();
        $skillTable = $this->getServiceLocator()->get('skillTable');
        $skill = $skillTable->getSkill($post->name);

        $form = $this->getServiceLocator()->get('SkillEditForm');
        $form->bind($skill);
        $form->setData($post);

        if (!$form->isValid()) {
            $model = new ViewModel(array(
                'error' => true,
                'form' => $form,
            ));
            $model->setTemplate('user/skillmanager/edit');
            return $model;
        }

        $this->getServiceLocator()->get('SkillTable')->saveMerchant($merchant);

        return $this->redirect()->toRoute('user/skillmanager');
    }

    public function UpdateAction() {
       
        $MerchantTable = $this->getServiceLocator()->get('MerchantTable');
        $merchant_id = $this->params()->fromRoute('merchant_id');
        $merchant_details = $MerchantTable->getMerchantById($this->params()->fromRoute('merchant_id'));
        $merchant_status = $this->params()->fromRoute('merchant_status');
        
        $merchant = array('id'=>$merchant_id,'status'=>$merchant_status);
        $MerchantTable->updateMerchantstatus($merchant);
        return $this->redirect()->toRoute('user/onlinemanager');
    }
  
}