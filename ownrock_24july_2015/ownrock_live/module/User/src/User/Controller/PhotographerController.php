<?php

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Zend\Http\Headers;

use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;

use User\Form\RegisterForm;
//use User\Form\RegisterFilter;

use User\Model\User;
use User\Model\UserTable;
use User\Model\Uploads;

use User\Model\Photographer;
use User\Model\PhotographerTable;

use User\Model\ImageUpload;
use User\Model\ImageUploadTable;
use User\Model\Album;
use User\Model\AlbumTable;

use User\Model\Tag;
use User\Model\TagTable;

use User\Model\AlbumTag;
use User\Model\AlbumTagTable;

use User\Model\AlbumImage;
use User\Model\AlbumImageTable;

use ZendSearch\Lucene;
use ZendSearch\Lucene\Document;
use ZendSearch\Lucene\Index;

use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;
use Zend\Session\Container;
use Zend\Mail\Message;

use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;

use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\FacebookRedirectLoginHelper;

use Facebook\FacebookSDKException;



class PhotographerController extends AbstractActionController
{
    protected $authservice;
    public function IndexAction()
    {
    	$this->layout('layout/home-layout');
        return new ViewModel(array());
    }

    public function DashboardAction()
    {   $user_session = new Container('user');
    
        if($user_session->email == "")
        {
            return $this->redirect()->toRoute('user/home');
        }
		//echo $user_session->user_id; exit;
		$photographerdata = $this->getServiceLocator()->get('PhotographerTable')->getPhotographerData($user_session->user_id);
		//echo '<pre>';print_r($photographerdata);exit;
        $this->layout('layout/dashboard-layout');
		return new ViewModel(array('photographerdata'=>$photographerdata));
    }

    /*public function ProfileAction()
    {
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/home');
        }
        $this->layout('layout/photographer-layout');
        return new ViewModel(array());
    }*/

    public function CreateProfileAction()
    {
        $user_session = new Container('user');
        $error = "";
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/photographer', array('action' => 'dashboard'));
        }
        $photographerdata = $this->getServiceLocator()->get('PhotographerTable')->getPhotographerData($user_session->user_id);
        $userdata = $this->getServiceLocator()->get('UserTable')->getUser($user_session->user_id);
        $total_albums = $this->getServiceLocator()->get('AlbumTable')->getTotalAlbumsByUserid($user_session->user_id);
        $photographerdata->phone = $userdata->phone;
        $photographerdata->total_albums = $total_albums;
        $this->layout('layout/photographer-layout');
        $request = $this->getRequest();
        if ($request->isPost()) 
        {
            $request = $this->getRequest();
            $photographer = new Photographer();
            $arr = $request->getPost($arr);
            $arr['user_id']=$user_session->id;

            if($arr['name']!="" && $arr['email']!="" && $arr['occupation']!="" && $arr['phone']!="")
            {
                $arr['profile_progress']=1;
            }
            else
            {
                $arr['profile_progress']=0;
            }
            if($arr['name']=="" || $arr['email']=="" || $arr['occupation']=="" || $arr['description'] =="" ||  $arr['phone']==""|| $arr['location']=="")
            {
                $error="Please fill all the required fields";
            } //echo '<pre>';print_r($arr);
            $photographer->exchangeArray($arr);

            //echo '<pre>';print_r($photographer);exit;
            $this->getServiceLocator()->get('PhotographerTable')->savePhotographer($photographer);

            $this->getServiceLocator()->get('UserTable')->updateUserPhone($arr['user_id'],$arr['phone']);
            return $this->redirect()->toRoute('user/photographer', array('action' => 'createalbum'));
        }
        //return $this->redirect()->toRoute('user/photographer', array('action' => 'dashboard'));
        return new ViewModel(array('photographerdata'=>$photographerdata));
    }

    public function ProfileAction()
    {
        $user_session = new Container('user');
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/photographer', array('action' => 'dashboard'));
        }
        $total_albums = 0;
        
        $photographerdata = $this->getServiceLocator()->get('PhotographerTable')->getPhotographerData($user_session->user_id);
        $userdata = $this->getServiceLocator()->get('UserTable')->getUser($user_session->user_id);
        $total_albums = $this->getServiceLocator()->get('AlbumTable')->getTotalAlbumsByUserid($user_session->user_id);
        $photographerdata->phone = $userdata->phone;
        $photographerdata->total_albums = $total_albums;

		 //profile image path 
        $config = $this->getServiceLocator()->get('config');
        $profile_img_path = $config['module_config']['photographer_profile_image'];

        $config = $this->getServiceLocator()->get('config');
        $album_image_path = $config['module_config']['album_image_path'];
        


        $albumsdataarry = $this->getServiceLocator()->get('AlbumTable')->getallAlbumsByUserid($user_session->user_id);
        $albumsdata = array();
        foreach($albumsdataarry as $albumsdata_row)
        {
            // echo '<pre>';print_r($albumsdata_row['album_id']);exit;
            $albumimagedata = $this->getServiceLocator()->get('AlbumImageTable')->getImagesByAlbumId($albumsdata_row['album_id']);
            $albumimagedata_array['image'] = $albumimagedata;
            //$photogdata = (array)$photogdata;
            $albumsdata[] = array_merge($albumsdata_row,$albumimagedata_array);
        }
        //echo '<pre>';print_r($albumsdata);exit;
        $albumtagdata_array = array();
        $tagdata = array();
        foreach($albumsdataarry as $albumsdata_row)
        {
            
            $albumtagdataarray = $this->getServiceLocator()->get('AlbumTagTable')->getTagByAlbumId($albumsdata_row['album_id']);
            foreach($albumtagdataarray as $albumtagdata_row)
            {
                //echo '<pre>';print_r($albumtagdata_row);exit;
                if(!in_array($albumtagdata_row['tag_id'], $albumtagdata_array)) {
                    $albumtagdata_array[]=$albumtagdata_row['tag_id'];
                }
            }
        }

        foreach($albumtagdata_array as $albumtagdata)
        {
            $album_Tags[] = (array)$this->getServiceLocator()->get('TagTable')->getTagByTagId($albumtagdata);
        }
        sort($album_Tags);
        //echo '<pre>';print_r($album_Tags);exit;
        $this->layout('layout/photographer-layout');
        return new ViewModel(array(
            'photographerdata' => $photographerdata, 
            'albumsdata' =>$albumsdata, 
            'album_Tags' => $album_Tags, 
            'profile_img_path' => $profile_img_path, 
            'album_image_path' => $album_image_path,
            'albumtagdata_array' => $albumtagdata_array
            ));
    }


    public function EditProfileAction()
    {
        $user_session = new Container('user');
        $error = "";
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/photographer', array('action' => 'dashboard'));
        }
        $this->layout('layout/photographer-layout');
        $photographerdata = $this->getServiceLocator()->get('PhotographerTable')->getPhotographerData($user_session->user_id);
        $userdata = $this->getServiceLocator()->get('UserTable')->getUser($user_session->user_id);
        $total_albums = $this->getServiceLocator()->get('AlbumTable')->getTotalAlbumsByUserid($user_session->user_id);
        $photographerdata->phone = $userdata->phone;
        $photographerdata->total_albums = $total_albums;

        //profile image path 
        $config = $this->getServiceLocator()->get('config');
        $profile_img_path = $config['module_config']['photographer_profile_image'];
        //echo '<pre>';print_r($photographerdata);exit;
        
        
        //return new ViewModel(array());

        $request = $this->getRequest();
        if ($request->isPost()) 
        {
            $request = $this->getRequest();
            $photographer = new Photographer();
            $arr = $request->getPost($arr);
            //echo '<pre>';print_r($arr);exit;
            $arr['user_id']=$user_session->id;

            if($arr['name']!="" && $arr['email']!="" && $arr['occupation']!="" && $arr['phone']!="")
            {
                $arr['profile_progress']=1;
            }
            else
            {
                $arr['profile_progress']=0;
            }
            if($arr['name']=="" || $arr['email']=="" || $arr['occupation']=="" || $arr['description'] =="" ||  $arr['phone']==""|| $arr['location']=="")
            {
                $error="Please fill all the required fields";
            } //echo '<pre>';print_r($arr); exit;
            $photographer->exchangeArray($arr);

            //echo '<pre>';print_r($photographer);exit;
            $this->getServiceLocator()->get('PhotographerTable')->savePhotographer($photographer);

            $this->getServiceLocator()->get('UserTable')->updateUserPhone($arr['user_id'],$arr['phone']);
            return $this->redirect()->toRoute('user/photographer', array('action' => 'editprofile'));
        }
        //return $this->redirect()->toRoute('user/photographer', array('action' => 'dashboard'));

        $this->layout()->photographerinfo = $photographerdata;
        return new ViewModel(array('photographerdata'=>$photographerdata, 'profile_img_path' => $profile_img_path));
    }

    public function allProfilesAction()
    {
        $user_session = new Container('user');
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/photographer', array('action' => 'dashboard'));
        }
        $this->layout('layout/photographer-layout');

        $total_albums = 0;
        $photographerdata = $this->getServiceLocator()->get('PhotographerTable')->getPhotographerData($user_session->user_id);
        $userdata = $this->getServiceLocator()->get('UserTable')->getUser($user_session->user_id);
        $total_albums = $this->getServiceLocator()->get('AlbumTable')->getTotalAlbumsByUserid($user_session->user_id);
        $photographerdata->phone = $userdata->phone;
        $photographerdata->total_albums = $total_albums;
        $allphotographeruserdata = $this->getServiceLocator()->get('UserTable')->getUserByUserType(2, $user_session->user_id);
        //echo '<pre>'; print_r($allphotographeruserdata);exit;
        $dataarray = array();
        foreach($allphotographeruserdata as $allphotographeruser)
        {   
            $id = $allphotographeruser['id']; 
            $photogdata = $this->getServiceLocator()->get('PhotographerTable')->getProfileByUserId($id);
            $photogdata = (array)$photogdata;
            $dataarray[] = array_merge($allphotographeruser,$photogdata);
        }
        //echo '<pre>'; print_r($dataarray);exit;

        $config = $this->getServiceLocator()->get('config');
        $profile_img_path = $config['module_config']['photographer_profile_image'];

        return new ViewModel(array('dataarray' => $dataarray, 'profile_img_path' => $profile_img_path,  'photographerdata' => $photographerdata));
    }

    public function PhotographerProfileAction()
    {   
        $user_session = new Container('user');
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/photographer', array('action' => 'dashboard'));
        }
        $pg_id = $this->params()->fromRoute('param1');
        $total_albums = 0;
        $photographerdata = $this->getServiceLocator()->get('PhotographerTable')->getPhotographerData($pg_id);
        $userdata = $this->getServiceLocator()->get('UserTable')->getUser($pg_id);
        $total_albums = $this->getServiceLocator()->get('AlbumTable')->getTotalAlbumsByUserid($pg_id);
        $photographerdata->phone = $userdata->phone;
        $photographerdata->phone = $userdata->name;
        $photographerdata->phone = $userdata->email;
        $photographerdata->total_albums = $total_albums;

         //profile image path 
        $config = $this->getServiceLocator()->get('config');
        $profile_img_path = $config['module_config']['photographer_profile_image'];


        $albumsdataarry = $this->getServiceLocator()->get('AlbumTable')->getallAlbumsByUserid($pg_id);
        // $albumsdataarry = $this->getServiceLocator()->get('AlbumTable')->getallAlbumsByUserid($user_session->user_id);
        $albumsdata = array();
        foreach($albumsdataarry as $albumsdata_row)
        {
            // echo '<pre>';print_r($albumsdata_row['album_id']);exit;
            $albumimagedata = $this->getServiceLocator()->get('AlbumImageTable')->getImagesByAlbumId($albumsdata_row['album_id']);
            $albumimagedata_array['image'] = $albumimagedata;
            //$photogdata = (array)$photogdata;
            $albumsdata[] = array_merge($albumsdata_row,$albumimagedata_array);
        }
        $album_Tags = $this->getServiceLocator()->get('TagTable')->getTag();

        //echo '<pre>';print_r($album_Tags);exit;
        $this->layout('layout/photographer-layout');

        return new ViewModel(array('photographerdata' => $photographerdata, 'albumsdata' =>$albumsdata, 'album_Tags' => $album_Tags, 'profile_img_path' => $profile_img_path));
    }


    public function CreateAlbumAction()
    {
        $user_session = new Container('user');
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/photographer', array('action' => 'dashboard'));
        }
        $total_albums = 0;
        $photographerdata = $this->getServiceLocator()->get('PhotographerTable')->getPhotographerData($user_session->user_id);
        $userdata = $this->getServiceLocator()->get('UserTable')->getUser($user_session->user_id);
        $total_albums = $this->getServiceLocator()->get('AlbumTable')->getTotalAlbumsByUserid($user_session->user_id);
        $photographerdata->phone = $userdata->phone;
        $photographerdata->total_albums = $total_albums;
        
        $request = $this->getRequest();
        if ($request->isPost()) 
        {   $arr = $request->getPost($arr);
            $arr['user_id']=$user_session->id;
            $album = new Album();
            $album->exchangeArray($arr);
            //echo '<pre>';print_r($album);  exit;
            $last_album_id = $this->getServiceLocator()->get('AlbumTable')->saveAlbum($album);  
            foreach($request->getPost()->e2 as $tagsdata)
            {
                $this->getServiceLocator()->get('AlbumTagTable')->saveAlbumTags($last_album_id,$tagsdata);
            }

            $images_name_array = explode(',',trim($arr['images_name'],','));
            //echo '<pre>'; print_r($images_name_array);
            foreach($images_name_array as $images)
            {   
            //echo '<pre>'; print_r($albumimage);
                $image_arr = array();
                $image_arr['album_id'] = $last_album_id;
                $image_arr['imagename'] = $images;
                $image_arr['thumbname'] = "thumbnail_".$images;
                //echo '<pre>'; print_r($image_arr);
                $albumimage = new AlbumImage();
                $albumimage->exchangeArray($image_arr);
                //echo '<pre>'; print_r($albumimage);
                $this->getServiceLocator()->get('AlbumImageTable')->saveImages($albumimage);
            }
            return $this->redirect()->toRoute('user/photographer', array('action' => 'invitefriends'));
        }

        /*$MerchantTable = $this->getServiceLocator()->get('MerchantTable');
        $result_merchant = $MerchantTable->getMerchantByEmail($user_session->email); */
        $this->layout('layout/photographer-layout');
        
        $album_data = array();
        $total_albums = $this->getServiceLocator()->get('AlbumTable')->getTotalAlbumsByUserid($user_session->id);
        $album_data['total_albums'] = $total_albums; 
        //echo '<pre>';print_r($album_data);exit;

        // echo "in";exit;
        $album_Tags = $this->getServiceLocator()->get('TagTable')->getTag();
        
        
        //echo '<pre>';print_r($album_Tags);exit;
        //return new ViewModel(array()); 
        
        $this->layout()->albuminfo = $album_data;
        return new ViewModel(array('album_data'=>$album_data,'album_Tags'=>$album_Tags,'photographerdata'=>$photographerdata));
    }

    public function AddAlbumAction()
    {
        $user_session = new Container('user');
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/photographer', array('action' => 'dashboard'));
        }
        $total_albums = 0;
        $photographerdata = $this->getServiceLocator()->get('PhotographerTable')->getPhotographerData($user_session->user_id);
        $userdata = $this->getServiceLocator()->get('UserTable')->getUser($user_session->user_id);
        $total_albums = $this->getServiceLocator()->get('AlbumTable')->getTotalAlbumsByUserid($user_session->user_id);
        $photographerdata->phone = $userdata->phone;
        $photographerdata->total_albums = $total_albums;
        
        $request = $this->getRequest();
        if ($request->isPost()) 
        {   $arr = $request->getPost($arr);
            $arr['user_id']=$user_session->id;
            $album = new Album();
            $album->exchangeArray($arr);
            //echo '<pre>';print_r($arr);exit;
            $last_album_id = $this->getServiceLocator()->get('AlbumTable')->saveAlbum($album);  
            foreach($request->getPost()->e2 as $tagsdata)
            {
                $this->getServiceLocator()->get('AlbumTagTable')->saveAlbumTags($last_album_id,$tagsdata);
            }
            //echo $arr['album_name'];exit;
            if(!empty($arr->images_name))
            {   
                $images_name_array = explode(',',trim($arr['images_name'],','));
                //echo '<pre>'; print_r($images_name_array);
                foreach($images_name_array as $images)
                {   
                //echo '<pre>'; print_r($albumimage);
                    $image_arr = array();
                    $image_arr['album_id'] = $last_album_id;
                    $image_arr['imagename'] = $images;
                    $image_arr['thumbname'] = "thumbnail_".$images;
                    //echo '<pre>'; print_r($image_arr);
                    $albumimage = new AlbumImage();
                    $albumimage->exchangeArray($image_arr);
                    //echo '<pre>'; print_r($albumimage);
                    $this->getServiceLocator()->get('AlbumImageTable')->saveImages($albumimage);
                }
            }
            return $this->redirect()->toRoute('user/photographer', array('action' => 'profile'));
        }

        /*$MerchantTable = $this->getServiceLocator()->get('MerchantTable');
        $result_merchant = $MerchantTable->getMerchantByEmail($user_session->email); */
        $this->layout('layout/photographer-layout');
        
        $album_data = array();
        $total_albums = $this->getServiceLocator()->get('AlbumTable')->getTotalAlbumsByUserid($user_session->id);
        $album_data['total_albums'] = $total_albums; 
        //echo '<pre>';print_r($album_data);exit;
        $config = $this->getServiceLocator()->get('config');
        $profile_img_path = $config['module_config']['photographer_profile_image'];

        // echo "in";exit;
        $album_Tags = $this->getServiceLocator()->get('TagTable')->getTag();
        
        
        //echo '<pre>';print_r($album_Tags);exit;
        //return new ViewModel(array()); 
        
        $this->layout()->albuminfo = $album_data;
        return new ViewModel(array('album_data'=>$album_data,'album_Tags'=>$album_Tags,'photographerdata'=>$photographerdata, 'profile_img_path' => $profile_img_path));
    }


    public function AlbumAction()
    {
        $user_session = new Container('user');
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/photographer', array('action' => 'dashboard'));
        }
        $total_albums = 0;
        $photographerdata = $this->getServiceLocator()->get('PhotographerTable')->getPhotographerData($user_session->user_id);
        $userdata = $this->getServiceLocator()->get('UserTable')->getUser($user_session->user_id);
        $total_albums = $this->getServiceLocator()->get('AlbumTable')->getTotalAlbumsByUserid($user_session->user_id);
        $photographerdata->phone = $userdata->phone;
        $photographerdata->total_albums = $total_albums;

        //$albumsdata = $this->getServiceLocator()->get('AlbumTable')->getallAlbumsByUserid($user_session->user_id);
        //echo '<pre>';print_r($albumsdata);exit;
        $this->layout('layout/photographer-layout');
        return new ViewModel(array('photographerdata'=>$photographerdata));
    }

    public function AlbumDetailAction()
    {
        $user_session = new Container('user');
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/photographer', array('action' => 'dashboard'));
        }
        $total_albums = 0;
        $photographerdata = $this->getServiceLocator()->get('PhotographerTable')->getPhotographerData($user_session->user_id);
        $userdata = $this->getServiceLocator()->get('UserTable')->getUser($user_session->user_id);
        $total_albums = $this->getServiceLocator()->get('AlbumTable')->getTotalAlbumsByUserid($user_session->user_id);
        $photographerdata->phone = $userdata->phone;
        $photographerdata->total_albums = $total_albums;
        $album_id = $this->params()->fromRoute('param1');
        if($user_session->user_id != $album_id)
        {
            $editable = 0;
        }
        else
        {
            $editable = 1;
        }
        $albumsdata = $this->getServiceLocator()->get('AlbumTable')->getAlbumsByAlbumId($album_id);
        //echo '<pre>';print_r($albumsdata);
        
        $config = $this->getServiceLocator()->get('config');
        $album_image_path = $config['module_config']['album_image_path'];
        
        $config = $this->getServiceLocator()->get('config');
        $profile_img_path = $config['module_config']['photographer_profile_image'];
        
        
        $albumimagedata = $this->getServiceLocator()->get('AlbumImageTable')->getImagesByAlbumId($album_id);
        $albumsdata->image = $albumimagedata;
       // echo '<pre>';print_r($albumsdata);exit;
        $this->layout('layout/photographer-layout');
        return new ViewModel(array('photographerdata'=>$photographerdata, 'albumsdata' => $albumsdata , 'album_image_path' => $album_image_path, 'profile_img_path' => $profile_img_path, 'editable' =>$editable));
    }

    public function EditAlbumAction()
    {
        $user_session = new Container('user');
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/photographer', array('action' => 'dashboard'));
        }
        $total_albums = 0;
        $photographerdata = $this->getServiceLocator()->get('PhotographerTable')->getPhotographerData($user_session->user_id);
        $userdata = $this->getServiceLocator()->get('UserTable')->getUser($user_session->user_id);
        $total_albums = $this->getServiceLocator()->get('AlbumTable')->getTotalAlbumsByUserid($user_session->user_id);
        $photographerdata->phone = $userdata->phone;
        $photographerdata->total_albums = $total_albums;

        $album_id = $this->params()->fromRoute('param1');
        
        $album_data = $this->getServiceLocator()->get('AlbumTable')->getAlbumsByAlbumId($album_id);
        $config = $this->getServiceLocator()->get('config');
        $album_image_path = $config['module_config']['album_image_path'];

        $album_data->album_image_path = $album_image_path;
        
        $albumimagedata = $this->getServiceLocator()->get('AlbumImageTable')->getImagesByAlbumId($album_id);
        $album_data->image = $albumimagedata;
        
        $album_tag_data = $this->getServiceLocator()->get('AlbumTagTable')->getAlbumTags($album_id);
        $album_data->tags = $album_tag_data;
        //echo '<pre>';print_r($album_tag_data); exit;
        $config = $this->getServiceLocator()->get('config');
        $profile_img_path = $config['module_config']['photographer_profile_image'];

        
        $request = $this->getRequest();
        if ($request->isPost()) 
        {   $arr = $request->getPost($arr);
            $arr['user_id']=$user_session->id;
            $album = new Album();
            $album->exchangeArray($arr);
            //echo '<pre>';print_r($album);  exit;
            $last_album_id = $this->getServiceLocator()->get('AlbumTable')->saveAlbum($album);  
            $this->getServiceLocator()->get('AlbumTagTable')->deleteAlbumTags($last_album_id);
            foreach($request->getPost()->e2 as $tagsdata)
            {
                $this->getServiceLocator()->get('AlbumTagTable')->saveAlbumTags($last_album_id,$tagsdata);
            }
            $images_name_array = explode(',',$arr['images_name']);

            foreach($images_name_array as $images)
            {
                $image_arr = array();
                $image_arr['album_id'] = $last_album_id;
                $image_arr['imagename'] = $images.".jpg";
                $image_arr['thumbname'] = "thumbnail_".$image_arr['imagename'];
                $albumimage = new AlbumImage();
                $albumimage->exchangeArray($image_arr);
                //echo '<pre>'; print_r($albumimage); exit;
                $this->getServiceLocator()->get('AlbumImageTable')->saveImages($albumimage);
            }
            return $this->redirect()->toRoute('user/photographer', array('action' => 'editalbum'));
        }

        /*$MerchantTable = $this->getServiceLocator()->get('MerchantTable');
        $result_merchant = $MerchantTable->getMerchantByEmail($user_session->email); */
        $this->layout('layout/photographer-layout');
        
        
        $total_albums = $this->getServiceLocator()->get('AlbumTable')->getTotalAlbumsByUserid($user_session->id);
        $album_data->total_albums = $total_albums; 
        //echo '<pre>';print_r($album_data);exit;

        // echo "in";exit;
        $album_Tags = $this->getServiceLocator()->get('TagTable')->getTag();
        
        
        //echo '<pre>';print_r($album_Tags);exit;
        //return new ViewModel(array()); 
        
        $this->layout()->albuminfo = $albumsdata;
        return new ViewModel(array('album_data'=>$album_data,'album_Tags'=>$album_Tags,'photographerdata'=>$photographerdata, 'profile_img_path' => $profile_img_path, 'album_image_path' => $album_image_path));
    }

    public function InviteFriendsAction()
    {
        $user_session = new Container('user');
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/home');
        }
        $this->layout('layout/photographer-layout');
        $photographerdata = $this->getServiceLocator()->get('PhotographerTable')->getPhotographerData($user_session->user_id);
        $userdata = $this->getServiceLocator()->get('UserTable')->getUser($user_session->user_id);
        $total_albums = $this->getServiceLocator()->get('AlbumTable')->getTotalAlbumsByUserid($user_session->user_id);
        $photographerdata->phone = $userdata->phone;
        $photographerdata->total_albums = $total_albums;
        $this->layout('layout/photographer-layout');
        return new ViewModel(array('photographerdata'=>$photographerdata));
    }

    public function restructureArrayAction(array $images)
    {
        $result = array();

        foreach ($images as $key => $value) 
        {
            foreach ($value as $k => $val) 
            {
                for ($i = 0; $i < count($val); $i++) 
                {
                    $result[$i][$k] = $val[$i];
                }
            }
        }
        return $result;
    }

    public function uploadImgAction()
    {
        define('UPLOAD_PROD_IMG_PATH',"public/images/photographer/uploads/");
        $data = array();
        if(isset( $_POST['image_upload'] ) && !empty( $_FILES['images'] ))
        {
            //get the structured array
            $images = $this->restructureArrayAction($_FILES);
            $allowedExts = array("gif", "jpeg", "jpg", "png");
            
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            
            foreach ( $images as $key => $value){
                $i = $key+1;
                //create directory if not exists
                if (!file_exists('images')) {
                    mkdir('images', 0777, true);
                }
                $image_name = $value['name'];
                //get image extension
                $ext = strtolower(pathinfo($image_name, PATHINFO_EXTENSION));
                //assign unique name to image
                $imgnamew = time();
                $name = $i*$imgnamew.'.'.$ext;
                //$name = $image_name;
                //image size calcuation in KB
                $image_size = $value["size"] / 1024;
                $image_flag = true;
                //max image size
                $max_size = 1024;
                if( in_array($ext, $allowedExts) && $image_size < $max_size ){
                    $image_flag = true;
                } else {
                    $image_flag = false;
                    $data[$i]['error'] = 'Maybe '.$image_name. ' exceeds max '.$max_size.' KB size or incorrect file extension';
                } 
                
                if( $value["error"] > 0 ){
                    $image_flag = false;
                    $data[$i]['error'] = '';
                    $data[$i]['error'].= '<br/> '.$image_name.' Image contains error - Error Code : '.$value["error"];
                }
                
                if($image_flag){
                    ///mkdir(UPLOAD_PROD_IMG_PATH,0777);
                    move_uploaded_file($value["tmp_name"], UPLOAD_PROD_IMG_PATH.$name);
                    $src = UPLOAD_PROD_IMG_PATH.$name;
                    $dist =UPLOAD_PROD_IMG_PATH."thumbnail_".$name;
                    $data[$i]['success'] = $thumbnail = 'thumbnail_'.$name;
                    $data[$i]['name'] = $name;
                    $data[$i]['unique_id'] =  time();
                    $this->thumbnailAction($src, $dist, 400);    
                    
                    
                    $asset['name'] = $name;
                    $asset['post_type'] = 'product';
                    $asset['status'] = 0;
                    $asset['date'] = date("y-m-d H:i:s");
                    $asset['location'] = UPLOAD_PROD_IMG_PATH;
                    $asset['unique_id'] =$data[$i]['unique_id'];
                    //$this->products_model->add_assets($asset);
                
                }
            }

            echo json_encode($data); die;
            
        } else {
                $data[] = 'No Image Selected..';
        }
    }

    function thumbnailAction($src, $dist, $dis_width = 100 )
    {

        $img = '';
        $extension = strtolower(strrchr($src, '.'));
        switch($extension)
        {
            case '.jpg':
            case '.jpeg':
                $img = @imagecreatefromjpeg($src);
                break;
            case '.gif':
                $img = @imagecreatefromgif($src);
                break;
            case '.png':
                $img = @imagecreatefrompng($src);
                break;
        }
        $width = imagesx($img);
        $height = imagesy($img);
        $dis_height = $dis_width * ($height / $width);

        $new_image = imagecreatetruecolor($dis_width, $dis_height);
        imagecopyresampled($new_image, $img, 0, 0, 0, 0, $dis_width, $dis_height, $width, $height);


        $imageQuality = 100;

        switch($extension)
        {
            case '.jpg':
            case '.jpeg':
                if (imagetypes() & IMG_JPG) {
                    imagejpeg($new_image, $dist, $imageQuality);
                }
                break;

            case '.gif':
                if (imagetypes() & IMG_GIF) {
                    imagegif($new_image, $dist);
                }
                break;

            case '.png':
                $scaleQuality = round(($imageQuality/100) * 9);
                $invertScaleQuality = 9 - $scaleQuality;

                if (imagetypes() & IMG_PNG) {
                    imagepng($new_image, $dist, $invertScaleQuality);
                }
                break;
        }
        imagedestroy($new_image);
    }

    public function editprofileimageAction() 
    {
        $user_session = new Container('user');       
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/photographer', array('action' => 'dashboard'));
        }
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $config = $this->getServiceLocator()->get('config');
            $uploadPath = $config['module_config']['photographer_profile_image'];

            $uploadFile = $this->params()->fromFiles('profilepic');
            
            //echo '<pre>'; print_r($_FILES);
            //echo '<pre>';  print_r($uploadFile); die;

            $adapter = new \Zend\File\Transfer\Adapter\Http();
            
            //echo $adapter;exit;
            $adapter->setDestination($uploadPath);

            $adapter->receive($uploadFile['name']);
            //echo '<pre>';  print_r($uploadFile); die;
            
            $rand = rand(2000,100000);
            $old = $uploadPath.'/'.$uploadFile['name'];
            $new = $uploadPath.'/'.$rand.'_'.$uploadFile['name'];
            rename($old, $new);
            
            $sourceImageFileName = $uploadPath . '/' . $rand.'_'.$uploadFile['name'];
            $thumbnailFileName = $rand.'_'.'tn120x120_'.$uploadFile['name'];
            $imageThumb = $this->getServiceLocator()->get('WebinoImageThumb');
            $thumb = $imageThumb->create($sourceImageFileName, $options = array());
            $thumb->resize(120, 120);
            $thumb->save($uploadPath . '/' . $thumbnailFileName);
            
            $profilepic = $rand . '_pp_' .$uploadFile['name']; 
            $this->getServiceLocator()->get('PhotographerTable')->updateProfileImage($user_session->user_id,$profilepic);
            return $this->redirect()->toRoute('user/photographer', array('action' => 'editprofile'));
            
        }
        
    }

    public function deleteImgAction() 
    {
        $user_session = new Container('user');  
        //echo $user_session->email;exit;     
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/photographer', array('action' => 'dashboard'));
        }
        //
        $request = $this->getRequest();
        if ($request->isPost()) 
        {
            $arr = $request->getPost();
            $unique_id = $arr['id'];
            $image_name = $arr['name'];
            //$photographerdata = $this->getServiceLocator()->get('PhotographerTable')->getPhotographerData($user_session->user_id);
            $config = $this->getServiceLocator()->get('config');
            $album_image_path = $config['module_config']['album_image_path'];
            unlink($album_image_path.$image_name);
            //echo '<pre>';print_r($arr);
            $response = 'success';
        }
        else 
        {
            $response = 'failed';
        }
        echo json_encode(array('response'=>$response));
        die();
    }

    public function filteredalbum() 
    {
        //echo "in"; exit;
        $user_session = new Container('user');  
        //echo $user_session->email;exit;     
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/photographer', array('action' => 'dashboard'));
        }
        //
        $request = $this->getRequest();
        if ($request->isPost()) 
        {
            $arr = $request->getPost();
            $tag_id = $arr['tag_id'];
            $user_id = $user_session->user_id;
            
            $albumsdataarry = $this->getServiceLocator()->get('AlbumTable')->getallAlbumsByUserid($user_session->user_id);
            //echo '<pre>'; print_r($albumsdataarry);exit;
            $albumsdata = array();
          
            foreach($albumsdataarry as $albumsdata_row)
            {
                $albumtagdataarray = $this->getServiceLocator()->get('AlbumTagTable')->getTagByAlbumandtagI($albumsdata_row['album_id'],$tag_id);
                if(!empty($albumtagdataarray))
                {
                    $albumimagedata = $this->getServiceLocator()->get('AlbumImageTable')->getImagesByAlbumId($albumtagdataarray['album_id']);

                    $albumimagedata_array['image'] = $albumimagedata;
                    //$photogdata = (array)$photogdata;
                    $albumsdata[] = array_merge($albumsdata_row,$albumimagedata_array);
                }
            }
            //echo '<pre>'; print_r($albumsdata);exit;     
            $response = 'success';

        }
        else 
        {
            $response = 'failed';
        }
        echo json_encode(array('response'=>$response, 'albumsdata'=>$albumsdata));
        die();
    }


}