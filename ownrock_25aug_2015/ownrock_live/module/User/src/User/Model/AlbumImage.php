<?php

namespace User\Model;

class AlbumImage {

    public $id;
    public $album_id;
    public $imagename;
    public $thumbname;
    public $description;

    public function exchangeArray($data) {
        $this->id = (!empty($data['id'])) ? $data['id'] : '';
        $this->album_id = (!empty($data['album_id'])) ? $data['album_id'] : '';
        $this->imagename = (!empty($data['imagename'])) ? $data['imagename'] : '';
        $this->thumbname = (!empty($data['thumbname'])) ? $data['thumbname'] : '';
        $this->description = (!empty($data['description'])) ? $data['description'] : '';
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}
