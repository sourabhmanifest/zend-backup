<?php
namespace User\Model;

use Zend\Text\Table\Row;

use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;

 
class UserTable
{
    protected $tableGateway;
    
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    public function saveUser(User $user)
    {
        
		//echo "<pre>"; print_r($user); exit;
		$data = array(
            'email' => $user->email,
			'fb_id' => $user->fb_id,
            'name' => $user->name,
            'password' => $user->password,
            'user_type' => $user->user_type,
            'verification_code' => $user->verification_code,
            'registration_date' => $user->registration_date,
            'status' => $user->status,
        );
        
        $id = (int)$user->id;
        if ($id == 0){
            $this->tableGateway->insert($data); 
            return $this->tableGateway->getLastInsertValue();
        } else {
            if ($this->getUser($id)) {
                if (empty($data['password'])){
                    unset($data['password']);
                }
                $this->tableGateway->update($data,array('id'=>$id));
            }else{
                throw new \Exception('User ID does not exist');
            }   
        }        
    }
    public function getUser($id)
    {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if(!$row){
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }
    public function getUserByUserType($user_type, $user_id)
    {
        $user_type = (int)$user_type;
        $user_id = (int)$user_id;
        /*$where = new Where();    
        $where->equalTo('user_type', $user_type)
        ->and
        ->notEqualTo('id',$user_id );
        $rowset = $this->tableGateway->select($where);
        return $rowset->toArray();*/
        $rowset = $this->tableGateway->select(array('user_type' => $user_type));
        return $rowset->toArray();
    }
    
    public function getUserByEmail($email)
    {
        $email = (string)$email;    
        $rowset = $this->tableGateway->select(array('email' => $email));
        $row = $rowset->current();
        return $row;
    }
    
    public function fetchAll($paginated=flase)
    { 
        if ($paginated) {
             // create a new Select object for the table album
             $select = new Select('user');
             // create a new result set based on the Album entity
             $resultSetPrototype = new ResultSet();
             $resultSetPrototype->setArrayObjectPrototype(new User());
             // create a new pagination adapter object
             $paginatorAdapter = new DbSelect(
                 // our configured select object
                 $select,
                 // the adapter to run it against
                 $this->tableGateway->getAdapter(),
                 // the result set to hydrate
                 $resultSetPrototype
             );
             $paginator = new Paginator($paginatorAdapter);
             return $paginator;
         }

        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
    
    public function deleteUser($id) {
        $this->tableGateway->delete(array('id' => (int) $id));
    }
    
    public function verifyUser($vericode)
    {
        $rowset = $this->tableGateway->select(array('verification_code' => "$vericode"));
        $row = $rowset->current();
        
        if($row->status == 0){
            $data['status'] = 1;
            $this->tableGateway->update($data,array('verification_code'=>$vericode));
            $message = 'SUCCESS_VERIFICATION';
        }else{
            $message = 'ALREADY_VERIFIED';
        }
        return $message.'|'.$row->email.'|'.$row->name.'|'.$row->id;
    }

	public function resetPassword($data)
    {
        $rowset = $this->tableGateway->select(array('verification_code' => $data['token']));
		$row = $rowset->current();
        if($rowset->count()>0)
		{		
			$res = array(
				'password' => md5($data['rpassword']),
				'verification_code' => '',           
			);			
			$this->tableGateway->update($res,array('verification_code'=>$data['token']));
		}
	}
	
	public function checkToken($data)
    {
        $rowset = $this->tableGateway->select(array('verification_code' => $data['token']));
		$row = $rowset->current();
        if($rowset->count()>0)
		{		
			return 1;
		}
	}


	public function updatetmpPasswordUser($token, $email)
    {
        $rowset = $this->tableGateway->select(array('email' => $email));
        $row = $rowset->current();
        
        if($rowset->count()>0){
            $data['verification_code'] = $token;
            $this->tableGateway->update($data,array('email'=> $email));
            $message = 'SUCCESS';
        }else{
            $message = 'NOTAVAILABLE';
        }
        return $message.'|'.$row->email;
    }

    public function updateUserType($email,$usertype)
    {
        //echo $usertype; exit;
        $data['user_type'] = $usertype;
        $this->tableGateway->update($data,array('email'=> $email));
    }
    public function updateUserPhone($user_id,$phone)
    {
        $data['phone'] = $phone;
        $this->tableGateway->update($data,array('id'=> $user_id));
    }
}