<?php

// filename : module/Users/src/Users/Form/RegisterForm.php

namespace User\Form;

use Zend\Form\Form;
use Zend\Form\Element;


class LocatestoreForm extends Form {

    public function __construct($name = null) {
        parent::__construct('Edit User');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'campaign_id',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => 'Enter campaign id',
            ),
            'options' => array(
            ),
        ));


        $this->add(array(
            'name' => 'address',
            'attributes' => array(
                'type' => 'address',
                'required' => 'required',
                'class' => 'form-control',
                'placeholder' => 'Enter address',
            ),
            'options' => array(
            ),
        ));



       
    }

}
