<?php

namespace User\Model;

use Zend\Text\Table\Row;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class SkillsTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function saveskills(skills $skills) {
        $data = array(
            'skillid' => $skills->skillid,
            'title' => $skills->title,
            'skill' => $skills->skill,
            'ispopular' => $skills->ispopular,
            
        );
        
        
        $id = (int) $profileskill->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getProject($profileskill->user_shortcut)) {
                $this->tableGateway->update($data, array('user_id' => $id));
            } else {
                throw new \Exception('User id does not exist');
            }
        }
    }

    public function getProject($project_shortcut) {
        //$id = (int) $id;
        $rowset = $this->tableGateway->select(array('project_shortcut' => $project_shortcut));
        $row = $rowset->current();
        
        if (!$row) {
            throw new \Exception("Could not find row $project_shortcut");
        }
        return $row;
    }

    public function fetchAll($paginated=false) {
   
        if ($paginated) {
             // create a new Select object for the table album
             $select = new Select('skills');
                
             // create a new result set based on the Album entity
             $resultSetPrototype = new ResultSet();
             $resultSetPrototype->setArrayObjectPrototype(new Project());
             // create a new pagination adapter object
             $paginatorAdapter = new DbSelect(
                 // our configured select object
                 $select,
                 // the adapter to run it against
                 $this->tableGateway->getAdapter(),
                 // the result set to hydrate
                 $resultSetPrototype
             );
        
             $paginator = new Paginator($paginatorAdapter);
             return $paginator;
         }
        $resultSet = $this->tableGateway->select();
        $resultSet->buffer();
        return $resultSet;
    }

    public function deleteskills($skill) {
        $this->tableGateway->delete(array('skill' =>  $skill));
    }

    public function selectskills($skill) {     
        $rowset = $this->tableGateway->select(array('skill' => $skill));
        return $rowset;    
    }

    public function allSkillsName($skill_ids) { 
        $select = $this->tableGateway->getSql()->select();
        $select->columns(array('skill'));
        $select->where('skillId IN ?',"$skill_ids");               
        $resultSet = $this->tableGateway->select($select);
        return $resultSet;    
}

    public function getSkillById($skill,$param) {     
        $skill_result=array();
        if($param == 'ZEND_ONE'){
            $rowset = $this->tableGateway->select(array('skillId' => $skill));
            $temp_data = $rowset->toArray();
            $skill_result = $temp_data[0];
        }else{
            foreach($skill as $v):
                $rowset = $this->tableGateway->select(array('skillId' => $v));
                $temp_data = $rowset->toArray();
                $skill_result[$v] = $temp_data[0];
            endforeach;
        }
        return $skill_result;    
    }
    
    public function getAllSkills() 
    {     
        $rowset = $this->tableGateway->select();
        $temp_data = $rowset->toArray();
        foreach($temp_data as $v)
        {
            $final_result[$v['skillId']] = $v;
        }
        return $final_result;    
    }
}