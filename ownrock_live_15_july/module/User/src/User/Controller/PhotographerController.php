<?php

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Zend\Http\Headers;

use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;

use User\Form\RegisterForm;
//use User\Form\RegisterFilter;

use User\Model\User;
use User\Model\UserTable;
use User\Model\Uploads;

use User\Model\Photographer;
use User\Model\PhotographerTable;

use User\Model\ImageUpload;
use User\Model\ImageUploadTable;
use User\Model\Album;
use User\Model\AlbumTable;

use User\Model\Tag;
use User\Model\TagTable;

use User\Model\AlbumTag;
use User\Model\AlbumTagTable;

use ZendSearch\Lucene;
use ZendSearch\Lucene\Document;
use ZendSearch\Lucene\Index;

use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;
use Zend\Session\Container;
use Zend\Mail\Message;

use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;

use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\FacebookRedirectLoginHelper;

use Facebook\FacebookSDKException;



class PhotographerController extends AbstractActionController
{
    protected $authservice;
    public function IndexAction()
    {
    	$this->layout('layout/home-layout');
        return new ViewModel(array());
    }

    public function DashboardAction()
    {   $user_session = new Container('user');
    
        if($user_session->email == "")
        {
            return $this->redirect()->toRoute('user/home');
        }
        $this->layout('layout/dashboard-layout');
        return new ViewModel(array());
    }

    /*public function ProfileAction()
    {
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/home');
        }
        $this->layout('layout/photographer-layout');
        return new ViewModel(array());
    }*/

    public function ProfileAction()
    {
        $user_session = new Container('user');
        $error = "";
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/photographer', array('action' => 'dashboard'));
        }
        $this->layout('layout/photographer-layout');
        $request = $this->getRequest();
        if ($request->isPost()) 
        {
            $request = $this->getRequest();
            $photographer = new Photographer();
            $arr = $request->getPost($arr);
            $arr['user_id']=$user_session->id;

            if($arr['name']!="" && $arr['email']!="" && $arr['occupation']!="" && $arr['phone']!="")
            {
                $arr['profile_progress']=1;
            }
            else
            {
                $arr['profile_progress']=0;
            }
            if($arr['name']=="" || $arr['email']=="" || $arr['occupation']=="" || $arr['description'] =="" ||  $arr['phone']==""|| $arr['location']=="")
            {
                $error="Please fill all the required fields";
            } //echo '<pre>';print_r($arr);
            $photographer->exchangeArray($arr);

            //echo '<pre>';print_r($photographer);exit;
            $this->getServiceLocator()->get('PhotographerTable')->savePhotographer($photographer);

            $this->getServiceLocator()->get('UserTable')->updateUserPhone($arr['user_id'],$arr['phone']);
            return $this->redirect()->toRoute('user/photographer', array('action' => 'album'));
        }
        //return $this->redirect()->toRoute('user/photographer', array('action' => 'dashboard'));
        return new ViewModel(array());
    }

    public function AlbumAction()
    {
        $user_session = new Container('user');
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/photographer', array('action' => 'dashboard'));
        }
        $total_albums = 0;
        $request = $this->getRequest();
        if ($request->isPost()) 
        {   $arr = $request->getPost($arr);
            $arr['user_id']=$user_session->id;
            $album = new Album();
            $album->exchangeArray($arr);
            //echo '<pre>';print_r($album);  exit;
            $last_album_id = $this->getServiceLocator()->get('AlbumTable')->saveAlbum($album);  
            foreach($request->getPost()->e2 as $tagsdata)
            {
                $this->getServiceLocator()->get('AlbumTagTable')->saveAlbumTags($last_album_id,$tagsdata);
            }
          
            /*$config = $this->getServiceLocator()->get('config');
            $uploadPath = $config['module_config']['upload_location'];
            $adapter = new \Zend\File\Transfer\Adapter\Http();
            
            $upload = new ImageUpload();
            
            $a = $request->getFiles()->toArray();
            
            $cnt = 0;
            for($k=1;$k<6;$k++){
                if(isset($a['fileupload'.$k]['name']) && $a['fileupload'.$k]['name'] != ""){
                    $cnt++;
                }
            }

            for($i=1;$i<$cnt+1;$i++)
            {
                $rand = rand(2000,100000);

                $uploadFile = $a['fileupload'.$i]['name'];
                
                $adapter->setDestination($uploadPath);  
                $adapter->receive($uploadFile);

                $image_data['filename'] = $rand.'_'.$uploadFile;
                $image_data['thumbnail'] = $rand.'_'.'tn_'.$uploadFile;
                $image_data['label'] = "Project Pic$i";
                $image_data['project_id'] = $last_id;

                $upload->exchangeArray($image_data);               
                $uploadTable = $this->getServiceLocator()->get('ImageUploadTable');
                $uploadTable->saveUploads($upload);

                rename($uploadPath.'/'.$uploadFile,$uploadPath.'/'.$rand.'_'.$uploadFile);

                $sourceImageFileName = $uploadPath . '/' . $rand.'_'.$uploadFile;
                $thumbnailFileName = $rand.'_'.'tn_'.$uploadFile;
                $imageThumb = $this->getServiceLocator()->get('WebinoImageThumb');
                $thumb = $imageThumb->create($sourceImageFileName, $options = array());
                $thumb->resize(50, 50);
                $thumb->save($uploadPath . '/' . $thumbnailFileName);
                //print_r($uploadPath);die;
            }*/
            
            
            return $this->redirect()->toRoute('user/photographer', array('action' => 'invitefriends'));
        }

        /*$MerchantTable = $this->getServiceLocator()->get('MerchantTable');
        $result_merchant = $MerchantTable->getMerchantByEmail($user_session->email); */
        $this->layout('layout/photographer-layout');
        
        $album_data = array();
        $total_albums = $this->getServiceLocator()->get('AlbumTable')->getTotalAlbumsByUserid($user_session->id);
        $album_data['total_albums'] = $total_albums; 
        //echo '<pre>';print_r($album_data);exit;

        // echo "in";exit;
        $album_Tags = $this->getServiceLocator()->get('TagTable')->getTag();
        
        
        //echo '<pre>';print_r($album_Tags);exit;
        //return new ViewModel(array()); 
        
        $this->layout()->albuminfo = $album_data;
        return new ViewModel(array('album_data'=>$album_data,'album_Tags'=>$album_Tags));
    }

    public function InviteFriendsAction()
    {
        $user_session = new Container('user');
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/home');
        }
         $this->layout('layout/photographer-layout');
        return new ViewModel(array());
    }

    function restructureArrayAction(array $images)
    {
    $result = array();

    foreach ($images as $key => $value) {
        foreach ($value as $k => $val) {
            for ($i = 0; $i < count($val); $i++) {
                $result[$i][$k] = $val[$i];
            }
        }
    }

    return $result;
}

     public function uploadImgAction()
    {
        define('UPLOAD_PROD_IMG_PATH',"public/images/photographer/uploads/");
        $data = array();
        if(isset( $_POST['image_upload'] ) && !empty( $_FILES['images'] ))
        {
            //get the structured array
            $images = $this->restructureArrayAction($_FILES);
            $allowedExts = array("gif", "jpeg", "jpg", "png");
            
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            
            foreach ( $images as $key => $value){
                $i = $key+1;
                //create directory if not exists
                if (!file_exists('images')) {
                    mkdir('images', 0777, true);
                }
                $image_name = $value['name'];
                //get image extension
                $ext = strtolower(pathinfo($image_name, PATHINFO_EXTENSION));
                //assign unique name to image
                $imgnamew = time();
                $name = $i*$imgnamew.'.'.$ext;
                //$name = $image_name;
                //image size calcuation in KB
                $image_size = $value["size"] / 1024;
                $image_flag = true;
                //max image size
                $max_size = 1024;
                if( in_array($ext, $allowedExts) && $image_size < $max_size ){
                    $image_flag = true;
                } else {
                    $image_flag = false;
                    $data[$i]['error'] = 'Maybe '.$image_name. ' exceeds max '.$max_size.' KB size or incorrect file extension';
                } 
                
                if( $value["error"] > 0 ){
                    $image_flag = false;
                    $data[$i]['error'] = '';
                    $data[$i]['error'].= '<br/> '.$image_name.' Image contains error - Error Code : '.$value["error"];
                }
                
                if($image_flag){
                    ///mkdir(UPLOAD_PROD_IMG_PATH,0777);
                    move_uploaded_file($value["tmp_name"], UPLOAD_PROD_IMG_PATH.$name);
                    $src = UPLOAD_PROD_IMG_PATH.$name;
                    $dist =UPLOAD_PROD_IMG_PATH."thumbnail_".$name;
                    $data[$i]['success'] = $thumbnail = 'thumbnail_'.$name;
                    $data[$i]['name'] = $imgnamew;
                    $data[$i]['unique_id'] =  time();
                    //thumbnail($src, $dist, 200);    
                    
                    
                    $asset['name'] = $name;
                    $asset['post_type'] = 'product';
                    $asset['status'] = 0;
                    $asset['date'] = date("y-m-d H:i:s");
                    $asset['location'] = UPLOAD_PROD_IMG_PATH;
                    $asset['unique_id'] =$data[$i]['unique_id'];
                    //$this->products_model->add_assets($asset);
                
                }
            }

            echo json_encode($data);
            
        } else {
                $data[] = 'No Image Selected..';
        }
    }
    
}