<?php
/*Template Name : Home

*/
?>
<?php get_header(); ?><!---Header---->
						<!--start-image-slider---->
								<div class="slider">					     
									<div class="fluid_container">
										<div class="camera_wrap camera_azure_skin" id="camera_wrap_1">									           
											      <div data-thumb="<?php bloginfo('template_directory'); ?>/images/thumbnails/slider-1.jpg" data-src="<?php bloginfo('template_directory'); ?>/images/slider-1.jpg">  </div> 
											      <div data-thumb="<?php bloginfo('template_directory'); ?>/images/thumbnails/slider-2.jpg" data-src="<?php bloginfo('template_directory'); ?>/images/slider-2.jpg">  </div>
											      <div data-thumb="<?php bloginfo('template_directory'); ?>/images/thumbnails/slider-3.jpg" data-src="<?php bloginfo('template_directory'); ?>/images/slider-3.jpg">  </div>
											      <div data-thumb="<?php bloginfo('template_directory'); ?>/images/thumbnails/slider-4.jpg" data-src="<?php bloginfo('template_directory'); ?>/images/slider-4.jpg">  </div>
												  <div data-thumb="<?php bloginfo('template_directory'); ?>/images/thumbnails/slider-4.jpg" data-src="<?php bloginfo('template_directory'); ?>/images/slider-5.jpg">  </div>
												  <div data-thumb="<?php bloginfo('template_directory'); ?>/images/thumbnails/slider-4.jpg" data-src="<?php bloginfo('template_directory'); ?>/images/slider-6.jpg">  </div>
												  <div data-thumb="<?php bloginfo('template_directory'); ?>/images/thumbnails/slider-4.jpg" data-src="<?php bloginfo('template_directory'); ?>/images/slider-7.jpg">  </div>
											</div>
							    	   </div>
							   		   <div class="clear"></div>					       
							 		</div>
		      			<!--End-image-slider---->
		<!---start-content---->
		   <div class="content">
		         <div class="wrap">
			          <div class="content_top">
				          <div class="grid1">
				     		<h2 id="section_one">Services</h2>
				     	        <div class="grides">
				     				<div class="sub_grid1">				   		
							     		<div class="grid_img">
							     			<img src="<?php bloginfo('template_directory'); ?>/images/198436_1.jpg" height="150px">
							     		</div>
								     		<div class="grid_data">
								     			<h3>Bride Makeover</h3>
								     			<p>We have a  makeover package for ceremonies. Before bride makeup our makeover give additional looks which make your dream day memorable.</p>
								     	  </div>
				     	   			     <div class="clear"></div>	
				     				  </div>
					     	<div class="sub_grid2">					     		
					     		<div class="grid_img">
				     			    <img src="<?php bloginfo('template_directory'); ?>/images/174205_3.jpg" height="150px">
				     		    </div>
				     		 <div class="grid_data">
				     		 	<h3>MakeUp</h3>
				     			<p> Our service of face makeup are as per the occasions. We also draped the dresses you want with different styles which suits the makeup.</p>
				     	     </div>
				         <div class="clear"></div>					     		
				 	 </div>

				 	 <div class="sub_grid3">					     		
					     		<div class="grid_img">
				     			    <img src="<?php bloginfo('template_directory'); ?>/images/mani.jpeg" height="150px">
				     		    </div>
				     		 <div class="grid_data">
				     		 	<h3>Hand & Feet</h3>
				     			<p>Beautiful hands and feet are also need care. We have services of Manicure, Pedicure, Waxing, Aroma care, Spa covered under different packages also.</p>
				     	     </div>
				         <div class="clear"></div>					     		
				 	 </div>


				 <div class="clear"></div>		
				      <div class="divider"> </div>					      
				         <div class="sub_grid1">			     		
				     		<div class="grid_img">
				     			<img src="<?php bloginfo('template_directory'); ?>/images/hair.jpeg" height="150px">
				     		</div>
				     		<div class="grid_data">
				     			<h3>Trending Hair Style </h3>
				     			<p>We always suggest to our client about the best looks suit to them. Our hair stylist know the new trends, which change hair looks and personality.</p>
				     	    </div>
				     	   <div class="clear"></div>	
				     	</div>
					     	<div class="sub_grid2">					     		
					     		<div class="grid_img">
				     			    <img src="<?php bloginfo('template_directory'); ?>/images/grid-img4.jpg" height="150px">
				     		    </div>
				     		    <div class="grid_data">
				     		    	<h3>Mehndi </h3>
				     			   <p>Heena also called Mehndi is the most important part of women’s beauty. It also counts as an omen specially for marriage ceremony it's good for health also.</p>
				     	        </div>
				     		<div class="clear"></div>					     		
				 		</div>
				 		 <div class="sub_grid3">					     		
					     		<div class="grid_img">
				     			    <img src="<?php bloginfo('template_directory'); ?>/images/grid-img3.jpg" height="150px">
				     		    </div>
				     		 <div class="grid_data">
				     		 	<h3> Face Care</h3>
				     			<p> Face is the first impression of any person and we take care of it with our service involve clean up, facial, tan removal, skin enlightening,  for all skin types.</p>
				     	     </div>
				         <div class="clear"></div>					     		
				 	 </div>

				       <div class="clear"></div>	
				     </div>
				 </div>				     
				         <!----<div class="grid2">
				         	  <h2>What's New</h2>
				         	  <img src="<?php bloginfo('template_directory'); ?>/images/grid2-img.jpg">
				         	  <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>				         	 
					   </div>		---->		
	       			<div class="clear"></div>
	          </div>
	       </div>
	          <div class="slogan">
		   		<div class="wrap">
				  <div class="content-slogan">
				 
					<p>Aromatherapy in beauty clinic is become best practice because chemical products damage the body. It is used for the treatment of various skin & hair related problems.</p>
				  </div>
				     <div class="slogan-sub">
						<p>We are using Aromatherapy and essential oils for the facial, body massage, body polishing, head oil massage. We also advice our  clients for their skin related problems and help them to provide appropriate solutions.</p>
					 </div>
		         </div>
		      </div>
		      <div class="wrap">
			   <div  class="dc-grids">
				<div class="dc-head">
					<div class="dc-head-img">
						<a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/staff4.jpg" title="dc-name" /></a>
					</div>
					<div class="dc-head-info">
						<h3>Skin Care</h3>
						<span>No Age Limit For Care</span>
					</div>
					<div class="clear"> </div>
					<div class="dc-profile">
						<p>There are never be too late to take care of your skin. Don’t let your age to cover your skin. Our Aromatherapy product range for all types of skin in our beauty clinic.</p>
						
					</div>
				</div>
				<div class="dc-head">
					<div class="dc-head-img">
						<a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/staff3.jpg" title="dc-name" /></a>
					</div>
					<div class="dc-head-info">
						<h3>Hair Care</h3>
						<span>Soft and Shining Hairs</span>
					</div>
					<div class="clear"> </div>
					<div class="dc-profile">
						<p>Hair is beauty status for us. We care for your hairs from dullness, falls, thinness and other regular problems. Our Aromatherapy products helps our clients to recover hair glow.</p>
						
					</div>
				</div>
				<div class="dc-head">
					<div class="dc-head-img">
						<a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/dc3.jpg" title="dc-name" /></a>
					</div>
					<div class="dc-head-info">
						<h3>Facial Treatment</h3>
						<span>Face reflect personality</span>
					</div>
					<div class="clear"> </div>
					<div class="dc-profile">
						<p>Facial skin is too sensitive which easily damaged that we cure by using natural extract products. Also we offers range of products for our clients that can uses at their home. </p>
						
					</div>
				</div>
				<div class="clear"> </div>
		 	 </div>
		  </div>
		 <!---End-content---->
	 </div>
	  <!---Footer---->
  <?php get_footer(); ?>