<?php
/**
 * Sample template for displaying single product posts.
 * Save this file as as single-product.php in your current theme.
 *
 * This sample code was based off of the Starkers Baseline theme: http://starkerstheme.com/
 */

get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	

	<h1><?php the_title(); ?></h1>
		<?php the_content(); ?>

		<h2>Custom Fields</h2>	
		
		<strong>discription</strong> <?php print_custom_field('discription'); ?><br />
		<strong>pic:</strong> <img src="<?php print_custom_field('pic:to_image_src'); ?>" /><br />



	
<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>