<!--
Author: Ankit Chouksey
Author URL: http://manifestinfotech.com
-->
<!DOCTYPE HTML>
<html>
<head>
  <link rel="shortcut icon" type="image/png" href="http://newshringarika.com/wp-content/uploads/2014/08/newshringa.png"/>
  <title><?php echo get_bloginfo( 'name', 'display' );?></title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->
  <meta name="p:domain_verify" content="1a26abc76e94ae533151502b28e72671"/>
  <link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet" type="text/css" media="all"/>
  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/slider.css" type="text/css" media="all" />
  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/demo.css" type="text/css" media="all" />

  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/main.css" type="text/css" media="all" />


<!-- Ankit -->
  <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
  <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.easing.1.3.js"></script> 
  <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/camera.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.meanmenu.js"></script>
		<script type="text/javascript">
			   jQuery(function(){
				
				jQuery('#camera_wrap_1').camera({
					thumbnails: true
				});
			});
		  </script>
		
</head>
	 <body>
	 <?php include_once("analyticstracking.php"); ?>​
		 <div class="header">			  	
				<div class="header_top">
				   <div class="wrap">
					<div class="logo">
					<?php if ( get_theme_mod( 'themeslug_logo' ) ) : ?>
						<div class='site-logo'>
							<a href='http://newshringarika.com/' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home' ><img src='http://newshringarika.com/wp-content/uploads/2014/08/newshringa.png' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'></a>
						</div>
					<?php else : ?>
						<!-- <hgroup>
							<h1 class='site-title'><a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><?php bloginfo( 'name' ); ?></a></h1>
							<h2 class='site-description'><?php bloginfo( 'description' ); ?></h2>
						</hgroup> -->
					<?php endif; ?>
						<!-- <a href="index.html"><img src="<?php bloginfo('template_directory'); ?>/images/sringarika logo.png" /></a>
						<h1><a href="<?php echo get_option('home'); ?>"><?php bloginfo('name'); ?></a></h1> -->
					</div><?php wp_nav_menu( array( 'sort_column' => 'menu_order', 'menu_class' => 'top-nav', 'theme_location' => 'primary-menu' ) ); ?>
						<!--<div class="top-nav">
							<ul>
								<li class="active"><a href="index.html">Home</a></li>
								<li><a href="about.html">About</a></li>
								<li><a href="staff.html">Staff</a></li>
								<li><a href="#Services">Services</a></li>
								<li><a href="#Contact">Contact</a></li>
							</ul>
						</div>-->
				    

				     <div class="wrapper">
			<div class="top-nav main">
				
				<!-- Nav -->
				<nav class="nav">
					<ul class="nav-list">
						<li class="nav-item active"><a href="index.php">Home</a></li>
						<li class="nav-item"><a href="#Services">Services</a></li>
						<li class="nav-item"><a href="#Contact">Contact</a></li>
					</ul>
				</nav>
				<!-- /Nav -->
				<div class="clear"> </div>
				
			</div>
		</div>
		
		<script>
		(function () {
		
		    // Create mobile element
		    var mobile = document.createElement('div');
		    mobile.className = 'nav-mobile';
		    document.querySelector('.nav').appendChild(mobile);
		
		    // hasClass
		    function hasClass(elem, className) {
		        return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
		    }
		
		    // toggleClass
		    function toggleClass(elem, className) {
		        var newClass = ' ' + elem.className.replace(/[\t\r\n]/g, ' ') + ' ';
		        if (hasClass(elem, className)) {
		            while (newClass.indexOf(' ' + className + ' ') >= 0) {
		                newClass = newClass.replace(' ' + className + ' ', ' ');
		            }
		            elem.className = newClass.replace(/^\s+|\s+$/g, '');
		        } else {
		            elem.className += ' ' + className;
		        }
		    }
		
		    // Mobile nav function
		    var mobileNav = document.querySelector('.nav-mobile');
		    var toggle = document.querySelector('.nav-list');
		    mobileNav.onclick = function () {
		        toggleClass(this, 'nav-mobile-open');
		        toggleClass(toggle, 'nav-active');
		    };
		})();
		</script>
	
		<!-- Demo Analytics -->
		
		
		<!-- Demo Ads -->
		
				
				
			</div>
		</div>
				   </div>
			   </div>
				   <div class="header_bottom phone-slogan">
					  <div class="wrap">
						<div class="header-phone">
						    <h2><img src="<?php bloginfo('template_directory'); ?>/images/phone.png" alt="" />+91 9926559242</h2>						  	 
						    <h4>Berasia road, Bhopal (M.P.)</h4>
						    <span class="triangle-arrow">&nbsp;</span>
						</div>
						<div class="header-tagline">
							<?php query_posts('p=10'); ?>
							<?php while (have_posts()) : the_post(); ?>
							<h3><img src="<?php bloginfo('template_directory'); ?>/images/clock.png" alt="" /><?php the_title(); ?><span>
							<?php the_content(); ?></span></h3><?php endwhile;?>
						</div>
					</div>
			   </div>
		  </div>
	<!---End-header--->

	