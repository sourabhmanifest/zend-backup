<?php
 
//Add support for WordPress 3.0's custom menus
add_action( 'init', 'register_my_menu' );
 
//Register area for custom menu
function register_my_menu() {
    register_nav_menu( 'primary-menu', __( 'Primary Menu' ) );
}

// Enable post thumbnails
add_theme_support('post-thumbnails');
set_post_thumbnail_size(520, 250, true);

//Some simple code for our widget-enabled sidebar
if ( function_exists('register_sidebar') )
    register_sidebar();

//Code for custom background support
//add_theme_support( 'custom-background', array('default-color' => 'e6e6e6',) );
add_theme_support( 'custom-background' );
add_custom_background();

//Enable post and comments RSS feed links to head
add_theme_support( 'automatic-feed-links' );

//Adding a logo uploader to your WordPress theme with the Theme Customizer
function themeslug_theme_customizer( $wp_customize ) {
    // Fun code will go here
	$wp_customize->add_section( 'themeslug_logo_section' , array(
    'title'       => __( 'Logo', 'themeslug' ),
    'priority'    => 30,
    'description' => 'Upload a logo to replace the default site name and description in the header',
) );

$wp_customize->add_setting( 'themeslug_logo' );
$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'themeslug_logo', array(
    'label'    => __( 'Logo', 'themeslug' ),
    'section'  => 'themeslug_logo_section',
    'settings' => 'themeslug_logo',
) ) );

}
add_action('customize_register', 'themeslug_theme_customizer');
 
 //Enable multisite feature (WordPress 3.0)
define('WP_ALLOW_MULTISITE', true); 

$whit_sections = "";

// [section]My Section Title[/section]                                          
function whit_section_shortcode( $atts, $title = null ) {
    // $content is the title you have between your [section] and [/section] 

    $id = urlencode(strip_tags($title)); 
    // strip_tags removes any formatting (like <em> etc) from the title.
    // Then urlencode replaces spaces and so on.

    global $whit_sections;
    $whit_sections .= '<li><a href="#'.$id.'">'.$title.'</a></li>';

    return '<span id="'.$id.'">'.$title.'</span>';

}
add_shortcode('section', 'whit_section_shortcode');


// [section_navigation]
function whit_section_navigation_shortcode( $atts, $title = null ) {

    global $whit_sections;
    return '<ul class="section-navigation">'.$whit_sections.'</ul>';

}
add_shortcode('section_navigation', 'whit_section_navigation_shortcode');
?>