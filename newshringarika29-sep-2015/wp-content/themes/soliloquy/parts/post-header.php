<div class="date">
    <?php 
    the_time(get_option('date_format'));
    ?>
</div>
<h2 class="post_title">
    <a href="<?php the_permalink(); ?>">
        <?php
        get_template_part( 'parts/title', 'post');
        ?>
    </a>
</h2>
<?php
get_template_part( 'parts/image', '1170_640');
get_template_part( 'parts/image', 'caption');
?>