<?php 
global $post;
// set variables if front-page
if (is_front_page()) {
    $banner_option = nimbus_get_option('nimbus_banner_option');
    $nimbus_content_width_banner = nimbus_get_option('nimbus_content_width_banner');
    $banner_content = nimbus_get_option('nimbus_banner_content_only');
// set variables if alternate template    
} else if (is_page_template('alt_frontpage.php')) {
    $banner_option = trim(get_post_meta($post->ID, 'banner_option', true));
    $nimbus_content_width_banner = trim(get_post_meta($post->ID, 'nimbus_content_width_banner', true));
    $banner_content = trim(get_post_meta($post->ID, 'banner_content', true));
}
// Do frontpage banner options
if ((is_front_page() && !is_paged()) || is_page_template('alt_frontpage.php')) {
    // Content width banner
    if ($banner_option == 'image_content_width') { 
    ?>
        <div>
            <?php
            if (!empty($nimbus_content_width_banner)) {
            ?>
            <img id="frontpage_banner" src="<?php echo $nimbus_content_width_banner; ?>" alt="Frontpage Banner" />
            <?php
            } else {
                if (nimbus_get_option('reminder_images') == 'on') {
                ?>
                    <img id="frontpage_banner" src="<?php echo get_template_directory_uri(); ?>/images/preview/nimbus_1170_385_<?php echo rand(1,3); ?>.jpg" alt="Frontpage Banner" />
                <?php
                }
            }
            ?>
        </div>
    <?php 
    } 
} else {
// Not on frontpage
}     
?>