<?php
/**
 * template for displaying galleries
 * uses a custom loop which displays the attached image
 */
 
//get_header(); ?>
 
<div id="main-content" class="main-content">
 
 
    <div id="primary" class="content-area">
        <div id="content" class="site-content" role="main">
         
        <header class="page-header">
         
            <?php $queried_object = get_queried_object();
                echo '<h1 class="page-title">Gallery - ' . $queried_object->name . '</h1>'; ?>
        </header><!-- .page-header -->

		<?php
    if ( have_posts() ) : ?>
     
        <section class="gallery <?php echo $queried_object->name; ?>">
     
            <?php // Start the Loop.
                while ( have_posts() ) : the_post();
                 
                // define attributes for image display
                $imgattr = array(
                    'alt'   => trim( strip_tags( get_post_meta( $attachment_id, '_wp_attachment_image_alt', true ) ) ),
                ); ?>
         
                <div class="gallery-image"><a href ="<?php echo get_attachment_link(); ?>"><?php echo wp_get_attachment_image( $post->ID, 'thumbnail', $imgattr ); ?></a></div>
                 
                <?php endwhile; ?>
         
        </section>
         
 
    <?php else :
        // If no content, include the "No posts found" template.
        get_template_part( 'content', 'none' );
 
    endif;
?>
 
        </div><!-- #content -->
    </div><!-- #primary -->
    <?php get_sidebar( 'content' ); ?>
</div><!-- #main-content -->
 
<?php
get_sidebar();
get_footer();
?>