<?php

namespace User\Model;

use Zend\Text\Table\Row;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;

class OnlinecouponTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function saveOnlinecoupon(onlinecoupon $onlinecoupon) {
        $data = array(
            'couponid' => $onlinecoupon->couponid,
            'merchant' => $onlinecoupon->merchant,
            'couponname' => $onlinecoupon->couponname,
            'shortdescription' => $onlinecoupon->shortdescription,
            'longdescription' => $onlinecoupon->longdescription,
            'couponcode' => $onlinecoupon->couponcode,
            'expirydate' => $onlinecoupon->expirydate,
            'couponurl' => $onlinecoupon->couponurl,
            'entrydate' => $onlinecoupon->entrydate,
            'category' => $onlinecoupon->category,
            'subcategory' => $onlinecoupon->subcategory,
        );

        $id = (int) $onlinecoupon->couponid;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getMerchantInfo($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Coupon id does not exist');
            }
        }
    }

    public function getMerchantinfo($id) {

        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();

        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function fetchAll(Select $select = null) {
          if(null==$select){
           $select = new Select();
        }
        $select->from('onlinecoupon');
        $resultSet = $this->tableGateway->selectWith($select);

       //$resultSet = $this->tableGateway->select();
        $resultSet->buffer();
        return $resultSet;
    }

    public function deleteMerchantInfo($id) {
        $this->tableGateway->delete(array('id' => (int) $id));
    }

    public function selectOnlinecoupon($merchant,Select $select = null) {   
         if(null==$select){
           $select = new Select();
        }
        $select->from('onlinecoupon');
        $select->where(array('merchant' => $merchant));     
        $resultSet = $this->tableGateway->selectWith($select);
        return $resultSet;    
    }
    
     public function selectMerchantname($merchantname) {     
        $rowset = $this->tableGateway->select(array('merchantname' => $merchantname));
        return $rowset;    
    }
    
      public function selectsubcategoryname($subcategory, Select $select = null) {   
        if(null==$select){
            $select = new Select();
        }
       $select->from('onlinecoupon');
        $select->where(array('subcategory' => $subcategory));     
        $resultSet = $this->tableGateway->selectWith($select);
        return $resultSet; 
    }

}
