<?php

namespace User\Model;

use Zend\Text\Table\Row;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class PhotographerTable 
{
    protected $tableGateway;
	private $offset;
	private $limit;
	private $keyword;

    public function __construct(TableGateway $tableGateway) 
    {
        $this->tableGateway = $tableGateway;
    }


    public function savePhotographer(photographer $photographer) 
    {   
        $data = array(
            'pg_id' => $photographer->pg_id,
            'user_id' => $photographer->user_id,
            'address' => $photographer->address,
            'location' => $photographer->location,
            'tagline' => $photographer->tagline,
            'description' => $photographer->description,
            'profilepic' => $photographer->description,
            'price' => $photographer->price,
            'website_url' => $photographer->website_url,
            'equipment' => $photographer->equipment,
            'product_and_services' => $photographer->product_and_services,
            'status' => $photographer->status,
            'profile_progress' => $photographer->profile_progress
        );

        
        //echo '<pre>'; print_r($photographer);exit;
        $user_id = (int) $photographer->user_id;
        $pg_id = (int) $photographer->pg_id;
        
        if ($pg_id == 0) 
        {    //echo '<pre>'; print_r($data);exit;
            $this->tableGateway->insert($data);
            //echo '<pre>'; print_r($data);exit;
        } 
        else 
        {
            $this->tableGateway->update($data, array('pg_id' => $pg_id));
        } 
    }

    public function getPhotographerData($user_id)
    { 
        $user_id  = (int) $user_id;      
        $resultSet = $this->tableGateway->select(array('user_id' => $user_id)); 
        $row = $resultSet->current();
        return $row;
    }
    public function updateProfileImage($user_id,$profilepic) 
    {
        $data = array('profilepic'=>$profilepic);
        $this->tableGateway->update($data, array('user_id'=>$user_id));
    }

    public function fetchallProfiles() 
    {
        $resultSet = $this->tableGateway->select(); 
        return $resultSet->toArray();
    }

    public function getProfileByUserId($user_id) 
    {   
        $user_id  = (int) $user_id;      
        //echo $user_id;exit;
        $resultSet = $this->tableGateway->select(array('user_id' => $user_id)); 
        return $resultSet->current();
    }

    public function updateStatus($user_id) 
    {
        $user_id  = (int) $user_id;  
        $data = array('status'=>1);
        $this->tableGateway->update($data, array('user_id'=>$user_id));
    }
}

 