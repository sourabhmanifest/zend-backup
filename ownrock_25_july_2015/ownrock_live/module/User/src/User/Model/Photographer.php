<?php

namespace User\Model;

class Photographer{
    public $pg_id;
    public $user_id;
    public $address;
    public $location;
    public $tagline;
    public $description;
    public $profilepic;
    public $price;
    public $website_url;
    public $equipment;
    public $product_and_services;
    public $status;
    public $profile_progress;

    
    public function exchangeArray($data)
    {
        $this->pg_id  = (!empty($data['pg_id'])) ? $data['pg_id'] : 0;
        $this->user_id  = (!empty($data['user_id'])) ? $data['user_id'] : '';
        $this->address = (!empty($data['address'])) ? $data['address'] : '';
        $this->location  = (!empty($data['location'])) ? $data['location'] : '';
        $this->tagline  = (!empty($data['tagline'])) ? $data['tagline'] : '';
        $this->description   = (!empty($data['description'])) ? $data['description'] : '';   
        $this->profilepic  = (!empty($data['profilepic'])) ? $data['profilepic'] : '';
        $this->price  = (!empty($data['price'])) ? $data['price'] : '';
        $this->website_url = (!empty($data['website_url'])) ? $data['website_url'] : '';
        $this->equipment  = (!empty($data['equipment'])) ? $data['equipment'] : '';
        $this->product_and_services   = (!empty($data['product_and_services'])) ? $data['product_and_services'] : '';   
        $this->status  = (!empty($data['status'])) ? $data['status'] : 0;
        $this->profile_progress  = (!empty($data['profile_progress'])) ? $data['profile_progress'] : '';
     }
     
     public function getArrayCopy()
     {
         return get_object_vars($this);
     }
            
}