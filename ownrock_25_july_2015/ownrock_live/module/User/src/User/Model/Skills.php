<?php

namespace User\Model;

class Skills {

    public $skillId;
    public $title;
    public $skill;
    public $ispopular;

    public function exchangeArray($data) {

        $this->skillId = (!empty($data['skillId'])) ? $data['skillId'] : null;
        $this->title = (!empty($data['title'])) ? $data['title'] : null;
        $this->skill = (!empty($data['skill'])) ? $data['skill'] : null;
        $this->ispopular = (!empty($data['ispopular'])) ? $data['ispopular'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}
