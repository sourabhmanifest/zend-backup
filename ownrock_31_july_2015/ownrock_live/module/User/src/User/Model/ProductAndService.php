<?php

namespace User\Model;

class ProductAndService {

    public $id;
    public $productandservices;

    public function exchangeArray($data) {

        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->productandservices = (!empty($data['productandservices'])) ? $data['productandservices'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}
