<?php

namespace User\Model;

class AlbumImage {

    public $id;
    public $album_id;
    public $imagename;
    public $thumbname;

    public function exchangeArray($data) {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->album_id = (!empty($data['album_id'])) ? $data['album_id'] : null;
        $this->imagename = (!empty($data['imagename'])) ? $data['imagename'] : null;
        $this->thumbname = (!empty($data['thumbname'])) ? $data['thumbname'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}
