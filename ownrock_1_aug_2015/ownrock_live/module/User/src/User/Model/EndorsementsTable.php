<?php

namespace User\Model;

use Zend\Text\Table\Row;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class EndorsementsTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function saveEndorsements(endorsements $endorsements) {
        $data = array(
            'id' => $endorsements->id,
            'project_id' => $endorsements->project_id,
            'name' => $endorsements->name,
            'endorsetext' => $endorsements->endorsetext,
            'clientname' => $endorsements->clientname,          
        );
        
        
        $id = (int) $project->project_id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            return $this->tableGateway->getLastInsertValue();
        } else {
            if ($this->getProject($project->project_id)) {
                $this->tableGateway->update($data, array('project_id' => $id));
            } else {
                throw new \Exception('Project id does not exist');
            }
        }
    }

    public function getProject($project_shortcut) {
        //$id = (int) $id;
        //$rowset = $this->tableGateway->select(array('project_shortcut' => $project_shortcut));
        $rowset = $this->tableGateway->select(array('project_id' => $project_shortcut));
        $row = $rowset->current();
        
        if (!$row) {
            throw new \Exception("Could not find row $project_shortcut");
        }
        return $row;
    }

    public function fetchAll($paginated=false,$searchtext="") {
   
        if ($paginated) {
             // create a new Select object for the table album
             $select = new Select('project');
             
             if($searchtext != ""){
                 $select->where->like('project_name', "%" . "$searchtext" . "%");
             }
             
             // create a new result set based on the Album entity
             $resultSetPrototype = new ResultSet();
             $resultSetPrototype->setArrayObjectPrototype(new Project());
             // create a new pagination adapter object
             $paginatorAdapter = new DbSelect(
                 // our configured select object
                 $select,
                 // the adapter to run it against
                 $this->tableGateway->getAdapter(),
                 // the result set to hydrate
                 $resultSetPrototype
             );
        
             $paginator = new Paginator($paginatorAdapter);
             return $paginator;
         }
        $resultSet = $this->tableGateway->select();
        
        /*$resultSet = $this->tableGateway->select(function(Select $select) {
                $select->join('projectskill', 'project.project_id = projectskill.project_id', array('skill'));
            });
         */
        
        $resultSet->buffer();
        return $resultSet;
    }

    public function deleteProject($project_id) {
        $this->tableGateway->delete(array('project_id' =>  $project_id));
    }

    public function selectProject($header) {      
        $rowset = $this->tableGateway->select(array('header' => $header));
        return $rowset;    
    }
    
    public function selectProjectname($name) {     
        $rowset = $this->tableGateway->select(array('name' => $name));
        return $rowset;    
    }

    public function searchProject($byName) {     
        $rowset = $this->tableGateway->select(array('project_name LIKE ' => $byName .'%'));
        return $rowset;    
    }
    
    public function getProjectByMerchant($merchant_name) {     
        $rowset = $this->tableGateway->select(array('user_name' => $merchant_name));
        $rowset->buffer();
        return $rowset;    
}
    
    public function getProjectById($project_id) {
        
        $rowset = $this->tableGateway->select(array('project_id' => $project_id));
        $row = $rowset->current();
        
        if (!$row) {
            throw new \Exception("Could not find row $project_id");
}
        return $row;
    }
    
}