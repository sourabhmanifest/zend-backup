<?php

namespace User\Model;

use Zend\Text\Table\Row;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class PhotographerTable 
{
    protected $tableGateway;
	private $offset;
	private $limit;
	private $keyword;

    public function __construct(TableGateway $tableGateway) 
    {
        $this->tableGateway = $tableGateway;
    }


    public function savePhotographer(photographer $photographer) 
    {   
        $data = array(
            'pg_id' => $photographer->pg_id,
            'user_id' => $photographer->user_id,
            'address' => $photographer->address,
            'location' => $photographer->location,
            'tagline' => $photographer->tagline,
            'description' => $photographer->description,
            'price' => $photographer->price,
            'equipment' => $photographer->equipment,
            'product_and_services' => $photographer->product_and_services,
            'status' => $photographer->status,
            'profile_progress' => $photographer->profile_progress
        );

        
        //echo '<pre>'; print_r($photographer);exit;
        $user_id = (int) $photographer->user_id;
        $pg_id = (int) $photographer->pg_id;
        
        if ($pg_id == 0) 
        {    //echo '<pre>'; print_r($data);exit;
            $this->tableGateway->insert($data);
            //echo '<pre>'; print_r($data);exit;
        } 
        else 
        {
            $this->tableGateway->update($data, array('pg_id' => $pg_id));
        } 
    }

    public function getPhotographerData($user_id)
    { 
        $user_id  = (int) $user_id;      
        $resultSet = $this->tableGateway->select(array('user_id' => $user_id)); 
        $row = $resultSet->current();
        return $row;
    }
    public function updateProfileImage($user_id,$profilepic) 
    {
        $data = array('profilepic'=>$profilepic);
        $this->tableGateway->update($data, array('user_id'=>$user_id));
    }

    public function fetchallProfiles() 
    {
        $resultSet = $this->tableGateway->select(); 
        return $resultSet->toArray();
    }

    public function getProfileByUserId($user_id) 
    {   
        $user_id  = (int) $user_id;      
        //echo $user_id;exit;
        $resultSet = $this->tableGateway->select(array('user_id' => $user_id)); 
        return $resultSet->current();
    }

    public function updateStatus($user_id) 
    {
        $user_id  = (int) $user_id;  
        $data = array('status'=>1);
        $this->tableGateway->update($data, array('user_id'=>$user_id));
    }

    public function getPhotographerByKeyword($keyword=array(), $offset=0, $limit=3, $location='', $sort_by='', $rate,$availability) 
    { 
        $this->keyword = array();   
        $response = array();
        $this->keyword = $keyword;

        $rowset = $this->tableGateway->select(function(Select $select) use ($limit, $offset, $location, $sort_by, $rate, $availability )
        {   
            $select->join('album', 'album.user_id = user_id',array('user_id' => 'user_id','cnt' => new \Zend\Db\Sql\Expression('COUNT(album.user_id)')));
            
        
            

           
            $select->where->EqualTo('status', '2');

            if(trim($location) != ''){

                    $select->where->like('location', "$location");               
            }
            
            /*if(trim($rate) != '0')
            {
                if(trim($rate)=="1")
                {
                    $select->where->lessThanOrEqualTo('rate', 5);
                }
                if(trim($rate)=="2")
                {
                    $select->where->lessThanOrEqualTo('rate', 10);
                }
                if(trim($rate)=="3")
                {
                    $select->where->lessThanOrEqualTo('rate', 15);
                }
                if(trim($rate)=="4")
                {
                    $select->where->lessThanOrEqualTo('rate', 20);
                }            
            }*/


            $select->group('album.user_id');
            $select->having('cnt>1');

            if(trim($sort_by) != '')
            {
                if(trim($sort_by)=="0")
                {
                    $select->order('pg_id ASC');
                }
                if(trim($sort_by)=="1")
                {
                    $select->order('price DESC');
                }
                if(trim($sort_by)=="2")
                {
                    $select->order('price ASC');
                }
                if(trim($sort_by)=="3")
                {
                    $select->order('pg_id DESC');
                }
            }

        });

        //echo  $select;
        
        //$response['result'] = $rowset->toArray();      
        //$row = $rowset->current();
        //print_r($response['result']); die;
        $response['totalcount'] = $rowset->count();     
       
        $rowset1 = $this->tableGateway->select(function(Select $select) use ($limit, $offset, $location, $sort_by, $rate, $availability)
        {
            $select->join('album', 'album.user_id = user_id',array('user_id' => 'user_id','cnt' => new \Zend\Db\Sql\Expression('COUNT(album.user_id)')));
            
        
            

           
            $select->where->EqualTo('status', '2');

            if(trim($location) != ''){

                    $select->where->like('location', "$location");               
            }

            if(trim($price) != '0')
            {
                if(trim($price)=="1")
                {
                    $select->where->lessThanOrEqualTo('price', 500);
                }
                if(trim($price)=="2")
                {
                    $select->where->lessThanOrEqualTo('price', 1000);
                }
                if(trim($price)=="3")
                {
                    $select->where->lessThanOrEqualTo('price', 1500);
                }
                if(trim($price)=="4")
                {
                    $select->where->lessThanOrEqualTo('price', 2000);
                }            
            }

            /*if(trim($availability) != '0')
            {
                if(trim($availability)=="1")
                {
                    $select->where->lessThanOrEqualTo('availability', 10);
                }
                if(trim($availability)=="2")
                {
                    $select->where->lessThanOrEqualTo('availability', 20);
                }
                if(trim($availability)=="3")
                {
                    $select->where->lessThanOrEqualTo('availability', 30);
                }
                if(trim($availability)=="4")
                {
                    $select->where->lessThanOrEqualTo('availability', 40);
                }
            }*/

            $select->group('album.user_id');
            $select->having('cnt>1');

            if(trim($sort_by) != '')
            {
                if(trim($sort_by)=="0")
                {
                    $select->order('pg_id ASC');
                }
                if(trim($sort_by)=="1")
                {
                    $select->order('price DESC');
                }
                if(trim($sort_by)=="2")
                {
                    $select->order('price ASC');
                }
                if(trim($sort_by)=="3")
                {
                    $select->order('pg_id DESC');
                }
            }

            
            $select->limit($limit)->offset($offset);
                
        });
        $response['result'] = $rowset1->toArray();      
        return $response;
    } 
}

 