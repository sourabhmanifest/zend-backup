<?php

namespace User\Model;

class Profileskill {

    public $id;
    public $user_shortcut;
    public $skill;
    

    public function exchangeArray($data) {

        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->user_shortcut = (!empty($data['user_shortcut'])) ? $data['user_shortcut'] : null;
        $this->skill = (!empty($data['skill'])) ? $data['skill'] : null;
       
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}
