<?php

namespace User\Model;

class Album {

    public $album_id;
    public $user_id;
    public $album_name;
    public $description;
    
    public function exchangeArray($data) {

        $this->album_id = (!empty($data['album_id'])) ? $data['album_id'] : null;
        $this->user_id = (!empty($data['user_id'])) ? $data['user_id'] : null;
        $this->album_name = (!empty($data['album_name'])) ? $data['album_name'] : null;
        $this->description = (!empty($data['description'])) ? $data['description'] : null;
    }
    
    public function getArrayCopy() {
        return get_object_vars($this);
    }

}