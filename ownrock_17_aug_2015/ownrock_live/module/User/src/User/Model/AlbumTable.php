<?php

namespace User\Model;

use Zend\Text\Table\Row;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class AlbumTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }
    
    public function saveAlbum(album $album) 
    {
        $data = array(
            'album_id' => $album->album_id,
            'user_id' => $album->user_id,
            'album_name' => $album->album_name,
            'description' => $album->description,
        );
        $id = (int) $album->album_id;
        if ($id == 0) 
        {
            $this->tableGateway->insert($data);        
            return $this->tableGateway->getLastInsertValue();
        } 
        else 
        {
            
            $this->tableGateway->update($data, array('album_id' => $id));
            return $id;
            
        }
    }

    public function getTotalAlbumsByUserid($user_id)
    { 
        $user_id  = (int) $user_id;      
        $resultSet = $this->tableGateway->select(array('user_id' => $user_id)); 
        return $resultSet->count(); 

    }
    public function getallAlbumsByUserid($user_id)
    { 
        $user_id  = (int) $user_id;
        $rowSet = $this->tableGateway->select(array('user_id' => $user_id)); 
        //$row = $resultSet->current();
        return $rowSet->toArray();
    }
    public function fetchall()
    { 
        $resultSet = $this->tableGateway->select();
        return $resultSet->toArray();
    }
    public function getAlbumsByAlbumId($album_id)
    { 
        $album_id  = (int) $album_id;
        $rowSet = $this->tableGateway->select(array('album_id' => $album_id)); 
        $row = $rowSet->current();
        return $row;
    }

    public function deleteAlbumsByAlbumId($album_id)
    { 
        $album_id  = (int) $album_id;
        $this->tableGateway->delete(array('album_id' => $album_id));
    }
    
    
    
}

