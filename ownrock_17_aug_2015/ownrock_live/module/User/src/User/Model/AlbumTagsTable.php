<?php

namespace User\Model;

use Zend\Text\Table\Row;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class AlbumTagsTable


    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) 
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveAlbumTags($album_id,$tag_id) {
        
        $data= array ( 
                    'album_id' => $album_id,
                    'tag_id'=> $tag_id,
                );
        deleteAlbumTags($album_id);
        $this->tableGateway->insert($data);
    }
    public function deleteAlbumTags($album_id) {
        $this->tableGateway->delete(array('album_id' => $album_id));
    }
}
