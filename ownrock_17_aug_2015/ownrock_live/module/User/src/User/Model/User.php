<?php

namespace User\Model;

class User {

    public $id;
    public $name;
    public $email;
    public $phone;
    public $password;
    public $user_type;
    public $verification_code;
    public $registration_date;
    public $status;
	public $fb_id;
    public $visit;
    

    public function setPassword($clear_password) {
        $this->password = md5($clear_password);
    }

    public function exchangeArray($data) {
        $this->id = (isset($data['id'])) ? $data['id'] : null;
        $this->name = (isset($data['name'])) ? $data['name'] : null;
        $this->email = (isset($data['email'])) ? $data['email'] : null;
        $this->phone = (isset($data['phone'])) ? $data['phone'] : null;
        $this->password = (isset($data['password'])) ? $data['password'] : null;
        $this->user_type = (isset($data['user_type'])) ? $data['user_type'] : null;
        $this->verification_code = (isset($data['verification_code'])) ? $data['verification_code'] : null;
        $this->registration_date = (isset($data['registration_date'])) ? $data['registration_date'] : null;
        $this->status = (isset($data['status'])) ? $data['status'] : null;
		$this->fb_id = (isset($data['fb_id'])) ? $data['fb_id'] : null;
        $this->visit = (isset($data['visit'])) ? $data['visit'] : 0;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}
