<?php

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use User\Form\UsereditForm;
use User\Form\LoginForm;
use User\Model\User;
use User\Model\UserTable;
use User\Model\Profileskill;
use User\Model\ProfileskillTable;
use User\Model\Merchant;
use User\Model\MerchantTable;
use User\Model\Frontportfolio;
use User\Model\FrontportfolioTable;
use User\Model\Projectskill;
use User\Model\ProjectskillTable;
use User\Model\Skills;
use User\Model\SkillsTable;

use User\Model\Categorylist;
use User\Model\CategorylistTable;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;
use User\Form\RegisterForm;
use Zend\Db\Sql\Select;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\Iterator as paginatorIterator;
use Zend\View\Model\JsonModel;

class FrontprojectController extends AbstractActionController {

    public function IndexAction() {
        
        $this->layout('layout/portfolio-layout');
        die;
        $MerchantTable = $this->getServiceLocator()->get('MerchantTable');
        $merchant = $MerchantTable->getActiveMerchantById($this->params()->fromRoute('merchant_id'));  
            
        if($merchant){
        $ProejctTable = $this->getServiceLocator()->get('ProjectTable');
        $project = $ProejctTable->getProjectByMerchant(str_replace('-',' ',$this->params()->fromRoute('merchant_name')));
        
        $parr=array();
        $sarr=array();
        
        $ProjectskillTable = $this->getServiceLocator()->get('ProjectskillTable');
        foreach($project as $pval):
            $parr[] = $pval->project_id;
        endforeach;
        $project_skills = $ProjectskillTable->allProjectskill($parr);

        $skill_names=array();
        if(count($project_skills)>0){
            foreach($project_skills as $sval):
                foreach($sval as $vval):
                    $sarr[] = $vval['skill'];
                endforeach;
            endforeach;
            $temp_saar = array_unique($sarr);
            $SkillsTable = $this->getServiceLocator()->get('SkillsTable');
            $skill_names = $SkillsTable->getSkillById($temp_saar,'ZEND_MULTI');
        }
        
        $ImageUploadTable = $this->getServiceLocator()->get('ImageUploadTable');
        $Image_Details = $ImageUploadTable->allUploadByProject($parr);
        
        $viewModel = new Viewmodel(array('merchant_result' => $merchant,'project_result' => $project,'skill_result'=>$skill_names,'image_result'=>$Image_Details,'total_projects'=>count($parr)));
        
        }else{
            return $this->redirect()->toRoute('user/frontportfolio', array('action' => 'notify','merchant_id'=>'100'));
        }
            
        return $viewModel;
    }
       
    public function notifyAction(){
        $viewModel = new viewModel(array());
        
        return $viewModel;
    }
       
}