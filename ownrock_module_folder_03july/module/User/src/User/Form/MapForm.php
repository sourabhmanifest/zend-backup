<?php

namespace User\Form;

use Zend\Form\Form;

class MapForm extends Form {

    Public Function __construct($name = NULL) {
        parent:: __construct('map');

        
        $this->add(array(
            'name' => 'address',
            'type' => 'text',
            'class' => 'form-control',
            'id'=>'findLocationArea',
            'value' => '',
            'attributes' => array(
                'label' => 'address',
                'placeholder' => 'Please enter address',
            ),
        ));
    }

}
