<?php

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Zend\Http\Headers;

use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;

use User\Form\RegisterForm;
//use User\Form\RegisterFilter;

use User\Model\User;
use User\Model\UserTable;
use User\Model\Uploads;

use User\Model\Photographer;
use User\Model\PhotographerTable;

use User\Model\ImageUpload;
use User\Model\ImageUploadTable;
use User\Model\Album;
use User\Model\AlbumTable;

use User\Model\Tag;
use User\Model\TagTable;

use User\Model\AlbumTag;
use User\Model\AlbumTagTable;

use User\Model\AlbumImage;
use User\Model\AlbumImageTable;

use User\Model\Equipment;
use User\Model\EquipmentTable;

use User\Model\ProductAndService;
use User\Model\ProductAndServiceTable;

use User\Model\RateAndReview;
use User\Model\RateAndReviewTable;

use ZendSearch\Lucene;
use ZendSearch\Lucene\Document;
use ZendSearch\Lucene\Index;

use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;
use Zend\Session\Container;
use Zend\Mail\Message;

use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;

use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\FacebookRedirectLoginHelper;

use Facebook\FacebookSDKException;



class PhotographerController extends AbstractActionController
{
    protected $authservice;
    public function IndexAction()
    {
    	$this->layout('layout/home-layout');
        return new ViewModel(array());
    }

    public function DashboardAction()
    {   $user_session = new Container('user');
    
        if($user_session->email == "")
        {
            return $this->redirect()->toRoute('user/home');
        }
		//echo $user_session->user_id; exit;
        $photographerdata = $this->getServiceLocator()->get('PhotographerTable')->getPhotographerData($user_session->user_id);

        $userdata = $this->getServiceLocator()->get('UserTable')->getUser($user_session->user_id);
        $total_albums = $this->getServiceLocator()->get('AlbumTable')->getTotalAlbumsByUserid($user_session->user_id);
        $photographerdata->phone = $userdata->phone;
        $photographerdata->name = $userdata->name;
        $photographerdata->visit = $userdata->visit;
        $photographerdata->total_albums = $total_albums;
        $config = $this->getServiceLocator()->get('config');
        $profile_img_path = $config['module_config']['photographer_profile_image'];
		//echo '<pre>';print_r($photographerdata);exit;
        $this->layout('layout/dashboard-layout');
		return new ViewModel(array('photographerdata'=>$photographerdata, 'profile_img_path' =>$profile_img_path));
    }

    /*public function ProfileAction()
    {
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/home');
        }
        $this->layout('layout/photographer-layout');
        return new ViewModel(array());
    }*/

    public function CreateProfileAction()
    {
        $user_session = new Container('user');
        $error = "";
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/photographer', array('action' => 'dashboard'));
        }
        $photographerdata = array();
        $photographerdata = $this->getServiceLocator()->get('PhotographerTable')->getPhotographerData($user_session->user_id);
        
        $userdata = $this->getServiceLocator()->get('UserTable')->getUser($user_session->user_id);
        $total_albums = $this->getServiceLocator()->get('AlbumTable')->getTotalAlbumsByUserid($user_session->user_id);
        //$photographerdata->phone = $userdata->phone;
        //echo $photographerdata->total_albums= $total_albums; 
        // echo '<pre>';print_r($userdata);exit;
        $equipmentdata = $this->getServiceLocator()->get('EquipmentTable')->getAllEquipment();
        $productandservicedata = $this->getServiceLocator()->get('ProductAndServiceTable')->getAllProductAndService();
        $this->layout('layout/photographer-layout');
        $request = $this->getRequest();
        if ($request->isPost()) 
        {
            $request = $this->getRequest();
            $photographer = new Photographer();
            $arr = $request->getPost($arr);
            //echo '<pre>';print_r($arr);exit;
            $arr['description'] = stripslashes(htmlspecialchars($arr['description']));
            $arr['address'] = stripslashes(htmlspecialchars($arr['address']));
            $arr['user_id']=$user_session->user_id;
            //echo '<pre>';print_r($arr);exit;
            if($arr['name']!="" && $arr['location']!="" && $arr['price']!="" && $arr['description']!="")
            {
                $arr['profile_progress']=1;
            }
            else
            {

                $arr['profile_progress']=0;
            }
            if($arr['name']!="" || $arr['location']!="" || $arr['price']!="" ||$arr['description']!="")
            {
                $error="Please fill all the required fields";
            } //echo '<pre>';print_r($arr);     
            $photographer->exchangeArray($arr);

            //echo '<pre>';print_r($photographer);exit;
            $this->getServiceLocator()->get('PhotographerTable')->savePhotographer($photographer);
            
            $this->getServiceLocator()->get('UserTable')->updateUserPhoneAndName($arr['user_id'],$arr['phone'],$arr['name']);
            return $this->redirect()->toRoute('user/photographer', array('action' => 'createalbum'));
        }
        //return $this->redirect()->toRoute('user/photographer', array('action' => 'dashboard'));
        return new ViewModel(array(
            'photographerdata'=>$photographerdata, 
            'equipmentdata' => $equipmentdata, 
            'productandservicedata' => $productandservicedata
            ));
    }

    public function getRatingAction($user_id)
    {
        $ratingandreview = $this->getServiceLocator()->get('RateAndReviewTable')->getRateAndReviewById($user_id);
        if(!empty($ratingandreview))
        {   
            $total_rating = 0;
            foreach($ratingandreview as $rating)
            {
                $total_rating = $total_rating + $rating['star'];
            }
            $avg_rating = $total_rating/count($ratingandreview);
            return round($avg_rating);
        }
        else
        {
            return 0;

        }
    }

    public function ProfileAction()
    {
        $user_session = new Container('user');
        /*if($user_session->email == ""){
            return $this->redirect()->toRoute('user/photographer', array('action' => 'dashboard'));
        }*/
        $total_albums = 0;
        $pg_id = $this->params()->fromRoute('param1');
        $photographerdata = $this->getServiceLocator()->get('PhotographerTable')->getPhotographerData($pg_id);
        $userdata = $this->getServiceLocator()->get('UserTable')->getUser($pg_id);
        $total_albums = $this->getServiceLocator()->get('AlbumTable')->getTotalAlbumsByUserid($pg_id);
        $photographerdata->phone = $userdata->phone;
        $photographerdata->name = $userdata->name;
        $photographerdata->total_albums = $total_albums;
        //echo '<pre>';print_r($photographerdata->user_id);exit;
        $photographerdata->star = $this->getRatingAction($photographerdata->user_id);
        $ratingandreview = $this->getServiceLocator()->get('RateAndReviewTable')->getRateAndReviewById($photographerdata->user_id);
        //echo '<pre>';print_r($ratingandreview);exit;
        // rating calculation
        

         //profile image path 
        $config = $this->getServiceLocator()->get('config');
        $profile_img_path = $config['module_config']['photographer_profile_image'];

        $config = $this->getServiceLocator()->get('config');
        $album_image_path = $config['module_config']['album_image_path'];
        if($user_session->user_id != $pg_id)
        {
            $editable = 0;
        }
        else
        {
            $editable = 1;
        }


        $albumsdataarry = $this->getServiceLocator()->get('AlbumTable')->getallAlbumsByUserid($pg_id);

        $albumsdata = array();
        if(!empty($albumsdataarry))
        {
            foreach($albumsdataarry as $albumsdata_row)
            {
                // echo '<pre>';print_r($albumsdata_row['album_id']);exit;
                $albumimagedata = $this->getServiceLocator()->get('AlbumImageTable')->getImagesByAlbumId($albumsdata_row['album_id']);
                $albumimagedata_array['image'] = $albumimagedata;
                //$photogdata = (array)$photogdata;
                $albumsdata[] = array_merge($albumsdata_row,$albumimagedata_array);
            }
        }
        //echo '<pre>';print_r($albumsdata);exit;
        $albumtagdata_array = array();
        $tagdata = array();
        if(!empty($albumsdataarry))
        {
        foreach($albumsdataarry as $albumsdata_row)
            {
                
            $albumtagdataarray = $this->getServiceLocator()->get('AlbumTagTable')->getTagByAlbumId($albumsdata_row['album_id']);
                foreach($albumtagdataarray as $albumtagdata_row)
                {
                    //echo '<pre>';print_r($albumtagdata_row);exit;
                    if(!in_array($albumtagdata_row['tag_id'], $albumtagdata_array)) {
                        $albumtagdata_array[]=$albumtagdata_row['tag_id'];
                    }
                }
            }
        }

        foreach($albumtagdata_array as $albumtagdata)
        {
            $album_Tags[] = (array)$this->getServiceLocator()->get('TagTable')->getTagByTagId($albumtagdata);
        }
        //sort($album_Tags);
        //echo '<pre>';print_r($album_Tags);exit;
        $this->layout('layout/photographer-layout');
        return new ViewModel(array(
            'photographerdata' => $photographerdata, 
            'albumsdata' =>$albumsdata, 
            'album_Tags' => $album_Tags, 
            'profile_img_path' => $profile_img_path, 
            'album_image_path' => $album_image_path,
            'albumtagdata_array' => $albumtagdata_array,
            'editable' => $editable,
            'pg_id'=> $pg_id,
            'ratingandreview' => $ratingandreview
            ));
    }

    public function EditProfileAction()
    {
        $user_session = new Container('user');
        $error = "";
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/photographer', array('action' => 'dashboard'));
        }
        $this->layout('layout/photographer-layout');
        $photographerdata = $this->getServiceLocator()->get('PhotographerTable')->getPhotographerData($user_session->user_id);
        $userdata = $this->getServiceLocator()->get('UserTable')->getUser($user_session->user_id);
        $total_albums = $this->getServiceLocator()->get('AlbumTable')->getTotalAlbumsByUserid($user_session->user_id);
        $photographerdata->phone = $userdata->phone;
        $photographerdata->name = $userdata->name;
        $photographerdata->total_albums = $total_albums;
        $photographerdata->star = $this->getRatingAction($photographerdata->user_id);
        $equipmentdata = $this->getServiceLocator()->get('EquipmentTable')->getAllEquipment();
        $productandservicedata = $this->getServiceLocator()->get('ProductAndServiceTable')->getAllProductAndService();

        //profile image path 
        $config = $this->getServiceLocator()->get('config');
        $profile_img_path = $config['module_config']['photographer_profile_image'];
        //echo '<pre>';print_r($photographerdata);exit;
        
        
        //return new ViewModel(array());

        $request = $this->getRequest();
        if ($request->isPost()) 
        {
            $request = $this->getRequest();
            $photographer = new Photographer();
            $arr = $request->getPost($arr);
            $arr['description'] = stripslashes(htmlspecialchars($arr['description']));
            $arr['address'] = stripslashes(htmlspecialchars($arr['address']));
            //echo '<pre>';print_r($arr);exit;
            $arr['user_id']=$user_session->user_id;

            if($arr['name']!="" && $arr['location']!="" && $arr['price']!="" && $arr['description']!="")
            {
                $arr['profile_progress']=1;
            }
            else
            {

                $arr['profile_progress']=0;
            }
            if($arr['name']!="" || $arr['location']!="" || $arr['price']!="" ||$arr['description']!="")
            {
                $error="Please fill all the required fields";
            } //echo '<pre>';print_r($arr);     
            $photographer->exchangeArray($arr);

            //echo '<pre>';print_r($photographer);exit;
            $this->getServiceLocator()->get('PhotographerTable')->savePhotographer($photographer);

            $this->getServiceLocator()->get('UserTable')->updateUserPhoneAndName($arr['user_id'],$arr['phone'],$arr['name']);
            $user_session->first_name = $arr['name'];
            //return $this->redirect()->toRoute('user/photographer', array('action' => 'editprofile'));
            return $this->redirect()->toRoute('user/photographer', array('action' => 'profile','param1'=>$user_session->user_id));
        }
        //return $this->redirect()->toRoute('user/photographer', array('action' => 'editprofile'));

        $this->layout()->photographerinfo = $photographerdata;
        return new ViewModel(array(
            'photographerdata'=>$photographerdata, 
            'profile_img_path' => $profile_img_path,
            'equipmentdata' => $equipmentdata, 
            'productandservicedata' => $productandservicedata
            ));
    }

    public function allProfilesAction()
    {
        $user_session = new Container('user');
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/photographer', array('action' => 'dashboard'));
        }
        $this->layout('layout/photographer-layout');

        $total_albums = 0;
        $photographerdata = $this->getServiceLocator()->get('PhotographerTable')->getPhotographerData($user_session->user_id);
        $userdata = $this->getServiceLocator()->get('UserTable')->getUser($user_session->user_id);
        $total_albums = $this->getServiceLocator()->get('AlbumTable')->getTotalAlbumsByUserid($user_session->user_id);
        $photographerdata->phone = $userdata->phone;
        $photographerdata->name = $userdata->name;
        $photographerdata->total_albums = $total_albums;
        $photographerdata->star = $this->getRatingAction($photographerdata->user_id);
        $allphotographeruserdata = $this->getServiceLocator()->get('UserTable')->getUserByUserType(2, $user_session->user_id);
        //echo '<pre>'; print_r($allphotographeruserdata);exit;
        $dataarray = array();
        foreach($allphotographeruserdata as $allphotographeruser)
        {   
            $id = $allphotographeruser['id']; 
            $shortlisted_userdata = (array)$this->getServiceLocator()->get('UserTable')->getUser($id);
            $shortlisted_pgdata = (array)$this->getServiceLocator()->get('PhotographerTable')->getPhotographerData($id);
            $albumsdataarry = $this->getServiceLocator()->get('AlbumTable')->getallAlbumsByUserid($id);
            $shortlisted_userdata['totalalbums'] = count($albumsdataarry);
            
            $albumimagedata = $this->getServiceLocator()->get('AlbumImageTable')->getImagesByAlbumId($albumsdataarry[0]['album_id']);
            $shortlisted_userdata['coverimage'] = $albumimagedata[0]['thumbname'];
            
            $dataarray[] = array_merge($shortlisted_userdata,$shortlisted_pgdata);
        }

        //echo '<pre>'; print_r($dataarray);exit;

        $config = $this->getServiceLocator()->get('config');
        $profile_img_path = $config['module_config']['photographer_profile_image'];
        $config = $this->getServiceLocator()->get('config');
        $album_image_path = $config['module_config']['album_image_path'];

        return new ViewModel(array('dataarray' => $dataarray, 'profile_img_path' => $profile_img_path,  'photographerdata' => $photographerdata, 'album_image_path' => $album_image_path));
    }

    public function PhotographerProfileAction()
    {   
        $user_session = new Container('user');
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/photographer', array('action' => 'dashboard'));
        }
        $pg_id = $this->params()->fromRoute('param1');
        $total_albums = 0;
        $photographerdata = $this->getServiceLocator()->get('PhotographerTable')->getPhotographerData($pg_id);
        $userdata = $this->getServiceLocator()->get('UserTable')->getUser($pg_id);
        $total_albums = $this->getServiceLocator()->get('AlbumTable')->getTotalAlbumsByUserid($pg_id);
        $photographerdata->phone = $userdata->phone;
        $photographerdata->phone = $userdata->name;
        $photographerdata->phone = $userdata->email;
        $photographerdata->total_albums = $total_albums;

         //profile image path 
        $config = $this->getServiceLocator()->get('config');
        $profile_img_path = $config['module_config']['photographer_profile_image'];


        $albumsdataarry = $this->getServiceLocator()->get('AlbumTable')->getallAlbumsByUserid($pg_id);
        // $albumsdataarry = $this->getServiceLocator()->get('AlbumTable')->getallAlbumsByUserid($user_session->user_id);
        $albumsdata = array();
        foreach($albumsdataarry as $albumsdata_row)
        {
            // echo '<pre>';print_r($albumsdata_row['album_id']);exit;
            $albumimagedata = $this->getServiceLocator()->get('AlbumImageTable')->getImagesByAlbumId($albumsdata_row['album_id']);
            $albumimagedata_array['image'] = $albumimagedata;
            //$photogdata = (array)$photogdata;
            $albumsdata[] = array_merge($albumsdata_row,$albumimagedata_array);
        }
        $album_Tags = $this->getServiceLocator()->get('TagTable')->getTag();

        //echo '<pre>';print_r($album_Tags);exit;
        $this->layout('layout/photographer-layout');

        return new ViewModel(array('photographerdata' => $photographerdata, 'albumsdata' =>$albumsdata, 'album_Tags' => $album_Tags, 'profile_img_path' => $profile_img_path));
    }


    public function CreateAlbumAction()
    {
        $user_session = new Container('user');
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/photographer', array('action' => 'dashboard'));
        }
        $total_albums = 0;
        $photographerdata = $this->getServiceLocator()->get('PhotographerTable')->getPhotographerData($user_session->user_id);
        $userdata = $this->getServiceLocator()->get('UserTable')->getUser($user_session->user_id);
        $total_albums = $this->getServiceLocator()->get('AlbumTable')->getTotalAlbumsByUserid($user_session->user_id);
        $photographerdata->phone = $userdata->phone;
        $photographerdata->name = $userdata->name;
        $photographerdata->total_albums = $total_albums;
        $photographerdata->star = $this->getRatingAction($photographerdata->user_id);
        
        $request = $this->getRequest();
        if ($request->isPost()) 
        {   $arr = $request->getPost($arr);
            $arr['description'] = stripslashes(htmlspecialchars($arr['description']));
            $arr['user_id']=$user_session->user_id;
            $album = new Album();
            $album->exchangeArray($arr);
            //echo '<pre>';print_r($arr);
            //echo '<pre>';print_r($album);exit;

            $last_album_id = $this->getServiceLocator()->get('AlbumTable')->saveAlbum($album);  
            foreach($request->getPost()->e2 as $tagsdata)
            {
                $this->getServiceLocator()->get('AlbumTagTable')->saveAlbumTags($last_album_id,$tagsdata);
            }
            //echo $arr['album_name'];exit;
            if(!empty($arr->images_name))
            {   
                $images_name_array = explode(',',trim($arr['images_name'],','));
                $images_des_array = explode(',',trim($arr['images_des'],','));
                $image_com_data = array_combine($images_name_array,$images_des_array);
                //echo '<pre>'; print_r($image_com_data);
                foreach($image_com_data as $image => $description)
                {   
                    //echo '<pre>'; print_r($albumimage);
                    $image_arr = array();
                    $image_arr['album_id'] = $last_album_id;
                    $image_arr['imagename'] = $image;
                    $image_arr['thumbname'] = "thumbnail_".$image;
                    $image_arr['description'] = $description;
                    //echo '<pre>'; print_r($image_arr);
                    $albumimage = new AlbumImage();
                    $albumimage->exchangeArray($image_arr);
                    //echo '<pre>'; print_r($albumimage); exit;
                    $this->getServiceLocator()->get('AlbumImageTable')->saveImages($albumimage);
                }
            }
            return $this->redirect()->toRoute('user/photographer', array('action' => 'invitefriends'));
        }

        /*$MerchantTable = $this->getServiceLocator()->get('MerchantTable');
        $result_merchant = $MerchantTable->getMerchantByEmail($user_session->email); */
        $this->layout('layout/photographer-layout');
        
        $album_data = array();
        $total_albums = $this->getServiceLocator()->get('AlbumTable')->getTotalAlbumsByUserid($user_session->user_id);
        $album_data['total_albums'] = $total_albums; 
        //echo '<pre>';print_r($album_data);exit;

        // echo "in";exit;
        $album_Tags = $this->getServiceLocator()->get('TagTable')->getTag();
        
        
        //echo '<pre>';print_r($album_Tags);exit;
        //return new ViewModel(array()); 
        
        $this->layout()->albuminfo = $album_data;
        return new ViewModel(array('album_data'=>$album_data,'album_Tags'=>$album_Tags,'photographerdata'=>$photographerdata));
    }


    public function AddAlbumAction()
    {
        $user_session = new Container('user');
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/photographer', array('action' => 'dashboard'));
        }
        $total_albums = 0;
        $photographerdata = $this->getServiceLocator()->get('PhotographerTable')->getPhotographerData($user_session->user_id);
        $userdata = $this->getServiceLocator()->get('UserTable')->getUser($user_session->user_id);
        $total_albums = $this->getServiceLocator()->get('AlbumTable')->getTotalAlbumsByUserid($user_session->user_id);
        $photographerdata->phone = $userdata->phone;
        $photographerdata->name = $userdata->name;
        $photographerdata->total_albums = $total_albums;
        $photographerdata->star = $this->getRatingAction($photographerdata->user_id);
        
        $request = $this->getRequest();
        if ($request->isPost()) 
        {   $arr = $request->getPost($arr);
            $arr['description'] = stripslashes(htmlspecialchars($arr['description']));
            $arr['user_id']=$user_session->user_id;
            $album = new Album();
            $album->exchangeArray($arr);
            //echo '<pre>';print_r($arr);
            //echo '<pre>';print_r($arr);exit;

            $last_album_id = $this->getServiceLocator()->get('AlbumTable')->saveAlbum($album);  
            foreach($request->getPost()->e2 as $tagsdata)
            {
                $this->getServiceLocator()->get('AlbumTagTable')->saveAlbumTags($last_album_id,$tagsdata);
            }
            //echo $arr['album_name'];exit;
            if(!empty($arr->images_name))
            {   
                $images_name_array = explode(',',trim($arr['images_name'],','));
                $images_des_array = explode(',',trim($arr['images_des'],','));
                $image_com_data = array_combine($images_name_array,$images_des_array);
                //echo '<pre>'; print_r($image_com_data);
                foreach($image_com_data as $image => $description)
                {   
                    $image_arr = array();
                    $image_arr['album_id'] = $last_album_id;
                    $image_arr['imagename'] = $image;
                    $image_arr['thumbname'] = "thumbnail_".$image;
                    $image_arr['description'] = $description;
                    //echo '<pre>'; print_r($image_arr);
                    $albumimage = new AlbumImage();
                    $albumimage->exchangeArray($image_arr);
                    //echo '<pre>'; print_r($albumimage); 
                    $this->getServiceLocator()->get('AlbumImageTable')->saveImages($albumimage);
                }
            }
            return $this->redirect()->toRoute('user/photographer', array('action' => 'albumdetail','param1'=>$last_album_id,'param2'=>$photographerdata->user_id));
        }

        /*$MerchantTable = $this->getServiceLocator()->get('MerchantTable');
        $result_merchant = $MerchantTable->getMerchantByEmail($user_session->email); */
        $this->layout('layout/photographer-layout');
        
        $album_data = array();
        $total_albums = $this->getServiceLocator()->get('AlbumTable')->getTotalAlbumsByUserid($user_session->user_id);
        $album_data['total_albums'] = $total_albums; 
        //echo '<pre>';print_r($album_data);exit;
        $config = $this->getServiceLocator()->get('config');
        $profile_img_path = $config['module_config']['photographer_profile_image'];

        // echo "in";exit;
        $album_Tags = $this->getServiceLocator()->get('TagTable')->getTag();
        
        
        //echo '<pre>';print_r($album_Tags);exit;
        //return new ViewModel(array()); 
        
        $this->layout()->albuminfo = $album_data;
        return new ViewModel(array('album_data'=>$album_data,'album_Tags'=>$album_Tags,'photographerdata'=>$photographerdata, 'profile_img_path' => $profile_img_path));
    }


    public function AlbumAction()
    {
        $user_session = new Container('user');
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/photographer', array('action' => 'dashboard'));
        }
        $total_albums = 0;
        $photographerdata = $this->getServiceLocator()->get('PhotographerTable')->getPhotographerData($user_session->user_id);
        $userdata = $this->getServiceLocator()->get('UserTable')->getUser($user_session->user_id);
        $total_albums = $this->getServiceLocator()->get('AlbumTable')->getTotalAlbumsByUserid($user_session->user_id);
        $photographerdata->phone = $userdata->phone;
        $photographerdata->name = $userdata->name;
        $photographerdata->total_albums = $total_albums;

        //$albumsdata = $this->getServiceLocator()->get('AlbumTable')->getallAlbumsByUserid($user_session->user_id);
        //echo '<pre>';print_r($albumsdata);exit;
        $this->layout('layout/photographer-layout');
        return new ViewModel(array('photographerdata'=>$photographerdata));
    }

    public function AlbumDetailAction()
    {
        $user_session = new Container('user');
        /*if($user_session->email == ""){
            return $this->redirect()->toRoute('user/photographer', array('action' => 'dashboard'));
        }*/
        $total_albums = 0;
        $user_id = $this->params()->fromRoute('param2');
        $photographerdata = $this->getServiceLocator()->get('PhotographerTable')->getPhotographerData($user_id);
        $userdata = $this->getServiceLocator()->get('UserTable')->getUser($user_id);
        $total_albums = $this->getServiceLocator()->get('AlbumTable')->getTotalAlbumsByUserid($user_id);
        $photographerdata->phone = $userdata->phone;
        $photographerdata->name = $userdata->name;
        $photographerdata->total_albums = $total_albums;
        $photographerdata->star = $this->getRatingAction($photographerdata->user_id);
        $album_id = $this->params()->fromRoute('param1');
        if($user_session->user_id != $album_id)
        {
            $editable = 0;
        }
        else
        {
            $editable = 1;
        }
        $album_id_array = array();
        $albumsdataarry = $this->getServiceLocator()->get('AlbumTable')->getallAlbumsByUserid($user_id);
        foreach($albumsdataarry as $ablum_row)
        {
            $album_id_array[] = $ablum_row['album_id'];
        }
        if(in_array($album_id, $album_id_array))
        {
            $album_editable = 1;
        }
        else
        {
            $album_editable = 0;
        }
        //echo '<pre>';print_r($album_id_array); exit;
        $albumsdata = $this->getServiceLocator()->get('AlbumTable')->getAlbumsByAlbumId($album_id);
       // echo '<pre>';print_r($albumsdataarry); exit;
        
        $config = $this->getServiceLocator()->get('config');
        $album_image_path = $config['module_config']['album_image_path'];
        
        $config = $this->getServiceLocator()->get('config');
        $profile_img_path = $config['module_config']['photographer_profile_image'];
        
        
        $albumimagedata = $this->getServiceLocator()->get('AlbumImageTable')->getImagesByAlbumId($album_id);
        $albumsdata->image = $albumimagedata;
        $album_tag_data = $this->getServiceLocator()->get('AlbumTagTable')->getTagByAlbumId($album_id);
        //echo '<pre>';print_r($album_tag_data);exit;
        $tag_id = array();
        foreach($album_tag_data as $album_tag_row)
        { 
            $album_Tags[] = (array)$this->getServiceLocator()->get('TagTable')->getTagByTagId($album_tag_row['tag_id']);
        }
        $this->layout('layout/photographer-layout');
        return new ViewModel(array('photographerdata'=>$photographerdata, 'albumsdata' => $albumsdata , 'album_image_path' => $album_image_path, 'profile_img_path' => $profile_img_path, 'editable' =>$editable , 'album_editable' => $album_editable, 'album_Tags' =>$album_Tags));
    }

    public function EditAlbumAction()
    {
        $user_session = new Container('user');
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/photographer', array('action' => 'dashboard'));
        }
        $total_albums = 0;
        $photographerdata = $this->getServiceLocator()->get('PhotographerTable')->getPhotographerData($user_session->user_id);
        $userdata = $this->getServiceLocator()->get('UserTable')->getUser($user_session->user_id);
        $total_albums = $this->getServiceLocator()->get('AlbumTable')->getTotalAlbumsByUserid($user_session->user_id);
        $photographerdata->phone = $userdata->phone;
        $photographerdata->name = $userdata->name;
        $photographerdata->total_albums = $total_albums;
        $photographerdata->star = $this->getRatingAction($photographerdata->user_id);

        $album_id = $this->params()->fromRoute('param1');
        
        $album_data = $this->getServiceLocator()->get('AlbumTable')->getAlbumsByAlbumId($album_id);
        $config = $this->getServiceLocator()->get('config');
        $album_image_path = $config['module_config']['album_image_path'];

        $album_data->album_image_path = $album_image_path;
        
        $albumimagedata = $this->getServiceLocator()->get('AlbumImageTable')->getImagesByAlbumId($album_id);
        $album_data->image = $albumimagedata;
        
        $album_tag_data = $this->getServiceLocator()->get('AlbumTagTable')->getTagByAlbumId($album_id);
        $tags = array();
        $album_select_Tags = array();
        foreach($album_tag_data as $album_row)
        {
            $tags = (array)$this->getServiceLocator()->get('TagTable')->getTagByTagId($album_row['tag_id']);
            $album_select_Tags[]  = array_merge($album_row,$tags);
        }
        //echo '<pre>';print_r($album_select_Tags); exit;
        $config = $this->getServiceLocator()->get('config');
        $profile_img_path = $config['module_config']['photographer_profile_image'];

        
        $request = $this->getRequest();
        if ($request->isPost()) 
        {   $arr = $request->getPost($arr);
            //print_r($arr);exit;
            $arr['description'] = stripslashes(htmlspecialchars($arr['description']));

            $arr['user_id'] = $user_session->user_id;
            $arr['album_id'] = $album_id;

            $album = new Album();
            $album->exchangeArray($arr);
            $last_album_id = $this->getServiceLocator()->get('AlbumTable')->saveAlbum($album);
            $this->getServiceLocator()->get('AlbumTagTable')->deleteAlbumTags($last_album_id);
            foreach($request->getPost()->e2 as $tagsdata)
            {   
                $this->getServiceLocator()->get('AlbumTagTable')->saveAlbumTags($last_album_id,$tagsdata);
            }
            //$images_name_array = explode(',',$arr['images_name']);

            if(!empty($arr->images_name))
            {   
                $images_name_array = explode(',',trim($arr['images_name'],','));
                $images_des_array = explode(',',trim($arr['images_des'],','));
                $image_com_data = array_combine($images_name_array,$images_des_array);
                //echo '<pre>'; print_r($images_name_array);
                //echo '<pre>'; print_r($images_des_array); exit;
                foreach($image_com_data as $image => $description)
                {   
                    if($description=='0')
                    {
                        $description = '';
                    }
                    $image_arr = array();
                    $image_arr['album_id'] = $last_album_id;
                    $image_arr['imagename'] = $image;
                    $image_arr['thumbname'] = "thumbnail_".$image;
                    $image_arr['description'] = $description;
                    //echo '<pre>'; print_r($image_arr);
                    $albumimage = new AlbumImage();
                    $albumimage->exchangeArray($image_arr);
                    //echo '<pre>'; print_r($albumimage); 
                    $this->getServiceLocator()->get('AlbumImageTable')->saveImages($albumimage);
                }
            }
            
            return $this->redirect()->toRoute('user/photographer', array('action' => 'albumdetail','param1'=>$last_album_id,'param2'=>$photographerdata->user_id));
        }

        /*$MerchantTable = $this->getServiceLocator()->get('MerchantTable');
        $result_merchant = $MerchantTable->getMerchantByEmail($user_session->email); */
        $this->layout('layout/photographer-layout');
        
        
        $total_albums = $this->getServiceLocator()->get('AlbumTable')->getTotalAlbumsByUserid($user_session->user_id);
        $album_data->total_albums = $total_albums; 
        //echo '<pre>';print_r($album_data);exit;

        // echo "in";exit;
        $album_Tags = $this->getServiceLocator()->get('TagTable')->getTag();
        
        
        //echo '<pre>';print_r($album_Tags);exit;
        //return new ViewModel(array()); 
        
        $this->layout()->albuminfo = $albumsdata;
        return new ViewModel(array('album_data'=>$album_data,'album_Tags'=>$album_Tags,'photographerdata'=>$photographerdata, 'profile_img_path' => $profile_img_path, 'album_image_path' => $album_image_path, 'album_select_Tags' => $album_select_Tags));
    }

    public function InviteFriendsAction()
    {
        $user_session = new Container('user');
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/home');
        }
        $this->layout('layout/photographer-layout');
        $photographerdata = $this->getServiceLocator()->get('PhotographerTable')->getPhotographerData($user_session->user_id);
        $userdata = $this->getServiceLocator()->get('UserTable')->getUser($user_session->user_id);
        $total_albums = $this->getServiceLocator()->get('AlbumTable')->getTotalAlbumsByUserid($user_session->user_id);
        $photographerdata->phone = $userdata->phone;
        $photographerdata->name = $userdata->name;
        $photographerdata->total_albums = $total_albums;
        $this->layout('layout/photographer-layout');
        return new ViewModel(array('photographerdata'=>$photographerdata));
    }

    public function restructureArrayAction(array $images)
    {
        $result = array();

        foreach ($images as $key => $value) 
        {
            foreach ($value as $k => $val) 
            {
                for ($i = 0; $i < count($val); $i++) 
                {
                    $result[$i][$k] = $val[$i];
                }
            }
        }
        return $result;
    }

    public function uploadImgAction()
    {
        define('UPLOAD_PROD_IMG_PATH',"public/images/photographer/uploads/");
        $data[$i] = '';
        if(isset( $_POST['image_upload'] ) && !empty( $_FILES['images'] ))
        {
            //get the structured array
            $images = $this->restructureArrayAction($_FILES);
            $allowedExts = array("gif", "jpeg", "jpg", "png");
            
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            
            foreach ( $images as $key => $value){
                
                //$data = array();
                $i = $key+1;
                //create directory if not exists
                if (!file_exists('images')) {
                    mkdir('images', 0777, true);
                }
                $image_name = $value['name'];
                //get image extension
                $ext = strtolower(pathinfo($image_name, PATHINFO_EXTENSION));
                //assign unique name to image
                $imgnamew = time();
                $name = $i*$imgnamew.'.'.$ext;
                $unique_id = $i*$imgnamew;
                //$name = $image_name;
                //image size calcuation in KB
                $image_size = $value["size"] / 1024;
                $image_flag = true;
                //max image size
                $max_size = 1024;
                if( in_array($ext, $allowedExts) && $image_size < $max_size ){
                    $image_flag = true;
                } else {
                    $image_flag = false;
                    $data[$i]['error'] = 'Maybe '.$image_name. ' exceeds max '.$max_size.' KB size or incorrect file extension';
                } 
                
                if( $value["error"] > 0 ){
                    $image_flag = false;
                    $data[$i]['error'] = '';
                    $data[$i]['error'].= '<br/> '.$image_name.' Image contains error - Error Code : '.$value["error"];
                }
                
                if($image_flag){
                    ///mkdir(UPLOAD_PROD_IMG_PATH,0777);
                    move_uploaded_file($value["tmp_name"], UPLOAD_PROD_IMG_PATH.$name);
                    $src = UPLOAD_PROD_IMG_PATH.$name;
                    $dist =UPLOAD_PROD_IMG_PATH."thumbnail_".$name;
                    $secondimagedist =UPLOAD_PROD_IMG_PATH."secondimagethumbnail_".$name;
                    $firstimagedist =UPLOAD_PROD_IMG_PATH."firstimagethumbnail_".$name;
                    $data[$i]['success'] = $thumbnail = 'thumbnail_'.$name;
                    $data[$i]['name'] = $name;
                    $data[$i]['unique_id'] =  $unique_id;
                    //$this->thumbnailAction($src, $dist, 400); 
                    $this->firstimageThumbnailAction($src, $dist, 350, 225); 
                    $this->firstimageThumbnailAction($src, $secondimagedist, 140, 108);   
                    $this->firstimageThumbnailAction($src, $firstimagedist, 210, 219 );  
                    
                    
                    $asset['name'] = $name;
                    $asset['post_type'] = 'product';
                    $asset['status'] = 0;
                    $asset['date'] = date("y-m-d H:i:s");
                    $asset['location'] = UPLOAD_PROD_IMG_PATH;
                    $asset['unique_id'] =$data[$i]['unique_id'];
                    //$this->products_model->add_assets($asset);
                }
            }

            echo json_encode($data); die;
            
        } else {
                $data[] = 'No Image Selected..';
        }
    }

    function thumbnailAction($src, $dist, $dis_width = 100 )
    {

        $img = '';
        $extension = strtolower(strrchr($src, '.'));
        switch($extension)
        {
            case '.jpg':
            case '.jpeg':
                $img = @imagecreatefromjpeg($src);
                break;
            case '.gif':
                $img = @imagecreatefromgif($src);
                break;
            case '.png':
                $img = @imagecreatefrompng($src);
                break;
        }
        $width = imagesx($img);
        $height = imagesy($img);
        $dis_height = $dis_width * ($height / $width);

        $new_image = imagecreatetruecolor($dis_width, $dis_height);
        imagecopyresampled($new_image, $img, 0, 0, 0, 0, $dis_width, $dis_height, $width, $height);


        $imageQuality = 100;

        switch($extension)
        {
            case '.jpg':
            case '.jpeg':
                if (imagetypes() & IMG_JPG) {
                    imagejpeg($new_image, $dist, $imageQuality);
                }
                break;

            case '.gif':
                if (imagetypes() & IMG_GIF) {
                    imagegif($new_image, $dist);
                }
                break;

            case '.png':
                $scaleQuality = round(($imageQuality/100) * 9);
                $invertScaleQuality = 9 - $scaleQuality;

                if (imagetypes() & IMG_PNG) {
                    imagepng($new_image, $dist, $invertScaleQuality);
                }
                break;
        }
        imagedestroy($new_image);
    }
    function firstimageThumbnailAction($src, $dist, $dis_width = 100, $dis_height = 100 )
    {

        $img = '';
        $extension = strtolower(strrchr($src, '.'));
        switch($extension)
        {
            case '.jpg':
            case '.jpeg':
                $img = @imagecreatefromjpeg($src);
                break;
            case '.gif':
                $img = @imagecreatefromgif($src);
                break;
            case '.png':
                $img = @imagecreatefrompng($src);
                break;
        }
        $width = imagesx($img);
        $height = imagesy($img);
        

        $new_image = imagecreatetruecolor($dis_width, $dis_height);
        imagecopyresampled($new_image, $img, 0, 0, 0, 0, $dis_width, $dis_height, $width, $height);


        $imageQuality = 100;

        switch($extension)
        {
            case '.jpg':
            case '.jpeg':
                if (imagetypes() & IMG_JPG) {
                    imagejpeg($new_image, $dist, $imageQuality);
                }
                break;

            case '.gif':
                if (imagetypes() & IMG_GIF) {
                    imagegif($new_image, $dist);
                }
                break;

            case '.png':
                $scaleQuality = round(($imageQuality/100) * 9);
                $invertScaleQuality = 9 - $scaleQuality;

                if (imagetypes() & IMG_PNG) {
                    imagepng($new_image, $dist, $invertScaleQuality);
                }
                break;
        }
        imagedestroy($new_image);
    }

    public function editprofileimageAction() 
    {
        $user_session = new Container('user');       
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/photographer', array('action' => 'dashboard'));
        }
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $config = $this->getServiceLocator()->get('config');
            $uploadPath = $config['module_config']['profile_image_upload'];

            $uploadFile = $this->params()->fromFiles('profilepic');
            
            //echo '<pre>'; print_r($_FILES);
            //echo '<pre>';  print_r($uploadFile); die;

            $adapter = new \Zend\File\Transfer\Adapter\Http();
            
            //echo $adapter;exit;
            $adapter->setDestination($uploadPath);

            $adapter->receive($uploadFile['name']);
            //echo '<pre>';  print_r($uploadFile); die;
            
            $rand = rand(2000,100000);
            $old = $uploadPath .$uploadFile['name'];
            $new = $uploadPath .$rand.'_'.$uploadFile['name'];
            rename($old, $new);
            
            $sourceImageFileName = $uploadPath . $rand.'_'.$uploadFile['name'];
            $thumbnailFileName = $rand.'_tn120x120_'.$uploadFile['name'];
            $headerthumbnail = $rand.'_tn50x50_'.$uploadFile['name'];
            $imageThumb = $this->getServiceLocator()->get('WebinoImageThumb');
            $thumb = $imageThumb->create($sourceImageFileName, $options = array());
            //echo '<pre>'; print_r($thumb); exit;
            //$thumb->resize(120, 120);'public/images/photographer/profile/'
            $thumbFileName = $thumbnailFileName;
            $image_path = $uploadPath . '/' . $thumbnailFileName;
            $des_path = $uploadPath . '/thumb/' . $thumbFileName;
            $des_path_50px = $uploadPath . '/thumb/' . $headerthumbnail;
            $thumb->save($image_path);

            $this->firstimageThumbnailAction($image_path, $des_path, 128, 128 );
            $this->firstimageThumbnailAction($image_path, $des_path_50px, 50, 50 );
            $profilepic = $rand . '_pp_' .$uploadFile['name']; 
            $this->getServiceLocator()->get('PhotographerTable')->updateProfileImage($user_session->user_id,$profilepic);
            $user_session->profilepic = $profilepic;
            $_SESSION['user']->profilepic = $profilepic;
            return $this->redirect()->toRoute('user/photographer', array('action' => 'editprofile'));
            
        }
        
    }

    public function deleteImgAction() 
    {
        $user_session = new Container('user');  
        //echo $user_session->email;exit;     
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/photographer', array('action' => 'dashboard'));
        }
        //
        $request = $this->getRequest();
        if ($request->isPost()) 
        {
            $arr = $request->getPost();
            $unique_id = $arr['id'];
            $image_name = $arr['name'];
            $this->getServiceLocator()->get('AlbumImageTable')->deleteByName($image_name);
            $config = $this->getServiceLocator()->get('config');
            $album_image_path = $config['module_config']['album_image_path'];
            unlink($album_image_path.$image_name);
            //echo '<pre>';print_r($arr);
            $response = 'success';
        }
        else 
        {
            $response = 'failed';
        }
        echo json_encode(array('response'=>$response));
        die();
    }
    public function deleteAlbumAction() 
    {
        $user_session = new Container('user');  
        //echo $user_session->email;exit;     
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/photographer', array('action' => 'dashboard'));
        }
        //
        $request = $this->getRequest();
        if ($request->isPost()) 
        {
            $arr = $request->getPost();
            $album_id = $arr['album_id'];
            $config = $this->getServiceLocator()->get('config');
            $album_image_path = $config['module_config']['album_image_path'];
            $albumimagedata = $this->getServiceLocator()->get('AlbumImageTable')->getImagesByAlbumId($album_id);
            if(!empty($albumimagedata))
            {
                foreach($albumimagedata as $row)
                {
                    unlink($album_image_path.$row['imagename']);
                    unlink($album_image_path.$row['thumbname']);
                    unlink($album_image_path."firstimage".$row['thumbname']);
                    unlink($album_image_path."secondimage".$row['thumbname']);
                }
                $this->getServiceLocator()->get('AlbumImageTable')->deleteAlbumImagesByAlbumId($album_id);
            }

            $this->getServiceLocator()->get('AlbumTable')->deleteAlbumsByAlbumId($album_id);
            $this->getServiceLocator()->get('AlbumTagTable')->deleteAlbumTags($album_id);
            
            
            //echo '<pre>';print_r($arr);
            $response = 'success';
        }
        else 
        {
            $response = 'failed';
        }
        echo json_encode(array('response'=>$response));
        die();
    }
    

    public function filteredAlbumAction() 
    {
        //echo "in"; exit;
        $user_session = new Container('user');  
        //echo $user_session->email;exit;     
        /*if($user_session->email == ""){
            return $this->redirect()->toRoute('user/photographer', array('action' => 'dashboard'));
        }*/
        $request = $this->getRequest();
        if ($request->isPost()) 
        {
            $arr = $request->getPost();
            $tag_id = $arr['tag_id'];
            $user_id = $arr['user_id'];

            // get all albums of current user
            $albumsdataarry = $this->getServiceLocator()->get('AlbumTable')->getallAlbumsByUserid($user_id);
            //echo '<pre>'; print_r($albumsdataarry);
            $albumsdata = array();

            if($tag_id == 0)
            {
                foreach($albumsdataarry as $albumsdata_row)
                {
                    // echo '<pre>';print_r($albumsdata_row['album_id']);exit;
                    $albumimagedata = $this->getServiceLocator()->get('AlbumImageTable')->getImagesByAlbumId($albumsdata_row['album_id']);
                    $albumimagedata_array['image'] = $albumimagedata;
                    //$photogdata = (array)$photogdata;
                    $albumsdata[] = array_merge($albumsdata_row,$albumimagedata_array);
                }
            }
            else
            {
                foreach($albumsdataarry as $albumsdata_row)
                {
                    //echo '<pre>'; print_r($albumsdata_row);
                    $album_id = $albumsdata_row['album_id'];
                    $albumtagdataarray =$this->getServiceLocator()->get('AlbumTagTable')->getTagByAlbumAndTagId($album_id,$tag_id);
                    
                    if(!empty($albumtagdataarray))
                    {   $albumtagdataarray = (array)$albumtagdataarray;
                        //echo '<pre>'; print_r($albumtagdataarray); 
                        $albumimagedata = $this->getServiceLocator()->get('AlbumImageTable')->getImagesByAlbumId($albumtagdataarray['album_id']);
                        //echo '<pre>'; print_r($albumimagedata);
                        $albumimagedata_array['image'] = $albumimagedata;
                        //echo '<pre>'; print_r($albumimagedata_array);     
                        $albumsdata[] = array_merge($albumsdata_row,$albumimagedata_array);
                        //echo '<pre>'; print_r($albumsdata);
                    }
                }
            }
            //echo '<pre>'; print_r($albumsdata);   
            $response = 'success';
        }
        else 
        {
            $response = 'failed';
        }
        echo json_encode(array('response'=>$response, 'albumsdata'=>$albumsdata));
        die();
    }


    public function incrementCounterAction() 
    {
        $user_session = new Container('user');  
        //echo $user_session->email;exit;     
        $request = $this->getRequest();
        if ($request->isPost()) 
        {
            $arr = $request->getPost();
            $pg_id = $arr['pg_id'];
            if($pg_id != $user_session->user_id)
            {
                $userdata = (array)$this->getServiceLocator()->get('UserTable')->getUserVisit($pg_id);
                $visit = $userdata['visit']; 
                $this->getServiceLocator()->get('UserTable')->updateUserVisit($pg_id, $visit);
            }
            
            $response = 'success';
        }
        else 
        {
            $response = 'failed';
        }
        echo json_encode(array('response'=>$response));
        die();
    }

    public function submitProfileAction() 
    {
        $user_session = new Container('user');  
        //echo $user_session->email;exit;     
        $request = $this->getRequest();
        if ($request->isPost()) 
        {
            $arr = $request->getPost();
            $user_id = $arr['user_id'];
            $this->getServiceLocator()->get('PhotographerTable')->updateStatus($user_id);
            $response = 'success';
        }
        else 
        {
            $response = 'failed';
        }
        echo json_encode(array('response'=>$response));
        die();
    }

    public function shortlistAction() 
    {
        $user_session = new Container('user');  
        //echo $user_session->email;exit;     
        $request = $this->getRequest();
        if ($request->isPost()) 
        {
            $arr = $request->getPost();
            //echo '<pre>'; print_r($arr); exit;
            $user_id = $arr['pg_id'];
            $cookie_name = array();
            $cookie_value = array();
            //echo $user_id;    
            $cookie_name = "shortlist";
            $cookie_value= $user_id;
            if(!isset($_COOKIE['user'])) 
            {
                //echo "1";
                //echo '<pre>'; print_r($arr);
                //echo '<pre>'; print_r($_COOKIE['user']);exit;
                setcookie('user[0]', $cookie_value, 0);
                $response = 'success';
            }
             // 86400 = 1 day
            //$_SESSION['shortlist'] = $user_id;
            else
            {
                //echo "2";
                //echo '<pre>'; print_r($arr); exit;
                $total_cookie = count($_COOKIE['user']);
                //$total_cookie = ++$total_cookie;
                //if(in_array($user))
                setcookie('user['.$total_cookie.']', $cookie_value, 0);
                $response = 'success';
            }
        }
        else 
        {
            $response = 'failed';
        }
        echo json_encode(array('response'=>$response));
        die();
    }

    public function unShortlistAction() 
    {
        $user_session = new Container('user');  
        //echo $user_session->email;exit;     
        $request = $this->getRequest();
        if ($request->isPost()) 
        {
            $arr = $request->getPost();
            //echo '<pre>'; print_r($arr);
            $user_id = $arr['pg_id'];
            
            //$key = array_search( $user_id,$_COOKIE['user']);
            //setcookie('user['.$key.']', "", time() - 3600); exit;
            if(in_array( $user_id,$_COOKIE['user']))
            {  
                $key = array_search( $user_id,$_COOKIE['user']);
                setcookie('user[0]', "", time() - 3600);
            }
            $response = 'success';
        }
        else 
        {
            $response = 'failed';
        }
        echo json_encode(array('response'=>$response));
        die();
    }

    public function ratingAction() 
    {
        $user_session = new Container('user');  
        //echo $user_session->email;exit;     
        $request = $this->getRequest();
        $message = "";
        if ($request->isPost()) 
        {   
            $arr = $request->getPost();
            $pg_id = $this->params()->fromRoute('param1');
            //echo '<pre>'; print_r($arr);  
            
            $is_exist = $this->getServiceLocator()->get('RateAndReviewTable')->phoneIsAlreadyExist($arr['phone'], $pg_id);
            if($is_exist == 0)
            { 
                $ratingandreview = new RateAndReview();
                $ratingandreview->exchangeArray($arr);
                $otp = rand(1000,9999);
                $ratingandreview->otp = $otp;
                //echo '<pre>';print_r($ratingandreview);exit;
                $ratingid = $this->getServiceLocator()->get('RateAndReviewTable')->saveRateAndReview($ratingandreview);
                if($ratingid)
                {
                    //$this->sendsms($arr['phone'])
                    $sid    =    "WEBSMS";           // sender id
                    $user   =    'nikitaeck';
                    $pwd    =    '394044';
                    $mob_no =    $arr['phone'];       //123, 456 being recepients number
                    $msg    =    'Dear '.$arr["name"]. 'Your OTP is : '.$otp.'.';       //your message
                    $str    =    trim(str_replace(' ', '%20', $msg));
   
                    $url    =    "http://login.smsgatewayhub.com/smsapi/pushsms.aspx?user=$user&pwd=$pwd&to=$mob_no&sid=$sid&msg=$str&fl=0&gwid=2";

                    $ch        =    curl_init();        
                    curl_setopt($ch, CURLOPT_URL,$url);        
                    curl_exec($ch);        
                    curl_close($ch);
                    $response = 'success';
                    $message = "OTP sent to ".$mob_no;
                }
                
            }
            else
            {
                $response = 'failed';
                $message = "Phone no. already exist";
            }
            
        }

        else 
        {
            $response = 'failed';
            $message = "Some error occured";
        }
        echo json_encode(array('response'=>$response, 'message'=>$message));
        die();
    }

    public function resendOtpAction() 
    {
        $user_session = new Container('user');  
        //echo $user_session->email;exit;     
        $request = $this->getRequest();
        $message = "";
        if ($request->isPost()) 
        {   
            $arr = $request->getPost();
            //echo '<pre>'; print_r($arr); 
            //echo $pg_id = $arr['pg_id'];exit;
            
            $is_exist = $this->getServiceLocator()->get('RateAndReviewTable')->phoneIsAlreadyExist($arr['phone']);
            if($is_exist == 1)
            { 
                $otp = rand(1000,9999);
                $this->getServiceLocator()->get('RateAndReviewTable')->updateOtp($arr['phone'], $otp);
                if($ratingid)
                {
                    $sid    =    "WEBSMS";           // sender id
                    $user   =    'nikitaeck';
                    $pwd    =    '394044';
                    $mob_no =    $arr['phone'];       //123, 456 being recepients number
                    $msg    =    'Dear '.$arr["name"]. 'Your OTP is : '.$otp.'.';      //your message
                    $str    =    trim(str_replace(' ', '%20', $msg));
                    $url    =    "http://login.smsgatewayhub.com/smsapi/pushsms.aspx?user=$user&pwd=$pwd&to=$mob_no&sid=$sid&msg=$str&fl=0&gwid=2";

                    $ch        =    curl_init();        
                    curl_setopt($ch, CURLOPT_URL,$url);        
                    curl_exec($ch);        
                    curl_close($ch);
                }
                $response = 'success';
                $message = "OTP resent to ".$arr['phone'];
            }
        }

        else 
        {
            $response = 'failed';
            $message = "Some error occured";
        }
        echo json_encode(array('response'=>$response, 'message'=>$message));
        die();
    }

    public function verifyOtpAction() 
    {
        $user_session = new Container('user');  
        //echo $user_session->email;exit;     
        $request = $this->getRequest();
        $message = "";
        if ($request->isPost()) 
        {   
            $arr = $request->getPost();
            //echo '<pre>'; print_r($arr); 
            //echo $pg_id = $arr['pg_id'];exit;
            
            $ratingdata = $this->getServiceLocator()->get('RateAndReviewTable')->getRateAndReview($arr['phone']);
            //echo '<pre>'; print_r($ratingdata); 
            //echo $otp = $ratingdata->otp;
            if($ratingdata->otp == $arr['otp'])
            {
                $ratingdata = $this->getServiceLocator()->get('RateAndReviewTable')->verifyOtp($ratingdata->id);
                $response = 'success';
                $message = "Thank You !!";
            }
            
            else
            {
                $response = 'failed';
                $message = "Invalid OTP";
            }
        }

        else 
        {
            $response = 'failed';
            $message = "Some error occured";
        }
        echo json_encode(array('response'=>$response, 'message'=>$message));
        die();
    }

    public function shortlistedAction()
    {
        $user_session = new Container('user');
        if($user_session->email!= "")
        {
            $total_albums = 0;
            //echo '<pre>'; print_r($_COOKIE);exit;
            $photographerdata = $this->getServiceLocator()->get('PhotographerTable')->getPhotographerData($user_session->user_id);
            $userdata = $this->getServiceLocator()->get('UserTable')->getUser($user_session->user_id);
            $total_albums = $this->getServiceLocator()->get('AlbumTable')->getTotalAlbumsByUserid($user_session->user_id);
            $photographerdata->phone = $userdata->phone;
            $photographerdata->name = $userdata->name;
            $photographerdata->total_albums = $total_albums;
            $photographerdata->star = $this->getRatingAction($photographerdata->user_id);
        }
        $config = $this->getServiceLocator()->get('config');
        $profile_img_path = $config['module_config']['photographer_profile_image'];
        $album_image_path = $config['module_config']['album_image_path'];
        $this->layout('layout/photographer-layout');
        $shortlisted_pgdata = array();
        $dataarray = array();
        if(!empty($_COOKIE['user']))
        {//echo '<pre>';print_r($photographerdata);exit;
            foreach($_COOKIE['user'] as $user_id)
            {
                $shortlisted_userdata = (array)$this->getServiceLocator()->get('UserTable')->getUser($user_id);
                $shortlisted_pgdata = (array)$this->getServiceLocator()->get('PhotographerTable')->getPhotographerData($user_id);
                $albumsdataarry = $this->getServiceLocator()->get('AlbumTable')->getallAlbumsByUserid($user_id);
                $shortlisted_userdata['totalalbums'] = count($albumsdataarry);
                
                $albumimagedata = $this->getServiceLocator()->get('AlbumImageTable')->getImagesByAlbumId($albumsdataarry[0]['album_id']);
                $shortlisted_userdata['coverimage'] = $albumimagedata[0]['thumbname'];
                
                $dataarray[] = array_merge($shortlisted_userdata,$shortlisted_pgdata);
            }
        }
        else
        {
            $dataarray_error = "No short list.";
        }
        //echo '<pre>'; print_r($dataarray); exit;
       
        $config = $this->getServiceLocator()->get('config');
        $profile_img_path = $config['module_config']['photographer_profile_image'];

        if($user_session->email== "")
        {
            return new ViewModel(array(
            'dataarray' => $dataarray,
            'profile_img_path' => $profile_img_path,
            'dataarray_error' => $dataarray_error, 
            'album_image_path'=> $album_image_path
            ));
        }
        else
        {
            return new ViewModel(array(
            'dataarray' => $dataarray,
            'profile_img_path' => $profile_img_path,
            'photographerdata' => $photographerdata, 
            'dataarray_error' => $dataarray_error, 
            'profile_img_path'=> $profile_img_path, 
            'album_image_path'=> $album_image_path
            ));
        }
        
    }


    
}