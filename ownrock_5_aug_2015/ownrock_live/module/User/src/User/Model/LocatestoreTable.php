<?php

namespace User\Model;

use Zend\Text\Table\Row;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class LocatestoreTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function saveLocatestore(Locatestore $locatestore) {
        $data = array(
            'id' => $locatestore->id,
            'campaign_id' => $locatestore->campaign_id,
            'name' => $locatestore->name,
            'address' => $locatestore->address,
            'lat' => $locatestore->lat,
            'lng' => $locatestore->lng,
            'type' => $locatestore->type,
        );

        $id = (int) $locatestore->id;

        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getLocatestore($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('id does not exist');
            }
        }
    }

    public function getLocatestore($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find $id");
        }
        return $row;
    }

    public function fetchAll() {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function deleteLocatestore($id) {
        $this->tableGateway->delete(array('id' => (int) $id));
    }

}
