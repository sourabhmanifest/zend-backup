<?php

namespace User\Model;

use Zend\Text\Table\Row;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;

class CategorylistTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function saveCategorylist(categorylist $categorylist) {
        $data = array(
            'category' => $categorylist->category,
            'subcategory' => $categorylist->subcategory,
        );

        $id = $categorylist->category;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getCategoryInfo($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Coupon id does not exist');
            }
        }
    }

    public function getCategoryinfo($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function fetchAll(Select $select = null) {
        if (null == $select) {
            $select = new Select();
        }
        $select->from('categorylist');
        $resultSet = $this->tableGateway->selectWith($select);
        //$resultSet = $this->tableGateway->select();
        $resultSet->buffer();
        return $resultSet;
    }

    public function deleteCategoryInfo($id) {
        $this->tableGateway->delete(array('id' => $id));
    }

    public function selectCategoryList($category, Select $select = null) {
        if (null == $select) {
            $select = new Select();
        }
        $select->from('categorylist');
        $select->where(array('Category' => $category));
        $resultSet = $this->tableGateway->selectWith($select);
        $resultSet->buffer();
        return $resultSet;
    }


    public function selectuniquecategory($dbadapter) {
     
       //$sql = "SELECT * FROM categorylist WHERE category = ?";
       $sql = "SELECT DISTINCT category FROM categorylist";
       $resultSet = $dbadapter->query($sql,array("*"));
       //var_dump($resultSet);die;
               $resultSet->buffer();

       return $resultSet;        
    }
       


}
