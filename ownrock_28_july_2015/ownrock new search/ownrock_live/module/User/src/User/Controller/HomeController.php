<?php

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use User\Form\LoginForm;
use User\Model\User;
use User\Model\UserTable;
use User\Model\Project;
use User\Model\ProjectTable;
use User\Model\Projectskill;
use User\Model\ProjectskillTable;
use User\Model\Skills;
use User\Model\SkillsTable;
use User\Model\Merchant;
use User\Model\MerchantTable;
use User\Model\ImageUpload;
use User\Model\ImageUploadTable;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;
use Zend\Mail;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\View\Model\JsonModel;

use Zend\Session\Container; // We need this when using sessions

use Zend\Mail\Message;

use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;

use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\FacebookRedirectLoginHelper;

use Facebook\FacebookSDKException;



class HomeController extends AbstractActionController
{
    protected $authservice;
    public function IndexAction()
    {
        
		
		$this->layout('layout/home-layout');
        
        //$this->sendEmail('webdeveloper24x7@gmail.com','Template','');
        
        $error_id = $this->params()->fromRoute('error_id');
        $error_msg = "";
        if(isset($error_id) && $error_id != ""){
            if($error_id == "25486"){
                $error_msg = 'Invalid login credentials ! Please try again.' ;
            }
            if($error_id == "55600"){
                $error_msg = 'Email address already registered !. Please try with different one.' ;
            }
        }
        
		//skills autosuggest code
		$skillsTable = $this->getServiceLocator()->get('skillstable');
        $allSkills = $skillsTable->getAllSkills();
		foreach($allSkills as $val){
			$newArr[] = "'".$val['skill']."'";
		}
		$skills = implode(', ',$newArr);
		$this->layout()->skills = $skills;		
		//skills autosuggest code end
		
		
		$this->layout()->error_msg = $error_msg;
        
        return new ViewModel(array());
        //return $this->_redirect('/sort/all');
    }
   
    public function getAuthService() {
        $this->authservice = $this->getServiceLocator()->get('AuthService');
        return $this->authservice;
    }
	
	
	
	public function LoginAction()
    {       
		$this->getAuthService()->getAdapter()
                ->setIdentity($this->request->getPost('loginemail'))
                ->setCredential(md5($this->request->getPost('loginpassword')));
        $result = $this->getAuthService()->authenticate();
        $auth = $this->getAuthService();
        $authAdapter = $this->getAuthService()->getAdapter();
        if ($result->isValid()) {
            
			$storage = $auth->getStorage();
            $storage->write($authAdapter->getResultRowObject(
                            null, 'password'));
            
            // Store username in session
            $user_session = new Container('user');
            $result1 = $authAdapter->getResultRowObject();
            
            $user_session->user_id = $result1->id;
            $user_session->first_name = $result1->name;
            $user_session->email = $result1->email;
            $user_session->user_type = $result1->user_type;
            if($result1->user_type == 1)
            {
                $MerchantTable = $this->getServiceLocator()->get('MerchantTable');
                $result_merchant = $MerchantTable->getMerchantByEmail($user_session->email);
                $user_session->profilepic = $result_merchant->profilepic;
                $_SESSION['user']->profilepic = $result_merchant->profilepic;
            }
            else
            {
                $photographerdata = $this->getServiceLocator()->get('PhotographerTable')->getPhotographerData($user_session->user_id);
                $user_session->profilepic = $photographerdata->profilepic;
                $_SESSION['user']->profilepic = $photographerdata->profilepic;
            }
            
            
            
            $_SESSION['user']->id = $result1->id;
			$_SESSION['user']->first_name = $result1->name;
			$_SESSION['user']->email = $result1->email;	
            $_SESSION['user']->user_type = $result1->user_type;
			
			
			$time = 1209600;
            if ($post['remember_me'] = "1") {
                $sessionManager = new \Zend\Session\SessionManager();
                $sessionManager->rememberMe($time);
            }
            $response = 'success';
            //return $this->redirect()->toRoute('user/home', array('action' => 'dashboard'));           
        } else {
            $response = 'failed';
        }
        echo json_encode(array('response'=>$response, 'user_type' =>$user_session->user_type));
        die();
    }

	public function ForgotAction()
    {       
		$post = $this->request->getPost();	
		$UserTable = $this->getServiceLocator()->get('usertable');
        $email_exists = $UserTable->getUserByEmail($post['forgetemail']);
        if($email_exists){
			$tmp_pass = $this->randomNumber(20);	
			$UserTable = $this->getServiceLocator()->get('usertable');
			$veriresponse = $UserTable->updatetmpPasswordUser($tmp_pass, $post['forgetemail']);

			$this->sendForgotPasswordEmail(trim($post['forgetemail']),'Reset password request OwnRock!!',$tmp_pass,$email_exists->name);
			$response = 'success';
			echo json_encode(array('response'=>$response));
			die();
		}else{
			$response = 'failed';
			echo json_encode(array('response'=>$response));
			die();
		}
    }

	public function resetAction()
    {       
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			$post = $this->request->getPost();	
			if($post['tokenavailabel']){		
				$userTable = $this->getServiceLocator()->get('UserTable');		
				$res = $userTable->checkToken($post);
				if($res){
					$response = 'success';
					echo json_encode(array('response'=>$response));
					die();	
				}
			}else{		
				$userTable = $this->getServiceLocator()->get('UserTable');		
				$userTable->resetPassword($post);
			}
			$response = 'success';
			echo json_encode(array('response'=>$response));
			die();		
		}else{
          
			
			
			$data['token'] = $_GET['token'];
			$userTable = $this->getServiceLocator()->get('UserTable');           		
				$res = $userTable->checkToken($data);                  
				if(!$res){                   
					$this->redirect()->toRoute('user/home', array('action' => 'dashboard'));
				}
			$this->layout('layout/reset-layout');            
			return new ViewModel();	
		}
    }

	public function searchAction()
    {       //echo "in";exit;
        
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
        {
            $keyword='';
            $sort_by='';
            $post = $this->request->getPost(); 
            //echo '<pre>'; print_r($post); exit; 
            if($post['keyword']!=''){
                $keyword2 = $post['keyword'];
                $keyword = explode(',', $keyword2);         
            }
            if($post['sort_by']!=''){
                $sort_by = $post['sort_by'];
            }
            
            $rate = $post['rate'];
            $availability = $post['availability'];
            $location = $post['location'];
            $user_type = $post['user_type'];
            $offset=0;
            $limit=3;
            if(isset($post['offset'])){
                $offset = (int) trim($post['offset']);
            }

            if($user_type == 1)
            {

                $MerchantTable = $this->getServiceLocator()->get('MerchantTable');
                $result_merchant = $MerchantTable->getMerchantByKeyword($keyword, $offset, $limit, $location, $sort_by, $rate, $availability);   

               // echo "<pre>"; print_r($result_merchant);
                if($result_merchant['totalcount']>0)
                {
                    if($httpHost=="localhost")
                    {
                    $aimg_path = '/ownrock/public/images/advertiserimage/';
                    }
                    else
                    {
                    $aimg_path = '/public/images/advertiserimage/';
                    }
                    if($val['profilepic']=="")
                    {
                      $val['profilepic'] ='58345_pp_profile.jpg';
                    }
                    $view = '<div class="row marj-row">';
                    foreach($result_merchant['result'] as $key=> $val)
                    {
                        //echo "<pre>"; print_r($result_merchant);  exit;
                        $ProjectTable = $this->getServiceLocator()->get('ProjectTable');
                        $result_project = $ProjectTable->selectProjectName($val['name']);
                        //echo $result_project->project_id; exit;
                        //$result_project['project_id'];  

                        $ImageUploadTable = $this->getServiceLocator()->get('ImageUploadTable');
                        $result_imageupload = $ImageUploadTable->getUploadByProjectId(trim($result_project->project_id)); 
                        //echo "<pre>"; print_r($result_imageupload); exit;
                        //$path = "/zend/public/images/advertiserimage/22943_crm-strategy.png";

                        // get total projects of a merchant...
                        $total_projects = $this->getServiceLocator()->get('ProjectTable')->getTotalProjectByMerchant($val['name']);
                        //$result_merchant->total_projects = $total_projects;

                        if($key%3==0){ $view.= '</div><div class="row marj-row">'; }
                        if($total_projects>=2)
                        {
                            $view.=  '<div class="col-sm-4">
                                <div class="user-box"> <img src="'.$aimg_path.$result_imageupload->filename.'" width="360" height="270" >
                                    <div class="dev-info">
                                        
                                        <img src="'.$aimg_path . str_replace('_pp_','_tn120x120_',$val['profilepic']).'"  class="pro-pic img-circle">
                                        <h3><a href="/user/frontportfolio/'.$val['id'].'/'.str_replace(' ', '-',$val['name']).'">'.$val['name'].'</a></h3>
                                        <Strong>'.$val['profession'].'</Strong>
                                        <p class="dipi"><span>Rate|'.$val['rate'].'$</span> &nbsp; <span>Projects|'.$total_projects.'</span> &nbsp; <span>Availability|'.$val['availability'].'/week</span></p>
                                    </div>
                                </div>
                            </div>';   
                        }             
                    }
                }
                else
                {
                    echo json_encode(array('response'=>'', 'total_count'=>0));
                    exit();
                }
                $view.= '</div>';
                echo json_encode(array('response'=>$view, 'total_count'=>$result_merchant['totalcount']));
                exit();
            }
            else // for photographer
            {
                $PhotographerTable = $this->getServiceLocator()->get('PhotographerTable');
                $result_photographer = $PhotographerTable->getPhotographerByKeyword($keyword, $offset, $limit, $location, $sort_by, $rate, $availability);   

               // echo "<pre>"; print_r($result_merchant);
                if($result_merchant['totalcount']>0)
                {
                    if($httpHost=="localhost")
                    {
                    $aimg_path = '/ownrock/public/images/advertiserimage/';
                    }
                    else
                    {
                    $aimg_path = '/public/images/advertiserimage/';
                    }
                    if($val['profilepic']=="")
                    {
                      $val['profilepic'] ='58345_pp_profile.jpg';
                    }
                    $view = '<div class="row marj-row">';
                    foreach($result_merchant['result'] as $key=> $val)
                    {
                        //echo "<pre>"; print_r($result_merchant);  exit;
                        $ProjectTable = $this->getServiceLocator()->get('ProjectTable');
                        $result_project = $ProjectTable->selectProjectName($val['name']);
                        //echo $result_project->project_id; exit;
                        //$result_project['project_id'];  

                        $ImageUploadTable = $this->getServiceLocator()->get('ImageUploadTable');
                        $result_imageupload = $ImageUploadTable->getUploadByProjectId(trim($result_project->project_id)); 
                        //echo "<pre>"; print_r($result_imageupload); exit;
                        //$path = "/zend/public/images/advertiserimage/22943_crm-strategy.png";

                        // get total projects of a merchant...
                        $total_projects = $this->getServiceLocator()->get('ProjectTable')->getTotalProjectByMerchant($val['name']);
                        //$result_merchant->total_projects = $total_projects;

                        if($key%3==0){ $view.= '</div><div class="row marj-row">'; }
                        if($total_projects>=2)
                        {
                            $view.=  '<div class="col-sm-4">
                                <div class="user-box"> <img src="'.$aimg_path.$result_imageupload->filename.'" width="360" height="270" >
                                    <div class="dev-info">
                                        
                                        <img src="'.$aimg_path . str_replace('_pp_','_tn120x120_',$val['profilepic']).'"  class="pro-pic img-circle">
                                        <h3><a href="/user/frontportfolio/'.$val['id'].'/'.str_replace(' ', '-',$val['name']).'">'.$val['name'].'</a></h3>
                                        <Strong>'.$val['profession'].'</Strong>
                                        <p class="dipi"><span>Rate|'.$val['rate'].'$</span> &nbsp; <span>Projects|'.$total_projects.'</span> &nbsp; <span>Availability|'.$val['availability'].'/week</span></p>
                                    </div>
                                </div>
                            </div>';   
                        }             
                    }
                }
                else
                {
                    echo json_encode(array('response'=>'', 'total_count'=>0));
                    exit();
                }
                $view.= '</div>';
                echo json_encode(array('response'=>$view, 'total_count'=>$result_merchant['totalcount']));
                exit();
            }
        }
        else
        {
            $this->redirect()->toRoute('user/home', array('action' => 'dashboard'));
        }
    }

    public function RegisterAction()
	{
        $vcode = $this->randomNumber(20);
        
        $post = array();
        $post = $this->request->getPost();
        $post['user_type'] = 0;
		$post['fb_id'] = '';
        $post['verification_code'] = $vcode;
        $post['registration_date'] = date("Y-m-d");
        $post['status'] = '0'; 

        
        $UserTable = $this->getServiceLocator()->get('usertable');
        $email_exists = $UserTable->getUserByEmail($post['email']);
        if(!$email_exists){
            
            $user_id = $this->createUser($post);
            //$this->insertMerchant($post);
  
            $this->sendEmail(trim($post['email']),'Welcome to OwnRock!!',$vcode);
        
            $response = 'success';
            
            //$user_session = new Container('user');
            //$user_session->email = trim($post['email']);
            
            //return $this->redirect()->toRoute('user/home', array('action' => 'dashboard'));
        }else{
            $response = 'failed';
            //return $this->redirect()->toRoute('user/home',array('error_id'=>'55600'));
        }
        echo json_encode(array('response'=>$response));
        die();
    
    }
    
    protected function createUser($data) 
    {
        $user = new User();
        $user->exchangeArray($data);
        if($data['password']!=''){
			$user->setPassword($data['password']);
		}
        $userTable = $this->getServiceLocator()->get('UserTable');
        $user_id = $userTable->saveUser($user);
        return $user_id ;
    }
    protected function getUser($email) 
    {
        $userTable = $this->getServiceLocator()->get('UserTable');
        $userdata = $userTable->getUserByEmail($email);
        return $userdata;
    }

    protected function insertMerchant($data) 
    {
        $merchantData['name'] = $data['name'];
        $merchantData['email'] = $data['email'];
        $merchant = new Merchant();
        $merchant->exchangeArray($merchantData);
        $merchantTable = $this->getServiceLocator()->get('MerchantTable');
        $merchantTable->insertMerchant($merchant);
    }
    
    public function dashboardAction() {
        $this->layout('layout/dashboard-layout');
        $user_session = new Container('user');
        $total_projects = 0;
        $error = "";
        //echo '<pre>'; print_r($user_session->email);exit;
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/home');
        }
        
        $result_merchant = $this->getServiceLocator()->get('MerchantTable')->getMerchantByEmail($user_session->email);
        //echo '<pre>'; print_r($result_merchant);exit;
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $request = $this->getRequest();
            $merchant = new Merchant();
        
               $arr = $request->getPost($arr);
               if($arr['name']!="" && $arr['email']!="" && $arr['profession']!="" && $arr['phone']!="")
               {
                    $arr['profile_progress']=1;
               }
               else
               {
                    $arr['profile_progress']=0;
               }
               if($arr['name']=="" || $arr['email']=="" || $arr['profession']=="" || $arr['longdescription'] =="" ||  $arr['phone']==""|| $arr['country']=="" || $arr['city']==""|| $arr['rate']==""|| $arr['availability']=="")
               {
                    $error="Please fill all the required fields";
               }

               $arr['smalldescription'] = 'please update small description';        
               $arr['profilepic'] = $result_merchant->profilepic;
               //echo '<pre>';print_r($arr); exit;
               
               $merchant->exchangeArray($arr);
               
               $this->getServiceLocator()->get('MerchantTable')->saveMerchant($merchant);

               // get all the project by merchant name from project table
               //$total_projects = $this->getServiceLocator()->get('ProjectTable')->getTotalProjectByMerchant($arr['name']);

               return $this->redirect()->toRoute('user/home', array('action' => 'dashboard'));
            }
        
        $total_projects = $this->getServiceLocator()->get('ProjectTable')->getTotalProjectByMerchant($result_merchant->name);
        
		if($total_projects){
			$result_merchant->total_projects = $total_projects; }
		if($error){
			$result_merchant->error = $error;
		}

        $this->layout()->merchantinfo = $result_merchant;
        return new ViewModel(array('result_merchant'=>$result_merchant));
    }
    
    public function editprofileimageAction() {
        $user_session = new Container('user');       
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/home');
        }       
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $config = $this->getServiceLocator()->get('config');
            $uploadPath = $config['module_config']['upload_location'];

            $uploadFile = $this->params()->fromFiles('profilepic');
            
            //print_r($_FILES);
            //print_r($uploadFile); die;

            $adapter = new \Zend\File\Transfer\Adapter\Http();
            $adapter->setDestination($uploadPath);
            $adapter->receive($uploadFile['name']);
            
            $rand = rand(2000,100000);
            $old = $uploadPath.'/'.$uploadFile['name'];
            $new = $uploadPath.'/'.$rand.'_'.$uploadFile['name'];
            rename($old, $new);
            
            $sourceImageFileName = $uploadPath . '/' . $rand.'_'.$uploadFile['name'];
            $thumbnailFileName = $rand.'_'.'tn120x120_'.$uploadFile['name'];
            $imageThumb = $this->getServiceLocator()->get('WebinoImageThumb');
            $thumb = $imageThumb->create($sourceImageFileName, $options = array());
            $thumb->resize(120, 120);
            $thumb->save($uploadPath . '/' . $thumbnailFileName);
            
            $profilepic = $rand . '_pp_' .$uploadFile['name'];
            $this->getServiceLocator()->get('MerchantTable')->updateProfileImage($user_session->email,$profilepic);
            return $this->redirect()->toRoute('user/home', array('action' => 'dashboard'));
            
        }
        
    }

    
    public function addnewprojectAction() {
        $user_session = new Container('user');
        $total_projects = 0;

        $request = $this->getRequest();
        if ($request->isPost()) 
        {
            $project = new Project();
            $project->exchangeArray($request->getPost());
            $last_id = $this->getServiceLocator()->get('ProjectTable')->saveProject($project);      
            
            foreach($request->getPost()->e2 as $skldata)
            {
                $this->getServiceLocator()->get('ProjectskillTable')->saveProjectskillFront($last_id,$project->project_shortcut,$skldata);
            }
          
            $config = $this->getServiceLocator()->get('config');
            $uploadPath = $config['module_config']['upload_location'];
            $adapter = new \Zend\File\Transfer\Adapter\Http();
            
            $upload = new ImageUpload();
            
            $a = $request->getFiles()->toArray();
            
            $cnt = 0;
            for($k=1;$k<6;$k++){
                if(isset($a['fileupload'.$k]['name']) && $a['fileupload'.$k]['name'] != ""){
                    $cnt++;
                }
            }

            for($i=1;$i<$cnt+1;$i++)
            {
                $rand = rand(2000,100000);

                $uploadFile = $a['fileupload'.$i]['name'];
                
                $adapter->setDestination($uploadPath);  
                $adapter->receive($uploadFile);

                $image_data['filename'] = $rand.'_'.$uploadFile;
                $image_data['thumbnail'] = $rand.'_'.'tn_'.$uploadFile;
                $image_data['label'] = "Project Pic$i";
                $image_data['project_id'] = $last_id;

                $upload->exchangeArray($image_data);               
                $uploadTable = $this->getServiceLocator()->get('ImageUploadTable');
                $uploadTable->saveUploads($upload);

                rename($uploadPath.'/'.$uploadFile,$uploadPath.'/'.$rand.'_'.$uploadFile);

                $sourceImageFileName = $uploadPath . '/' . $rand.'_'.$uploadFile;
                $thumbnailFileName = $rand.'_'.'tn_'.$uploadFile;
                $imageThumb = $this->getServiceLocator()->get('WebinoImageThumb');
                $thumb = $imageThumb->create($sourceImageFileName, $options = array());
                $thumb->resize(50, 50);
                $thumb->save($uploadPath . '/' . $thumbnailFileName);
                //print_r($uploadPath);die;
            }
            
            
            return $this->redirect()->toRoute('user/home', array('action' => 'viewproject'));
        }

        $this->layout('layout/dashboard-layout');
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/home');
        }
        $MerchantTable = $this->getServiceLocator()->get('MerchantTable');
        $result_merchant = $MerchantTable->getMerchantByEmail($user_session->email); 

        $total_projects = $this->getServiceLocator()->get('ProjectTable')->getTotalProjectByMerchant($user_session->first_name);
        if($total_projects){
		$result_merchant->total_projects = $total_projects;      
        }
        $SkillsTable = $this->getServiceLocator()->get('SkillsTable');
        $result_skills = $SkillsTable->getAllSkills();
        
        $this->layout()->merchantinfo = $result_merchant;
        return new ViewModel(array('result_merchant'=>$result_merchant,'result_skills'=>$result_skills));
    }
    
    public function editprojectAction(){
        $request = $this->getRequest();
        //echo '<pre>'; print_r($request->getPost()); die;
		
		if ($request->isPost()) {
            $project = new Project();
            $project->exchangeArray($request->getPost());
            $this->getServiceLocator()->get('ProjectTable')->saveProject($project);      
            
            $project_id = $request->getPost()->project_id;
            
            $this->getServiceLocator()->get('ProjectskillTable')->deleteProjectSkillAll($project_id);
            foreach($request->getPost()->e2 as $skldata){
                    $this->getServiceLocator()->get('ProjectskillTable')->saveProjectskillFront($project_id,$request->getPost()->project_shortcut,$skldata);
            }
            
            $config = $this->getServiceLocator()->get('config');
            $uploadPath = $config['module_config']['upload_location'];
            $adapter = new \Zend\File\Transfer\Adapter\Http();
            
            $upload = new ImageUpload();
                
            $a = $request->getFiles()->toArray();
            //echo '<pre>';print_r($a);exit;
            $cnt = 0;
            for($k=1;$k<6;$k++){
                if(isset($a['fileupload'.$k]['name']) && $a['fileupload'.$k]['name'] != ""){
                    $cnt++;
                }
            }

            //for($i=1;$i<$cnt+1;$i++)
            for($k=1;$k<6;$k++)
            {
                if(isset($a['fileupload'.$k]['name']) && $a['fileupload'.$k]['name'] != ""){
                    $rand = rand(2000,100000);
                    //echo '<pre>';print_r($_FILES);
                    //echo $a['fileupload'.$i]['name'];

                    $uploadFile = $a['fileupload'.$k]['name'];
                    if($uploadFile!="")
                    {
                    
                        $adapter->setDestination($uploadPath);  
                        $adapter->receive($uploadFile);

                        $image_data['filename'] = $rand.'_'.$uploadFile;
                        $image_data['thumbnail'] = $rand.'_'.'tn_'.$uploadFile;
                        $image_data['label'] = "Project Pic$i";
                        $image_data['project_id'] = $project_id;

                        $upload->exchangeArray($image_data);               
                        $uploadTable = $this->getServiceLocator()->get('ImageUploadTable');
                        $uploadTable->saveUploads($upload);

                        rename($uploadPath.'/'.$uploadFile,$uploadPath.'/'.$rand.'_'.$uploadFile);

                        $sourceImageFileName = $uploadPath . '/' . $rand.'_'.$uploadFile;
                        $thumbnailFileName = $rand.'_'.'tn_'.$uploadFile;
                        $imageThumb = $this->getServiceLocator()->get('WebinoImageThumb');
                        $thumb = $imageThumb->create($sourceImageFileName, $options = array());
                        $thumb->resize(50, 50);
                        $thumb->save($uploadPath . '/' . $thumbnailFileName);
                    }
                }
                
            }
            return $this->redirect()->toRoute('user/home', array('action' => 'viewproject'));
            
            }else{
        
            $this->layout('layout/dashboard-layout');
            $user_session = new Container('user');
            if($user_session->email == ""){
                return $this->redirect()->toRoute('user/home');
            }
            $MerchantTable = $this->getServiceLocator()->get('MerchantTable');
            $result_merchant = $MerchantTable->getMerchantByEmail($user_session->email);

            $total_projects = $this->getServiceLocator()->get('ProjectTable')->getTotalProjectByMerchant($user_session->first_name);
            $result_merchant->total_projects = $total_projects;       

            $ProjectTable = $this->getServiceLocator()->get('ProjectTable');
            $result_project = $ProjectTable->getProject($this->params()->fromRoute('param1'));

            $ProjectskillTable = $this->getServiceLocator()->get('ProjectskillTable');
            $result_projectskill = $ProjectskillTable->selectProjectskill($this->params()->fromRoute('param1'));

            $SkillTable = $this->getServiceLocator()->get('SkillsTable');
            foreach($result_projectskill as $row1){
                //$SkillTable = $this->getServiceLocator()->get('SkillsTable');
                $skillres[] = $SkillTable->getSkillById($row1->skill,'ZEND_ONE');
            }

            $result_skills = $SkillTable->getAllSkills();
            
            $ImageUploadTable = $this->getServiceLocator()->get('ImageUploadTable');
            $parr[] = $this->params()->fromRoute('param1');
            $Image_Details = $ImageUploadTable->allUploadByProject($parr);
           
			//echo '<pre>';
			//print_r($skillres); 			
			//print_r($result_skills); 
			//die;
            $this->layout()->merchantinfo = $result_merchant;
            return new ViewModel(array('result_merchant'=>$result_merchant,'result_project'=>$result_project,'result_skill'=>$skillres,'image_result'=>$Image_Details,'result_skills'=>$result_skills));
        }
    }

    public function projectdetailAction() 
    { 
   
        $project_id = $this->params()->fromRoute('param1');
        $user_name = $this->params()->fromRoute('param2');

        $this->layout('layout/projectdetail-layout');
        $user_session = new Container('user');
        
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/home');
        }

      
        //echo '<pre>'; print_r($_GET);exit;

        $MerchantTable = $this->getServiceLocator()->get('MerchantTable');
        $result_merchant = $MerchantTable->getMerchantByEmail($user_session->email);
        //echo '<pre>'; print_r($result_merchant); exit;
        
        $ProjectTable = $this->getServiceLocator()->get('ProjectTable');
        $result_project = $ProjectTable->getProjectById($project_id);
        //echo '<pre>'; print_r($result_project);exit;

        $ProjectskillTable = $this->getServiceLocator()->get('ProjectskillTable');
        $result_projectskill = $ProjectskillTable->selectProjectskill($project_id);
        //echo '<pre>'; print_r($result_projectskill);exit;

        $SkillTable = $this->getServiceLocator()->get('SkillsTable');
        foreach($result_projectskill as $row1){
            //echo '<pre>'; print_r($row1->skill);exit;
            //$SkillTable = $this->getServiceLocator()->get('SkillsTable');
            $skillres[] = $SkillTable->getSkillById($row1->skill,'ZEND_ONE');
        }
        //echo '<pre>'; print_r($skillres);

        $result_skills = $SkillTable->getAllSkills();
        //echo '<pre>'; print_r($result_skills);exit;

        $ImageUploadTable = $this->getServiceLocator()->get('ImageUploadTable');
        $project_images = $ImageUploadTable->allUploadByProjectId($project_id);
        //echo '<pre>'; print_r($project_images);exit;

        $this->layout()->result_merchant = $result_merchant;
        $this->layout()->result_project = $result_project;
        $this->layout()->result_skill = $skillres;
        $this->layout()->result_skills = $result_skills;
        $this->layout()->project_images = $project_images;
        
       return new ViewModel(array('result_merchant'=>$result_merchant,'result_project'=>$result_project,'result_skill'=>$skillres,'result_skills'=>$result_skills,'project_images'=>$project_images));
    }
    
    public function deleteprojectAction(){
        
        $this->layout('layout/dashboard-layout');
        $user_session = new Container('user');
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/home');
        }
                
        $project_id = $this->params()->fromRoute('param1'); 
        $this->getServiceLocator()->get('ProjectskillTable')->deleteProjectSkillAll($project_id);
        $this->getServiceLocator()->get('ImageUploadTable')->deleteUploadsByProjectId($project_id);
        $this->getServiceLocator()->get('ProjectTable')->deleteProject($project_id);
        return $this->redirect()->toRoute('user/home', array('action' => 'viewproject'));
    } 
    
    public function viewprojectAction() {
        $this->layout('layout/dashboard-layout');
        $user_session = new Container('user');
        
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/home');
        }
        
        $MerchantTable = $this->getServiceLocator()->get('MerchantTable');
        $result_merchant = $MerchantTable->getMerchantByEmail($user_session->email);

        $total_projects = $this->getServiceLocator()->get('ProjectTable')->getTotalProjectByMerchant($user_session->first_name);
        if($total_projects){
			$result_merchant->total_projects = $total_projects;    
        }
        $ProejctTable = $this->getServiceLocator()->get('ProjectTable');
        $project = $ProejctTable->getProjectByMerchant($result_merchant->name,'project_order');
        
        $parr=array();
        foreach($project as $pval):
            $parr[] = $pval->project_id;
        endforeach;
        
        $ImageUploadTable = $this->getServiceLocator()->get('ImageUploadTable');
        $Image_Details = $ImageUploadTable->allUploadByProject($parr);
        
        $this->layout()->merchantinfo = $result_merchant;
        return new ViewModel(array('result_merchant'=>$result_merchant,'project_result'=>$project,'image_result'=>$Image_Details,'total_projects'=>count($parr)));
    }
    
    public function addprojectimageAction(){
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $project_id = $request->getPost()->project_id;
            
            $config = $this->getServiceLocator()->get('config');
            $uploadPath = $config['module_config']['upload_location'];
            $adapter = new \Zend\File\Transfer\Adapter\Http();
            
            $upload = new ImageUpload();
                
            $cnt = 0;
            foreach($request->getFiles()->toArray() as $selectedFile){
                if($selectedFile['name'] != ""){
                    $cnt++;
                }
            }    
                
            for($i=1;$i<$cnt+1;$i++){

                $rand = rand(2000,100000);

                $uploadFile = $this->params()->fromFiles("projectpic$i");

                $adapter->setDestination($uploadPath);  
                $adapter->receive($uploadFile['name']);

                $image_data['filename'] = $rand.'_'.$uploadFile['name'];
                $image_data['thumbnail'] = $rand.'_'.'tn_'.$uploadFile['name'];
                $image_data['label'] = "Project Pic$i";
                $image_data['project_id'] = $project_id;

                $upload->exchangeArray($image_data);               
                $uploadTable = $this->getServiceLocator()->get('ImageUploadTable');
                $uploadTable->saveUploads($upload);

                rename($uploadPath.'/'.$uploadFile['name'],$uploadPath.'/'.$rand.'_'.$uploadFile['name']);

                $sourceImageFileName = $uploadPath . '/' . $rand.'_'.$uploadFile['name'];
                $thumbnailFileName = $rand.'_'.'tn_'.$uploadFile['name'];
                $imageThumb = $this->getServiceLocator()->get('WebinoImageThumb');
                $thumb = $imageThumb->create($sourceImageFileName, $options = array());
                $thumb->resize(50, 50);
                $thumb->save($uploadPath . '/' . $thumbnailFileName);

            }
            
            return $this->redirect()->toRoute('user/home', array('action' => 'viewproject'));
            
        }else{
        
            $this->layout('layout/dashboard-layout');
            $user_session = new Container('user');
            if($user_session->email == ""){
                return $this->redirect()->toRoute('user/home');
            }
            $MerchantTable = $this->getServiceLocator()->get('MerchantTable');
            $result_merchant = $MerchantTable->getMerchantByEmail($user_session->email);       

            $ProjectTable = $this->getServiceLocator()->get('ProjectTable');
            $result_project = $ProjectTable->getProject($this->params()->fromRoute('param1'));

            $this->layout()->merchantinfo = $result_merchant;
            return new ViewModel(array('result_merchant'=>$result_merchant,'result_project'=>$result_project));
        }
    }
    
    public function updateprojectorderAction() {
        $user_session = new Container('user');       
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/home');
        }
        $request = $this->getRequest();
        $ProejctTable = $this->getServiceLocator()->get('ProjectTable');
        
        $req_data = $request->getPost();
        
        $i = 1;
        foreach($req_data->project as $projectid){
            $ProejctTable->updateProjectOrder($projectid,$i,$req_data->mid);
            $i++;
        }
        
        return $this->redirect()->toRoute('user/home', array('action' => 'viewproject'));
    }
    
    public function enquiryAction() {
        $this->layout('layout/dashboard-layout');
        $user_session = new Container('user');
        
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/home');
        }

        $MerchantTable = $this->getServiceLocator()->get('MerchantTable');
        $result_merchant = $MerchantTable->getMerchantByEmail($user_session->email);
        
        $total_projects = $this->getServiceLocator()->get('ProjectTable')->getTotalProjectByMerchant($user_session->first_name);
        if($total_projects){
			$result_merchant->total_projects = $total_projects;
        }
        $EnquiryTable = $this->getServiceLocator()->get('EnquiryTable');
        $paginator = $EnquiryTable->fetchAll(true,$user_session->id);
        $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
        $paginator->setItemCountPerPage(2);

        $this->layout()->merchantinfo = $result_merchant;
        return new ViewModel(array('result_merchant'=>$result_merchant,'paginator' => $paginator));
    }
    
    public function googleloginAction() {
         
        require_once 'Plugin/trunk/src/Google_Client.php';
        //require_once 'Plugin/trunk/src/contrib/Google_Oauth2Service.php';
        require_once 'Plugin/trunk/src/contrib/Google_PlusService.php';
        $user_session = new Container('user');
        
        $client = new \Google_Client();
        $client->setApplicationName('Google+ server-side flow');
        $client->setClientId('94792287589-1alpa58iju7ol8qhamgj3fnupg5u8s1l.apps.googleusercontent.com');
        $client->setClientSecret('4f0Qi3MK8_kKneM0c1P7LQ7N');
        $client->setRedirectUri('http://ownrock.com/user/home/googlelogin');
        $client->setDeveloperKey('AIzaSyA2gkZh34yULNSDbjn087KpEbGWXa5cwxE');
        $plus = new \Google_PlusService($client);
        //$oauth2 = new \Google_Oauth2Service($client);
        
        $me = $plus->people->get('107194039893970907338');
        session_start();
        if (isset($_GET['code'])) {
            $client->authenticate($_GET['code']);
            $at = $client->getAccessToken();
            $_SESSION['token'] = $at;
        }
        if (isset($_SESSION['token'])) {
          $client->setAccessToken($_SESSION['token']);
        }
        
        $access_details = json_decode($at,TRUE);       
        $atkn = $access_details['access_token'];
        if ($client->getAccessToken()) {
  
            $google_data_temp = file_get_contents('https://www.googleapis.com/userinfo/v2/me/?access_token='.$atkn);
            $google_data = json_decode($google_data_temp,true);
            $name = $google_data['name'];
            $googleid = $google_data['id'];
            $email = $google_data['email'];
            
            $UserTable = $this->getServiceLocator()->get('usertable');
            $email_exists = $UserTable->getUserByEmail($email);
            //initialize session
            
            $controller = "home";
            if(!$email_exists){
                $create_user_array = array(
                    'name'=>$name,
                    'email'=>$email,
                    'password'=>$this->randomNumber(20),
                    'user_type'=>0,
					'fb_id'=>'',
                    'registration_date'=>date("Y-m-d"),
                    'verification_code'=>$this->randomNumber(20),
                    'status' => '1' 
                );
                $last_id = $this->createUser($create_user_array);
                $user_session->user_id = $last_id;
                $user_session->email = $email;
                $user_session->firstname = $name;

                $_SESSION['user']->user_id = $last_id;
                $_SESSION['user']->first_name = $name;
                $_SESSION['user']->email = $email;
                $action ='selectprofile';
                //return $this->redirect()->toRoute('user/home', array('action' => 'selectprofile'));
            }
            else
            {   
                $action ='dashboard';
                $user_session->user_id = $email_exists->id;
                $user_session->firstname = $email_exists->name;
                $user_session->email = $email_exists->email;
                $user_session->user_type = $email_exists->user_type;

                $_SESSION['user']->user_id = $email_exists->id;
                $_SESSION['user']->first_name = $email_exists->name;
                $_SESSION['user']->email = $email_exists->email;
                $_SESSION['user']->user_type = $email_exists->user_type;
                if($user_session->user_type == 1)
                {
                    $controller = "home";
                }
                if($user_session->user_type == 2)
                {
                    $controller = "photographer";
                }



            }
            
            return $this->redirect()->toRoute('user/'.$controller, array('action' => $action));
            $_SESSION['token'] = $client->getAccessToken();
        } else {
          //$authUrl = $client->createAuthUrl();
        }
        //return $this->redirect()->toRoute('user/home', array('action' => 'selectprofile'));
    }
    
    public function facebookloginAction() 
	{
      
        $post = array();
        $post = $this->request->getPost();
        $post['fb_id'] = $this->request->getPost('fb_id');;
        $post['email'] = $this->request->getPost('fb_email');
        $post['name'] = $this->request->getPost('fb_name');
		$post['user_type'] = 0;      
		$post['password'] = '';   
		$post['verification_code'] = '';   
        $post['registration_date'] = date("Y-m-d");
		$post['status'] = '1';	
		
		if($post['email']==''){
			$response = 'success';		
			echo json_encode(array('response'=>"Email id is not accessible",'status'=>'Email id is not accessible','signup'=>'Invalid'));			
		}
		
		$UserTable = $this->getServiceLocator()->get('usertable');      
		
		$email_exists = $UserTable->getUserByEmail($post['email']);
     		//echo "<pre>"; print_r($email_exists); exit;
		if(!$email_exists)
        {		
            $last_id = $this->createUser($post);
			//$this->insertMerchant($post);
			
			$auth = $this->getAuthService();			
			$storage = $auth->getStorage();
			$storage->write($post['email']);
			
			// Store username in session
			$user_session = new Container('user');		
			
			//$user_session->id = $post['email'];
            $user_session->user_id = $last_id;
			$user_session->first_name = $post['name'];
			$user_session->email = $post['email'];
			
			//$_SESSION['user']->id = $email_exists->id;
			$_SESSION['user']->first_name = $post['name'];
			$_SESSION['user']->email = $post['email'];
            $_SESSION['user']->id = $last_id;

			$response = 'success';		
			echo json_encode(array('response'=>$response,'status'=>1,'signup'=>1));
			die();
		}
        else
        {
			$auth = $this->getAuthService();			
			$storage = $auth->getStorage();
			$storage->write($post['email']);
			
			// Store username in session
			$user_session = new Container('user');		
			
			$user_session->user_id = $email_exists->id;
			$user_session->first_name = $email_exists->name;
			$user_session->email = $email_exists->email;		
			
			$_SESSION['user']->id = $email_exists->id;
			$_SESSION['user']->first_name = $email_exists->name;
			$_SESSION['user']->email = $email_exists->email;		
			
			$response = 'success';		
			echo json_encode(array('response'=>$response,'status'=>$email_exists->status,'signup'=>0));
			die();
		}
        
    }
    
    public function logoutAction()
    {
        //echo $baseURL = $this->getRequest()->getHttpHost();exit;
        $user_session = new Container('user');
        $user_session->getManager()->getStorage()->clear();
        if ($user_session->access_token != "") {
            //$user_session->access_token = "";
            $user_session->getManager()->getStorage()->clear();
            
            require_once 'Plugin/trunk/src/Google_Client.php';
            $client = new \Google_Client();
            
            $client->revokeToken();
        }
        
        $user_session->email = '';
        
        return $this->redirect()->toRoute('user/home');
    }
    
    protected function randomNumber($length=8) {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }
    
    /*protected function sendEmail($to,$subject,$option1=''){
        
        $viewRender = $this->getServiceLocator()->get('ViewRenderer');

        $layout = new ViewModel();
        $layout->setTemplate("layout/mailtemplate/register.phtml");
        $layout->setVariable("content", $viewRender->render($layout));

        $layout->setVariable("vericode",$option1); 
        
        $html = $viewRender->render($layout);
        
        require_once 'Plugin/PHPMailerMain/src/class.phpmailer.php';
        $mail = new \PHPMailer();
        $mail->IsSMTP();                                      // set mailer to use SMTP
        $mail->Host = "ownrock.com";  // specify main and backup server
        $mail->SMTPAuth = true;     // turn on SMTP authentication
        $mail->Username = "info@ownrock.com";  // SMTP username
        $mail->Password = "info@123"; // SMTP password
        $mail->From = "info@ownrock.com";
        $mail->FromName = "Ownrock";
        $mail->AddAddress($to, "Tahir Khan");
        //$mail->AddAddress("nikitaeck@gmail.com");                  // name is optional
        //$mail->AddReplyTo("info@example.com", "Information");
        $mail->WordWrap = 50;                                 // set word wrap to 50 characters
        //$mail->AddAttachment("/var/tmp/file.tar.gz");         // add attachments
        //$mail->AddAttachment("/tmp/image.jpg", "new.jpg");    // optional name
        $mail->IsHTML(true);                                  // set email format to HTML
        $mail->Subject = $subject;
        $mail->Body    = $html;
        $mail->AltBody = "This is the body in plain text for non-HTML mail clients";
        if(!$mail->Send())
        {
           echo "Message could not be sent. <p>";
           echo "Mailer Error: " . $mail->ErrorInfo;
           $res = 'ERROR_SENDING_MAIL';
        }else{
           $res = 'SUCCESS_SENDING_MAIL';
        }
        return $res;
    }
*/

	 protected function sendForgotPasswordEmail($to,$subject,$option1='', $name){
        
        $viewRender = $this->getServiceLocator()->get('ViewRenderer');

        $layout = new ViewModel();
        $layout->setTemplate("layout/mailtemplate/forgotpassword.phtml");
        $layout->setVariable("content", $viewRender->render($viewModel));

        $layout->setVariable("vericode",$option1); 
		$layout->setVariable("name",$name); 
		$layout->setVariable("email",$to); 
        
        $html = $viewRender->render($layout);
        
        require_once 'Plugin/PHPMailerMain/src/class.phpmailer.php';
        $mail = new \PHPMailer();
        $mail->IsSMTP();                                      // set mailer to use SMTP
        $mail->Host = "ownrock.com";  // specify main and backup server
        $mail->SMTPAuth = true;     // turn on SMTP authentication
        $mail->Username = "info@ownrock.com";  // SMTP username
        $mail->Password = "info@123"; // SMTP password
        $mail->From = "info@ownrock.com";
        $mail->FromName = "Ownrock";
        $mail->AddAddress($to, "Tahir Khan");
        //$mail->AddAddress("nikitaeck@gmail.com");                  // name is optional
        //$mail->AddReplyTo("info@example.com", "Information");
        $mail->WordWrap = 50;                                 // set word wrap to 50 characters
        //$mail->AddAttachment("/var/tmp/file.tar.gz");         // add attachments
        //$mail->AddAttachment("/tmp/image.jpg", "new.jpg");    // optional name
        $mail->IsHTML(true);                                  // set email format to HTML
        $mail->Subject = $subject;
        $mail->Body    = $html;
        $mail->AltBody = "This is the body in plain text for non-HTML mail clients";
        if(!$mail->Send())
        {
           echo "Message could not be sent. <p>";
           echo "Mailer Error: " . $mail->ErrorInfo;
           $res = 'ERROR_SENDING_MAIL';
        }else{
           $res = 'SUCCESS_SENDING_MAIL';
        }
        return $res;
    }
    
    public function verifyaccountAction(){
        
       
        $user_session = new Container('user');
        $vericode = $this->params()->fromRoute('param2');
        
        $UserTable = $this->getServiceLocator()->get('usertable');
        $veriresponse = $UserTable->verifyUser($vericode);
        
        $temp = explode('|',$veriresponse);
        $user_session->email = $temp['1'];
        $user_session->first_name = $temp['2'];
        $user_session->user_id = $temp['3'];
        
        //return $this->redirect()->toRoute('user/home', array('action' => 'dashboard'));
        return $this->redirect()->toRoute('user/home', array('action' => 'selectprofile'));
        //return $this->redirect()->toRoute('user/home');
        
    }

    public function deleteprojectimgAction()
    {
        $user_session = new Container('user');
        if($user_session->email == ""){
            return $this->redirect()->toRoute('user/home');
        }
        $id = $this->request->getPost('id');
        //$id = $request->getPost()->id;
        $this->getServiceLocator()->get('ImageUploadTable')->deleteUploads($id);
        $response = 'success';       
        echo json_encode(array('response'=>$response,'status'=>$email_exists->status));
        die();

    }
    public function selectProfileAction()
    {
        $this->layout('layout/selectprofile-layout');
        return new ViewModel(array());  
    }

    public function saveProfileAction()
    {
        $user_session = new Container('user');
        $post = $this->request->getPost(); 
        $usertype =  $post['type'];
        $post['email'] = $user_session->email;
        $post['name'] = $user_session->first_name;
        
        
        $UserTable = $this->getServiceLocator()->get('usertable');
        $updateresponse = $UserTable->updateUserType($post['email'],$usertype);
        if($usertype==1)
        {
            $this->insertMerchant($post);
        }
        $user_session->usertype = $updateresponse;
        $response = 'success';
        echo json_encode(array('response'=>$response));
        die();
    }
}