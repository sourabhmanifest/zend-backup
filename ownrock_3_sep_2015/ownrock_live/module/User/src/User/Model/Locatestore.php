<?php

namespace User\Model;

class Locatestore{

    public $id;
    public $campaign_id;
    public $name;
    public $address;
    public $lat;
    public $lng;
    public $type;

   

    public function exchangeArray($data) {
        $this->id = (isset($data['id'])) ? $data['id'] : null;
        $this->campaign_id = (isset($data['campaign_id'])) ? $data['campaign_id'] : null;
        $this->name = (isset($data['name'])) ? $data['name'] : null;
        $this->address = (isset($data['address'])) ? $data['address'] : null;
        $this->lat = (isset($data['lat'])) ? $data['lat'] : null;
        $this->lng = (isset($data['lng'])) ? $data['lng'] : null;
        $this->type = (isset($data['type'])) ? $data['type'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}
