<?php
// filename : module/Users/src/Users/Form/RegisterForm.php
namespace User\Form;

use Zend\Form\Form;


class ProjectskilleditForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('Projectskilledit');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype','multipart/form-data');
        
        $this->add(array(
            'name' => 'project_id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'project_shortcut',
            'attributes' => array(
                'type'  => 'text',
                'class' => 'form-control',
                'readonly' => 'readonly'
            ),
            'options' => array(
                'label' => '',
            ),
        )); 
          
        $this->add(array(
            'name' => 'skill',
            'type'  => 'Zend\Form\Element\MultiCheckbox',
           
            'options' => array(
//                'label' => 'Skill',
                'value_options' => array(
                    '19' => 'Web Developer',
                    '23' => 'Web Designer',
                    '3' => 'CSS',
                    '4' => 'HTML',
                    '5' => 'Photoshop',
                    '10' => 'PHP',
                    '1' => 'Other',
                    
                ),
                ),
            'attributes' => array(
                'value' => 19,
            )   
        )); 
             
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Upload Now'
            ),
        )); 
        
    }
    public function populateValues($data)
    {   
        foreach($data as $key=>$row)
        {
         
           if (is_array(@json_decode($row))){
           $data[$key] =   new \ArrayObject(\Zend\Json\Json::decode($row), \ArrayObject::ARRAY_AS_PROPS);
                      //  $data[$key] = \Zend\Json\Json::encode($row);
            }       
        } 
        parent::populateValues($data);
    }
}