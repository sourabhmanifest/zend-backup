<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace User;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use User\Model\User;
use User\Model\UserTable;
use User\Model\Campaigns;
use User\Model\CampaignsTable;
use User\Model\Onlinecoupon;
use User\Model\OnlinecouponTable;
use User\Model\Categorylist;
use User\Model\CategorylistTable;
use User\Model\Merchant;
use User\Model\MerchantTable;
use User\Model\Locatestore;
use User\Model\LocatestoreTable;
use User\Model\Project;
use User\Model\ProjectTable;
use User\Model\Profileskill;
use User\Model\ProfileskillTable;
use User\Model\Projectskill;
use User\Model\EndorsementsTable;
use User\Model\Endorsements;
use User\Model\ProjectskillTable;
use User\Model\Skills;
use User\Model\SkillsTable;
use User\Model\Uploads;
use User\Model\UploadsTable;
use User\Model\ImageUpload;
use User\Model\ImageUploadTable;
use User\Model\Photographer;
use User\Model\PhotographerTable;
use User\Model\Enquiry;
use User\Model\EnquiryTable;
use User\Model\Tag;
use User\Model\TagTable;
use User\Model\AlbumTag;
use User\Model\AlbumTagTable;
use User\Model\Album;
use User\Model\AlbumTable;
use User\Model\AlbumImage;
use User\Model\AlbumImageTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;
use Zend\Session\Config\SessionConfig;
use Zend\Session\Container;
use Zend\Session\SessionManager;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\ModuleManager\ModuleManager;
use Zend\EventManager\StaticEventManager;
use Zend\Module\Consumer\AutoloaderProvider;

class Module implements AutoloaderProviderInterface, ConfigProviderInterface {

    public function onBootstrap(MvcEvent $e) {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $this->initSession(array(
            'remember_me_seconds' => 180,
            'use_cookies' => true,
            'cookie_httponly' => true,
        ));
        $e->getApplication()->getServiceManager()->get('translator');
        $e->getApplication()->getServiceManager()->get('viewhelpermanager')->setFactory('controllerName', function($sm) use ($e) {
            $viewHelper = new View\Helper\ControllerName($e->getRouteMatch());
            return $viewHelper;
        });

        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
//        $di = $e->getTarget()->getServiceManager();
//       
//        $auth = $di->get('User\Event\Authentication');      
//        $events = StaticEventManager::getInstance();
//        $events->attach('Zend\Mvc\Controller\ActionController', 'dispatch', array($auth, 'mvcPreDispatch') , 100);
//
// echo 'nikita';die;
//        $application = $e->getApplication();
//        $em = $application->getEventManager();
//        $em->attach('route', array($this, 'onRoute'), -100);
    }

    public function init(ModuleManager $moduleManager) {
        $sharedManager = $moduleManager->getEventManager()->getSharedManager();
        $sharedManager->attach('Zend\Mvc\Application', 'dispatch', array($this, 'mvcPreDispatch'), 100);
    }

    public function mvcPreDispatch($event) {
        $di = $event->getTarget()->getServiceManager();
        $auth = $di->get('User\Event\Authentication');
        return $auth->preDispatch($event);
    }

    public function initSession($config) {
        $sessionConfig = new SessionConfig();
        $sessionConfig->setOptions($config);
        $sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();
        Container::setDefaultManager($sessionManager);
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig() {
        return array(
            'factories' => array(
                'AuthService' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $dbTableAuthAdapter = new DbTableAuthAdapter($dbAdapter, 'user', 'email', 'password');
                    $authService = new AuthenticationService();
                    $authService->setAdapter($dbTableAuthAdapter);
                    return $authService;
                },
                'SkillsTable' => function($sm) {
                    $tableGateway = $sm->get('SkillsTableGateway');
                    $table = new SkillsTable($tableGateway);
                    return $table;
                },
                'SkillsTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Skills());
                    return new TableGateway('skills', $dbAdapter, null, $resultSetPrototype);
                }, 
                'TagTable' => function($sm) {
                    $tableGateway = $sm->get('TagTableGateway');
                    $table = new TagTable($tableGateway);
                    return $table;
                },
                'TagTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Tag());
                    return new TableGateway('tag', $dbAdapter, null, $resultSetPrototype);
                },           
                'UserTable' => function($sm) {
                    $tableGateway = $sm->get('UserTableGateway');
                    $table = new UserTable($tableGateway);
                    return $table;
                },
                'UserTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new User());
                    return new TableGateway('user', $dbAdapter, null, $resultSetPrototype);
                },
                'CampaignsTable' => function($sm) {
                    $tableGateway = $sm->get('CampaignsTableGateway');
                    $table = new CampaignsTable($tableGateway);
                    return $table;
                },
                'CampaignsTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Campaigns());
                    return new TableGateway('campaigns', $dbAdapter, null, $resultSetPrototype);
                },
                'MerchantTable' => function($sm) {
                    $tableGateway = $sm->get('MerchantTableGateway');
                    $table = new MerchantTable($tableGateway);
                    return $table;
                },
                'MerchantTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Merchant());
                    return new TableGateway('merchant', $dbAdapter, null, $resultSetPrototype);
                },
                'PhotographerTable' => function($sm) {
                    $tableGateway = $sm->get('PhotographerTableGateway');
                    $table = new PhotographerTable($tableGateway);
                    return $table;
                },
                'PhotographerTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Photographer());
                    return new TableGateway('photographer', $dbAdapter, null, $resultSetPrototype);
                },
                'ProjectTable' => function($sm) {
                    $tableGateway = $sm->get('ProjectTableGateway');
                    $table = new ProjectTable($tableGateway);
                    return $table;
                },
                'ProjectTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Project());
                    return new TableGateway('project', $dbAdapter, null, $resultSetPrototype);
                },
                'AlbumTable' => function($sm) {
                    $tableGateway = $sm->get('AlbumTableGateway');
                    $table = new AlbumTable($tableGateway);
                    return $table;
                },
                'AlbumTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Album());
                    return new TableGateway('album', $dbAdapter, null, $resultSetPrototype);
                },

                'AlbumImageTable' => function($sm) {
                    $tableGateway = $sm->get('AlbumImageTableGateway');
                    $table = new AlbumImageTable($tableGateway);
                    return $table;
                },
                'AlbumImageTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new AlbumImage());
                    return new TableGateway('albumimage', $dbAdapter, null, $resultSetPrototype);
                },

                'ProfileskillTable' => function($sm) {
                    $tableGateway = $sm->get('ProfileskillTableGateway');
                    $table = new ProfileskillTable($tableGateway);
                    return $table;
                },
                'ProfileskillTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Profileskill());
                    return new TableGateway('profileskill', $dbAdapter, null, $resultSetPrototype);
                },
                'ProjectskillTable' => function($sm) {
                    $tableGateway = $sm->get('ProjectskillTableGateway');
                    $table = new ProjectskillTable($tableGateway);
                    return $table;
                },
                'ProjectskillTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Projectskill());
                    return new TableGateway('projectskill', $dbAdapter, null, $resultSetPrototype);
                },
                'AlbumTagTable' => function($sm) {
                    $tableGateway = $sm->get('AlbumTagTableGateway');
                    $table = new AlbumTagTable($tableGateway);
                    return $table;
                },
                'AlbumTagTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\AlbumTag());
                    return new TableGateway('albumtag', $dbAdapter, null, $resultSetPrototype);
                },
                'OnlinecouponTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Onlinecoupon());
                    return new TableGateway('onlinecoupon', $dbAdapter, null, $resultSetPrototype);
                },
                'CategorylistTable' => function($sm) {
                    $tableGateway = $sm->get('CategorylistTableGateway');
                    $table = new CategorylistTable($tableGateway);
                    return $table;
                },
                'CategorylistTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Categorylist());
                    return new TableGateway('categorylist', $dbAdapter, null, $resultSetPrototype);
                },
                'dbAuthService' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    return $dbAdapter;
                },
                'LocatestoreTable' => function($sm) {
                    $tableGateway = $sm->get('LocatestoreTableGateway');
                    $table = new LocatestoreTable($tableGateway);
                    return $table;
                },
                'LocatestoreTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Locatestore());
                    return new TableGateway('locatestore', $dbAdapter, null, $resultSetPrototype);
                },
                'ImageUploadTable' => function($sm) {
                    $tableGateway = $sm->get('ImageUploadTableGateway');
                    $table = new ImageUploadTable($tableGateway);
                    return $table;
                },
                'ImageUploadTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ImageUpload());
                    return new TableGateway('imageupload', $dbAdapter, null, $resultSetPrototype);
                },
                'EnquiryTable' => function($sm) {
                    $tableGateway = $sm->get('EnquiryTableGateway');
                    $table = new EnquiryTable($tableGateway);
                    return $table;
                },
                'EnquiryTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Enquiry());
                    return new TableGateway('enquiry', $dbAdapter, null, $resultSetPrototype);
                },
                'UploadsTable' => function($sm) {
                    $tableGateway = $sm->get('UploadsTableGateway');
                    $table = new UploadsTable($tableGateway);
                    return $table;
                },
                'UploadsTableGateway' => function ($sm) {

                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');

                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Uploads());
                    return new TableGateway('uploads', $dbAdapter, null, $resultSetPrototype);
                },
                'EndorsementsTable' => function($sm) {
                    $tableGateway = $sm->get('EndorsementsTableGateway');
                    $table = new EndorsementsTable($tableGateway);
                    return $table;
                },
                'EndorsementsTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Endorsements());
                    return new TableGateway('endorsements', $dbAdapter, null, $resultSetPrototype);
                },
                'UploadForm' => function ($sm) {
                    $form = new Form\UploadForm();
                    return $form;
                },
                'SkillForm' => function ($sm) {
                    $form = new Form\SkillForm();
                    return $form;
                },
                'ProfileskillForm' => function ($sm) {
                    $form = new Form\ProfileskillForm();
                    return $form;
                },
                'UploadEditForm' => function ($sm) {
                    $form = new Form\UploadEditForm();
                    return $form;
                },
                'UserEditForm' => function ($sm) {
                    $form = new \User\Form\UsereditForm();
                    $form->setInputFilter($sm->get('UserEditFilter'));
                    return $form;
                },
                'UserEditFilter' => function ($sm) {
                    return new \User\Form\UserEditFilter();
                },
                'ProjectEditForm' => function ($sm) {
                    $form = new \User\Form\ProjecteditForm();
                    return $form;
                },
                'ProjectskilleditForm' => function ($sm) {
                    $form = new \User\Form\ProjectskilleditForm();
                    return $form;
                },
                'MerchantEditForm' => function ($sm) {
                    $form = new \User\Form\MerchanteditForm();
                    $form->setInputFilter($sm->get('MerchantEditFilter'));
                    return $form;
                },
                'MerchantEditFilter' => function ($sm) {
                    return new \User\Form\MerchantEditFilter();
                },
                'LocatestoreForm' => function ($sm) {
                    $form = new \User\Form\LocatestoreForm();
                    return $form;
                },
                 'EndorsementsForm ' => function ($sm) {
                    $form = new \User\Form\EndorsementsForm();
                    return $form;
                },
            ),
        );
    }

    public function getAutoloaderConfig() {

        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php'
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

}