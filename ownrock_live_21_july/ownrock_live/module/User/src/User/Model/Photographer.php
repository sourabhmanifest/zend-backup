<?php

namespace User\Model;

class Photographer{
    public $pg_id;
    public $user_id;
    public $occupation;
    public $location;
    public $description;
    public $profilepic;
    public $status;
    public $profile_progress;
    
    public function exchangeArray($data)
    {
        $this->pg_id  = (!empty($data['pg_id'])) ? $data['pg_id'] : null;
        $this->user_id  = (!empty($data['user_id'])) ? $data['user_id'] : null;
        $this->occupation = (!empty($data['occupation'])) ? $data['occupation'] : null;
        $this->location  = (!empty($data['location'])) ? $data['location'] : null;
        $this->description   = (!empty($data['description'])) ? $data['description'] : null;   
        $this->profilepic  = (!empty($data['profilepic'])) ? $data['profilepic'] : null;
        $this->status  = (!empty($data['status'])) ? $data['status'] : null;
        $this->profile_progress  = (!empty($data['profile_progress'])) ? $data['profile_progress'] : null;
     }
     
     public function getArrayCopy()
     {
         return get_object_vars($this);
     }
            
}