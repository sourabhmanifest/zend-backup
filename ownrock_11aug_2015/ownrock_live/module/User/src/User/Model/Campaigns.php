<?php

namespace User\Model;

class Campaigns{
    public $campaign_id;
    public $preview;
    public $payout;
    public $categories;
    public $MerchantName;
    public $phonenumber;
    public $websiteUrl;
    public $description;
    public $email;
    
    public function exchangeArray($data)
     {
  
         $this->campaign_id    = (!empty($data['campaign_id'])) ? $data['campaign_id'] : null;   
         $this->preview = (!empty($data['preview'])) ? $data['preview'] : null;
         $this->payout  = (!empty($data['payout'])) ? $data['payout'] : null;
         $this->categories  = (!empty($data['categories'])) ? $data['categories'] : null;
         $this->MerchantName   = (!empty($data['MerchantName'])) ? $data['MerchantName'] : null;   
         $this->phonenumber = (!empty($data['phonenumber'])) ? $data['phonenumber'] : null;
         $this->websiteUrl  = (!empty($data['websiteUrl'])) ? $data['websiteUrl'] : null;
         $this->description  = (!empty($data['description'])) ? $data['description'] : null;
         $this->email  = (!empty($data['email'])) ? $data['email'] : null;
         
     }
     
     public function getArrayCopy()
     {
         return get_object_vars($this);
     }
            
}


