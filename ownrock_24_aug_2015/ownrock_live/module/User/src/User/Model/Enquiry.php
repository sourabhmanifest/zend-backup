<?php

namespace User\Model;

class Enquiry {

    public $name;
    public $email;
    public $phone;
    public $budget;
    public $skills;
    public $startdate;
    public $enddate;
    public $description;
    
    public function exchangeArray($data) {

        $this->name = (!empty($data['name'])) ? $data['name'] : null;
        $this->email = (!empty($data['email'])) ? $data['email'] : null;
        $this->phone = (!empty($data['phone'])) ? $data['phone'] : null;
        $this->budget = (!empty($data['budget'])) ? $data['budget'] : null;
        $this->skills = (!empty($data['skills'])) ? $data['skills'] : null;
        $this->startdate = (!empty($data['startdate'])) ? $data['startdate'] : null;
        $this->enddate = (!empty($data['enddate'])) ? $data['enddate'] : null;
        $this->description = (!empty($data['description'])) ? $data['description'] : null;
       
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}