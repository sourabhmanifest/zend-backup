<?php

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use User\Form\UsereditForm;
use User\Form\LoginForm;
use User\Form\ProjectskilleditForm;
use User\Model\User;
use User\Model\UserTable;
use User\Model\Merchant;
use User\Model\Projectskill;
use User\Model\MerchantTable;
use User\Model\Project;
use User\Model\ProjectTable;
use User\Model\Endorsements;
use User\Model\EndorsementsTable;
use User\Model\Categorylist;
use User\Model\CategorylistTable;
use User\Model\ProejctskillTable;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;
use User\Form\RegisterForm;
use User\Form\EndorsementsForm ;
use Zend\Db\Sql\Select;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\Iterator as paginatorIterator;
use Zend\View\Model\JsonModel;
use User\Model\Uploads;
use User\Model\ImageUpload;

class ProjectmanagerController extends AbstractActionController {

    protected $authservice;

    public function getAuthService() {
        $this->authservice = $this->getServiceLocator()->get('AuthService');
        return $this->authservice;
    }

    public function IndexAction() {
        $this->layout('layout/common-layout');
        $projectTable = $this->getServiceLocator()->get('projecttable');
        
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            $searchtxt = $this->params()->fromPost('search');
            $paginator = $projectTable->fetchAll(true,$searchtxt);             
        }else{
            $paginator = $projectTable->fetchAll(true);
            $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 10));
        }
         $paginator->setItemCountPerPage(10);
        $viewModel = new Viewmodel(array('paginator' => $paginator));
        //$this->getServiceLocator()->get('log')->debug("A Debug Log Message");
        return $viewModel;
    }

    public function EditAction() {
        $this->layout('layout/common-layout');
        
        $ProjectTable = $this->getServiceLocator()->get('ProjectTable');
        //$project = $ProjectTable->getProject($this->params()->fromRoute('project_shortcut'));
        $project = $ProjectTable->getProject($this->params()->fromRoute('project_id'));
        $form = $this->getServiceLocator()->get('ProjecteditForm');
        $form->bind($project);
        //$viewModel = new ViewModel(array('form' => $form, 'project_id' => $this->params()->fromRoute('project_shortcut')));
        
        $ImageUploadTable = $this->getServiceLocator()->get('ImageUploadTable');
        $parr[] = $this->params()->fromRoute('project_id');
        $Image_Details = $ImageUploadTable->allUploadByProject($parr);
        
        $viewModel = new ViewModel(array('form' => $form, 'project_id' => $this->params()->fromRoute('project_id'),'image_result'=>$Image_Details));
        return $viewModel;
        
    }

    public function DeleteAction() {
        $this->layout('layout/common-layout');
        $this->getServiceLocator()->get('ProjectTable')->deleteProject($this->params()->fromRoute('project_id'));
        return $this->redirect()->toRoute('user/projectmanager');
    }

    public function AddAction() {
        $this->layout('layout/common-layout');

        $form = $this->getServiceLocator()->get('ProjecteditForm');
        $user_name = str_replace('-',' ',$this->params()->fromRoute('user_name'));
        $form->get('user_name')->setValue($user_name);
        $form->get('submit')->setValue('Add');
        $request = $this->getRequest();
        
        $adapter = new \Zend\File\Transfer\Adapter\Http();
        
        if ($request->isPost()) {
            
            $config = $this->getServiceLocator()->get('config');
            $uploadPath = $config['module_config']['upload_location'];
            $adapter = new \Zend\File\Transfer\Adapter\Http();
            
            $project = new Project();
            $upload = new ImageUpload();
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $project->exchangeArray($form->getData());
                $last_id = $this->getServiceLocator()->get('ProjectTable')->saveProject($project);
                
                $cnt = 0;
                foreach($request->getFiles()->toArray() as $selectedFile){
                    if($selectedFile['name'] != ""){
                        $cnt++;
                    }
                }    
                
                    for($i=1;$i<$cnt+1;$i++){
                    
                        $rand = rand(2000,100000);
                        
                        $uploadFile = $this->params()->fromFiles("projectpic$i");
                        
                        $adapter->setDestination($uploadPath);  
                        $adapter->receive($uploadFile['name']);
                        
                        $image_data['filename'] = $rand.'_'.$uploadFile['name'];
                        $image_data['thumbnail'] = $rand.'_'.'tn_'.$uploadFile['name'];
                        $image_data['label'] = "Project Pic$i";
                        $image_data['project_id'] = $last_id;
                        
                        $upload->exchangeArray($image_data);               
                        $uploadTable = $this->getServiceLocator()->get('ImageUploadTable');
                        $uploadTable->saveUploads($upload);
                        
                        rename($uploadPath.'/'.$uploadFile['name'],$uploadPath.'/'.$rand.'_'.$uploadFile['name']);
                        
                        $sourceImageFileName = $uploadPath . '/' . $rand.'_'.$uploadFile['name'];
                        $thumbnailFileName = $rand.'_'.'tn_'.$uploadFile['name'];
                        $imageThumb = $this->getServiceLocator()->get('WebinoImageThumb');
                        $thumb = $imageThumb->create($sourceImageFileName, $options = array());
                        $thumb->resize(50, 50);
                        $thumb->save($uploadPath . '/' . $thumbnailFileName);
                        
                        
                }
                
                return $this->redirect()->toRoute('user/projectmanager', array('action' => 'index'));
            }
        }
        return array('form' => $form);
    }

    public function ProcessAction() {
        if (!$this->request->isPost()) {
            return $this->redirect()->toRoute('user/projectmanager', array('action' => 'edit'));
        }
        $post = $this->request->getPost();

        $projectTable = $this->getServiceLocator()->get('projectTable');

        $project = $projectTable->getProjectById($post->project_id);
        
        $form = $this->getServiceLocator()->get('ProjectEditForm');
        $form->bind($project);
        $form->setData($post);

        if (!$form->isValid()) {
            $model = new ViewModel(array(
                'error' => true,
                'form' => $form,
            ));
            $model->setTemplate('user/projectmanager/edit');
            return $model;
        }

        $this->getServiceLocator()->get('ProjectTable')->saveProject($project);

        $config = $this->getServiceLocator()->get('config');
        $uploadPath = $config['module_config']['upload_location'];
        $adapter = new \Zend\File\Transfer\Adapter\Http();
        $upload = new ImageUpload();
        
        for($i=1;$i<6;$i++){
                    
            $rand = rand(2000,100000);

            $uploadFile = $this->params()->fromFiles("projectpic$i");
            
            if($uploadFile['name'] != ""){
                $adapter->setDestination($uploadPath);  
                $adapter->receive($uploadFile['name']);

                $image_data['filename'] = $rand.'_'.$uploadFile['name'];
                $image_data['thumbnail'] = $rand.'_'.'tn_'.$uploadFile['name'];
                $image_data['label'] = "Project Pic$i";
                $image_data['project_id'] = $post->project_id;

                $upload->exchangeArray($image_data);               
                $uploadTable = $this->getServiceLocator()->get('ImageUploadTable');
                $uploadTable->saveUploads($upload);

                rename($uploadPath.'/'.$uploadFile['name'],$uploadPath.'/'.$rand.'_'.$uploadFile['name']);

                $sourceImageFileName = $uploadPath . '/' . $rand.'_'.$uploadFile['name'];
                $thumbnailFileName = $rand.'_'.'tn_'.$uploadFile['name'];
                $imageThumb = $this->getServiceLocator()->get('WebinoImageThumb');
                $thumb = $imageThumb->create($sourceImageFileName, $options = array());
                $thumb->resize(50, 50);
                $thumb->save($uploadPath . '/' . $thumbnailFileName);
            }
        }
        return $this->redirect()->toRoute('user/projectmanager');
    }

    public function AddprojectskillAction(){
        $this->layout('layout/common-layout');

        $form = $this->getServiceLocator()->get('ProjectskilleditForm');
        
        //echo $id= $this->params()->fromRoute('user_id');
        //echo '::::';
        $project_id = $this->params()->fromRoute('project_id');
        $project_shortcut = $this->params()->fromRoute('project_shortcut');
        $form->get('project_id')->setValue($project_id);
        $form->get('project_shortcut')->setValue($project_shortcut);
        
        $form->get('submit')->setValue('Add');
        $request = $this->getRequest();
        if ($request->isPost()) {
            //echo '<pre>';
            //print_r($request->getPost());
            //exit;
            
            $projectskill = new Projectskill();
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $projectskill->exchangeArray($form->getData());
                $this->getServiceLocator()->get('ProjectskillTable')->saveProjectskill($projectskill);
                return $this->redirect()->toRoute('user/projectmanager', array('action' => 'index'));
            }
        }
        return array('form' => $form);
    }
    
      public function AddendorsementsAction(){
      
        $this->layout('layout/common-layout');

        $form = $this->getServiceLocator()->get('EndorsementsForm ');
      
        //echo $id= $this->params()->fromRoute('user_id');
        //echo '::::';
        $project_id = $this->params()->fromRoute('project_id');      
        $project_shortcut = $this->params()->fromRoute('project_shortcut');
        
        $form->get('project_id')->setValue($project_id);
        $form->get('project_shortcut')->setValue($project_shortcut);
       
        $form->get('submit')->setValue('Add');
        $request = $this->getRequest();
      
        if ($request->isPost()) {
            //echo '<pre>';
            //print_r($request->getPost());
            //exit;
            
            $projectskill = new Projectskill();
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $projectskill->exchangeArray($form->getData());
                $this->getServiceLocator()->get('ProjectskillTable')->saveProjectskill($projectskill);
                return $this->redirect()->toRoute('user/projectmanager', array('action' => 'index'));
            }
        }
        return array('form' => $form);
    }
    
    public function Index3Action() {
        $this->layout('layout/online-layout');
        $merchantTable = $this->getServiceLocator()->get('merchanttable');
        $merchants = $merchantTable->fetchAll();
        $merchants2 = $merchantTable->selectMerchant("Shopping");
        $merchants3 = $merchantTable->selectMerchant("Food");
        $merchants4 = $merchantTable->selectMerchant("Recharge");
        $merchants5 = $merchantTable->selectMerchant("Travel");
        $user = $this->getAuthService()->getStorage()->read();
        $dbadapter = $this->getServiceLocator()->get('dbAuthService');
        $categorylistTable = $this->getServiceLocator()->get('categorylisttable');
        $category = $categorylistTable->selectuniquecategory($dbadapter);
//foreach ($category as $cat) :
//{
//    echo ($cat->category);
//}
//endforeach;


        if ($user == NULL) {
            $username = 'guest';
        } else {
            $username = $user->name;
        }
        $form = new LoginForm();
        $viewModel = new ViewModel(array('merchants' => $merchants,
            'merchants2' => $merchants2,
            'merchants3' => $merchants3,
            'merchants4' => $merchants4,
            'merchants5' => $merchants5,
            'username' => $username,
            'form' => $form,
            'category' => $category,
            'categorylistTable' => $categorylistTable,
        ));

        return $viewModel;
    }

    public function StoreAction() {
        $this->layout('layout/online-layout');
        $merchant = $this->getEvent()->getRouteMatch()->getParam('id');
        $merchantTable = $this->getServiceLocator()->get('merchanttable');
        $merchanttable = $merchantTable->selectMerchantname($merchant);

        $select = new Select();
        $order_by = $this->params()->fromRoute('order_by') ? $this->params()->fromRoute('order_by') : 'entrydate';
        $order = $this->params()->fromRoute('order') ? $this->params()->fromRoute('order') : Select::ORDER_ASCENDING;
        $page = $this->params()->fromRoute('page') ? (int) $this->params()->fromRoute('page') : 1;
        $onlinemanagertable = $this->getServiceLocator()->get('onlinecouponTable');
        $onlinemanager = $onlinemanagertable->fetchAll($select->order($order_by . ' ' . $order));
        $itemsPerPage = 2;
        $onlinemanager->current();
        $paginator = new Paginator(new paginatorIterator($onlinemanager));
        $paginator->setCurrentPageNumber($page)
                ->setItemCountPerPage($itemsPerPage)
                ->setPageRange(7);



        return new ViewModel(array('merchanttable' => $merchanttable,
            'onlinecoupons' => $onlinemanagertable->selectOnlinecoupon($merchant, $select),
            'order_by' => $order_by,
            'order' => $order,
            'page' => $page,
            'paginator' => $paginator,
        ));
    }

    public function CategoriesAction() {
        $this->layout('layout/online-layout');
        $subcategory = $this->getEvent()->getRouteMatch()->getParam('id');
        $onlinecouponTable = $this->getServiceLocator()->get('onlinecoupontable');

        $onlinecoupontable = $onlinecouponTable->selectsubcategoryname($subcategory);
//        foreach ($onlinecoupontable as $cat) :
//        {
//           echo ($cat->longdescription);
//}
//endforeach;
//die;
        $merchantTable = $this->getServiceLocator()->get('merchanttable');

        $select = new Select();
        $order_by = $this->params()->fromRoute('order_by') ? $this->params()->fromRoute('order_by') : 'entrydate';
        $order = $this->params()->fromRoute('order') ? $this->params()->fromRoute('order') : Select::ORDER_ASCENDING;
        $page = $this->params()->fromRoute('page') ? (int) $this->params()->fromRoute('page') : 1;
        $onlinemanagertable = $this->getServiceLocator()->get('onlinecouponTable');
        $onlinemanager = $onlinemanagertable->fetchAll($select->order($order_by . ' ' . $order));
        $itemsPerPage = 2;
        $onlinemanager->current();
        $paginator = new Paginator(new paginatorIterator($onlinemanager));
        $paginator->setCurrentPageNumber($page)
                ->setItemCountPerPage($itemsPerPage)
                ->setPageRange(7);



        return new ViewModel(array(
            //'onlinecoupons' => $onlinemanagertable->selectOnlinecoupon($merchant, $select),
            'order_by' => $order_by,
            'order' => $order,
            'page' => $page,
            'paginator' => $paginator,
            'onlinecoupontable' => $onlinecoupontable,
            'merchantTable' => $merchantTable,
        ));
    }

}