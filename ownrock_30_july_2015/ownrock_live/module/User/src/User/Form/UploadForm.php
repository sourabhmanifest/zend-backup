<?php
// filename : module/Users/src/Users/Form/RegisterForm.php
namespace User\Form;

use Zend\Form\Form;


class UploadForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('Upload');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype','multipart/form-data');
        
        $this->add(array(
            'name' => 'label',
            'attributes' => array(
                'type'  => 'text',
                'placeholder' => 'Enter Label',
            ),
            'options' => array(
                'label' => 'File Description',
            ),
        ));

        
        $this->add(array(
            'name' => 'fileupload',
            'attributes' => array(
                'type'  => 'file',
                'multiple' => 'true',
                'id'=> 'file-3',
                'data-preview-file-type'=>'any',
                'class' => 'file',
            ),
            'options' => array(
                'label' => 'File Upload',
                  'id' => 'file-3',
                 'data-preview-file-type'=>'any',
                'class' => 'file',
            ),
        )); 
        
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Upload Now'
            ),
        )); 
        
        
    }
}
