<?php
// filename : module/Users/src/Users/Form/RegisterForm.php
namespace User\Form;

use Zend\Form\Form;

class ProjecteditForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('Edit User');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype','multipart/form-data');
        
        $this->add(array(
        	'name' => 'user_name',
        	'attributes' => array(
                'type'  => 'text',
                'required' => 'required',
                'class' => 'form-control',
                'placeholder' => 'Enter User Id',
                'readonly'=>'readonly'    
        	),
        ));
        
         $this->add(array(
        	'name' => 'project_name',
        	'attributes' => array(
                'type'  => 'text',
                'required' => 'required',
                'class' => 'form-control',
                'placeholder' => 'Enter proj name',

        	),
        ));
        $this->add(array(
        	'name' => 'project_id',
        	'attributes' => array(
                'type'  => 'text',
                'required' => 'required',
                'class' => 'form-control',
                'placeholder' => 'Enter Project name',

        	),
        ));
        
        $this->add(array(
        	'name' => 'project_shortcut',
        	'attributes' => array(
                'type'  => 'text',
                'required' => 'required',
                'class' => 'form-control',
                'placeholder' => 'Enter Project name',

        	),
        ));
        
        $this->add(array(
        	'name' => 'about',
        	'attributes' => array(
                'type'  => 'text',
                'required' => 'required',
                'class' => 'form-control',

        	),
        ));
        
        $this->add(array(
            'name' => 'website_url',
            'attributes' => array(
                'type'  => 'text',
                'class' => 'form-control',
                'placeholder' => 'Enter website url',

            ),
            'options' => array(
            ),
        ));

        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'industry',
            'attributes' => array(               
                'required' => 'required',
                'class' => 'form-control',
                'placeholder' => 'Enter industry',
            ),
            'options' => array(
               'value_options' => self::industry_list()
            ),
        )); 
        
        $this->add(array(
            'name' => 'role',
            'attributes' => array(
                'type'  => 'text', 
                'class' => 'form-control',
                                'placeholder' => 'Enter role',
            ),
            'options' => array(
            ),
        )); 
        
        $this->add(array(
            'name' => 'time',
            'attributes' => array(
                'type'  => 'text',  
                'class' => 'form-control',
                'placeholder' => 'Enter time to complete the project',
            ),
            'options' => array(
            ),
        )); 
        
        $this->add(array(
            'name' => 'cost',
            'attributes' => array(
                'type'  => 'text',      
                'class' => 'form-control',
                'placeholder' => 'Enter availability',
            ),
            'options' => array(
            ),
        )); 
        
        
        $this->add(array(
            'name' => 'endorsement',
            'attributes' => array(
                'type'  => 'text',      
                'class' => 'form-control',
                'placeholder' => 'Enter small description',
            ),
            'options' => array(
            ),
        )); 
        
         $this->add(array(
            'name' => 'stars',
            'attributes' => array(
                'type'  => 'text',      
                'class' => 'form-control',
                'placeholder' => 'Enter long description',
            ),
            'options' => array(
            ),
        )); 
         
         $this->add(array(
            'name' => 'quality',
            'attributes' => array(
                'type'  => 'text',      
                'class' => 'form-control',
                'placeholder' => 'Enter profile pic',
            ),
            'options' => array(
            ),
        )); 
         
          $this->add(array(
            'name' => 'effective_cost',
            'attributes' => array(
                'type'  => 'text',      
                'class' => 'form-control',
                'placeholder' => 'Enter email',
            ),
            'options' => array(
            ),
        ));
          
          $this->add(array(
            'name' => 'effective_time',
            'attributes' => array(
                'type'  => 'text',      
                'class' => 'form-control',
                'placeholder' => 'Enter time stars',
            ),
            'options' => array(
            ),
        )); 
          
           $this->add(array(
            'name' => 'experience',
            'attributes' => array(
                'type'  => 'text',      
                'class' => 'form-control',
                'placeholder' => 'Enter experience star',
            ),
            'options' => array(
            ),
        ));
           
           $this->add(array(
            'name' => 'communication',
            'attributes' => array(
                'type'  => 'text',      
                'class' => 'form-control',
                'placeholder' => 'Enter communication star',
            ),
            'options' => array(
            ),
        ));
        
        $this->add(array(
            'type' => 'hidden',
            'name' => 'added_by',
            'options' => array(),
            'attributes' => array(
                'value' => 'admin', //set selected to '1',
                'class'=>'form-control',
            )
        ));   
           
        /*
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'added_by',
            'options' => array(
                'value_options' => array(
                    '' => '--Please select--',
                    'admin' => 'admin',
                    'user' => 'user',
                ),
            ),
            'attributes' => array(
                'value' => 'admin', //set selected to '1',
                'required'=>'required',
                'class'=>'form-control',
                'disabled'=>'disabled'
            )
        ));*/   
           
        for($i=1;$i<6;$i++){
            $this->add(array(
                'name' => "projectpic$i",
                'attributes' => array(
                    'type'  => 'file',      
                    'class' => 'form-control',
                    'placeholder' => "Enter pic",
                    //'multiple' =>'multiple',
                ),
                'options' => array(
                ),
            ));   
        }
           
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Save'
            ),
        )); 
        
         $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Cancel'
            ),
        )); 
    }
    
        public function industry_list(){
        $list = array(
        'Arts' =>'Arts', 'Advertising', 'Airline', 'Apparel & Accessories', 'Bars', 'Beauty & Personal Care', 'Blogging', 'Business Services',
        'Community', 'Consumer Products', 'Cosmetics', 'Digital Product & Services', 'Education', 'Electronics', 'Employment & Jobs',
        'Entertainment', 'Events & Venues', 'Financial Services', 'Fitness', 'Food & Beverages', 'Games & Toys', 'Grocery',
        'Health', 'Home & Decor', 'Hospitality', 'Insurance', 'Investing', 'Legal', 'Motion Picture & Video', 'Music', 
        'News & Media', 'Pet Services & Supplies', 'Photography', 'Real Estate', 'Restaurants & Dining', 'Retail & Shopping',
        'Software', 'Technology', 'Training', 'Travel',
     );
        return $list;
}
}