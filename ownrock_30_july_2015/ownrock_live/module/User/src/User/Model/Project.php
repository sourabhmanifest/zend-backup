<?php

namespace User\Model;

class Project {

    public $project_id;
    public $user_name;
    public $about;
    public $website_url;
    public $industry;
    public $role;
    public $time;
    public $cost;
    public $endorsement;
    public $stars;
    public $quality;
    public $effective_cost;
    public $effective_time;
    public $experience;
    public $communication;
    public $project_name;
    public $project_shortcut;
    public $added_by;

    public function exchangeArray($data) {

        $this->project_id = (!empty($data['project_id'])) ? $data['project_id'] : null;
        $this->user_name = (!empty($data['user_name'])) ? $data['user_name'] : null;
        $this->about = (!empty($data['about'])) ? $data['about'] : null;
        $this->website_url = (!empty($data['website_url'])) ? $data['website_url'] : null;
        $this->industry = (!empty($data['industry'])) ? $data['industry'] : null;
        $this->role = (!empty($data['role'])) ? $data['role'] : null;
        $this->time = (!empty($data['time'])) ? $data['time'] : null;
        $this->cost = (!empty($data['cost'])) ? $data['cost'] : null;
        $this->endorsement = (!empty($data['endorsement'])) ? $data['endorsement'] : null;
        $this->stars = (!empty($data['stars'])) ? $data['stars'] : null;
        $this->quality = (!empty($data['quality'])) ? $data['quality'] : null;
        $this->effective_cost = (!empty($data['effective_cost'])) ? $data['effective_cost'] : null;
        $this->effective_time = (!empty($data['effective_time'])) ? $data['effective_time'] : null;
        $this->experience = (!empty($data['experience'])) ? $data['experience'] : null;
        $this->communication = (!empty($data['communication'])) ? $data['communication'] : null;
        $this->project_name = (!empty($data['project_name'])) ? $data['project_name'] : null;
        $this->project_shortcut = $this->seoUrl($data['project_name']);
        $this->added_by = (!empty($data['added_by'])) ? $data['added_by'] : null;
    }
    
        function seoUrl($string) {
       
    //Lower case everything
    $string = strtolower($string);
    //Make alphanumeric (removes all other characters)
    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
    //Clean up multiple dashes or whitespaces
    $string = preg_replace("/[\s-]+/", " ", $string);
    //Convert whitespaces and underscore to dash
    $string = preg_replace("/[\s_]/", "-", $string);
    
    return $string;
}

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}